package com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class PushNotificationRelation {
@Id long id;
public String pushValue;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPushValue() {
        return pushValue;
    }

    public void setPushValue(String pushValue) {
        this.pushValue = pushValue;
    }
}
