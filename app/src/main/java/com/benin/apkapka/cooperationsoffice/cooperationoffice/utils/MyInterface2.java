package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import java.io.StringReader;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;


/**
 * Created by joel on 28/02/2016.
 */
public interface MyInterface2 {

 @FormUrlEncoded
 @POST("/cooperationv2/get_list_product_by_category_paging.php")
 Call<String>getAllProduct_By_Category(@Field("category") String category,@Field("pays") String pays,@Field("famille") String famille,@Field("lang") String lang,@Field("type") String  type,@Field("zone")String zone,@Field("page") int page);
 @FormUrlEncoded
 @POST("/cooperationv2/get_list_product_group_by_category_paging.php")
 Call<String>getAllProduct_Group_By_Category(@Field("category") String category,@Field("pays") String pays,@Field("lang") String lang,@Field("type") String  type,@Field("zone") String zone,@Field("page") int page);
 @FormUrlEncoded
 @POST("/cooperationv2/get_list_product_search.php")
 Call<String>getAllProduct_By_Word(@Field("category") String category,@Field("pays") String pays,@Field("lang") String lang,@Field("word") String word,@Field("type") String  type,@Field("zone")String zone);
 @FormUrlEncoded
 @POST("/cooperationv2/get_list_product_title_search.php")
 Call<String>getAllProduct_By_title(@Field("word") String word,@Field("lang") String lang,@Field("zone")String zone );
 @GET("/cooperationv2/get_abonnement.php")
 Call<String>getabonnement();
 @GET("/cooperationv2/get_count_users_actif.php")
 Call<String>getCountAtifUser();
 @FormUrlEncoded
 @POST("/cooperationv2/get_list_product_title_search2.php")
 Call<String>getAllProduct_By_title2(@Field("word") String word,@Field("type") String type,@Field("category") String category,@Field("lang") String lang,@Field("zone") String zone);
 @GET("/cooperationv2/app_new.php")
 Call<String>getAppVersion();
 @FormUrlEncoded
 @POST("/cooperationv2/paypal_autre_verification.php")
 Call<String>verifyPayment(@Field("id") long id,@Field("paymentId")String paymentId,@Field("clientId") String clientId,@Field("Category")String category,@Field("lang") String lang);
 @FormUrlEncoded
 @POST("/cooperationv2/paypal_projet_verification.php")
 Call<String>verifyPaymentprojet(@Field("id") long id,@Field("paymentId")String paymentId,@Field("clientId") String clientId,@Field("Category")String category,@Field("lang") String lang);
 @FormUrlEncoded
 @POST("/cooperationv2/managestatistique/update.php")
 Call<String>updatestatistique(@Field("id") int id);
 @FormUrlEncoded
 @POST("/cooperationv2/compteur/add_session.php")
 Call<String>add_session(@Field("id") long id);
 @FormUrlEncoded
 @POST("/cooperationv2/managestatistique/updateItem.php")
 Call<String>updatestatistiqueItem(@Field("id") long id,@Field("mytable") String table);
 @FormUrlEncoded
 @POST("/cooperationv2/managestatistique/updateItemFamille.php")
 Call<String>updatestatistiqueItemFamille(@Field("mytable") String table,@Field("type") String type,@Field("category") String category,@Field("famille") String famille);
 @FormUrlEncoded
 @POST("/cooperationv2/paypal_produit_verification.php")
 Call<String>verifyPaymentprojetProduit(@Field("id") long id,@Field("paymentId")String paymentId,@Field("clientId") String clientId,@Field("Category")String category,@Field("lang") String lang);
}
