package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Location;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.benin.apkapka.cooperationsoffice.cooperationoffice.BuildConfig;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.BottomProfileFavori;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.MyDateFragment;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Favorite;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by joel on 19/11/2017.
 */
public class DrawerCompteActivity extends BaseLanguageActivity implements View.OnClickListener,MyDateFragment.MyDatePickerInterface{

    //C'est le pays de résidence de l'utilisateur qui permet de déterminer le code indicatif du pays de résidence
    String bad_url="https://cooperationsoffice.net/cooperation/logo/";
    EditText editNum,editFace,editGmail,editPrefix,editNumero;
    Button btn;
    TextView tvChangeDate;
    FragmentManager fm;
    private boolean is_fr=false;
    AlertDialog alertDialogPro;
    String num;
    String face;
    String google;
    String nom;
    String prenom;
    private boolean can_save_num=false,cans_save_face=false,can_save_google=false;
    private boolean error_num=false,erro_face=false,erro_google=false;
    ProgressDialog dialog1;
    ImageView imgWhatsapp,imgGoogle,imgFacebook;
    int currentCountryNameId;
    String countryCode[];
    String countryCodeName[];
    EditText tvNom;
    private CircularImageView profielImage;
    EditText tvPrenom;
    Spinner  spinner;
    AppCompatImageView tackePhotoImageView;
    String pays;
    String paysSpinner []={};
    public  static  final String [] actions={"",""};
    public  static final  int REQUEST_TAKE_PUCTURE=0;
    public  static  final  int REQUEST_GET_FROM_GALLERY=1;
    AlertDialog.Builder builder;
    //Location variable
    AlertDialog alertDialog;
    FusedLocationProviderClient mFusedLocationProviderClient;
    SettingsClient mSettingsClient;
    LocationRequest mLocationRequest;
    LocationSettingsRequest mLocationSettingsRequest;
    LocationCallback mLocationCallback;
    Location mLocation;
    private static final int LOCATION_UPDATE_INTERVAL=10000;
    private static  final int LOCATION_FAST_UPDATE_INTERVAL=60*1000;
    private  static  final int REQUEST_CHECK_SETTINGS=10;
    //Location variable

    User user1;
    int year=0,month=0,day=0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_compte_layout);
        actions[0]=getResources().getString(R.string.form_action_pt_new);
        actions[1]=getResources().getString(R.string.form_action_pt_chx);
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        imgWhatsapp=(ImageView)findViewById(R.id.imgWhatsapp);
        imgFacebook=(ImageView)findViewById(R.id.imgFacebook);
        imgGoogle=(ImageView)findViewById(R.id.imgGoogle);
        tackePhotoImageView=(AppCompatImageView)findViewById(R.id.edit_profile_image_take_photo);
        paysSpinner=getResources().getStringArray(R.array.toppays);
        spinner=(Spinner)findViewById(R.id.edit_profile_spinner);
        tvNom=(EditText)findViewById(R.id.drawer_nom);
        tvPrenom=(EditText)findViewById(R.id.drawer_prenom);
        editPrefix=(EditText)findViewById(R.id.drawer_prefix);
        editNumero=(EditText)findViewById(R.id.drawer_tel);
        tvChangeDate=(TextView) findViewById(R.id.drawer_tv_date);
        profielImage=(CircularImageView)findViewById(R.id.edit_profile_image_profile);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,paysSpinner);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pays=paysSpinner[position];
                currentCountryNameId=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tvChangeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDateFragment myDateFragment=new MyDateFragment();
                myDateFragment.setMyDatePickerInterface(DrawerCompteActivity.this);
                myDateFragment.show(getSupportFragmentManager(),myDateFragment.getClass().getName()+"co");
            }
        });


        Picasso.with(this).load(R.drawable.compte_whatsapp).into(imgWhatsapp);
        Picasso.with(this).load(R.drawable.compte_facebook).into(imgFacebook);
        Picasso.with(this).load(R.drawable.compte_google_plus).into(imgGoogle);

        Glide.with(this).load("").placeholder(R.drawable.compte_facebook).into(imgFacebook);
        Glide.with(this).load("").placeholder(R.drawable.compte_google_plus).into(imgGoogle);
        Glide.with(this).load("").placeholder(R.drawable.compte_whatsapp).into(imgWhatsapp);


        countryCode=getResources().getStringArray(R.array.contryCode);
        countryCodeName=getResources().getStringArray(R.array.countryCodeName);
        editNum=(EditText)findViewById(R.id.drawer_numerowhatsapp);
        editFace=(EditText)findViewById(R.id.drawer_comptefacebook);
        editGmail=(EditText)findViewById(R.id.drawer_comptegmail);


        fm=getSupportFragmentManager();

        SessionManager sm=new SessionManager(this);
        user1=sm.getUser(this);
        if(user1.getLatitude()!=null && !user1.getLatitude().equalsIgnoreCase("") && user1.getLongitude()!=null && !user1.getLongitude().equalsIgnoreCase("")){
            UpdateUserLocation(user1.getId(),""+user1.getLatitude(),""+user1.getLongitude());
        }
        if(user1.getPays()!=null && !user1.getPays().equalsIgnoreCase("") && !user1.getPays().equalsIgnoreCase("vide")){

            currentCountryNameId=0;
            for(int i=0;i<paysSpinner.length;i++){
                String name=paysSpinner[i];
                if(name.equalsIgnoreCase(user1.getPays())){
                    currentCountryNameId=i;

                    break;
                }
            }
            spinner.setSelection(currentCountryNameId);
        }
        else{
            TelephonyManager telephonyManager=(TelephonyManager)getSystemService(TELEPHONY_SERVICE);

            String currentCountryName=telephonyManager.getSimCountryIso().toUpperCase();
            if(!isSimAvailable(this)){
                currentCountryName=telephonyManager.getNetworkCountryIso().toUpperCase();
            }
            currentCountryNameId=0;
            for(int i=0;i<countryCodeName.length;i++){
                String name=countryCodeName[i];
                if(name.equalsIgnoreCase(currentCountryName)){
                    currentCountryNameId=i;
                    pays=paysSpinner[currentCountryNameId];
                    break;
                }
            }
            spinner.setSelection(currentCountryNameId);
        }

        if(sm.getCompteFacebook()!=null && !sm.getCompteFacebook().equalsIgnoreCase("") && !sm.getCompteFacebook().equalsIgnoreCase("vide")){
            editFace.setText(sm.getCompteFacebook());
        }
        else{
            editFace.setText("");
        }
        if(sm.getCompteGoogle()!=null && !sm.getCompteGoogle().equalsIgnoreCase("") && !sm.getCompteGoogle().equalsIgnoreCase("vide")){
            editGmail.setText(sm.getCompteGoogle());
        }
        else {
            editGmail.setText("");
        }
        if(sm.getCompteWhatsapp()!=null && !sm.getCompteWhatsapp().equalsIgnoreCase("") && !sm.getCompteWhatsapp().equalsIgnoreCase("vide")){
            editNum.setText(sm.getCompteWhatsapp());
        }
        else{
            editNum.setText("");
        }

        User user=sm.getUser(this);
        if(user.getPhoto_url()!=null && !user.getPhoto_url().equalsIgnoreCase("") && !user.getPhoto_url().equalsIgnoreCase(bad_url)){
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String modifier = formatter.format(Calendar.getInstance().getTime());
            Glide.with(this).load(user.getPhoto_url()).asBitmap().signature(new StringSignature(modifier)).placeholder(R.drawable.bg_cooperationsoffice).error(R.drawable.bg_cooperationsoffice).into(profielImage);
        }
        String nom=user.getName();
        String prenom=user.getPrenom();
        String numero=user.getNumero();
        String prefix=user.getPrefix();
        if(nom!=null && !nom.trim().equalsIgnoreCase("") && !nom.trim().equalsIgnoreCase("vide")){
         tvNom.setText(nom);
        }
        if(prenom!=null && !prenom.trim().equalsIgnoreCase("") && !prenom.trim().equalsIgnoreCase("vide")){
            tvPrenom.setText(prenom);
        }
        if(numero!=null && !numero.trim().equalsIgnoreCase("") && !numero.trim().equalsIgnoreCase("vide")){
            editNumero.setText(numero);
        }
        if(prefix!=null && !prefix.trim().equalsIgnoreCase("") && !prefix.trim().equalsIgnoreCase("vide")){
            editPrefix.setText(prefix);
        }

       profielImage.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               builder=new AlertDialog.Builder(DrawerCompteActivity.this);
               builder.setItems(actions, new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {

                       switch (which)
                       {
                           case 0:
                               Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                               if(intent.resolveActivity(getPackageManager())!=null)
                               {
                                   startActivityForResult(Intent.createChooser(intent, ""+getResources().getString(R.string.form_envoi_chx_img)), REQUEST_TAKE_PUCTURE);
                               }
                               else
                               {
                                   Toast.makeText(DrawerCompteActivity.this,""+getResources().getString(R.string.form_install_photo),Toast.LENGTH_SHORT).show();
                               }
                               break;
                           case 1:

                               Intent   intent1=new Intent(DrawerCompteActivity.this,Activity_Gallery2.class);
                               startActivityForResult(intent1,REQUEST_GET_FROM_GALLERY);

                               break;





                       }


                   }
               });
               builder.show();
           }//fin click
       });

       tackePhotoImageView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               builder=new AlertDialog.Builder(DrawerCompteActivity.this);
               builder.setItems(actions, new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {

                       switch (which)
                       {
                           case 0:
                               Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                               if(intent.resolveActivity(getPackageManager())!=null)
                               {
                                   startActivityForResult(Intent.createChooser(intent, ""+getResources().getString(R.string.form_envoi_chx_img)), REQUEST_TAKE_PUCTURE);
                               }
                               else
                               {
                                   Toast.makeText(DrawerCompteActivity.this,""+getResources().getString(R.string.form_install_photo),Toast.LENGTH_SHORT).show();
                               }
                               break;
                           case 1:

                               Intent   intent1=new Intent(DrawerCompteActivity.this,Activity_Gallery2.class);
                               startActivityForResult(intent1,REQUEST_GET_FROM_GALLERY);

                               break;





                       }


                   }
               });
               builder.show();
           }//fin click
       });
     initLocation();
    }

    @Override
    public void onDatePick(int mday, int mmonth, int myear) {
        year=myear;
        month=mmonth;
        day=mday;
        String naissance=""+year+"-"+month+"-"+day;
        String naissance_fr=""+day+"-"+month+"-"+year;
        tvChangeDate.setText(naissance);
        if(is_fr){
            tvChangeDate.setText(naissance_fr);
        }

    }

    private void initLocation(){
        mFusedLocationProviderClient= LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient=LocationServices.getSettingsClient(this);
        mLocationCallback=new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if(locationResult!=null){

                    mLocation=locationResult.getLastLocation();
                    SessionManager sessionManager=new SessionManager(DrawerCompteActivity.this);
                    User user=sessionManager.getUser(DrawerCompteActivity.this);
                    sessionManager.putUserLongitude(""+mLocation.getLongitude());
                    sessionManager.putUserLatitude(""+mLocation.getLatitude());
                    user.setLongitude(""+mLocation.getLongitude());
                    user.setLatitude(""+mLocation.getLatitude());
                    sessionManager.setUser(DrawerCompteActivity.this,user);
                    sessionManager.putCanRequestLocationUpdate(false);

                    UpdateUserLocation(user.getId(),""+mLocation.getLatitude(),""+mLocation.getLongitude());
                }
            }
        };

        mLocationRequest=LocationRequest.create();
        mLocationRequest.setInterval(LOCATION_UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(LOCATION_FAST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder=new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest=builder.build();
    }
    @SuppressWarnings({"MissingPermission"})
    private void requestLocationUpdate(){
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,mLocationCallback,null);
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if(e instanceof ResolvableApiException){
                            try{
                                ResolvableApiException apiException=(ResolvableApiException)e;
                                apiException.startResolutionForResult(DrawerCompteActivity.this,
                                        REQUEST_CHECK_SETTINGS);
                            }
                            catch (IntentSender.SendIntentException ex){

                            }
                        }//fin if
                    }
                });
    }
    private void checkPermission(){
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        requestLocationUpdate();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if(response.isPermanentlyDenied()){
                            AlertDialog.Builder  builder=new AlertDialog.Builder(DrawerCompteActivity.this);
                            builder.setMessage(is_fr?"La permission a été refusée, elle est nécessaire pour le bon fonctionnement de l'application":"Permission was refused, it is necessary for the proper functioning of the application");
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if(alertDialog!=null){
                                        alertDialog.dismiss();

                                    }
                                  checkPermission();
                                }
                            });
                            alertDialog=builder.show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                    }
                }).check();

    }
    public void UpdateUserLocation(long id,String latitude,String longitude){


        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.updateUserLocation(""+id,longitude,latitude);

        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {

                            } else {


                            }

                        } catch (Exception e) {

                        }

                    }//fin is success,
                    else {


                    }//fin else is success

                }
                else {

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SessionManager sessionManager=new SessionManager(this);
        User user=sessionManager.getUser(this);
        if(requestCode==REQUEST_TAKE_PUCTURE && resultCode==RESULT_OK)
        {
            Bundle  extra=data.getExtras();

           Bitmap bitmap=(Bitmap)extra.get("data");
            profielImage.setImageBitmap(bitmap);
            bitmap=compressImage(bitmap);
            changePhoto(user.getId(),bitmap);
        }
        else if(requestCode==REQUEST_GET_FROM_GALLERY && resultCode==RESULT_OK)
        {

            if(data!=null)
            {
                Bundle  bundle=data.getExtras();
                if(bundle!=null)
                {
                    String path=bundle.getString("mpath");
                    BitmapFactory.Options options=new BitmapFactory.Options();
                    Bitmap  bitmap=BitmapFactory.decodeFile(path,options);
                     if(bitmap!=null){
                         profielImage.setImageBitmap(bitmap);
                         bitmap=compressImage(bitmap);
                         changePhoto(user.getId(),bitmap);
                     }
                     else {

                     }
                }

            }
        }
    }

    public Bitmap compressImage(Bitmap mBitmap) {


        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        mBitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
        byte []byteArray = bos.toByteArray();
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray,0,byteArray.length,options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {               imgRatio = maxHeight / actualHeight;                actualWidth = (int) (imgRatio * actualWidth);               actualHeight = (int) maxHeight;             } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;
//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeByteArray(byteArray,0,byteArray.length,options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly


        return scaledBitmap;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }
    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }
    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height/ (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;      }       final float totalPixels = width * height;       final float totalReqPixelsCap = reqWidth * reqHeight * 2;       while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }
    private void  changePhoto(long id, Bitmap btm){
        OkHttpClient client=new OkHttpClient();



        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(client)
                .build();


        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        btm.compress(Bitmap.CompressFormat.PNG, 0, bos);
        byte []byteArray = bos.toByteArray();
        MediaType mediaType=MediaType.parse("multipart/form-data");
        RequestBody requestBody = RequestBody.create(mediaType, byteArray);
        MyInterface service=retrofit.create(MyInterface.class);

        if(requestBody!=null){
            dialog1=new ProgressDialog(this);
            dialog1.setMessage(""+getResources().getString(R.string.form_envoi_en_cour));
            dialog1.show();
            Call<String> call=service.saveUserPhoto(""+id,requestBody);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call1,Response<String> response) {
                    if(response!=null)
                    {
                        if(response.isSuccessful())
                        {
                            try {
                                String rep = response.body() != null ? response.body() : "";

                                JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                                boolean error=o.getBoolean("error");
                                if(error==true)
                                {
                                    if(dialog1!=null){
                                        dialog1.dismiss();
                                    }
                                    Toast.makeText(getApplicationContext(),is_fr?"Impossible de modifier l'image":"Can not edit the image",Toast.LENGTH_SHORT).show();
                                }
                                else
                                {

                                    SessionManager sessionManager=new SessionManager(DrawerCompteActivity.this);
                                    User user=sessionManager.getUser(DrawerCompteActivity.this);
                                    user.setPhoto_url(o.getString("data"));

                                    sessionManager.setUser(DrawerCompteActivity.this,user);

                                    dialog1.dismiss();
                                    AlertDialog.Builder  builder=new AlertDialog.Builder(DrawerCompteActivity.this);
                                    builder.setMessage(is_fr?"Enregistrées":"Save");
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            alertDialogPro.dismiss();

                                        }
                                    });
                                    alertDialogPro=builder.show();
                                }

                            }
                            catch(Exception e)
                            {
                                if(dialog1!=null){
                                    dialog1.dismiss();
                                }
                                Toast.makeText(getApplicationContext(),is_fr?"Impossible de modifier l'image":"Can not edit the image",Toast.LENGTH_SHORT).show();
                            }



                        }
                        else
                        {
                            if(dialog1!=null){
                                dialog1.dismiss();
                            }
                            Toast.makeText(getApplicationContext(),is_fr?"Impossible de modifier l'image":"Can not edit the image",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        if(dialog1!=null){
                            dialog1.dismiss();
                        }
                    }


                }

                @Override
                public void onFailure(Call<String> call1,Throwable t) {
                    if(dialog1!=null){
                        dialog1.dismiss();
                    }
                    Toast.makeText(getApplicationContext(),is_fr?"Impossible de modifier l'image":"Can not edit the image",Toast.LENGTH_SHORT).show();
                }
            });


        }
        else{
            Toast.makeText(getApplicationContext(),is_fr?"Impossible de modifier l'image":"Can not edit the image",Toast.LENGTH_SHORT).show();
        }

    }
    public boolean isSimAvailable(Context context){
        TelephonyManager manager=(TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        if(!(manager.getSimState()==TelephonyManager.SIM_STATE_ABSENT)){

            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {


    }
    private boolean isConnected(){
        ConnectivityManager cm=(ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        return (info!=null && info.isConnectedOrConnecting());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_profile_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                setResult(RESULT_OK);
                finish();

                return true;
            case R.id.edit_profile_done:
                num=editNum.getText().toString();
                face=editFace.getText().toString();
                google=editGmail.getText().toString();
                nom=tvNom.getText().toString();
                prenom=tvPrenom.getText().toString();
                pays=paysSpinner[currentCountryNameId];
                String numero=editNumero.getText().toString();
                String prefix=editPrefix.getText().toString();
                if(numero==null){
                    numero="";
                }
                if(prefix==null){
                    prefix="";
                }
                if(prefix.startsWith("0")){
                    editPrefix.setError(is_fr?"Indicatif invalide":"Invalid indicative");
                    return true;
                }

                SessionManager sm=new SessionManager(this);
                User user=sm.getUser(this);

               /* if(!TextUtils.isEmpty(num)){
                    PhoneNumberUtil phoneNumberUtil=PhoneNumberUtil.getInstance();
                    try {
                        Phonenumber.PhoneNumber numbercode = phoneNumberUtil.parse(num.trim(), ""+countryCodeName[currentCountryNameId]);
                        if(phoneNumberUtil.isValidNumber(numbercode)){
                            editNum.setText(""+phoneNumberUtil.format(numbercode, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL));

                        }
                        else {
                            MyToast.show(this,is_fr?"Veuillez entrer un numéro de téléphone valide et réessayer":"Please enter a valid phone number and try again",true);
                            return true;
                        }
                    } catch (NumberParseException e) {
                        MyToast.show(this,is_fr?"Veuillez entrer un numéro de téléphone valide et réessayer":"Please enter a valid phone number and try again",true);
                        return true;
                    }
                }
                */

                if(sm.getLogginType()==1 && !sm.getCompteGoogle().equalsIgnoreCase(google)){

                    AlertDialog.Builder builder=new AlertDialog.Builder(this);
                    builder.setMessage(is_fr?"Vous ne pouvez pas changer l'email de création de votre compte":"You can not change your account creation email");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(alertDialog!=null){
                                alertDialog.dismiss();
                            }
                        }
                    });
                    alertDialog=builder.show();
                    return true;
                }

                if(sm.getLogginType()==2 && !sm.getCompteFacebook().equalsIgnoreCase(face)){

                    AlertDialog.Builder builder=new AlertDialog.Builder(this);
                    builder.setMessage(is_fr?"Vous ne pouvez pas changer l'email de création de votre compte":"You can not change your account creation email");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(alertDialog!=null){
                                alertDialog.dismiss();
                            }
                        }
                    });
                    alertDialog=builder.show();
                    return true;
                }
                if(sm.getLogginType()==3 && !sm.getCompteWhatsapp().equalsIgnoreCase(num)){
                    AlertDialog.Builder builder=new AlertDialog.Builder(this);
                    builder.setMessage(is_fr?"Vous ne pouvez pas changer le numéro de création de votre compte":"You can not change your account creation number");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(alertDialog!=null){
                                alertDialog.dismiss();
                            }
                        }
                    });
                    alertDialog=builder.show();
                    return true;
                }

                if(!TextUtils.isEmpty(face.trim())){
                    if(!Patterns.EMAIL_ADDRESS.matcher(face.trim()).matches()){
                        editFace.setError(is_fr?"Entrez une addresse email valide":"enter a valid email address");
                        return true;
                    }

                }

                if(!TextUtils.isEmpty(google.trim())){
                    if(!Patterns.EMAIL_ADDRESS.matcher(google.trim()).matches()){
                        editGmail.setError(is_fr?"Entrez une addresse email valide":"enter a valid email address");
                        return true;
                    }
                }


                if(sm.getIS_Num_Whatsapp()){
                    if(num.trim().equals("")){
                        editNum.setError(is_fr?"Entrez le numéro déja enregistré ou changez de numéro":"Enter the number already registered or change number");
                        return true;
                    }
                }
                if(sm.getIS_Facebook()){
                    if(face.trim().equals("")){
                        editFace.setError(is_fr?"Entrez l'email déja enregistré ou changez d'email":"Enter the already registered email or change email");
                        return true;
                    }
                }
                if(sm.getIS_Google()){
                    if(google.trim().equals("")){
                        editGmail.setError(is_fr?"Entrez l'email déja enregistré ou changez d'email":"Enter the already registered email or change email");
                        return true;
                    }
                }


                String naissance=""+year+"-"+month+"-"+day;
                String naissance_fr=""+day+"-"+month+"-"+year;
                if(year!=0 && month!=0 && day!=0){
                    user1.setNaissance(naissance);
                }
                user1.setNumero(numero);
                user1.setPrefix(prefix);
                sm.setUser(this,user1);
                dialog1=new ProgressDialog(this);
                dialog1.setMessage(""+getResources().getString(R.string.form_envoi_en_cour));
                dialog1.show();
                updateUserdata(user.getId(),nom.trim(),prenom.trim(),pays,num.trim(),face.trim(),google.trim(),numero,(year!=0 && month!=0 && day!=0)?naissance:"",prefix);

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        super.onBackPressed();

    }

    private void updateUserdata(long id, final String nom, final String prenom, final String pays, final String num, String face_email, String google_email,String tel,String date_naissance,String prefix){
        final Gson gson=new Gson();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);
        Call<String> call=service.updateUserDataEditProdile(id,nom,prenom,pays,num,face_email,google_email,tel,date_naissance,prefix);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {

                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {

                                if(dialog1!=null){
                                    dialog1.dismiss();
                                }

                                Toast.makeText(DrawerCompteActivity.this,is_fr?"Vos informations n'ont pas été modifié":"Your information has not been changed",Toast.LENGTH_LONG).show();

                            } else {

                                dialog1.dismiss();
                                SessionManager sm=new SessionManager(DrawerCompteActivity.this);
                                User user=sm.getUser(DrawerCompteActivity.this);
                                user.setName(nom);
                                user.setPrenom(prenom);
                                user.setPays(pays);
                                sm.setUser(DrawerCompteActivity.this,user);
                                sm.setCompteFacebook(face);
                                sm.setCompteGoogle(google);
                                sm.setCompteWhatsapp(num);

                                AlertDialog.Builder  builder=new AlertDialog.Builder(DrawerCompteActivity.this);
                                builder.setMessage(is_fr?"Enregistrées":"Save");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        alertDialogPro.dismiss();
                                        setResult(RESULT_OK);
                                        finish();
                                    }
                                });
                                alertDialogPro=builder.show();

                            }

                        } catch (Exception e) {


                            if(dialog1!=null){
                                dialog1.dismiss();
                            }

                           Toast.makeText(DrawerCompteActivity.this,is_fr?"Vos informations n'ont pas été modifié":"Your information has not been changed",Toast.LENGTH_LONG).show();
                        }

                    }//fin is success,
                    else {


                        if(dialog1!=null){
                            dialog1.dismiss();
                        }
                        Toast.makeText(DrawerCompteActivity.this,is_fr?"Vos informations n'ont pas été modifié":"Your information has not been changed",Toast.LENGTH_LONG).show();
                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {


                if(dialog1!=null){
                    dialog1.dismiss();
                }
                Toast.makeText(DrawerCompteActivity.this,is_fr?"Vos informations n'ont pas été modifié":"Your information has not been changed",Toast.LENGTH_LONG).show();
            }

        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(user1!=null){
            if(user1.getLongitude()==null || !user1.getLongitude().equalsIgnoreCase("") || user1.getLongitude().equalsIgnoreCase("vide") || user1.getLatitude()==null && !user1.getLatitude().equalsIgnoreCase("") || !user1.getLatitude().equalsIgnoreCase("vide")){
                checkPermission();
            }
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
    }
}
