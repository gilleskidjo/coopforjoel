package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

/**
 * Created by joel on 06/12/2017.
 */
public class Country {

    int id;
    String name;

    public Country() {

    }

    public Country(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
