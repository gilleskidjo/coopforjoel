package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

/**
 * Created by joel on 19/11/2017.
 */
public class ExpandableModele {
    private int image;
    private String title;

    public ExpandableModele(int image, String title) {
        this.image = image;
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
