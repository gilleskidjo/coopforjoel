package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;

public class BottomChatPreviewImage extends AppCompatActivity {

    Toolbar  toolbar;
    ImageView imageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottom_chat_preview_mayout);
        imageView=(ImageView)findViewById(R.id.bottom_chat_preveiw);
        toolbar=(Toolbar)findViewById(R.id.bottom_chat_preveiw_toolbar);
        BitmapFactory.Options options=new BitmapFactory.Options();
       String path=((MyApplication)getApplicationContext()).getBottomChatSelectedImage_path();
        Bitmap bitmap=BitmapFactory.decodeFile(path,options);
        if(bitmap!=null){
            imageView.setImageBitmap(bitmap);
        }
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bottom_chat_preview_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();

                return true;
            case  R.id.bottom_chat_preview_send_menu_id:
                ((MyApplication)getApplicationContext()).setCanSendSelectedImage(true);
                Intent intent=new Intent();
                intent.putExtra("mpath",((MyApplication)getApplicationContext()).getBottomChatSelectedImage_path());
                setResult(RESULT_OK,intent);
                finish();
                return  true;
        }
        return super.onOptionsItemSelected(item);
    }
}
