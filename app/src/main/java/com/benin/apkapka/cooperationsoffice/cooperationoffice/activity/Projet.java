package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.SearchEvent1;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.SearchEvent2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.SearchEvent3;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.ProjetFragmentPagerAdapteur;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetUpdateStatistique;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetworkSearchByTitleAndType;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentProjet_appel_offre;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentProjet_developpement;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentProjet_financement;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushCategory;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushCategoryAsyncLoader;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Conf;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Country;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyTabLayout;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.PersistentSeachView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.TabLayoutCustomView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.lapism.searchview.SearchAdapter;
import com.lapism.searchview.SearchItem;
import com.lapism.searchview.SearchView;


import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;

import ru.dimorinny.floatingtextbutton.FloatingTextButton;

/**
 * Created by joel on 28/01/2016.
 */
public class Projet  extends BaseLanguageActivity implements LoaderManager.LoaderCallbacks<List<PushCategory>>,FragmentForNetworkSearchByTitleAndType.HandlerNetWorkRequestResponseHomeSearch {

    MyTabLayout tabLayout;
    ViewPager pager;
    Toolbar toolbar;
    ProjetFragmentPagerAdapteur projetFragmentPagerAdapteur;

    PersistentSeachView searchView ;
    String pays1 []={};
    String pays_code []={};
    AlertDialog searchalertDialog;
    private int position1;
    private boolean request=false;
    FloatingTextButton publier;
    PushCategoryAsyncLoader loader;
    int position2,position3,position4;
    boolean isposition1,isposition2,isposition3;
    FragmentManager fm;
    FragmentForNetworkSearchByTitleAndType forNetworkSearchOnHome;
    List<String> listSearch=new ArrayList<>();
    boolean  is_fr;
    Spinner spinner;
    List<Country> mCountries=new ArrayList<>();
    List<SearchItem> items=new ArrayList<>();
    SearchAdapter adapter;
    CoordinatorLayout  coordinatorLayout;
    BottomNavigationView navigationView;
    String search="";
    private android.support.v7.widget.SwitchCompat switchCompat;
    private boolean getSwitchValue;
    private AlertDialog mDialog;
    private SessionManager sessionManagerProjet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.projet_primary_dark));
        }
        setContentView(R.layout.activity_projet_layout);
        coordinatorLayout=(CoordinatorLayout)findViewById(R.id.coordinator);
        navigationView=(BottomNavigationView)findViewById(R.id.bottom_navigation_view);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){

                    case R.id.bootom_chat:
                        Intent intent=new Intent(Projet.this,BottomChatConversation_Activity.class);
                        startActivity(intent);
                        return true;
                    case R.id.bootom_profile:
                        Intent intent1=new Intent(Projet.this,BottomProfileActivity.class);
                        startActivity(intent1);
                        return  true;
                    case R.id.bootom_entertement:
                        Intent intent2=new Intent(Projet.this,BottomDivertissementActivity.class);
                        startActivity(intent2);
                        return true;
                }
                return false;
            }
        });

        sessionManagerProjet = new SessionManager(this);
        publier=(FloatingTextButton) findViewById(R.id.fab);
        publier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (pager.getCurrentItem())
                {
                    case 0:
                        Intent intent = new Intent(Projet.this, Formulaire_Projet.class);
                        intent.putExtra("category","appel_offre");
                        startActivity(intent);
                        break;
                    case 1:
                        Intent intent1 = new Intent(Projet.this, Formulaire_Projet.class);
                        intent1.putExtra("category","financement");
                        startActivity(intent1);
                        break;
                    case 2:
                        Intent intent2 = new Intent(Projet.this, Formulaire_Projet.class);
                        intent2.putExtra("category","developpement");
                        startActivity(intent2);
                        break;
                }
            }
        });
        toolbar=(Toolbar)findViewById(R.id.activity_projet_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_navigate_before_black_24dp);
        pager=(ViewPager)findViewById(R.id.pager);
        tabLayout=(MyTabLayout) findViewById(R.id.activity_projet_tab_layout);
        setupPager(pager);
        pager.setOffscreenPageLimit(2);

        pays1=getResources().getStringArray(R.array.pays);
        pays_code=getResources().getStringArray(R.array.pays_code);

        if (savedInstanceState!=null)
        {
            position1=savedInstanceState.getInt("position",0);

        }
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        if(is_fr){
            Projet.this.setTitle("Projets");
        }
        else{
            Projet.this.setTitle("Projects");
        }
        if(actionBar != null){
            TextView tv = new TextView(getApplicationContext());
            ViewGroup.LayoutParams lp = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, // Width of TextView
                    ViewGroup.LayoutParams.WRAP_CONTENT); // Height of TextView
            tv.setLayoutParams(lp);
            tv.setText(actionBar.getTitle());
            tv.setGravity(Gravity.LEFT);
            tv.setTextColor(Color.WHITE);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            tv.setTypeface(Typeface.DEFAULT_BOLD);
            //actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setCustomView(tv);
        }

        fm=getSupportFragmentManager();
        forNetworkSearchOnHome=(FragmentForNetworkSearchByTitleAndType) fm.findFragmentByTag(FragmentForNetworkSearchByTitleAndType.TAG);
        if(forNetworkSearchOnHome==null){
            forNetworkSearchOnHome=new FragmentForNetworkSearchByTitleAndType();
            fm.beginTransaction().add(forNetworkSearchOnHome,FragmentForNetworkSearchByTitleAndType.TAG).commit();
            forNetworkSearchOnHome.setInterface(this);
        }
        else{
            forNetworkSearchOnHome.setInterface(this);
        }
        searchView=(PersistentSeachView) findViewById(R.id.search_view);
        searchView.setHint(getResources().getString(R.string.main_menu_search));
        searchView.setVersionMargins(SearchView.VERSION_MARGINS_MENU_ITEM);
        searchView.setVersion(SearchView.VERSION_MENU_ITEM);
        searchView.setVoice(true);
        searchView.setOnVoiceClickListener(new SearchView.OnVoiceClickListener() {
            @Override
            public void onVoiceClick() {

                requestRecordePermission();
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                String category="";

                switch (pager.getCurrentItem()){
                    case 0:
                        category="appel_offre";
                        break;
                    case 1:
                        category="financement";
                        break;
                    case 2:
                        category="developpement";
                        break;
                }
                if(!newText.equals("")){
                    search=newText;
                    SessionManager sessionManager=new SessionManager(getApplicationContext());
                    User user=sessionManager.getUser(getApplicationContext());
                    String zone="";
                    zone=user.getPays();
                    if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
                        zone="france";
                    }
                    forNetworkSearchOnHome.doRequestProduitSearchbytype(newText,is_fr?"fr":"en","projet",category,zone);
                }
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.close(true);

                if(!isConnected()){
                    Snackbar snackbar=Snackbar.make(coordinatorLayout,is_fr?"Connectez-vous et réessayer":"No internet connection detected",Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                else {
                    String category="";

                    switch (pager.getCurrentItem()){
                        case 0:
                            category="appel_offre";
                            break;
                        case 1:
                            category="financement";
                            break;
                        case 2:
                            category="developpement";
                            break;
                    }
                    Intent intent=new Intent(Projet.this,SearchActivity.class);
                    intent.putExtra("query",query);
                    intent.putExtra("type","projet");
                    intent.putExtra("category",category);
                    startActivity(intent);
                }

                return false;
            }
        });
//        searchView.setOnOpenCloseListener(new SearchView.OnOpenCloseListener() {
//            @Override
//            public boolean onClose() {
//                publier.show();
//                return false;
//            }
//
//            @Override
//            public boolean onOpen() {
//                publier.hide();
//                return false;
//            }
//        });

        Loader<List<PushCategory>> loader=getSupportLoaderManager().getLoader(PushHandler.LOADER_FRAGMENT_ACTIVITY_ID);
        if(loader==null){
            getSupportLoaderManager().initLoader(PushHandler.LOADER_FRAGMENT_ACTIVITY_ID,null,this);
        }
        else {
            getSupportLoaderManager().restartLoader(PushHandler.LOADER_FRAGMENT_ACTIVITY_ID,null,this);
        }
        FragmentManager fm=getSupportFragmentManager();
        FragmentForNetUpdateStatistique netWorkupdate=(FragmentForNetUpdateStatistique) fm.findFragmentByTag(FragmentForNetUpdateStatistique.TAG);
        if(netWorkupdate==null){
            netWorkupdate=new FragmentForNetUpdateStatistique();
            fm.beginTransaction().add(netWorkupdate,FragmentForNetUpdateStatistique.TAG).commit();
            netWorkupdate.doRequestUpdateStatistique(8);
        }

        List<Integer>pays=new ArrayList<>();
        pays.add(R.drawable.universal);
        pays.add(R.drawable.afghanistan);
        pays.add(R.drawable.southafrica);
        pays.add(R.drawable.albania);
        pays.add(R.drawable.algeria);
        pays.add(R.drawable.germany);
        pays.add(R.drawable.andorra);
        pays.add(R.drawable.angola);
        pays.add(R.drawable.saudiarabia);
        pays.add(R.drawable.argentina);
        pays.add(R.drawable.armenia);
        pays.add(R.drawable.australia);
        pays.add(R.drawable.austria);
        pays.add(R.drawable.belgium);
        pays.add(R.drawable.belize);
        pays.add(R.drawable.benin);
        pays.add(R.drawable.burma);
        pays.add(R.drawable.botswana);
        pays.add(R.drawable.brazil);
        pays.add(R.drawable.bulgaria);
        pays.add(R.drawable.burkinafaso);
        pays.add(R.drawable.burundi);
        pays.add(R.drawable.cambodja);
        pays.add(R.drawable.cameroon);
        pays.add(R.drawable.canada);
        pays.add(R.drawable.capeverde);
        pays.add(R.drawable.centralafrican);
        pays.add(R.drawable.chile);
        pays.add(R.drawable.china);
        pays.add(R.drawable.cyprus);
        pays.add(R.drawable.colombia);
        pays.add(R.drawable.comoros);
        pays.add(R.drawable.congobrazzaville);
        pays.add(R.drawable.northkorea);
        pays.add(R.drawable.southkorea);
        pays.add(R.drawable.costarica);
        pays.add(R.drawable.coteivoire);
        pays.add(R.drawable.croatia);
        pays.add(R.drawable.cuba);
        pays.add(R.drawable.denmark);
        pays.add(R.drawable.djibouti);
        pays.add(R.drawable.dominica);
        pays.add(R.drawable.guinea);
        pays.add(R.drawable.libya);
        pays.add(R.drawable.egypt);
        pays.add(R.drawable.unitedarabemirates);
        pays.add(R.drawable.ecuador);
        pays.add(R.drawable.eritrea);
        pays.add(R.drawable.spain);
        pays.add(R.drawable.estonia);
        pays.add(R.drawable.unitedstatesamerica);
        pays.add(R.drawable.ethiopia);
        pays.add(R.drawable.fiji);
        pays.add(R.drawable.finland);
        pays.add(R.drawable.france);
        pays.add(R.drawable.gabon);
        pays.add(R.drawable.gambia);
        pays.add(R.drawable.georgia);
        pays.add(R.drawable.ghana);
        pays.add(R.drawable.greece);
        pays.add(R.drawable.grenada);
        pays.add(R.drawable.guatemala);
        pays.add(R.drawable.guernsey);
        pays.add(R.drawable.guineabissau);
        pays.add(R.drawable.equatorialguinea);
        pays.add(R.drawable.guyana);
        pays.add(R.drawable.haiti);
        pays.add(R.drawable.honduras);
        pays.add(R.drawable.hungary);
        pays.add(R.drawable.india);
        pays.add(R.drawable.indonezia);
        pays.add(R.drawable.iraq);
        pays.add(R.drawable.iran);
        pays.add(R.drawable.ireland);
        pays.add(R.drawable.iceland);
        pays.add(R.drawable.israel);
        pays.add(R.drawable.italy);
        pays.add(R.drawable.jamaica);
        pays.add(R.drawable.japan);
        pays.add(R.drawable.jersey);
        pays.add(R.drawable.jordan);
        pays.add(R.drawable.kazakhstan);
        pays.add(R.drawable.kenya);
        pays.add(R.drawable.kuwait);
        pays.add(R.drawable.lesotho);
        pays.add(R.drawable.latvia);
        pays.add(R.drawable.lebanon);
        pays.add(R.drawable.liberia);
        pays.add(R.drawable.lithuania);
        pays.add(R.drawable.luxembourg);
        pays.add(R.drawable.madagascar);
        pays.add(R.drawable.malaysia);
        pays.add(R.drawable.maldives);
        pays.add(R.drawable.mali);
        pays.add(R.drawable.malta);
        pays.add(R.drawable.morocco);
        pays.add(R.drawable.marshall);
        pays.add(R.drawable.mauritius);
        pays.add(R.drawable.mauritania);
        pays.add(R.drawable.mexico);
        pays.add(R.drawable.micronesia);
        pays.add(R.drawable.mozambique);
        pays.add(R.drawable.namibia);
        pays.add(R.drawable.nigeria);
        pays.add(R.drawable.niger);
        pays.add(R.drawable.norway);
        pays.add(R.drawable.newzealand);
        pays.add(R.drawable.uganda);
        pays.add(R.drawable.uzbekistan);
        pays.add(R.drawable.pakistan);
        pays.add(R.drawable.palau);
        pays.add(R.drawable.panama);
        pays.add(R.drawable.paraguay);
        pays.add(R.drawable.netherlands);
        pays.add(R.drawable.peru);
        pays.add(R.drawable.philippines);
        pays.add(R.drawable.poland);
        pays.add(R.drawable.portugal);
        pays.add(R.drawable.qatar);
        pays.add(R.drawable.dominicanrepublic);
        pays.add(R.drawable.czechrepublic);
        pays.add(R.drawable.romania);
        pays.add(R.drawable.unitedkingdom);
        pays.add(R.drawable.russianfederation);
        pays.add(R.drawable.rwanda);
        pays.add(R.drawable.saoprincipe);
        pays.add(R.drawable.senegal);
        pays.add(R.drawable.serbia);
        pays.add(R.drawable.sierraleone);
        pays.add(R.drawable.singapore);
        pays.add(R.drawable.slovakia);
        pays.add(R.drawable.slovenia);
        pays.add(R.drawable.somalia);
        pays.add(R.drawable.sudan);
        pays.add(R.drawable.srilanka);
        pays.add(R.drawable.sweden);
        pays.add(R.drawable.suriname);
        pays.add(R.drawable.syria);
        pays.add(R.drawable.taiwan);
        pays.add(R.drawable.tanzania);
        pays.add(R.drawable.thailand);
        pays.add(R.drawable.togo);
        pays.add(R.drawable.tonga);
        pays.add(R.drawable.tunisia);
        pays.add(R.drawable.turkey);
        pays.add(R.drawable.ukraine);
        pays.add(R.drawable.uruguay);
        pays.add(R.drawable.vaticancity);
        pays.add(R.drawable.venezuela);
        pays.add(R.drawable.vietnam);
        pays.add(R.drawable.yemen);
        pays.add(R.drawable.zambia);
        pays.add(R.drawable.zimbabwe);

        for(int i=0;i<pays.size();i++){
            Country country=new Country(pays.get(i),pays1[i]);
            mCountries.add(country);
        }
       items =new ArrayList<>();
      adapter=new SearchAdapter(this,items);
      adapter.addOnItemClickListener(new SearchAdapter.OnItemClickListener() {
          @Override
          public void onItemClick(View view, int position) {
              searchView.close(true);
              if (items!=null){
                  String query=items.get(position).get_text().toString();
                  switch (pager.getCurrentItem())
                  {
                      case 0:
                          EventBus.getDefault().post(new SearchEvent1(query,2,pays_code[position1]));
                          break;
                      case 1:
                          EventBus.getDefault().post(new SearchEvent2(query,2,pays_code[position1]));
                          break;
                      case 2:
                          EventBus.getDefault().post(new SearchEvent3(query,2,pays_code[position1]));
                          break;

                  }
              }
          }
      });

        searchView.setAdapter(adapter);


    }

    @Override
    public void onResposeSuccessSearch(String ss) {
        if(ss.equalsIgnoreCase(search)){
            List<String> list=forNetworkSearchOnHome.getListTitle();
            if(list!=null && list.size()>0){

                items.clear();
                for (String s:list){
                    items.add(new SearchItem(s));
                }


                adapter.notifyDataSetChanged();
                searchView.showSuggestions();
            }
        }
    }
    private void requestRecordePermission(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.RECORD_AUDIO)){

                AlertDialog.Builder builder=new AlertDialog.Builder(this);
                builder.setTitle(is_fr?"Autorisation d'enregistrement":"Registration Authorization ");
                builder.setMessage(is_fr?"Vous devez activer la recherche vocale pour utiliser cette fonctionnalité ":"You must enable voice search to use this feature");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        searchalertDialog.dismiss();
                    }
                });
                builder.setNegativeButton(is_fr?"Annuler":"Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        searchalertDialog.dismiss();
                    }
                });
                searchalertDialog=builder.show();

            }
            else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.RECORD_AUDIO}, Conf.RECORD_REQUEST_CODE);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case Conf.RECORD_REQUEST_CODE:
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){

                }
                else {

                }
                return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode==SearchView.SPEECH_REQUEST_CODE && resultCode==RESULT_OK){
            List<String> results=data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if(results!=null && results.size()>0){
                String query=results.get(0);
                if(!TextUtils.isEmpty(query)){
                    searchView.setTextOnly(query);
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onresponseSearchFailAndPrintErrorResponse() {

    }
    private boolean isConnected()
    {
        ConnectivityManager cm=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        if(info!=null && info.isConnected() && info.isAvailable())
        {
            return  true;
        }

        return  false;
    }


    @Override
    public void onBackPressed() {

        if (searchView.isSearchOpen()) {
            searchView.close(true);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public Loader<List<PushCategory>> onCreateLoader(int id, Bundle args) {
        loader=new PushCategoryAsyncLoader(this, PushHandler.type9,PushHandler.PRO_OFFRE,PushHandler.PRO_FIN,PushHandler.PRO_DEV);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<PushCategory>> loader, List<PushCategory> data) {
      switch (loader.getId()){
          case PushHandler.LOADER_FRAGMENT_ACTIVITY_ID:
              if (data!=null){
                  TabLayoutCustomView customView1 = tabLayout.getTabCustomViewAt(0);
                  TabLayoutCustomView customView2 = tabLayout.getTabCustomViewAt(1);
                  TabLayoutCustomView customView3 = tabLayout.getTabCustomViewAt(2);
                  if(data.size()>0){

                      Predicate<PushCategory> predicate1=new Predicate<PushCategory>() {
                          @Override
                          public boolean apply(@Nullable PushCategory input) {
                              return PushHandler.PRO_OFFRE.equalsIgnoreCase(input.getCategory());
                          }
                      };

                      List<PushCategory> categories1= Lists.newArrayList(Iterables.filter(data,predicate1));
                      if(categories1!=null && categories1.size()>0){
                          PushCategory f=categories1.get(0);
                          if(f!=null && f.getCount()>0)
                          {
                              customView1.setTabCount(f.getCount());
                          }

                      }
                      else {
                          customView1.setTabCount(0);
                      }

                      Predicate<PushCategory> predicate2=new Predicate<PushCategory>() {
                          @Override
                          public boolean apply(@Nullable PushCategory input) {
                              return PushHandler.PRO_FIN.equalsIgnoreCase(input.getCategory());
                          }
                      };

                      List<PushCategory> categories2= Lists.newArrayList(Iterables.filter(data,predicate2));
                      if(categories2!=null && categories2.size()>0){
                          PushCategory f=categories2.get(0);
                          if(f!=null && f.getCount()>0)
                          {
                              customView2.setTabCount(f.getCount());
                          }

                      }
                      else {
                          customView2.setTabCount(0);
                      }

                      Predicate<PushCategory> predicate3=new Predicate<PushCategory>() {
                          @Override
                          public boolean apply(@Nullable PushCategory input) {
                              return PushHandler.PRO_DEV.equalsIgnoreCase(input.getCategory());
                          }
                      };

                      List<PushCategory> categories3= Lists.newArrayList(Iterables.filter(data,predicate3));
                      if(categories3!=null && categories3.size()>0){
                          PushCategory f=categories3.get(0);
                          if(f!=null && f.getCount()>0)
                          {
                              customView3.setTabCount(f.getCount());
                          }

                      }
                      else {
                          customView3.setTabCount(0);
                      }

                  }
                  else {
                      customView1.setTabCount(0);
                      customView2.setTabCount(0);
                      customView3.setTabCount(0);
                  }
              }
              return;
      }
    }

    @Override
    public void onLoaderReset(Loader<List<PushCategory>> loader) {

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("position",position1);

    }
    private void setupPager(ViewPager pager)
    {
        String nom1=getResources().getString(R.string.projet_adapteer_appel_offre).toUpperCase();
        String nom2=getResources().getString(R.string.projet_adapteer_financement).toUpperCase();
        String nom3=getResources().getString(R.string.projet_adapteer_developement).toUpperCase();
        projetFragmentPagerAdapteur=new ProjetFragmentPagerAdapteur(getSupportFragmentManager(),nom1,nom2,nom3);
        projetFragmentPagerAdapteur.addFragment(new FragmentProjet_appel_offre());
        projetFragmentPagerAdapteur.addFragment(new FragmentProjet_financement());
        projetFragmentPagerAdapteur.addFragment(new FragmentProjet_developpement());
        pager.setAdapter(projetFragmentPagerAdapteur);
        tabLayout.setupWithViewPager(pager);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu,menu);
        MenuItem menuItem1=(MenuItem)menu.findItem(R.id.menu_spinner);
        spinner=(Spinner)menuItem1.getActionView();
        MenuItem menuItem2 = (MenuItem) menu.findItem(R.id.action_saverdata);
        switchCompat = (SwitchCompat) MenuItemCompat.getActionView(menuItem2);
        switchCompat.setTextOn("Oui");
        switchCompat.setTextOff("Non");

        if(sessionManagerProjet.getKeySaverDataProjet().equals("Oui")){
            switchCompat.setChecked(true);
        }else{
            switchCompat.setChecked(false);
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(request)
                {
                    String query=pays_code[position];
                    position1=position;

                    String pays=pays1[position];
                    if(position==0){
                        pays="tout";
                    }
                    EventBus.getDefault().post(new SearchEvent1(query,1,pays));
                    EventBus.getDefault().post(new SearchEvent2(query,1,pays));
                    EventBus.getDefault().post(new SearchEvent3(query,1,pays));
                    if(!isConnected()){
                        Snackbar snackbar=Snackbar.make(coordinatorLayout,is_fr?"Connectez-vous et réessayer":"No internet connection detected",Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    if(!isConnected()){
                        if(spinner!=null){
                            spinner.setSelection(0);
                        }
                    }

                }
                else
                {
                    request=true;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        MyAdapteur myAdapteur=new MyAdapteur(this,mCountries);

        spinner.setAdapter(myAdapteur);
        spinner.setSelection(position1);

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                if (isChecked){
                    getSwitchValue = isChecked;
                    if(getDialogStatus()) {
                        mDialog.hide();
                    }else {

                        AlertDialog.Builder mBuilder = new AlertDialog.Builder(Projet.this);
                        View mView = getLayoutInflater().inflate(R.layout.layout_lite_option, null);
                        CheckBox mCheckBox = mView.findViewById(R.id.checkBoxLiteOption);
                        mBuilder.setView(mView);
                        mBuilder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                sessionManagerProjet.putSaverDataProjet("Oui");
                                dialogInterface.dismiss();
                                switchCompat.setChecked(isChecked);
                                recreate();
                            }
                        });

                        mBuilder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sessionManagerProjet.putSaverDataProjet("Non");
                                dialog.dismiss();
                                switchCompat.setChecked(false);
                            }
                        });

                        mDialog = mBuilder.create();
                        mDialog.show();
                        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (compoundButton.isChecked()) {
                                    storeDialogStatus(true);
                                } else {
                                    storeDialogStatus(false);
                                }
                            }
                        });
                    }
                    mDialog.show();
                    /*Singleton.Instance().setSaverDataBooleanProjet(switchCompat.isChecked());
                    switchCompat.setChecked(isChecked);
                    startActivity(new Intent(Projet.this, Projet.class));
                    finish();*/
                }else if(!isChecked){
                    sessionManagerProjet.putSaverDataProjet("Non");
                    switchCompat.setChecked(false);
                    recreate();
                }
            }
        });
        return true;
    }

    private void storeDialogStatus(boolean isChecked){
        SharedPreferences mSharedPreferences = getSharedPreferences("CheckItem", MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putBoolean("item", isChecked);
        mEditor.apply();
    }

    private boolean getDialogStatus(){
        SharedPreferences mSharedPreferences = getSharedPreferences("CheckItem", MODE_PRIVATE);
        return mSharedPreferences.getBoolean("item", false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_search:
                searchView.open(true);
                return true;
            case R.id.action_refresh:
                String query=pays_code[position1];
                String pays=pays1[position1];
                if(position1==0){
                    pays="tout";
                }
                EventBus.getDefault().post(new SearchEvent1(query,1,pays));
                EventBus.getDefault().post(new SearchEvent2(query,1,pays));
                EventBus.getDefault().post(new SearchEvent3(query,1,pays));
                if(!isConnected()){
                    Snackbar snackbar=Snackbar.make(coordinatorLayout,is_fr?"Connectez-vous et réessayer":"No internet connection detected",Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                if(!isConnected()){
                    if(spinner!=null){
                        spinner.setSelection(0);
                    }
                }
                return true;
            case R.id.autre_share:
                Intent intent2=new Intent(Intent.ACTION_SEND);
                intent2.setType("text/plain");
                String url= "https://play.google.com/store/apps/details?id=com.benin.apkapka.cooperationsoffice.cooperationoffice";
                intent2.putExtra(Intent.EXTRA_TEXT, url);
                intent2.putExtra(Intent.EXTRA_SUBJECT, "Cooperations office");
                if(intent2.resolveActivity(getPackageManager())!=null)
                {

                    String[] blacklist = new String[]{"com.google.android.gm","com.facebook.katana" };
                    startActivity(generateCustomChooserIntent(intent2, blacklist));
                }
                else
                {
                    MyToast.show(this, getResources().getString(R.string.une_activity_installer_app_partage), false);
                }
                return true;


        }

        return super.onOptionsItemSelected(item);
    }
    private Intent generateCustomChooserIntent(Intent prototype, String[] forbiddenChoices) {
        List<Intent> targetedShareIntents = new ArrayList<>();
        List<HashMap<String, String>> intentMetaInfo = new ArrayList<HashMap<String, String>>();
        Intent chooserIntent;

        Intent dummy = new Intent(prototype.getAction());
        dummy.setType(prototype.getType());
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(dummy, 0);

        if (!resInfo.isEmpty()) {
            for (ResolveInfo resolveInfo : resInfo) {
                if (resolveInfo.activityInfo == null || Arrays.asList(forbiddenChoices).contains(resolveInfo.activityInfo.packageName))
                    continue;

                HashMap<String, String> info = new HashMap<String, String>();
                info.put("packageName", resolveInfo.activityInfo.packageName);
                info.put("className", resolveInfo.activityInfo.name);
                info.put("simpleName", String.valueOf(resolveInfo.activityInfo.loadLabel(getPackageManager())));
                intentMetaInfo.add(info);
            }

            if (!intentMetaInfo.isEmpty()) {
                // sorting for nice readability
                Collections.sort(intentMetaInfo, new Comparator<HashMap<String, String>>() {
                    @Override
                    public int compare(HashMap<String, String> map, HashMap<String, String> map2) {
                        return map.get("simpleName").compareTo(map2.get("simpleName"));
                    }
                });

                // create the custom intent list
                for (HashMap<String, String> metaInfo : intentMetaInfo) {
                    Intent targetedShareIntent = (Intent) prototype.clone();
                    targetedShareIntent.setPackage(metaInfo.get("packageName"));
                    targetedShareIntent.setClassName(metaInfo.get("packageName"), metaInfo.get("className"));
                    targetedShareIntents.add(targetedShareIntent);
                }

                chooserIntent = Intent.createChooser(targetedShareIntents.remove(targetedShareIntents.size() - 1), getResources().getString(R.string.une_activity_partager_via));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
                return chooserIntent;
            }
        }

        return Intent.createChooser(prototype, getResources().getString(R.string.une_activity_partager_via));
    }

    public class MyAdapteur extends BaseAdapter {


        public List<Country> country;
        Context c;

        public MyAdapteur(Context c,List<Country> country){
            this.c=c;
            this.country=country;

        }

        @Override
        public int getCount() {
            return country.size();
        }

        @Override
        public Object getItem(int position) {
            return country.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class DropdownViewHolder{
            TextView textView;
            ImageView imageView;
        }
        class  CViewHolder{
            ImageView imageView;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            CViewHolder holder=null;
            if(convertView==null){
                LayoutInflater inflater=LayoutInflater.from(c);
                convertView=inflater.inflate(R.layout.country_view,null);
                holder=new CViewHolder();
                holder.imageView=(ImageView) convertView.findViewById(R.id.country_image);
                convertView.setTag(holder);
            }
            else {
                holder=(CViewHolder)convertView.getTag();
            }
            Country countr1=country.get(position);
            holder.imageView.setImageResource(countr1.getId());

            return convertView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            DropdownViewHolder holder;
            if( convertView==null){
                LayoutInflater inflater=LayoutInflater.from(c);
                convertView=inflater.inflate(R.layout.country_dropdown_view,null);
                holder=new DropdownViewHolder();
                holder.imageView=(ImageView) convertView.findViewById(R.id.country_frop_image);
                holder.textView=(TextView)convertView.findViewById(R.id.country_drop_name);
                convertView.setTag(holder);
            }
            else{
                holder=(DropdownViewHolder)convertView.getTag();
            }
            Country country1=country.get(position);
            holder.textView.setText(country1.getName());
            holder.imageView.setImageResource(country1.getId());

            return convertView;
        }
    }
}
