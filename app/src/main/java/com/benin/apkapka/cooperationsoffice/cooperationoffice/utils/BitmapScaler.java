package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;

/**
 * Created by joel on 29/09/2016.
 */
public class BitmapScaler {


    public  static Bitmap scaleToWidth(Bitmap bitmap,int width)
    {
        float factor=width/(float)bitmap.getWidth();
       return  Bitmap.createScaledBitmap(bitmap,width,(int)(bitmap.getHeight()*factor),true);
    }

    public  static  Bitmap scaleToHeight(Bitmap bitmap,int height)
    {
        float factor=height/(float)bitmap.getHeight();
        return  Bitmap.createScaledBitmap(bitmap,(int)(bitmap.getWidth()*factor),height,true);
    }
    public  static  Bitmap scaleToActualSspectRatio(Bitmap bitmap)

    {
        if(bitmap!=null) {
            boolean flag = true;
        }



       return  null;
    }
    public Bitmap scaleToActualAspectRatio(Context c, Bitmap bitmap) {
        if (bitmap != null) {
            boolean flag = true;

            DisplayMetrics metrics=c.getResources().getDisplayMetrics();
            int deviceWidth = metrics.widthPixels;
            int deviceHeight =metrics.heightPixels;

            int bitmapHeight = bitmap.getHeight(); // 563
            int bitmapWidth = bitmap.getWidth(); // 900

// aSCPECT rATIO IS Always WIDTH x HEIGHT rEMEMMBER 1024 x 768

            if (bitmapWidth > deviceWidth) {
                flag = false;

// scale According to WIDTH
                int scaledWidth = deviceWidth;
                int scaledHeight = (scaledWidth * bitmapHeight) / bitmapWidth;

                try {
                    if (scaledHeight > deviceHeight)
                        scaledHeight = deviceHeight;

                    bitmap = Bitmap.createScaledBitmap(bitmap, scaledWidth,
                            scaledHeight, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (flag) {
                if (bitmapHeight > deviceHeight) {
                    // scale According to HEIGHT
                    int scaledHeight = deviceHeight;
                    int scaledWidth = (scaledHeight * bitmapWidth)
                            / bitmapHeight;

                    try {
                        if (scaledWidth > deviceWidth)
                            scaledWidth = deviceWidth;

                        bitmap = Bitmap.createScaledBitmap(bitmap, scaledWidth,
                                scaledHeight, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return bitmap;
    }
}
