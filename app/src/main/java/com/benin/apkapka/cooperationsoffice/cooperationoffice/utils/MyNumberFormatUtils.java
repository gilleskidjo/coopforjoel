package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.util.Log;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by joel on 13/12/2017.
 */

public class MyNumberFormatUtils {

    public static  MyNumberFormatUtils numberFormatUtils;

    public static MyNumberFormatUtils getInstance(){
          if(numberFormatUtils==null){
              numberFormatUtils=new MyNumberFormatUtils();
          }
      return  numberFormatUtils;
    }

    public String formateNumber(String value,boolean is_fr){
        NumberFormat nf=null;
        if(is_fr){
            nf=NumberFormat.getInstance(Locale.GERMANY);
        }
        else {
            nf=NumberFormat.getInstance(Locale.US);
        }
         if(nf!=null){
             DecimalFormat decimalFormat = (DecimalFormat)nf;
              decimalFormat.applyPattern("###,###.###");


            return decimalFormat.format(Double.parseDouble(value));
         }

       return "";
    }
}
