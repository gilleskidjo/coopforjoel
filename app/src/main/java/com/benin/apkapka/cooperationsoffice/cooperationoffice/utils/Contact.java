package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

/**
 * Created by joel on 28/08/2017.
 */
public class Contact {
    private String name;
    private String  number;
    private boolean IsChecked;

    public Contact() {
    }

    public Contact(String name, String number, boolean isChecked) {
        this.name = name;
        this.number = number;
        IsChecked = isChecked;
    }

    public String getName() {
        return name;
    }

    public boolean isChecked() {
        return IsChecked;
    }

    public String getNumber() {
        return number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setChecked(boolean checked) {
        IsChecked = checked;
    }

    @Override
    public String toString() {
        return "Nom "+name+" Numero "+number;
    }

    @Override
    public boolean equals(Object obj) {
        Contact c=(Contact)obj;
        return (this.number.equalsIgnoreCase(c.getNumber())&& this.name.equalsIgnoreCase(c.name)) ;
    }

    @Override
    public int hashCode() {
        return (this.name.length())+this.number.length();
    }
}
