package com.benin.apkapka.cooperationsoffice.cooperationoffice;

import android.content.SharedPreferences;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushNotification;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushNotificationRelation;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MenuVitrine;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyPushdataIntentService;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import io.objectbox.Box;
import io.objectbox.query.Query;
import io.objectbox.query.QueryBuilder;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by joel on 08/06/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {


    List<PushNotification> pushNotificationList=new ArrayList<>();

    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        SessionManager manager=new SessionManager(getApplicationContext());
        User user=manager.getUser(getApplicationContext());
        if(user!=null && user.getId()!=0){
            getUserToken(user.getId(),s);
        }
    }

    public void getUserToken(long useer_id,String token){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.get_user_token(""+useer_id,token);

        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {


                            } else {

                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {

                                } else {


                                }
                            }

                        } catch (Exception e) {


                        }

                    }//fin is success,
                    else {

                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {


            }

        });
    }

    @Override
    public void onMessageReceived( final RemoteMessage remoteMessage) {
          if(remoteMessage.getData()!=null && remoteMessage.getData().size()>0){

              String type_n=remoteMessage.getData().get("type_n");
                if(type_n.equalsIgnoreCase("message")){
                    String id=remoteMessage.getData().get("id");
                    String category=remoteMessage.getData().get("category");
                    String famille=remoteMessage.getData().get("famille");
                    String action=remoteMessage.getData().get("action");
                    String notification1=remoteMessage.getData().get("notification");
                    String type=remoteMessage.getData().get("type");
                    String icon=remoteMessage.getData().get("icon");
                    String url=remoteMessage.getData().get("url");
                    String id_notification=remoteMessage.getData().get("id_notification");
                    String title=remoteMessage.getData().get("title");
                    String title_en=remoteMessage.getData().get("title_en");
                    String body=remoteMessage.getData().get("body");
                    String body_en=remoteMessage.getData().get("body_en");
                    if (id!=null && !id.equalsIgnoreCase("") && type!=null && category!=null && famille!=null && action!=null){

                        preferences=getSharedPreferences("les_options",MODE_PRIVATE);
                        editor=preferences.edit();

                        final Gson gson2=new Gson();
                        final String json=preferences.getString("MES_MENUS",null);
                        List<MenuVitrine> data=null;
                        if(json != null) {
                            MenuVitrine[] menuVitrines = gson2.fromJson(json, MenuVitrine[].class);
                                data = Arrays.asList(menuVitrines);
                                ArrayList<String> str=new ArrayList<>();
                                for (int i=0;i<data.size();i++)
                                {
                                    str.add(data.get(i).getType());
                                }

                                if (!str.isEmpty())
                                {
                                    if (str.contains(type))
                                    {

                                                        Data.Builder intent1 = new Data.Builder();
                                                        intent1.putString("id",id);
                                                        intent1.putString("type",type);
                                                        intent1.putString("category",category);
                                                        intent1.putString("famille",famille);
                                                        intent1.putString("action",action);
                                                        intent1.putString("icon",icon);
                                                        intent1.putString("type_n",type_n);
                                                        intent1.putString("url",url);
                                                        intent1.putString("id_notification",id_notification);
                                                        intent1.putString("title",title);
                                                        intent1.putString("title_en",title_en);
                                                        intent1.putString("body",body);
                                                        intent1.putString("body_en",body_en);

                                                        OneTimeWorkRequest compressionWork =
                                                                new OneTimeWorkRequest.Builder(MyPushdataIntentService.class)
                                                                        .setInputData(intent1.build())
                                                                        .build();
                                                        WorkManager.getInstance().enqueue(compressionWork);


                                                        Gson gson=new Gson();
                                                        Box<PushNotificationRelation> pushNotificationRelationBox=((MyApplication)getApplicationContext()).getBoxStore().boxFor(PushNotificationRelation.class);
                                                        QueryBuilder<PushNotificationRelation> builder=pushNotificationRelationBox.query();
                                                        Query<PushNotificationRelation> query=builder.build();
                                                        PushNotificationRelation relation=query.findFirst();
                                                        if(relation!=null){
                                                            String relationData=relation.pushValue;
                                                            if(relationData!=null && !relationData.equalsIgnoreCase("")){
                                                                Type type_push=new TypeToken<List<PushNotification>>(){}.getType();
                                                                pushNotificationList=gson.fromJson(relationData,type_push);
                                                                if(pushNotificationList!=null ){
                                                                    PushNotification pushNotification=new PushNotification();
                                                                    pushNotification.setAction(action);
                                                                    pushNotification.setBody(body);
                                                                    pushNotification.setCategory(category);
                                                                    pushNotification.setFamille(famille);
                                                                    pushNotification.setType(type);
                                                                    pushNotification.setIcon(icon);
                                                                    pushNotification.setUrl(url);
                                                                    pushNotification.setTitle(title);
                                                                    pushNotification.setId(Integer.valueOf(id));
                                                                    pushNotification.setId_notification(id_notification);
                                                                    pushNotification.setType_n(type_n);
                                                                    pushNotification.setViewHolder_type(1);
                                                                    pushNotificationList.add(pushNotification);

                                                                    String pushvalue2=gson.toJson(pushNotificationList,type_push);
                                                                    relation.pushValue=pushvalue2;
                                                                    pushNotificationRelationBox.put(relation);
                                                                }
                                                                else {
                                                                    pushNotificationList=new ArrayList<>();
                                                                    PushNotification pushNotification=new PushNotification();
                                                                    pushNotification.setAction(action);
                                                                    pushNotification.setBody(body);
                                                                    pushNotification.setCategory(category);
                                                                    pushNotification.setFamille(famille);
                                                                    pushNotification.setIcon(icon);
                                                                    pushNotification.setUrl(url);
                                                                    pushNotification.setType(type);
                                                                    pushNotification.setTitle(title);
                                                                    pushNotification.setId(Integer.valueOf(id));
                                                                    pushNotification.setId_notification(id_notification);
                                                                    pushNotification.setType_n(type_n);
                                                                    pushNotification.setViewHolder_type(1);
                                                                    pushNotificationList.add(pushNotification);

                                                                    String pushvalue2=gson.toJson(pushNotificationList,type_push);
                                                                    relation.pushValue=pushvalue2;
                                                                    pushNotificationRelationBox.put(relation);

                                                                }
                                                            }
                                                        }
                                                        else{
                                                            Type type_push=new TypeToken<List<PushNotification>>(){}.getType();
                                                            PushNotificationRelation relation1=new PushNotificationRelation();
                                                            PushNotification pushNotification=new PushNotification();
                                                            pushNotification.setAction(action);
                                                            pushNotification.setBody(body);
                                                            pushNotification.setCategory(category);
                                                            pushNotification.setFamille(famille);
                                                            pushNotification.setIcon(icon);
                                                            pushNotification.setType(type);
                                                            pushNotification.setUrl(url);
                                                            pushNotification.setTitle(title);
                                                            pushNotification.setId(Integer.valueOf(id));
                                                            pushNotification.setId_notification(id_notification);
                                                            pushNotification.setType_n(type_n);
                                                            pushNotification.setViewHolder_type(1);
                                                            pushNotificationList.add(pushNotification);

                                                            String pushvalue2=gson.toJson(pushNotificationList,type_push);
                                                            relation1.pushValue=pushvalue2;
                                                            pushNotificationRelationBox.put(relation1);

                                                        }

                                    }



                                }



                                }


                        }


                }
                else if(type_n.equalsIgnoreCase("push")){

                    String icon=remoteMessage.getData().get("icon");
                    String id_notification=remoteMessage.getData().get("id_notification");
                    String title=remoteMessage.getData().get("title");
                    String title_en=remoteMessage.getData().get("title_en");
                    String body=remoteMessage.getData().get("body");
                    String body_en=remoteMessage.getData().get("body_en");
                    String conversation_id=remoteMessage.getData().get("conversation_id");

                    Data.Builder intent1 = new Data.Builder();
                    intent1.putString("id","0");
                    intent1.putString("icon",icon);
                    intent1.putString("type_n",type_n);
                    intent1.putString("id_notification",id_notification);
                    intent1.putString("title",title);
                    intent1.putString("title_en",title_en);
                    intent1.putString("body",body);
                    intent1.putString("body_en",body_en);


                    OneTimeWorkRequest compressionWork =
                            new OneTimeWorkRequest.Builder(MyPushdataIntentService.class)
                                    .setInputData(intent1.build())
                                    .build();
                    WorkManager.getInstance().enqueue(compressionWork);


                    Gson gson=new Gson();
                    Box<PushNotificationRelation> pushNotificationRelationBox=((MyApplication)getApplicationContext()).getBoxStore().boxFor(PushNotificationRelation.class);
                    QueryBuilder<PushNotificationRelation> builder=pushNotificationRelationBox.query();
                    Query<PushNotificationRelation> query=builder.build();
                    PushNotificationRelation relation=query.findFirst();
                    if(relation!=null){
                        String relationData=relation.pushValue;
                        if(relationData!=null && !relationData.equalsIgnoreCase("")){
                            Type type_push=new TypeToken<List<PushNotification>>(){}.getType();
                            pushNotificationList=gson.fromJson(relationData,type_push);
                            if(pushNotificationList!=null ){
                                PushNotification pushNotification=new PushNotification();
                                pushNotification.setIcon(icon);
                                pushNotification.setBody(body);
                                pushNotification.setTitle(title);
                                pushNotification.setId_notification(id_notification);
                                pushNotification.setType_n(type_n);
                                pushNotification.setViewHolder_type(2);
                                pushNotification.setConversation_id(conversation_id);
                                pushNotificationList.add(pushNotification);

                                String pushvalue2=gson.toJson(pushNotificationList,type_push);
                                relation.pushValue=pushvalue2;
                                pushNotificationRelationBox.put(relation);

                            }
                            else{
                                pushNotificationList=new ArrayList<>();
                                PushNotification pushNotification=new PushNotification();
                                pushNotification.setIcon(icon);
                                pushNotification.setBody(body);
                                pushNotification.setTitle(title);
                                pushNotification.setId_notification(id_notification);
                                pushNotification.setType_n(type_n);
                                pushNotification.setViewHolder_type(2);
                                pushNotification.setConversation_id(conversation_id);
                                pushNotificationList.add(pushNotification);

                                String pushvalue2=gson.toJson(pushNotificationList,type_push);
                                relation.pushValue=pushvalue2;

                                pushNotificationRelationBox.put(relation);

                            }
                        }
                    }
                    else{
                        Type type_push=new TypeToken<List<PushNotification>>(){}.getType();
                        PushNotificationRelation relation1=new PushNotificationRelation();
                        PushNotification pushNotification=new PushNotification();
                        pushNotification.setBody(body);
                        pushNotification.setIcon(icon);
                        pushNotification.setTitle(title);
                        pushNotification.setId_notification(id_notification);
                        pushNotification.setType_n(type_n);
                        pushNotification.setViewHolder_type(2);
                        pushNotification.setConversation_id(conversation_id);
                        pushNotificationList.add(pushNotification);

                        String pushvalue2=gson.toJson(pushNotificationList,type_push);

                        relation1.pushValue=pushvalue2;
                        pushNotificationRelationBox.put(relation1);
                    }
                }
                else{

                    String icon=remoteMessage.getData().get("icon");
                    String id_notification=remoteMessage.getData().get("id_notification");
                    String title=remoteMessage.getData().get("title");
                    String title_en=remoteMessage.getData().get("title_en");
                    String body=remoteMessage.getData().get("body");
                    String body_en=remoteMessage.getData().get("body_en");
                    Data.Builder intent1 = new Data.Builder();
                    intent1.putString("id","0");
                    intent1.putString("icon",icon);
                    intent1.putString("type_n",type_n);
                    intent1.putString("id_notification",id_notification);
                    intent1.putString("title",title);
                    intent1.putString("title_en",title_en);
                    intent1.putString("body",body);
                    intent1.putString("body_en",body_en);

                    OneTimeWorkRequest compressionWork =
                            new OneTimeWorkRequest.Builder(MyPushdataIntentService.class)
                                    .setInputData(intent1.build())
                                    .build();
                    WorkManager.getInstance().enqueue(compressionWork);


                    Gson gson=new Gson();
                    Box<PushNotificationRelation> pushNotificationRelationBox=((MyApplication)getApplicationContext()).getBoxStore().boxFor(PushNotificationRelation.class);
                    QueryBuilder<PushNotificationRelation> builder=pushNotificationRelationBox.query();
                    Query<PushNotificationRelation> query=builder.build();
                    PushNotificationRelation relation=query.findFirst();
                    if(relation!=null){
                        String relationData=relation.pushValue;
                        if(relationData!=null && !relationData.equalsIgnoreCase("")){
                            Type type_push=new TypeToken<List<PushNotification>>(){}.getType();
                            pushNotificationList=gson.fromJson(relationData,type_push);
                            if(pushNotificationList!=null ){
                                PushNotification pushNotification=new PushNotification();
                                pushNotification.setIcon(icon);
                                pushNotification.setBody(body);
                                pushNotification.setTitle(title);
                                pushNotification.setId_notification(id_notification);
                                pushNotification.setType_n(type_n);
                                pushNotification.setViewHolder_type(0);

                                pushNotificationList.add(pushNotification);

                                String pushvalue2=gson.toJson(pushNotificationList,type_push);
                                relation.pushValue=pushvalue2;
                                pushNotificationRelationBox.put(relation);

                            }
                            else{
                                pushNotificationList=new ArrayList<>();
                                PushNotification pushNotification=new PushNotification();
                                pushNotification.setIcon(icon);
                                pushNotification.setBody(body);
                                pushNotification.setTitle(title);
                                pushNotification.setId_notification(id_notification);
                                pushNotification.setType_n(type_n);
                                pushNotification.setViewHolder_type(0);

                                pushNotificationList.add(pushNotification);

                                String pushvalue2=gson.toJson(pushNotificationList,type_push);
                                relation.pushValue=pushvalue2;

                                pushNotificationRelationBox.put(relation);

                            }
                        }
                    }
                    else{
                        Type type_push=new TypeToken<List<PushNotification>>(){}.getType();
                        PushNotificationRelation relation1=new PushNotificationRelation();
                        PushNotification pushNotification=new PushNotification();
                        pushNotification.setBody(body);
                        pushNotification.setIcon(icon);
                        pushNotification.setTitle(title);
                        pushNotification.setId_notification(id_notification);
                        pushNotification.setType_n(type_n);
                        pushNotification.setViewHolder_type(0);

                        pushNotificationList.add(pushNotification);

                        String pushvalue2=gson.toJson(pushNotificationList,type_push);

                        relation1.pushValue=pushvalue2;
                        pushNotificationRelationBox.put(relation1);
                    }

                }//fin

          }
          else{

          }


    }



}
