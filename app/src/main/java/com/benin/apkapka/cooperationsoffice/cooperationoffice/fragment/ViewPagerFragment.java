package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by joel on 15/09/2016.
 */
public class ViewPagerFragment extends Fragment {



    ImageView image;
    ProgressBar  bar;
      public  static  ViewPagerFragment  getInstance(String url)
      {
         ViewPagerFragment v=new ViewPagerFragment();
          Bundle bundle=new Bundle();
          bundle.putString("url",url);
          v.setArguments(bundle);
          return  v;
      }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v=inflater.inflate(R.layout.aa_pager_item,container,false);
            image=(ImageView)v.findViewById(R.id.un_modele_view_image);
            bar=(ProgressBar)v.findViewById(R.id.un_modele_view_progressBar);
        String url=getArguments().getString("url");
        Picasso.with(getContext())
                .load(url)

                .into(image, new Callback() {
                    @Override
                    public void onSuccess() {
                        bar.setVisibility(View.GONE);
                        image.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        bar.setVisibility(View.GONE);
                        image.setVisibility(View.VISIBLE);
                    }
                });
        return v;
    }
}
