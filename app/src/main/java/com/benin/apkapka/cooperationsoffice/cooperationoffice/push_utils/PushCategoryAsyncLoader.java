package com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.PushDataEvent;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.MyPushDatabaseHelper;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushDataDao;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joel on 15/08/2017.
 */
public class PushCategoryAsyncLoader extends AsyncTaskLoader<List<PushCategory>> {
    List<PushCategory> list1=new ArrayList<PushCategory>();
    PushDataDao pushDataDao;
    String type,category,category2,category3;
    EventBus bus;
    public PushCategoryAsyncLoader(Context c, String type,String category,String category2,String category3 ){
        super(c);
        pushDataDao=new PushDataDao( getContext());
        this.type=type;
        this.category=category;
        this.category2=category2;
        this.category3=category3;
       bus=EventBus.getDefault();
       bus.register(this);
    }

       @Subscribe
      public void onEvent(PushDataEvent event){
          onContentChanged();

      }

    @Override
    protected void onStartLoading() {
       if(list1.size()>0){
           deliverResult(list1);
       }
        if(list1.size()<1 || takeContentChanged()){
            forceLoad();
        }
    }

    @Override
    public List<PushCategory> loadInBackground() {
        List<PushCategory> list=new ArrayList<PushCategory>();
        Cursor c= pushDataDao.getPushDataCategoryFamilleCount(type,category,category2,category3);
        if(c!=null){

            while (c.moveToNext()){

                String mcategory=c.getString(c.getColumnIndexOrThrow(MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_CATEGORY));
                int count=c.getInt(c.getColumnIndexOrThrow("count"));
                PushCategory pushCategory=new PushCategory(mcategory,count);
                if(count>0)
                {

                    list.add(pushCategory);
                }
            }
            c.close();
        }
        list1=list;
        return list;
    }

    @Override
    public void deliverResult(List<PushCategory> data) {
        if(isReset()){
            return;
        }
        super.deliverResult(data);
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        bus.unregister(this);
        super.onReset();

    }
}
