package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;




import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


/**
 * Created by joel on 11/02/2016.
 */
public interface MyInterface {
    @Multipart
    @POST("/cooperationv2/post_produit.php")
    Call<String> uploadProduit(@Part("user")String user,@Part("title")String title,@Part("description") String description,@Part("price") String price,@Part("reduction") String reduction,@Part("texte_prix")String texte_prix,@Part("texte_reduction")String texte_reduction,@Part("auteur")String auteur,@Part("contact_fournisseur")String contact_fournisseur,@Part("famille")String famille,@Part("lien_ensavoir_plus")String lien_ensavoir_plus,@Part("lien_description")  String lien_description,@Part("texte_invitation")String texte_invitation,@Part("texte_notification") String texte_notfication,@Part("etat") String etat,@Part("pays") String pays,@Part("type") String type,@Part("category") String category,@Part("longitude") String longitude,@Part("latitude") String latitude,@Part("lang") String lang,@Part("picture\"; filename=\"imagee.png\"") RequestBody requestBody,@Part("zone") String zone);

    @Multipart
    @POST("/cooperationv2/edit_post_produit.php")
    Call<String>editPostProduit(@Part("title")String title,@Part("description") String description,@Part("famille")String famille,@Part("id") String id,@Part("price")String price,@Part("reduction")String reduction,@Part("url")String url,@Part("picture\"; filename=\"imagee.png\"") RequestBody requestBody);

    @FormUrlEncoded
    @POST("/cooperationv2/login.php")
    Call<String>login(@Field("email") String email, @Field("password") String password, @Field("lang") String lang);
    @Multipart
    @POST("/cooperationv2/register2.php")
    Call<String>register(@Part("name") String name,@Part("email") String email,@Part("lang") String lang,@Part("id") String id,@Part("is_social") String is_social,@Part("numwhatsapp") String numwhatsapp,@Part("addresse_gmail") String addresse_gmail,@Part("addresse_face") String addresse_face,@Part("is_num_send") String is_num_send,@Part("is_adre_face_send") String is_adre_face_send,@Part("is_adre_gmail_send") String is_adre_gmail_send,@Part("prenom") String prenom,@Part("prefix")String pays,@Part("picture\"; filename=\"imagee2.png\"") RequestBody requestBody2);
    @FormUrlEncoded
    @POST("/cooperationv2/Update_user_adresse.php")
    Call<String>updateAdresse(@Field("id") long id,@Field("adresse") String adresse,@Field("lang") String lang);

    @Multipart
    @POST("/cooperationv2/save_user_photo.php")
    Call<String>saveUserPhoto(@Part("user") String id,@Part("picture\"; filename=\"imagee2.png\"") RequestBody requestBody2);
    @FormUrlEncoded
    @POST("/cooperationv2/update_user_data_from_edit_profile.php")
    Call<String>updateUserDataEditProdile(@Field("user") long id,@Field("nom") String nom,@Field("prenom") String prenom,@Field("pays") String pays,@Field("phone")String phone,@Field("email_face")String email_face,@Field("email_google")String email_google,@Field("tel") String tel,@Field("date_naissance") String date_naissance,@Field("prefix") String prefix);

    @FormUrlEncoded
    @POST("/cooperationv2/save_users_avis.php")
    Call<String>save_users_avis(@Field("id") long id,@Field("description") String description,@Field("lang") String lang);

    @FormUrlEncoded
    @POST("/cooperationv2/favori_save_favorite.php")
    Call<String>saveFavori(@Field("action") String action,@Field("favoris") String favoris,@Field("user_id") long user_id);
    @FormUrlEncoded
    @POST("/cooperationv2/signalement_create_signalement.php")
    Call<String>createSignalement(@Field("user") long id,@Field("produit_id") long produit_id,@Field("message") String message);
    @FormUrlEncoded
    @POST("/cooperationv2/update_user_location.php")
    Call<String>updateUserLocation(@Field("user") String id,@Field("longitude") String longitude,@Field("latitude")String latitude);
    @FormUrlEncoded
    @POST("/cooperationv2/chat_create_conversation.php")
    Call<String>create_conversation(@Field("user") long id);
    @FormUrlEncoded
    @POST("/cooperationv2/checkuserasannonce.php")
    Call<String>check_user_has_annonce(@Field("user") String id);
    @FormUrlEncoded
    @POST("/cooperationv2/get_target_user_data.php")
    Call<String>get_target_user_data(@Field("user") String id);
    @FormUrlEncoded
    @POST("/cooperationv2/get_user_token.php")
    Call<String>get_user_token(@Field("user") String id,@Field("token") String token);
    @FormUrlEncoded
    @POST("/cooperationv2/chat_get_chat_conversation.php")
    Call<String>get_All_conversation(@Field("user") long id);
    @FormUrlEncoded
    @POST("/cooperationv2/get_all_user_favorite.php")
    Call<String>get_All_Favori(@Field("user") String id,@Field("zone") String zone,@Field("page") int page);
    @FormUrlEncoded
    @POST("/cooperationv2/get_all_user_favorite_login.php")
    Call<String>get_All_Favori_In_Login(@Field("user") String id,@Field("zone") String zone);
    @FormUrlEncoded
    @POST("/cooperationv2/get_all_user_vitrine.php")
    Call<String>get_All_user_vitrine(@Field("user") String id,@Field("zone") String zone,@Field("page") int page);
    @FormUrlEncoded
    @POST("/cooperationv2/get_all_public_user_vitrine.php")
    Call<String>get_All_Public_user_vitrine(@Field("user") String id,@Field("zone") String zone,@Field("page") int page);
    @FormUrlEncoded
    @POST("/cooperationv2/get_target_user.php")
    Call<String>get_User(@Field("user") long id);
    @FormUrlEncoded
    @POST("/cooperationv2/chat_get_chat_message_list.php")
    Call<String>get_All_chat_message(@Field("conversation_id") long id);
    @FormUrlEncoded
    @POST("/cooperationv2/get_product_famille_list.php")
    Call<String>get_All_Famille(@Field("type") String type,@Field("category") String category);
    @FormUrlEncoded
    @POST("/cooperationv2/get_publish_annonce.php")
    Call<String>get_publish_annonce(@Field("id") String id);
    @FormUrlEncoded
    @POST("/cooperationv2/update_publish_annonce_state.php")
    Call<String>update_publish_annonce(@Field("id") String id,@Field("state") String state);
    @FormUrlEncoded
    @POST("/cooperationv2/delete_publish_annonce.php")
    Call<String>delete_publish_annonce(@Field("id") String id);
    @FormUrlEncoded
    @POST("/cooperationv2/update_annonce_user_share_state.php")
    Call<String>updateAnnonceUserShareState(@Field("id") String id);
    @FormUrlEncoded
    @POST("/cooperationv2/chat_create_chat_message.php")
    Call<String>create_chat_message(@Field("conversation_id") long id,@Field("user") long user,@Field("message") String message);
    @FormUrlEncoded
    @POST("/cooperationv2/get_all_divertissement_famille.php")
    Call<String>get_All_Divertissement_famille(@Field("famille")String famille,@Field("zone") String zone,@Field("page") int page);
    @FormUrlEncoded
    @POST("/cooperationv2/get_all_divertissement_in_famille.php")
    Call<String>get_All_Divertissement_In_famille(@Field("famille")String famille,@Field("zone")String zone,@Field("page") int page);
    @Multipart
    @POST("/cooperationv2/chat_create_chat_photo.php")
    Call<String>create_create_chat_photo(@Part("conversation_id") String id,@Part("user") String user,@Part("picture\"; filename=\"imagee2.png\"") RequestBody requestBody2);
    @FormUrlEncoded
    @POST("/cooperationv2/update_user_data.php")
    Call<String>updateUserData(@Field("id") long id,@Field("numwhatsapp") String numwhatsapp,@Field("addresse_gmail") String addresse_gmail,@Field("addresse_face")String addresse_face,@Field("is_num_send") String is_num_send,@Field("is_adre_face_send") String is_adre_face_send,@Field("is_adre_gmail_send") String is_adre_gmail_send,@Field("lang") String lang);
    @FormUrlEncoded
    @POST("/cooperationv2/get_list_home.php")
    Call<String>getListHome(@Field("zone") String zone);
    @FormUrlEncoded
    @POST("/cooperationv2/get_list_product_top_vue.php")
    Call<String>getListProductTopVue(@Field("user") String user,@Field("zone") String zone);
}
