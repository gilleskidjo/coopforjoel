package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.BuildConfig;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class WelcomActivity extends BaseLanguageActivity {

    FusedLocationProviderClient mFusedLocationProviderClient;
    SettingsClient mSettingsClient;
    LocationRequest mLocationRequest;
    LocationSettingsRequest mLocationSettingsRequest;
    LocationCallback mLocationCallback;
    Location mLocation;
    private static final int LOCATION_UPDATE_INTERVAL=10000;
    private static  final int LOCATION_FAST_UPDATE_INTERVAL=60*1000;
    private  static  final int REQUEST_CHECK_SETTINGS=10;

    boolean is_fr;
    AlertDialog alertDialog;
    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_welcom);

        is_fr=getResources().getBoolean(R.bool.lang_fr);

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        btn= (Button) findViewById(R.id.dummy_button);
          btn.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  Intent intent=new Intent(WelcomActivity.this,SettingMenuActivity.class);
                  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                  startActivity(intent);
                  WelcomActivity.this.finish();
              }
          });

        initLocation();
        SessionManager sessionManager=new SessionManager(this);
        sessionManager.putCanRequestLocation(false);
          if(getIntent().getExtras()!=null){
              Bundle bundle=getIntent().getExtras();
               if(bundle.containsKey("inscription")){
                String inscription=bundle.getString("inscription","non");
                 if(inscription.equalsIgnoreCase("non")){
                     SessionManager manager=new SessionManager(this);
                     User user=manager.getUser(this);
                      if(user!=null && user.getId()!=0){
                          String zone="";
                          zone=user.getPays();
                          if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
                              zone="france";
                          }
                          ((MyApplication)getApplicationContext()).FetchFavori(user.getId(),zone);
                          ((MyApplication)getApplicationContext()).GetUserData(user.getId());
                      }

                 }
               }
          }

    }
    private void initLocation(){
        mFusedLocationProviderClient= LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient=LocationServices.getSettingsClient(this);
        mLocationCallback=new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
              if(!isFinishing()){
                  if(locationResult!=null){

                      mLocation=locationResult.getLastLocation();
                      SessionManager sessionManager=new SessionManager(WelcomActivity.this);
                      User user=sessionManager.getUser(WelcomActivity.this);
                      sessionManager.putUserLongitude(""+mLocation.getLongitude());
                      sessionManager.putUserLatitude(""+mLocation.getLatitude());
                      user.setLongitude(""+mLocation.getLongitude());
                      user.setLatitude(""+mLocation.getLatitude());
                      sessionManager.setUser(WelcomActivity.this,user);
                      sessionManager.putCanRequestLocationUpdate(false);
                      UpdateUserLocation(user.getId(),""+mLocation.getLatitude(),""+mLocation.getLongitude());

                  }
              }
            }
        };

        mLocationRequest=LocationRequest.create();
        mLocationRequest.setInterval(LOCATION_UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(LOCATION_FAST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder=new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest=builder.build();
    }

    @SuppressWarnings({"MissingPermission"})
    private void requestLocationUpdate(){
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,mLocationCallback,null);
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if(e instanceof ResolvableApiException){
                            try{
                                ResolvableApiException apiException=(ResolvableApiException)e;
                                apiException.startResolutionForResult(WelcomActivity.this,
                                        REQUEST_CHECK_SETTINGS);
                            }
                            catch (IntentSender.SendIntentException ex){

                            }
                        }//fin if
                    }
                });
    }
    private void checkPermission(){
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        requestLocationUpdate();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if(response.isPermanentlyDenied()){
                            AlertDialog.Builder  builder=new AlertDialog.Builder(WelcomActivity.this);
                            builder.setMessage(is_fr?"L'ouverture des contenus nécessite votre position":"Opening content requires your position");
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if(alertDialog!=null){
                                        alertDialog.dismiss();

                                    }
                                    // Build intent that displays the App settings screen.

                                }
                            });

                            alertDialog=builder.show();
                        }
                        else{

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission,final PermissionToken token) {

                        AlertDialog.Builder  builder=new AlertDialog.Builder(WelcomActivity.this);
                        builder.setMessage(is_fr?"L'ouverture des contenus nécessite votre position":"Opening content requires your position");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(alertDialog!=null){
                                    alertDialog.dismiss();

                                }
                                // Build intent that displays the App settings screen.
                                token.continuePermissionRequest();
                            }
                        });

                        alertDialog=builder.show();
                    }
                }).check();

    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPermission();

    }

    public void UpdateUserLocation(long id,String latitude,String longitude){


        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.updateUserLocation(""+id,longitude,latitude);

        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                           /* if (error) {
                                btn.setEnabled(true);
                            } else {

                                btn.setEnabled(true);
                            }
                            */

                        } catch (Exception e) {

                        }

                    }//fin is success,
                    else {


                    }//fin else is success

                }
                else {

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

            }

        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
    }









}
