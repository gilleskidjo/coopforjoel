package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.DialogFragment;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ListDataHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyDateServeur;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyTrackDateIntenteService;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by joel on 24/09/2016.
 */
public class LoginActivity  extends BaseLanguageActivity implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener{


    String countryCode[];
    String countryCodeName[];

    Button btnConnect;
    AlertDialog dialogPro;
    ProgressDialog dialog;
  String email="";
    boolean is_fr=false;

  LoginButton loginButton;
   CallbackManager callbackManager;
    private boolean canlisten=false;
    private  static final  int REQUEST_LOGIN=2222;
    public  static  final  int REQUEST_CODE_GOOGLE_PLUS=122;
    public static  final  int APP_REQUEST_CODE=200;
    SignInButton signInButton;
    GoogleApiClient client;
    DialogFragment f;

    ProfileTracker profileTracker;
    Target  target;


    //Google login variable
    String firstname="";
    String nom="";
    String prenom="";
    String id="";
  String mpays [];
  String pays="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.primary_dark));
        }

        callbackManager=CallbackManager.Factory.create();
        setContentView(R.layout.login_layout);
        mpays=getResources().getStringArray(R.array.toppays);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        countryCode=getResources().getStringArray(R.array.contryCode);
        countryCodeName=getResources().getStringArray(R.array.countryCodeName);




        btnConnect=(Button)findViewById(R.id.sign_connect);


        loginButton=(LoginButton)findViewById(R.id.btn_facebook);
        signInButton=(SignInButton)findViewById(R.id.btn_google);

        btnConnect.setOnClickListener(this);

        FragmentManager fm=getSupportFragmentManager();
        f=(DialogFragment)fm.findFragmentByTag(DialogFragment.TAG);
        if(f==null)
        {
            f=new DialogFragment();
            fm.beginTransaction().add(f,DialogFragment.TAG).commit();
        }

        prepareFacebookForOncreate();
     prepareGoogleForOncreate();


    }
    private void prepareFacebookForOncreate()

    {
        loginButton.setReadPermissions(Arrays.asList("public_profile","email"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                    f.showDialogue("Connexion...");
                FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);
          GraphRequest request =  GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(final JSONObject me, GraphResponse response) {

                          if(response.getError()!=null)
                          {
                              f.showDialogPro("Connexion"+response.toString());

                          }
                           else
                          {
                             FacebookuploadPhotoFromFailImageRequeste(me);

                          }
                    f.hideDialog();
                    }
                });
                  Bundle bundle=new Bundle();
                    bundle.putString("fields","id, email, first_name, last_name");
                   request.setParameters(bundle);
                  request.executeAsync();
            }



            @Override
            public void onCancel() {
                Log.e("Cancel","OK");
            }

            @Override
            public void onError(FacebookException error) {
             Log.e("FaceExecptio",""+error.getMessage());
           if(!isFinishing()){
               f.showDialogPro(is_fr==true?"Une erreur inconnue s'est produite":"Unknown error occured");
           }
            }
        });



        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isConnected()){
                   
                    LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile","email"));
                    lauchService();
                     setUpAboonement();
                 }
                 else{
                   if(!isFinishing()){
                       MyToast.show(LoginActivity.this,is_fr?"Aucune connexion internet détectée":"No internet connection detected",true);
                   }
                }
            }
        });
    }
    private void FacebookuploadPhotoFromFailImageRequeste(JSONObject me){
        try {

            String id = me.getString("id");
            final String email =me.has("email")==true? me.getString("email"):"email facebook absent,compte créé avec numero";
            String firstname = me.has("first_name")==true?me.getString("first_name"):"";
            String lastname =me.has("last_name")==true?me.getString("last_name"):"";



            String name=lastname;

            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(Config.GET_BASE_URL_RETROFIT)
                    .addConverterFactory(ScalarsConverterFactory.create())

                    .build();

            MyInterface service=retrofit.create(MyInterface.class);
            Call<String> call=service.register(name==null?"":name,email,is_fr==true?"fr":"en","facebook"+id,"oui","","",email,"non","oui","",firstname,pays,null);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call1,Response<String> response) {


                    if(response!=null)
                    {
                        if(response.isSuccessful())
                        {
                            try {
                                String rep = response.body() != null ? response.body() : "";

                                JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                                boolean error=o.getBoolean("error");
                                if(error==true)
                                {


                                    if(!isFinishing()){
                                        MyToast.show(LoginActivity.this,o.getString("message"),false);
                                    }

                                }
                                else
                                {


                                    f.hideDialog();

                                    User user=ParseJson.parseUser(LoginActivity.this,o);
                                    SessionManager s=new SessionManager(LoginActivity.this);
                                    s.setIsFromSocial(true);
                                    s.setUser(LoginActivity.this,user);
                                    s.setCompteFacebook(email);
                                    s.setIs_Facebook(true);
                                    s.setLogginType(2);
                                    s.setLogin(true);

                                    Intent intent=new Intent(LoginActivity.this,WelcomActivity.class);
                                    boolean inscription=o.getBoolean("inscription");
                                     if(inscription==true){
                                         intent.putExtra("inscription","oui");
                                     }
                                     else{
                                         intent.putExtra("inscription","non");
                                     }
                                    startActivity(intent);
                                    finish();
                                }

                            }
                            catch(Exception e)
                            {

                            }



                        }
                        else
                        {


                            if(!isFinishing()){
                                f.hideDialog();
                                f.showDialogPro(is_fr?"Aucune connexion internet détectée":"No internet connection detected");
                            }
                        }
                    }
                    else {

                    }
                    f.hideDialog();

                }


                @Override
                public void onFailure(Call<String> call1,Throwable t) {


                    if(!isFinishing()){
                        final AlertDialog.Builder builder=new AlertDialog.Builder(LoginActivity.this);


                        f.hideDialog();
                        f.showDialogPro(is_fr?"Aucune connexion internet détectée":"No internet connection detected");
                    }


                }
            });
        }
        catch (JSONException e)
        {

            if(!isFinishing()){
                f.showDialogPro(is_fr?"Aucune connexion internet détectée":"No internet connection detected");
            }
        }
    }
    private void FacebookuploadPhotoFromSuccessImageRequeste(JSONObject me,Bitmap bitmap){
        try {

            ByteArrayOutputStream out=new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,0,out);
            byte bytes[]=out.toByteArray();
            MediaType mediaType=MediaType.parse("multipart/form-data");
            RequestBody requestBody1 = RequestBody.create(mediaType, bytes);

            String id = me.getString("id");
            final String email =me.has("email")==true? me.getString("email"):"email facebook absent,compte créé avec numero";
            String firstname = me.has("first_name")==true?me.getString("first_name"):"";
            String lastname =me.has("last_name")==true?me.getString("last_name"):"";
            String url=me.getJSONObject("picture").getJSONObject("data").getString("url");


            String name=lastname;
            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(Config.GET_BASE_URL_RETROFIT)
                    .addConverterFactory(ScalarsConverterFactory.create())

                    .build();

            MyInterface service=retrofit.create(MyInterface.class);
            Call<String> call=service.register(name==null?"":name,email,is_fr==true?"fr":"en","facebook"+id,"oui","","",email,"non","oui","",firstname,pays,requestBody1);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call1,Response<String> response) {


                    if(response!=null)
                    {
                        if(response.isSuccessful())
                        {
                            try {
                                String rep = response.body() != null ? response.body() : "";

                                JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                                boolean error=o.getBoolean("error");
                                if(error==true)
                                {


                                    if(!isFinishing()){
                                        MyToast.show(LoginActivity.this,o.getString("message"),false);
                                    }
                                }
                                else
                                {



                                    f.hideDialog();


                                    User user=ParseJson.parseUser(LoginActivity.this,o);
                                    SessionManager s=new SessionManager(LoginActivity.this);
                                    s.setIsFromSocial(true);
                                    s.setUser(LoginActivity.this,user);
                                    s.setCompteFacebook(email);
                                    s.setIs_Facebook(true);
                                    s.setLogginType(2);
                                    s.setLogin(true);


                                    Intent intent=new Intent(LoginActivity.this,WelcomActivity.class);
                                    boolean inscription=o.getBoolean("inscription");
                                    if(inscription==true){
                                        intent.putExtra("inscription","oui");
                                    }
                                    else{
                                        intent.putExtra("inscription","non");
                                    }
                                    startActivity(intent);
                                    finish();
                                }

                            }
                            catch(Exception e)
                            {

                            }



                        }
                        else
                        {

                            if(!isFinishing()){
                                f.hideDialog();
                                f.showDialogPro(is_fr?"Aucune connexion internet détectée":"No internet connection detected");
                            }
                        }
                    }
                    else {

                    }
                    f.hideDialog();

                }


                @Override
                public void onFailure(Call<String> call1,Throwable t) {


                    if(!isFinishing()){
                        final AlertDialog.Builder builder=new AlertDialog.Builder(LoginActivity.this);


                        f.hideDialog();
                        f.showDialogPro(is_fr?"Aucune connexion internet détectée":"No internet connection detected");
                    }


                }
            });
        }
        catch (JSONException e)
        {

            if(!isFinishing()){
                f.showDialogPro(is_fr?"Aucune connexion internet détectée":"No internet connection detected");
            }
        }
    }

    private void lauchService(){
        startService(new Intent(this, MyTrackDateIntenteService.class));
    }
    private void prepareGoogleForOncreate()
    {

        GoogleSignInOptions gso=new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        client=new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)

                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();

        signInButton.setOnClickListener(this);


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    public boolean isSimAvailable(Context context){
        TelephonyManager manager=(TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        if(!(manager.getSimState()==TelephonyManager.SIM_STATE_ABSENT)){

            return true;
        }
        return false;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {

            case  R.id.sign_connect:
                checkToken();
                if(isConnected()){
                    setUpAboonement();

                }
                break;
            case  R.id.btn_facebook:

                break;

            case R.id.btn_google:

                if(isConnected()){
                    Intent intent3=Auth.GoogleSignInApi.getSignInIntent(client);
                    startActivityForResult(intent3,REQUEST_CODE_GOOGLE_PLUS);
                   lauchService();
                    setUpAboonement();
                }
                else {
                    if(!isFinishing()){
                        MyToast.show(this,is_fr?"Aucune connexion internet détectée":"No internet connection detected",true);
                    }
                }
                break;
        }
    }


    private void loginUser()
    {


        if(isConnected()){
            startService(new Intent(this, MyTrackDateIntenteService.class));
            setUpAboonement();
         email=""+email;
            f.showDialogue("Connexion...");

            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(Config.GET_BASE_URL_RETROFIT)
                    .addConverterFactory(ScalarsConverterFactory.create())

                    .build();

            MyInterface service=retrofit.create(MyInterface.class);
            Call<String> call=service.register("","",is_fr==true?"fr":"en","","non",email,"","","","","","",pays,null);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call1,Response<String> response) {


                    if(response!=null)
                    {
                        if(response.isSuccessful())
                        {
                            try {
                                String rep = response.body() != null ? response.body() : "";

                                JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));

                                boolean error=o.getBoolean("error");
                                if(error==true)
                                {


                                    MyToast.show(LoginActivity.this,o.getString("message"),false);
                                }
                                else
                                {
                                    f.hideDialog();

                                    User user=ParseJson.parseUser(LoginActivity.this,o);

                                    SessionManager s=new SessionManager(LoginActivity.this);
                                    s.setIsFromSocial(false);
                                    s.setUser(LoginActivity.this,user);
                                    s.setLogginType(3);
                                    s.setLogin(true);
                                    s.setIs_num_whatsapp(true);
                                    s.setCompteWhatsapp(email);
                                    Intent intent=new Intent(LoginActivity.this,WelcomActivity.class);
                                    boolean inscription=o.getBoolean("inscription");
                                    if(inscription==true){
                                        intent.putExtra("inscription","oui");
                                    }
                                    else{
                                        intent.putExtra("inscription","non");
                                    }
                                    startActivity(intent);
                                    finish();
                                }

                            }
                            catch(Exception e)
                            {


                            }

                            f.hideDialog();

                        }
                        else
                        {
                            Log.e("Reponse pas success",response.message());
                            if(!isFinishing()){
                                f.hideDialog();
                                f.showDialogPro(is_fr?"Aucune connexion internet détectée":"No internet connection detected");
                            }
                        }
                    }
                    else
                    {

                    }

                    f.hideDialog();
                }

                @Override
                public void onFailure(Call<String> call1,Throwable t) {

                  if(!isFinishing()){
                      f.hideDialog();
                      f.showDialogPro(is_fr?"Aucune connexion internet détectée":"No internet connection detected");
                  }


                }
            });
        }
        else{
            if(!isFinishing()){
                MyToast.show(this,is_fr?"Aucune connexion internet détectée":"No internet connection detected",true);
            }
        }
    }

    private boolean isConnected(){
        ConnectivityManager cm=(ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
  return (info!=null && info.isConnectedOrConnecting());
    }
    @Override
    protected void onStart() {
        super.onStart();

    }

    private void disConnect()
    {
        Auth.GoogleSignInApi.signOut(client).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                //deconnecter l'utilisateur
              //  updateUi(false);
            }
        });

    }
    private  void sup()
    {
        Auth.GoogleSignInApi.revokeAccess(client).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                //suppression du compte de l'utilisateur localement et sur le server
             //   updateUi(false);
            }
        });

    }

    private void handleResult(GoogleSignInResult result)
    {
        GoogleSignInAccount account=null;
        f.showDialogue("Connexion");
        if(result.isSuccess())
        {
           if(result!=null){
               account=result.getSignInAccount();
              if(account!=null){

                   firstname=account.getDisplayName();
                   email=account.getEmail();
                   String[] parts = account.getDisplayName().split("\\s+");
                     nom=parts[parts.length-1];
                     prenom=parts[0];
                  id=account.getId();
                  if(account.getPhotoUrl()!=null){
                      target=new Target() {
                          @Override
                          public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                              GoogleUploadFromSuccessImageFetch(bitmap);
                          }//fin

                          @Override
                          public void onBitmapFailed(Drawable errorDrawable) {
                              GoogleUploadFromFetchImageFail();
                          }//

                          @Override
                          public void onPrepareLoad(Drawable placeHolderDrawable) {

                          }
                      };//fin target
                      Picasso.with(LoginActivity.this).load(account.getPhotoUrl()).into(target);
                  }
                  else{
                      GoogleUploadFromFetchImageFail();
                  }//Photo url null
              }
           }



        }
        else{
           // updateUi(false);
          if(!isFinishing()){
              f.hideDialog();
              f.showDialogPro(is_fr?"Aucune connexion internet détectée":"No internet connection detected");
          }
        }

    }

    private void GoogleUploadFromFetchImageFail(){

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);
        Call<String> call=service.register(nom==null?"":firstname,email,is_fr==true?"fr":"en","google"+id,"oui","",email,"","non","","oui",prenom,pays,null);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,Response<String> response) {


                if(response!=null)
                {

                    if(response.isSuccessful())
                    {

                        try {
                            String rep = response.body() != null ? response.body() : "";

                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error=o.getBoolean("error");
                            if(error==true)
                            {

                                MyToast.show(LoginActivity.this,o.getString("message"),false);
                            }
                            else
                            {

                                f.hideDialog();
                                User user=ParseJson.parseUser(LoginActivity.this,o);
                                SessionManager s=new SessionManager(LoginActivity.this);
                                s.setIsFromSocial(true);

                                s.setUser(LoginActivity.this,user);
                                s.setLogginType(1);
                                s.setLogin(true);
                                s.setCompteGoogle(email);
                                s.setIs_Google(true);
                                Intent intent=new Intent(LoginActivity.this,WelcomActivity.class);
                                boolean inscription=o.getBoolean("inscription");
                                if(inscription==true){
                                    intent.putExtra("inscription","oui");
                                }
                                else{
                                    intent.putExtra("inscription","non");
                                }
                                startActivity(intent);
                                finish();
                            }

                        }
                        catch(Exception e)
                        {


                        }



                    }
                    else
                    {

                        if(!isFinishing()){
                            f.hideDialog();
                            f.showDialogPro(is_fr?"Aucune connexion internet détectée":"No internet connection detected");
                        }
                    }
                }
                f.hideDialog();

            }


            @Override
            public void onFailure(Call<String> call1,Throwable t) {


                if(!isFinishing()){
                    final AlertDialog.Builder builder=new AlertDialog.Builder(LoginActivity.this);


                    f.hideDialog();
                    f.showDialogPro(is_fr?"Aucune connexion internet détectée":"No internet connection detected");
                }


            }
        });
    }
    private void GoogleUploadFromSuccessImageFetch(Bitmap bitmap){

        ByteArrayOutputStream out=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,0,out);
        byte bytes[]=out.toByteArray();
        MediaType mediaType=MediaType.parse("multipart/form-data");
        RequestBody requestBody1 = RequestBody.create(mediaType, bytes);
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);
        Call<String> call=service.register(nom==null?"":firstname,email,is_fr==true?"fr":"en","google"+id,"oui","",email,"","non","","oui",prenom,pays,requestBody1);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,Response<String> response) {


                if(response!=null)
                {

                    if(response.isSuccessful())
                    {

                        try {
                            String rep = response.body() != null ? response.body() : "";

                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error=o.getBoolean("error");
                            if(error==true)
                            {

                                MyToast.show(LoginActivity.this,o.getString("message"),false);
                            }
                            else
                            {

                                f.hideDialog();
                                User user=ParseJson.parseUser(LoginActivity.this,o);
                                SessionManager s=new SessionManager(LoginActivity.this);
                                s.setIsFromSocial(true);

                                s.setUser(LoginActivity.this,user);
                                s.setLogginType(1);
                                s.setLogin(true);
                                s.setCompteGoogle(email);
                                s.setIs_Google(true);
                                Intent intent=new Intent(LoginActivity.this,WelcomActivity.class);
                                boolean inscription=o.getBoolean("inscription");
                                if(inscription==true){
                                    intent.putExtra("inscription","oui");
                                }
                                else{
                                    intent.putExtra("inscription","non");
                                }
                                startActivity(intent);
                                finish();
                            }

                        }
                        catch(Exception e)
                        {


                        }



                    }
                    else
                    {

                        if(!isFinishing()){
                            f.hideDialog();
                            f.showDialogPro(is_fr?"Aucune connexion internet détectée":"No internet connection detected");
                        }
                    }
                }
                f.hideDialog();

            }


            @Override
            public void onFailure(Call<String> call1,Throwable t) {

                if(!isFinishing()){
                    final AlertDialog.Builder builder=new AlertDialog.Builder(LoginActivity.this);


                    f.hideDialog();
                    f.showDialogPro(is_fr?"Aucune connexion internet détectée":"No internet connection detected");
                }


            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
        if(resultCode== Activity.RESULT_OK && requestCode==REQUEST_CODE_GOOGLE_PLUS)
        {
            GoogleSignInResult result=Auth.GoogleSignInApi.getSignInResultFromIntent(data);
           handleResult(result);
        }
        else if (requestCode == APP_REQUEST_CODE) { // confirm that this response matches your request
            com.facebook.accountkit.AccessToken accessToken = AccountKit.getCurrentAccessToken();

            if (accessToken != null) {
                //Handle Returning User
                getData();
            }
        }

    }
    private void checkToken(){
        com.facebook.accountkit.AccessToken accessToken = AccountKit.getCurrentAccessToken();

        if (accessToken != null) {
            //Handle Returning User
            getData();
        } else {
            //Handle new or logged out user
            startConnexion();
        }
    }
    private void startConnexion(){
        final Intent intent = new Intent(this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);
    }

    private void getData(){

        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                // Get Account Kit ID
                String accountKitId = account.getId();

                // Get phone number
                PhoneNumber phoneNumber = account.getPhoneNumber();
                if (phoneNumber != null) {
                    String phoneNumberString = phoneNumber.toString();
                    String countryCodeValue=phoneNumber.getCountryCode();
                   String coV ="+ "+countryCode;
                    if(phoneNumberString.length()>countryCodeValue.length()+1){
                        String number=phoneNumberString.substring(countryCodeValue.length()+1);
                        if((""+Long.MAX_VALUE).length()>number.length()){
                            String pattern ="#,##";
                            DecimalFormatSymbols formatSymbols=new DecimalFormatSymbols();
                                  formatSymbols.setGroupingSeparator(' ');
                            DecimalFormat format=new DecimalFormat(pattern);
                            format.setDecimalFormatSymbols(formatSymbols);
                            String formatnumber=format.format(Long.valueOf(number));
                            number=formatnumber;
                        }
                        email=number;
                        for(int i=0;i<countryCode.length;i++){
                            String name=countryCode[i];
                            if(name.equalsIgnoreCase(coV)){
                               if(i>-1 && i<mpays.length){
                                   pays=mpays[i];
                               }
                               else{
                                   pays=mpays[2];
                               }
                                break;
                            }
                        }

                        loginUser();
                    }
                }


            }

            @Override
            public void onError(final AccountKitError error) {
                // Handle Error
            }
        });

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(profileTracker!=null){
            profileTracker.stopTracking();
        }

    }
    private void setUpAboonement(){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        MyInterface2 service=retrofit.create(MyInterface2.class);

        Call<String> call=service.getabonnement();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {
                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {

                            } else {
                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {


                                } else {
                                    SessionManager manager=new SessionManager(LoginActivity.this);
                                    JSONArray produits=o.getJSONArray("produits");
                                    JSONObject  o1=produits.getJSONObject(0);
                                    String s1=o1.getString("url");
                                    manager.putLinkCondition(s1);
                                    JSONObject  o2=produits.getJSONObject(0);
                                    String s2=o2.getString("url");
                                    manager.putLinkApropos(s2);




                                }

                            }

                        } catch (Exception e) {

                        }

                    }//fin is success,
                    else {

                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

            }
        });

    }

    public void doRequestDate()
    {

        String url= Config.URL_CURRENT_DATE;


        JsonObjectRequest request=new JsonObjectRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(response!=null)
                {
                    try {
                        boolean error = response.getBoolean("error");
                        if(error)
                        {
                        }
                        else
                        {
                            String date=response.getString("c_date");
                            try{
                                SimpleDateFormat sdfa=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                sdfa.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
                                Log.e("Date du serveur",""+date);
                                Date date2=sdfa.parse(date);
                                sdfa.setTimeZone(TimeZone.getDefault());
                                date=sdfa.format(date2);
                                date2=sdfa.parse(date);
                                Log.e("Date Locale",""+date);
                                MyDateServeur.getInstance().initDate(new Date(date2.getTime()),getApplicationContext());
                                MyDateServeur.getInstance().setCanReque(true,LoginActivity.this);
                            }
                            catch (ParseException e){

                            }
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
                else
                {
                }


            }//fin onResponse
        },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });


        MyApplication.getInstance().addToRequestQueue(request);
    }
}
