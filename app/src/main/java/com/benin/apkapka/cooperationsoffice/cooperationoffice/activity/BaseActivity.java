package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.BuildConfig;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.ExpandableDrawerAdapter;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.BottomProfileVitrine;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetCurrentDate;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetUpdateStatistique;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetworkSearchOnHome;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Conf;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ExpandableModele;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.HelperActivity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.LanguageNotifier;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ListDataHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyDateServeur;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyTrackDateIntenteService;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.PersistentSeachView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.lapism.searchview.SearchAdapter;
import com.lapism.searchview.SearchItem;
import com.lapism.searchview.SearchView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class BaseActivity extends BaseLanguageActivity implements FragmentForNetworkSearchOnHome.HandlerNetWorkRequestResponseHomeSearch,FragmentForNetCurrentDate.HandlerNetWorkRequestResponseDate,LanguageNotifier.LanguageNotifierInterface {

    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ExpandableListView expandableListView;
    ExpandableDrawerAdapter drawerAdapter;
    List<ExpandableModele> listHeader=new ArrayList<>();
    HashMap<ExpandableModele,List<ExpandableModele>> listData=new HashMap<>();
    NavigationView navigationView;
    boolean is_small,is_medium,is_large,is_landescape;
    private static boolean activityStarted;
    private  boolean is_fr;
    FragmentManager fm;
    SearchAdapter adapter;
    List<SearchItem> searchItems=new ArrayList<>();
    PersistentSeachView searchView;
    List<Produit> listSearchProdduit=new ArrayList<>();
    MenuItem menuItem;
    FragmentForNetworkSearchOnHome forNetworkSearchOnHome;
    List<String> listSearch=new ArrayList<>();
    FragmentForNetCurrentDate fNetDate;

    String query="";
    AlertDialog alertDialog;

    //Location variable
    FusedLocationProviderClient mFusedLocationProviderClient;
    SettingsClient mSettingsClient;
    LocationRequest mLocationRequest;
    LocationSettingsRequest mLocationSettingsRequest;
    LocationCallback mLocationCallback;
    Location mLocation;
    private static final int LOCATION_UPDATE_INTERVAL=10000;
    private static  final int LOCATION_FAST_UPDATE_INTERVAL=60*1000;
    private  static  final int REQUEST_CHECK_SETTINGS=10;
    //Location variable
    AlertDialog searchalertDialog=null;
    private SharedPreferences pref;
    private SharedPreferences.Editor edit;
    private static  final  String SHARE_NAME="main_activity_share_name";
    BottomNavigationView bottomNavigationView;
    FrameLayout baseView;
    boolean isreverse=false;
    HomeFragment homeFragment;
    HomeFragmentReverse homeFragmentReverse;
    TextView tvCountUser;
    String search;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
 private static  final  String BACK_STACK_NAME="co_fragment_reverse_show_backstackname";
      BottomNavigationView.OnNavigationItemSelectedListener mNavigationItemSelectedListener=new BottomNavigationView.OnNavigationItemSelectedListener() {
          @Override
          public boolean onNavigationItemSelected(@NonNull MenuItem item) {
              switch (item.getItemId()){
                  case R.id.bootom_home:

                      if(isreverse){
                          item.setTitle(R.string.bootom_navigation_home);
                          item.setIcon(R.drawable.home_reverse_start);

                          if(homeFragmentReverse==null){
                              homeFragmentReverse=new HomeFragmentReverse();
                          }
                          showFragment2(homeFragmentReverse,isreverse);
                          isreverse=false;
                      }
                      else{
                          item.setTitle(R.string.bootom_navigation_home_reverse);
                          item.setIcon(R.drawable.home_reverse_end);
                          if(homeFragment==null){
                              homeFragment=new HomeFragment();
                          }
                          showFragment(homeFragment,isreverse);
                          isreverse=true;
                      }
                      return true;
                  case R.id.bootom_chat:
                      Intent intent=new Intent(BaseActivity.this,BottomChatConversation_Activity.class);
                      startActivity(intent);
                      return true;
                  case R.id.bootom_entertement:
                      Intent intent2=new Intent(BaseActivity.this,BottomDivertissementActivity.class);
                      startActivity(intent2);
                      return true;
              }
              return false;
          }
      };
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.primary_dark));
        }
        if (   activityStarted
                && getIntent() != null
                && (getIntent().getFlags() & Intent.FLAG_ACTIVITY_REORDER_TO_FRONT) != 0) {
            finish();
            return;
        }

        activityStarted = true;
        setContentView(R.layout.base_layout);
        pref=getSharedPreferences(SHARE_NAME,MODE_PRIVATE);
        edit=pref.edit();
        final boolean start=pref.getBoolean("start",false);
        SessionManager sessionManager=new SessionManager(this);
        initLocation();


        //Permet de savoir si un utilisateur a  reçu la demande de localisation la premiere fois
        //fois depuis l'activité de bienvenu ou depuis l'activité Baseactivité.Cette logique est mit en place pour distinguer ceuyx qui avaient deja installé l'application
        //avant la mise à jour et les nouveaux utilisateurs.Tout sa pour maintenir une bonne expérience utilisateur et ne pas
        //pertubé l'utilisateur

        //pour les preference de menu (actualite, alliance, projet, ....etc)
        preferences=getSharedPreferences("les_options",MODE_PRIVATE);
        editor=preferences.edit();

       if(sessionManager.isLoggedIn()){

           String jso=preferences.getString("MES_MENUS",null);
           if(jso == null) {
               Intent intent=new Intent(this,SettingMenuActivity.class);
               startActivity(intent);
               finish();
               return;
           }else{
               if(sessionManager.getCanRequestLocation()){
                   //L'utilsateur avait déja l'application et a fait une mise à jour
                   User user=sessionManager.getUser(this);
                   if(user.getId()!=0){
                       GetUserData(user.getId());
                   }
                   if(sessionManager.getCanRequestLocationAtHome()){
                       checkPermission();
                       sessionManager.putCanRequestLocationAtHome(false);
                   }
                   else{
                       if(sessionManager.getCanRequestLocationUpdate()){
                           checkPermission();
                       }

                   }

               }
               else{

              /* User user=sessionManager.getUser(this);
               String longi=user.getLongitude();
               String lati=user.getLatitude();

               if(longi==null || longi.trim().equalsIgnoreCase("") || longi.equalsIgnoreCase("null") || lati==null || lati.trim().equalsIgnoreCase("") || lati.equalsIgnoreCase("null")){
                   Intent intent=new Intent(this,WelcomActivity.class);
                   startActivity(intent);
                   finish();
                   return;
               }
               */
               }
           }


        }
        else{
            Intent intent=new Intent(this,LoginActivity.class);
            startActivity(intent);
            finish();
            return;
        }


        is_fr=getResources().getBoolean(R.bool.lang_fr);
        LanguageNotifier.getInstance().setNotifierInterface(this);
        setUpToolBar();
        setUpExpandable();
        setUpSeachView();
        setUpAboonement();
        bottomNavigationView=(BottomNavigationView)findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(mNavigationItemSelectedListener);

        if(isreverse){
            if(homeFragmentReverse==null){
                homeFragmentReverse=new HomeFragmentReverse();
            }
            showFragment2(homeFragmentReverse,isreverse);
            isreverse=false;
        }
        else{
            if(homeFragment==null){
                homeFragment=new HomeFragment();
            }
            showFragment(homeFragment,isreverse);
            isreverse=true;
        }
        //getValueFromBundle(savedInstanceState);
        if(savedInstanceState!=null){
             isreverse=savedInstanceState.getBoolean("is_reverse_home",false);
            if(!isreverse){
                if(homeFragmentReverse==null){
                    homeFragmentReverse=new HomeFragmentReverse();
                }
                showFragment2(homeFragmentReverse,isreverse);

            }

        }
        NotificationManager notificationManager=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        if(notificationManager!=null){
            notificationManager.cancel(1);
        }
        is_small=getResources().getBoolean(R.bool.is_small);
        is_medium=getResources().getBoolean(R.bool.is_medium);
        is_large=getResources().getBoolean(R.bool.is_large);
        is_landescape=getResources().getBoolean(R.bool.is_landscape);

        if(!MyDateServeur.getInstance().canRequest(this)){
            startService(new Intent(this, MyTrackDateIntenteService.class));
        }
        fm=getSupportFragmentManager();
        FragmentForNetUpdateStatistique netWorkupdate=(FragmentForNetUpdateStatistique) fm.findFragmentByTag(FragmentForNetUpdateStatistique.TAG);
        if(netWorkupdate==null){
            netWorkupdate=new FragmentForNetUpdateStatistique();
            fm.beginTransaction().add(netWorkupdate,FragmentForNetUpdateStatistique.TAG).commit();
            netWorkupdate.doRequestUpdateStatistique(18);
            sessionManager=new SessionManager(this);
            User user=sessionManager.getUser(this);

            if(sessionManager!=null){
                if(user!=null){
                    netWorkupdate.addSession(user.getId());
                }
            }
        }
        Date date1= MyDateServeur.getInstance().getCurrentDate(this);
        Calendar c=Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MINUTE,1);
        Date date3=c.getTime();
        AlertDialog dialog;
        if(date3.compareTo(date1)<0){
            AlertDialog.Builder builder=new AlertDialog.Builder(this);
            builder.setMessage(is_fr?"Définissez l'heure sur votre téléphone.":"Set the time on your phone.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            BaseActivity.this.finish();
                        }
                    });
            dialog=builder.show();
        }

        getVersionInfo();
         if(savedInstanceState==null){
           User user=sessionManager.getUser(this);
            if(user!=null){
             long id=user.getId();
             if(id!=0){
                 checkUserAsAds(id);
             }
            }
         }


    }//fin Oncreate

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_reverse_home",isreverse);
    }
    private  void getValueFromBundle(Bundle bundle){
         if(bundle!=null){
             if(bundle.containsKey("is_reverse_home")){
                 isreverse=bundle.getBoolean("is_reverse_home",false);
             }
         }
    }

    private void initLocation(){
        mFusedLocationProviderClient= LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient=LocationServices.getSettingsClient(this);
        mLocationCallback=new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if(locationResult!=null){

                    mLocation=locationResult.getLastLocation();
                    SessionManager sessionManager=new SessionManager(BaseActivity.this);
                    User user=sessionManager.getUser(BaseActivity.this);
                    sessionManager.putUserLongitude(""+mLocation.getLongitude());
                    sessionManager.putUserLatitude(""+mLocation.getLatitude());
                    user.setLongitude(""+mLocation.getLongitude());
                    user.setLatitude(""+mLocation.getLatitude());
                    sessionManager.setUser(BaseActivity.this,user);
                    sessionManager.putCanRequestLocationUpdate(false);
                    UpdateUserLocation(user.getId(),""+mLocation.getLatitude(),""+mLocation.getLongitude());
                }
            }
        };

        mLocationRequest=LocationRequest.create();
        mLocationRequest.setInterval(LOCATION_UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(LOCATION_FAST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder=new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest=builder.build();

        User user=(new SessionManager(this)).getUser(this);
        if(user!=null){
            Log.e("User id"," "+user.getId());
        }
    }
    @SuppressWarnings({"MissingPermission"})
    private void requestLocationUpdate(){
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,mLocationCallback,null);
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if(e instanceof ResolvableApiException){
                            try{
                                ResolvableApiException apiException=(ResolvableApiException)e;
                                apiException.startResolutionForResult(BaseActivity.this,
                                        REQUEST_CHECK_SETTINGS);
                            }
                            catch (IntentSender.SendIntentException ex){

                            }
                        }//fin if
                    }
                });
    }
    private void checkPermission(){
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        requestLocationUpdate();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if(response.isPermanentlyDenied()){
                            AlertDialog.Builder  builder=new AlertDialog.Builder(BaseActivity.this);
                            builder.setMessage(is_fr?"La permission a été refusée, elle est nécessaire pour le bon fonctionnement de l'application":"Permission was refused, it is necessary for the proper functioning of the application");
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if(alertDialog!=null){
                                        alertDialog.dismiss();

                                    }
                                    // Build intent that displays the App settings screen.

                                }
                            });
                            alertDialog=builder.show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                    }
                }).check();

    }
    public void UpdateUserLocation(long id,String latitude,String longitude){


        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.updateUserLocation(""+id,longitude,latitude);

        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {

                            } else {


                            }

                        } catch (Exception e) {

                        }

                    }//fin is success,
                    else {


                    }//fin else is success

                }
                else {

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

            }

        });
    }
    private void showFragment(HomeFragment fragment,boolean isreverse) {
        FragmentManager fragmentManager = getSupportFragmentManager();
       // findViewById(R.id.baseview).setVisibility(View.VISIBLE);
        //findViewById(R.id.baseview2).setVisibility(View.GONE);

        boolean reverseFragmentPoped=fragmentManager.popBackStackImmediate(BACK_STACK_NAME,0);
         if(!reverseFragmentPoped){
             fragmentManager.beginTransaction()
                     .replace(R.id.baseview, fragment)
                     .commit();
         }
    }
    private void showFragment2(HomeFragmentReverse fragment,boolean isreverse) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        //findViewById(R.id.baseview).setVisibility(View.GONE);
        //findViewById(R.id.baseview2).setVisibility(View.VISIBLE);
        fragmentManager.beginTransaction()
                .replace(R.id.baseview, fragment)
                .addToBackStack(BACK_STACK_NAME)
                .commit();
    }







    private void getVersionInfo() {

        // Modifier Versioncode Ici et dans le session manager
        String versionName = "";
        int versionCode =9;
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

            versionCode = packageInfo.versionCode;
            versionName=""+versionCode;
            SessionManager sessionManager=new SessionManager(this);
            String code=sessionManager.getLinkCooperationVersion();
            AlertDialog dialog;
            if(isInt(code)){
                int codeversion=Integer.parseInt(code);
                if(versionCode<codeversion){
                   // Log.e("INfo ","VERSION SUR APP"+versionCode);
                    //Log.e("INFO","VERSION SERVEUR"+codeversion);
                    AlertDialog.Builder builder=new AlertDialog.Builder(this);
                    builder.setMessage(is_fr?"Nouvelle version disponible."+"METTRE A JOUR".toUpperCase()+" Cooperations office"+" MAINTENANT".toUpperCase():"New version available. "+" UPDATE ".toUpperCase () +" Cooperations office "+" NOW")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if(dialog!=null){
                                        dialog.dismiss();
                                    }
                                    String playStoreMarketUrl = "market://details?id=";
                                    String playStoreWebUrl = "https://play.google.com/store/apps/details?id=";
                                    String packageName = "com.benin.apkapka.cooperationsoffice.cooperationoffice";
                                    try {
                                        Intent intent = getPackageManager().getLaunchIntentForPackage("com.android.vending");
                                        if (intent != null) {
                                            intent = new Intent(Intent.ACTION_VIEW);
                                            intent.setData(Uri.parse(playStoreMarketUrl+packageName));
                                        } else {
                                            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(playStoreMarketUrl+packageName));
                                        }
                                        startActivity(intent);
                                    } catch (ActivityNotFoundException e) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(playStoreWebUrl + packageName));
                                        startActivity(intent);
                                    }
                                }
                            });
                    dialog=builder.show();
                }

            }



        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


    }
    private void setUpSeachView(){
        searchView=(PersistentSeachView) findViewById(R.id.search_view);
        searchView.setHint(getResources().getString(R.string.main_menu_search));
        searchView.setVersionMargins(SearchView.VERSION_MARGINS_MENU_ITEM);
        searchView.setVersion(SearchView.VERSION_MENU_ITEM);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                if(!newText.equals("")){
                    SessionManager sessionManager=new SessionManager(getApplicationContext());
                    User user=sessionManager.getUser(getApplicationContext());
                    String zone="";
                    zone=user.getPays();
                    if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
                        zone="france";
                    }
                    search=newText;
                    forNetworkSearchOnHome.doRequestProduitSearch(newText,is_fr?"fr":"en",zone);
                }


                return false;
            }

            @Override
            public boolean onQueryTextSubmit( final String query1) {
                searchView.close(true);
                Intent intent=new Intent(BaseActivity.this,SearchActivity.class);
                intent.putExtra("query",query1);

                intent.putExtra("type","home");
                startActivity(intent);
                return false;
            }
        });

        searchItems=new ArrayList<>();
        adapter=new SearchAdapter(this,searchItems);
        adapter.addOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if(searchItems!=null){
                    query=searchItems.get(position).get_text().toString();
                    Log.e("Le texte selectionne",""+query);
                    Predicate<Produit> predicate6=new Predicate<Produit>() {
                        @Override
                        public boolean apply(@javax.annotation.Nullable Produit input) {
                            if(is_fr){
                                return input.getTitle().contains(query )||  input.getFamille().contains(query) || input.getExtend_proprety2().contains(query);
                            }
                            else{
                                return  input.getTitle_en().equalsIgnoreCase(query);
                            }

                        }
                    };
                    List<Produit> types6= Lists.newArrayList(Iterables.filter(listSearchProdduit,predicate6));
                    if(types6!=null && types6.size()>0){
                        Produit f=types6.get(0);
                        if(f!=null )
                        {
                            HelperActivity.getInstance().setProduit(f);
                            Intent intent=new Intent();
                            Produit   p=f;
                            String type=p.getType();
                            if(type.equals("actualite"))
                            {
                                intent=new Intent(BaseActivity.this,Une_actualite_activity.class);
                            }
                            else if(type.equals("boutique")){
                                intent =new Intent(BaseActivity.this,Un_produit_activity.class);
                            }
                            else if(type.equals("emploi")){
                                intent =new Intent(BaseActivity.this,Un_emploi_activity.class);
                            }
                            else if(type.equals("competence")){
                                intent =new Intent(BaseActivity.this,Une_competence_annuaire_activity.class);
                            }
                            else if(type.equals("bourse")){
                                intent =new Intent(BaseActivity.this,Une_bourse_activity.class);
                            }
                            else if(type.equals("annuaire")){

                                intent =new Intent(BaseActivity.this,UnAnnuaire.class);
                            }
                            else if(type.equals("service")){
                                intent =new Intent(BaseActivity.this,Un_service_activity.class);
                            }
                            else if(type.equals("location")){
                                intent =new Intent(BaseActivity.this,Une_location_activity.class);
                            }
                            else if(type.equals("partenariat")){
                                intent =new Intent(BaseActivity.this,Un_partenariat.class);
                            }
                            else if(type.equals("projet")){
                                intent =new Intent(BaseActivity.this,Un_projet_activity.class);
                            }
                            intent.putExtra("id",p.getId());
                            if(is_fr==true)
                            {
                                intent.putExtra("title",p.getTitle());
                                intent.putExtra("description",p.getDescription());
                                intent.putExtra("price",p.getPrice());
                                intent.putExtra("nb_price",p.getNb_price().toString());
                                intent.putExtra("nb_price_en",p.getNb_price_en().toString());
                                intent.putExtra("text_reduction",p.getText_reduction());
                                intent.putExtra("reduction",p.getReduction());
                                intent.putExtra("extend_proprety1",p.getExtend_proprety1());
                                intent.putExtra("extend_proprety2",p.getExtend_proprety2());
                                intent.putExtra("nb_share",p.getNb_share());
                                intent.putExtra("nb_read_catalogue",p.getNb_read_catalogue());
                            }
                            else
                            {
                                intent.putExtra("title",p.getTitle_en());
                                intent.putExtra("description",p.getDescription_en());
                                intent.putExtra("price",p.getPrice_en());
                                intent.putExtra("nb_price",p.getNb_price().toString());
                                intent.putExtra("nb_price_en",p.getNb_price_en().toString());
                                intent.putExtra("text_reduction",p.getText_reduction_en());
                                intent.putExtra("reduction",p.getReduction());
                                intent.putExtra("extend_proprety1",p.getExtend_proprety1_en());
                                intent.putExtra("extend_proprety2",p.getExtend_proprety2_en());
                                intent.putExtra("nb_share",p.getNb_share());
                                intent.putExtra("nb_read_catalogue",p.getNb_read_catalogue());

                            }
                            intent.putExtra("lien_catalogue",p.getLien_catalogue());
                            intent.putExtra("lien_description",p.getLien_description());
                            intent.putExtra("choixf",p.getChoixf());
                            intent.putExtra("url",p.getUrl());
                            intent.putExtra("url2",p.getUrl2());
                            intent.putExtra("url3",p.getUrl3());
                            intent.putExtra("type",p.getType());
                            intent.putExtra("famille",p.getFamille());
                            intent.putExtra("pays",p.getPays());
                            intent.putExtra("category",p.getCategory());
                            intent.putExtra("type2",0);
                            intent.putExtra("from_co_direct","oui");
                            intent.putExtra("date",p.getDate());
                            intent.putExtra("famille_en",p.getFamille_en());
                            intent.putExtra("lien_fichier",p.getLien_fichier());
                            startActivity(intent);

                        }
                        else{
                            Intent intent=new Intent(BaseActivity.this,SearchActivity.class);
                            intent.putExtra("search",query);
                            startActivity(intent);
                        }

                    }
                    else {
                        Intent intent=new Intent(BaseActivity.this,SearchActivity.class);
                        intent.putExtra("search",query);
                        //startActivity(intent);
                    }
                }
                searchView.close(true);
            }
        });

        searchView.setVoice(true);
        searchView.setOnVoiceClickListener(new SearchView.OnVoiceClickListener() {
            @Override
            public void onVoiceClick() {
                requestRecordePermission();
            }
        });

        searchView.setAdapter(adapter);
        //   searchView.setNavigationIconAnimation(false);

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            setUpChannel();
        }



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==SearchView.SPEECH_REQUEST_CODE && resultCode==RESULT_OK){
            List<String> results=data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if(results!=null && results.size()>0){
                String query=results.get(0);
                if(!TextUtils.isEmpty(query)){
                    searchView.setTextOnly(query);
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private boolean isInt(String s){
        try {
            int v=Integer.parseInt(s);
            return  true;
        }
        catch (NumberFormatException e){

        }

        return false;
    }

    private void setUpToolBar()
    {
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.mipmap.ic_reorder_white);
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    private void setUpExpandable(){
        fm=getSupportFragmentManager();
        forNetworkSearchOnHome=(FragmentForNetworkSearchOnHome) fm.findFragmentByTag(FragmentForNetworkSearchOnHome.TAG);
        if(forNetworkSearchOnHome==null){
            forNetworkSearchOnHome=new FragmentForNetworkSearchOnHome();
            fm.beginTransaction().add(forNetworkSearchOnHome,FragmentForNetworkSearchOnHome.TAG).commit();
            forNetworkSearchOnHome.setInterface(this);
        }
        else{
            forNetworkSearchOnHome.setInterface(this);
        }

        drawerLayout=(DrawerLayout)findViewById(R.id.drawerLayout);
        navigationView=(NavigationView)findViewById(R.id.navigationview);
       tvCountUser=(TextView)findViewById(R.id.header_count_actif_user);
       SessionManager sessionManager=new SessionManager(this);
       getCountActif();
       String count=sessionManager.getActifuser();
       tvCountUser.setText(is_fr?""+count+" Utilisateurs ":""+count+" users");

        expandableListView=(ExpandableListView)findViewById(R.id.expandableListView);
        preparedate();
        drawerAdapter=new ExpandableDrawerAdapter(this,listHeader,listData);
        expandableListView.setAdapter(drawerAdapter);

        final SessionManager sessionManager2=new SessionManager(this);
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
               /* if(i==0){
                    if(i1==0){
                        Intent intent=new Intent(BaseActivity.this,DrawerLanguageActivity.class);
                        startActivity(intent);
                        drawerLayout.closeDrawers();
                    }

                }
                */
                 if(i==0){
                    if(i1==0){
                        String link=sessionManager2.getLinkConddition();
                        drawerLayout.closeDrawers();
                        Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse((link.trim().equals("")||link==null)?"https://www.facebook.com/Cooperations-Office-679660405393562/":link));

                        if(intent.resolveActivity(getPackageManager())!=null)
                        {
                            startActivity(intent);
                        }


                    }
                    else if(i1==1){
                        String link=sessionManager2.getLinkApropos();
                        drawerLayout.closeDrawers();
                        Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse((link.trim().equals("")||link==null)?"https://www.facebook.com/Cooperations-Office-679660405393562/":link));

                        if(intent.resolveActivity(getPackageManager())!=null)
                        {
                            startActivity(intent);
                        }

                    }
                }
                else if(i==1){
                    if(i1==0){
                        drawerLayout.closeDrawers();
                        Intent intent=new Intent(BaseActivity.this,DrawerHelpActivity.class);
                        startActivity(intent);

                    }

                }
                else if(i==2){
                    SessionManager sessionManager=new SessionManager(BaseActivity.this);
                    if(i1==0){
                        drawerLayout.closeDrawers();
                        String link=sessionManager.getLinkFacebook();
                        Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse((link==null || link.trim().equalsIgnoreCase(""))?"\"https://www.facebook.com/Cooperations-Office-679660405393562/\"":link));

                        if(intent.resolveActivity(getPackageManager())!=null)
                        {
                            startActivity(intent);
                        }
                    }
                    else if(i1==1){
                        String link3=sessionManager.getLinkYoutube();
                        drawerLayout.closeDrawers();
                        Intent intent3=new Intent(Intent.ACTION_VIEW,Uri.parse((link3==null || link3.trim().equalsIgnoreCase(""))?"\"https://www.youtube.com/channel/UCN3UQuqt7tnwA81TgAOoI5A\"":link3));

                        if(intent3.resolveActivity(getPackageManager())!=null)
                        {
                            startActivity(intent3);
                        }
                    }
                    else if (i1==2){
                        String link2=sessionManager.getLinkCooperation();
                        drawerLayout.closeDrawers();
                        Intent intent1=new Intent(Intent.ACTION_VIEW,Uri.parse((link2==null || link2.trim().equalsIgnoreCase(""))?"\"http://cooperationsoffice.com\"":link2));

                        if(intent1.resolveActivity(getPackageManager())!=null)
                        {
                            startActivity(intent1);
                        }
                    }
                }
                else if(i==3){
                    if(i1==0){
                        drawerLayout.closeDrawers();
                        Intent intent2=new Intent(Intent.ACTION_SEND);
                        intent2.setType("text/plain");
                        String url= "https://play.google.com/store/apps/details?id=com.benin.apkapka.cooperationsoffice.cooperationoffice";
                        intent2.putExtra(Intent.EXTRA_TEXT, url);
                        intent2.putExtra(Intent.EXTRA_SUBJECT, "Cooperations office");
                        if(intent2.resolveActivity(getPackageManager())!=null)
                        {

                            String[] blacklist = new String[]{"com.google.android.gm","com.facebook.katana" };
                            startActivity(generateCustomChooserIntent(intent2, blacklist));
                        }
                        else
                        {
                            MyToast.show(BaseActivity.this, getResources().getString(R.string.une_activity_installer_app_partage), false);
                        }
                    }
                }else if (i==4){
                    if (i1==0){
                        drawerLayout.closeDrawers();
                        Intent intent=new Intent(BaseActivity.this,SettingMenuActivity.class);
                        intent.putExtra("setting","setting");
                        startActivity(intent);
                        finish();
                    }
                 }

                return false;
            }
        });

    }
    private  void preparedate(){


        //listHeader.add(new ExpandableModele(R.drawable.expandable_setting,is_fr?"PARAMETRES":"SETTINGS"));
        listHeader.add(new ExpandableModele(R.drawable.expandable_subscribe,is_fr?"Abonnement ":"SUBSCRIPTION"));
        listHeader.add(new ExpandableModele(R.drawable.expandable_opnion,is_fr?"Votre avis":"YOUR OPINION"));
        listHeader.add(new ExpandableModele(R.drawable.expandable_follow,is_fr?"Nous suivre sur":"Follow us on"));
        listHeader.add(new ExpandableModele(R.drawable.expandable_share,is_fr?"Partager Cooperations office":"Share Cooperations office"));
        listHeader.add(new ExpandableModele(R.drawable.expandable_setting,is_fr?"Mes Préférences":"Your Preferences"));


        List<ExpandableModele> list6 = new ArrayList<ExpandableModele>();
        list6.add(new ExpandableModele(R.drawable.expandable_setting,is_fr?"Mes Préférences":"Your Preferences"));
//        list1.add(new ExpandableModele(R.drawable.expandable_language,is_fr?"Langue":"Language"));
//       list1.add(new ExpandableModele(R.drawable.expandable_account,is_fr?"Ajouter un compte":"Add an account"));



        List<ExpandableModele> list2 = new ArrayList<ExpandableModele>();
        list2.add(new ExpandableModele(R.drawable.expandable_condition,"Condition"));
        list2.add(new ExpandableModele(R.drawable.expandable_about,is_fr?"A propos":"About"));

        List<ExpandableModele> list3 = new ArrayList<ExpandableModele>();
        list3.add(new ExpandableModele(R.drawable.expandable_statistic,is_fr?"Aidez-nous à ameliorer \n votre application cooperations office":"Help us improve\n" +
                "  your application cooperations office"));
        List<ExpandableModele> list4=new ArrayList<>();
        list4.add(new ExpandableModele(R.drawable.home_facebook,"FaceBook"));
        list4.add(new ExpandableModele(R.drawable.home_youtube,"YouTube"));
        list4.add(new ExpandableModele(R.drawable.home_cooperation,"Cooperations office"));
        List<ExpandableModele> list5=new ArrayList<>();
        list5.add(new ExpandableModele(R.drawable.expandable_share,is_fr?"Partager":"Share"));


        listData.put(listHeader.get(0),list2);
        listData.put(listHeader.get(1),list3);
        listData.put(listHeader.get(2),list4);
        listData.put(listHeader.get(3),list5);
        listData.put(listHeader.get(4),list6);
    }



    private Intent generateCustomChooserIntent(Intent prototype, String[] forbiddenChoices) {
        List<Intent> targetedShareIntents = new ArrayList<>();
        List<HashMap<String, String>> intentMetaInfo = new ArrayList<HashMap<String, String>>();
        Intent chooserIntent;

        Intent dummy = new Intent(prototype.getAction());
        dummy.setType(prototype.getType());
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(dummy, 0);

        if (!resInfo.isEmpty()) {
            for (ResolveInfo resolveInfo : resInfo) {
                if (resolveInfo.activityInfo == null || Arrays.asList(forbiddenChoices).contains(resolveInfo.activityInfo.packageName))
                    continue;

                HashMap<String, String> info = new HashMap<String, String>();
                info.put("packageName", resolveInfo.activityInfo.packageName);
                info.put("className", resolveInfo.activityInfo.name);
                info.put("simpleName", String.valueOf(resolveInfo.activityInfo.loadLabel(getPackageManager())));
                intentMetaInfo.add(info);
            }

            if (!intentMetaInfo.isEmpty()) {
                // sorting for nice readability
                Collections.sort(intentMetaInfo, new Comparator<HashMap<String, String>>() {
                    @Override
                    public int compare(HashMap<String, String> map, HashMap<String, String> map2) {
                        return map.get("simpleName").compareTo(map2.get("simpleName"));
                    }
                });

                // create the custom intent list
                for (HashMap<String, String> metaInfo : intentMetaInfo) {
                    Intent targetedShareIntent = (Intent) prototype.clone();
                    targetedShareIntent.setPackage(metaInfo.get("packageName"));
                    targetedShareIntent.setClassName(metaInfo.get("packageName"), metaInfo.get("className"));
                    targetedShareIntents.add(targetedShareIntent);
                }

                chooserIntent = Intent.createChooser(targetedShareIntents.remove(targetedShareIntents.size() - 1), getResources().getString(R.string.une_activity_partager_via));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
                return chooserIntent;
            }
        }

        return Intent.createChooser(prototype, getResources().getString(R.string.une_activity_partager_via));
    }
    @Override
    public void languageChange(Resources resources, String langague) {


        getSupportActionBar().setTitle(resources.getString(R.string.app_name));
        listData.clear();
        listHeader.clear();
        is_fr=resources.getBoolean(R.bool.lang_fr);
        listHeader.clear();
        listData.clear();
        preparedate();
        getSupportActionBar().setTitle(resources.getString(R.string.app_name));


        drawerAdapter.notifyDataSetChanged();
        searchView.setHint(resources.getString(R.string.main_menu_search));
        searchView.setHint(resources.getString(R.string.main_menu_search));



    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onresponseFailAndPrintErrorResponseDate() {

    }
    @Override
    public void onResposeSuccessDate(String date) {
        try{
            SimpleDateFormat sdfa=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdfa.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
            Date date2=sdfa.parse(date);
            sdfa.setTimeZone(TimeZone.getDefault());
            date=sdfa.format(date2);
            date2=sdfa.parse(date);

            ListDataHandler.saveDate(this,date2.getTime());
            long time=ListDataHandler.getDate(this);
            Date date1=new Date(time);
            Calendar c=Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.MINUTE,30);
            Date date3=c.getTime();
            AlertDialog dialog;
            if(date3.compareTo(date1)<0){
                AlertDialog.Builder builder=new AlertDialog.Builder(this);
                builder.setMessage(is_fr?"Définissez l'heure sur votre téléphone.":"Set the time on your phone.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                BaseActivity.this.finish();
                            }
                        });
                dialog=builder.show();
            }
        }
        catch (ParseException e){

        }
    }
    @Override
    public void onresponseSearchFailAndPrintErrorResponse() {

    }

    @Override
    public void onResposeSuccessSearch(String w) {

       if(w.equalsIgnoreCase(search)){
           List<String> list=forNetworkSearchOnHome.getListTitle();
           listSearchProdduit=forNetworkSearchOnHome.getListproduit();
           if(list!=null && list.size()>0){

               searchItems.clear();
               for (String s:list){
                   searchItems.add(new SearchItem(s));
               }

               adapter.notifyDataSetChanged();
               searchView.showSuggestions();



           }
       }
       else {
           searchItems.clear();
           adapter.notifyDataSetChanged();
           searchView.showSuggestions();
       }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setUpChannel(){

        NotificationManager notificationManager=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        String channel_id=getResources().getString(R.string.default_notification_channel_id);
        String channel_name=getResources().getResourceName(R.string.default_notification_channel_name);
        String channel_description=getResources().getString(R.string.notification_channel_description);

        NotificationChannel channel;
        channel=new NotificationChannel(channel_id,channel_name, NotificationManager.IMPORTANCE_LOW);
        channel.setDescription(channel_description);
        channel.enableLights(true);
        channel.setSound(null,null);
        channel.setLightColor(getColor(R.color.primary));
        if(notificationManager!=null){
            notificationManager.createNotificationChannel(channel);
        }


    }
    private void requestRecordePermission(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.RECORD_AUDIO)){

                AlertDialog.Builder builder=new AlertDialog.Builder(this);
                builder.setTitle(is_fr?"Autorisation d'enregistrement":"Registration Authorization ");
                builder.setMessage(is_fr?"Vous devez activer la recherche vocale pour utiliser cette fonctionnalité ":"You must enable voice search to use this feature");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        searchalertDialog.dismiss();
                    }
                });
                builder.setNegativeButton(is_fr?"Annuler":"Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        searchalertDialog.dismiss();
                    }
                });
                searchalertDialog=builder.show();

            }
            else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.RECORD_AUDIO}, Conf.RECORD_REQUEST_CODE);

            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    public void onBackPressed() {

        if(drawerLayout.isDrawerOpen(navigationView))
        {
            drawerLayout.closeDrawers();

        }
        else
        {
            if (searchView.isSearchOpen()) {
                searchView.close(true);
                navigationView.setVisibility(View.VISIBLE);
            } else {
                if(isreverse ){

                    AlertDialog.Builder builder=new AlertDialog.Builder(this);

                    builder.setMessage(is_fr?"Voulez vous quitter l'application ":"Do you want to exit the application");
                    builder.setPositiveButton(is_fr?"Oui":"YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if(searchalertDialog!=null){
                                searchalertDialog.dismiss();
                            }
                            finish();
                        }
                    });
                    builder.setNegativeButton(is_fr?"NON":"NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if(searchalertDialog!=null){
                                searchalertDialog.dismiss();
                            }
                        }
                    });
                    searchalertDialog=builder.show();
                }
                else{

                    getSupportFragmentManager().popBackStackImmediate();
                    if(isreverse){
                        isreverse=false;
                    }
                    else{
                        isreverse=true;
                    }
                }
            }
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @SuppressWarnings("")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        SessionManager sessionManager=new SessionManager(this);
        switch (item.getItemId())
        {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.action_search:
                searchView.open(true);
                bottomNavigationView.setVisibility(View.GONE);
                return true;
            case R.id.home_user:
                Intent intent1=new Intent(this,BottomProfileActivity.class);
                startActivity(intent1);
                return  true;

        }

        return super.onOptionsItemSelected(item);
    }

   private void checkUserAsAds(long useer_id){
       Retrofit retrofit=new Retrofit.Builder()
               .baseUrl(Config.GET_BASE_URL_RETROFIT)
               .addConverterFactory(ScalarsConverterFactory.create())

               .build();

       MyInterface service=retrofit.create(MyInterface.class);

       Call<String> call=service.check_user_has_annonce(""+useer_id);

       call.enqueue(new retrofit2.Callback<String>() {
           @Override
           public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
               String message = "";
               if (response != null) {
                   if (response.isSuccessful()) {

                       try {
                           String rep = response.body();
                           JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                           boolean error = o.getBoolean("error");
                           if (error) {


                           } else {
                               AlertDialog dialog;
                               AlertDialog.Builder builder=new AlertDialog.Builder(BaseActivity.this);
                               builder.setMessage(R.string.home_annonce_tip)
                                       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                           @Override
                                           public void onClick(DialogInterface dialog, int which) {


                                               if(dialog!=null){
                                                   dialog.dismiss();
                                               }
                                               Intent intent1=new Intent(BaseActivity.this,BottomProfileActivity.class);
                                               intent1.putExtra("booste","booste");
                                               startActivity(intent1);
                                           }
                                       })
                                       .setNegativeButton(R.string.string_btn_cancel_annonce_tip, new DialogInterface.OnClickListener() {
                                           @Override
                                           public void onClick(DialogInterface dialog, int which) {
                                               if(dialog!=null){
                                                   dialog.dismiss();
                                               }
                                           }
                                       });
                               dialog=builder.show();
                           }

                       } catch (Exception e) {


                       }

                   }//fin is success,
                   else {

                   }//fin else is success

               }


           }

           @Override
           public void onFailure(Call<String> call1,Throwable t) {


           }

       });
   }
    private void setUpAboonement(){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        MyInterface2 service=retrofit.create(MyInterface2.class);

        Call<String> call=service.getabonnement();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {
                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {

                            } else {
                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {

                                } else {
                                    SessionManager manager=new SessionManager(BaseActivity.this);
                                    JSONArray produits=o.getJSONArray("produits");

                                    if(produits!=null && produits.length()>0){
                                        for (int i=0;i<produits.length();i++){
                                            JSONObject object=produits.getJSONObject(i);
                                            String id=object.getString("id");
                                            id=id.trim();
                                            if(id.equalsIgnoreCase("1")){
                                                String url=object.getString("url");
                                                manager.putLinkCondition(url);
                                            }
                                            else if(id.equalsIgnoreCase("2")){
                                                String url=object.getString("url");
                                                manager.putLinkApropos(url);
                                            }
                                            else if(id.equalsIgnoreCase("3")){
                                                String url=object.getString("url");
                                                manager.putLinkFacebook(url);
                                            }
                                            else if(id.equalsIgnoreCase("4")){
                                                String url=object.getString("url");
                                                manager.putLinkYoutube(url);
                                            }
                                            else if(id.equalsIgnoreCase("5")){
                                                String url=object.getString("url");
                                                manager.putLinkCooperation(url);
                                            }
                                            else if(id.equalsIgnoreCase("6")){
                                                String url=object.getString("url");
                                                manager.putLinkCooperationVersion(url);
                                            }

                                        }

                                    }



                                }

                            }

                        } catch (Exception e) {

                        }

                    }//fin is success,
                    else {

                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

            }
        });

    }


    private void GetUserData(long useer_id){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.get_target_user_data(""+useer_id);

        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {


                            } else {

                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {

                                } else {

                                    User user=ParseJson.parseUserFromHome(BaseActivity.this,o);
                                    SessionManager sessionManager=new SessionManager(BaseActivity.this);
                                    sessionManager.setUser(BaseActivity.this,user);
                                }
                            }

                        } catch (Exception e) {


                        }

                    }//fin is success,
                    else {

                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {


            }

        });
    }

    private void getCountActif(){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        MyInterface2 service=retrofit.create(MyInterface2.class);

        Call<String> call=service.getCountAtifUser();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {
                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {

                            } else {

                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {


                                } else {
                                    SessionManager manager=new SessionManager(BaseActivity.this);
                                    JSONArray produits=o.getJSONArray("users");

                                    if(produits!=null && produits.length()>0){
                                        JSONObject object=produits.getJSONObject(0);
                                        String count=object.getString("nb");
                                        manager.putActifUser(count);

                                        tvCountUser.setText(is_fr?""+count+" Utilisateurs":""+count+" Users");

                                    }



                                }

                            }

                        } catch (Exception e) {

                        }

                    }//fin is success,
                    else {

                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

            }
        });

    }
    public void doRequestDate()
    {

        String url= Config.URL_CURRENT_DATE;


        JsonObjectRequest request=new JsonObjectRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(response!=null)
                {
                    try {
                        boolean error = response.getBoolean("error");
                        if(error)
                        {
                        }
                        else
                        {
                            String date=response.getString("c_date");
                            try{
                                SimpleDateFormat sdfa=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                sdfa.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
                                Log.e("Date du serveur",""+date);
                                Date date2=sdfa.parse(date);
                                sdfa.setTimeZone(TimeZone.getDefault());
                                date=sdfa.format(date2);
                                date2=sdfa.parse(date);
                                Log.e("Date Locale",""+date);
                                MyDateServeur.getInstance().initDate(new Date(date2.getTime()),getApplicationContext());
                                MyDateServeur.getInstance().setCanReque(true,BaseActivity.this);
                            }
                            catch (ParseException e){

                            }
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
                else
                {
                }


            }//fin onResponse
        },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });


        MyApplication.getInstance().addToRequestQueue(request);
    }

}
