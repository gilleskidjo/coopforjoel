package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

public class MenuVitrine {
    private int id;
    private String label;
    private int img;
    private String type;
    private String notif;

    public String getNotif() {
        return notif;
    }

    public void setNotif(String notif) {
        this.notif = notif;
    }

    public MenuVitrine(int id, String label, int img, String type, String notif) {
        this.id = id;
        this.label = label;
        this.img = img;
        this.type = type;
        this.notif = notif;
    }

    public MenuVitrine(int id, String label, int img, String type) {
        this.id = id;
        this.label = label;
        this.img = img;
        this.type = type;
    }

    public MenuVitrine(int id, String label, int img) {
        this.id = id;
        this.label = label;
        this.img = img;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }


}
