package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.ProfileFramenPagerAdapteur;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.BottomProfileFavori;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.BottomProfileHistorique;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.BottomProfileVitrine;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.BottomPublicProfileVitrine;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class PublicProfileActivity extends AppCompatActivity {
    String bad_url="https://cooperationsoffice.net/cooperation/logo/";
    Toolbar  toolbar;
    TabLayout tabLayout;
    ViewPager pager;
    TextView tvNom;
    TextView tvLocation;
    CircularImageView circularImageView;
    boolean is_fr;
    User user=null;
    long id;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottom_public_profile_layout);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        toolbar=(Toolbar)findViewById(R.id.mtool);
        tvNom=(TextView)findViewById(R.id.profile_tvnom);
        tvLocation=(TextView)findViewById(R.id.profile_tvlocation);
        circularImageView=(CircularImageView)findViewById(R.id.bottom_profil_profil_image);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        pager=(ViewPager)findViewById(R.id.profile_viewpager);
        if(getIntent().getExtras()!=null){
            id=getIntent().getExtras().getLong("id");
            getUser(id);
        }
        setUpPager(pager);
        tabLayout=(TabLayout)findViewById(R.id.profile_tab);
        tabLayout.setupWithViewPager(pager);

    }
        private void setUpData(){
            if(getIntent().getExtras()!=null){

                if(user!=null){

                    String nom=user.getName();
                    String prenom=user.getPrenom();
                    String create_at=user.getCreate_at();
                    String pays=user.getPays();


                    if(create_at!=null && !create_at.equalsIgnoreCase("") && !create_at.equalsIgnoreCase("vide")){
                        setLocation(create_at,pays);

                    }
                    else{

                        if(pays!=null && !pays.equalsIgnoreCase("") && !pays.equalsIgnoreCase("vide")){
                            tvLocation.setText(pays);
                        }
                    }

                    if(nom!=null && !nom.equalsIgnoreCase("") && !nom.equalsIgnoreCase("vide")){
                        if(prenom!=null && !prenom.equalsIgnoreCase("") && !prenom.equalsIgnoreCase("vide")){
                            tvNom.setText(""+prenom+" "+nom);
                        }
                        else{
                            tvNom.setText(""+nom);
                        }
                    }
                    else{
                        if(prenom!=null && !prenom.equalsIgnoreCase("") && !prenom.equalsIgnoreCase("vide")){
                            tvNom.setText(""+prenom);
                        }
                    }
                    if(user.getPhoto_url()!=null && !user.getPhoto_url().equalsIgnoreCase("") && !user.getPhoto_url().equalsIgnoreCase(bad_url)){
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String modifier = formatter.format(Calendar.getInstance().getTime());
                        Glide.with(this).load(user.getPhoto_url()).asBitmap().signature(new StringSignature(modifier)).override(400,400).placeholder(R.drawable.bg_cooperationsoffice).error(R.drawable.bg_cooperationsoffice).into(circularImageView);
                    }

                }
                else{

                }

            }
        }
    public void getUser(long id){

        final Gson gson=new Gson();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.get_User(id);

        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {

                            } else {

                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {

                                } else {
                                    user=ParseJson.parseUser(PublicProfileActivity.this,o);
                                    setUpData();
                                }

                            }

                        } catch (Exception e) {


                        }

                    }//fin is success,
                    else {


                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

            }

        });
    }
    private void setLocation(String create_at,String pays){

        SimpleDateFormat sdfa=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // sdfa.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
        if(create_at!=null && !create_at.equalsIgnoreCase("")){
            try{

                Date startDate=sdfa.parse(create_at);
                sdfa.setTimeZone(TimeZone.getDefault());
                Date endDate=new Date();
                long different = endDate.getTime() - startDate.getTime();

                long secondsInMilli = 1000;
                long minutesInMilli = secondsInMilli * 60;
                long hoursInMilli = minutesInMilli * 60;
                long daysInMilli = hoursInMilli * 24;
                long monthInMilli=daysInMilli*30;
                long yearsInMilli=monthInMilli*12;

                long elapsedyears = different / yearsInMilli;
                different = different % yearsInMilli;
                long elapsedMonth = different / monthInMilli;
                different = different % monthInMilli;

                long elapsedDays = different / daysInMilli;
                different = different % daysInMilli;

                long elapsedHours = different / hoursInMilli;
                different = different % hoursInMilli;

                long elapsedMinutes = different / minutesInMilli;
                different = different % minutesInMilli;

                long elapsedSeconds = different / secondsInMilli;
                String elapsed="";
                pays=pays!=null && !pays.equalsIgnoreCase("")&& !pays.equalsIgnoreCase("vide")?pays:"";
                if(elapsedyears!=0){

                    if(is_fr){
                        tvLocation.setText(""+pays+" / Membre il y a "+elapsedyears+" ans");
                    }
                    else{
                        tvLocation.setText(""+pays+" / Member since "+elapsedyears+" old");
                    }

                }
                else if(elapsedMonth!=0){

                    if(is_fr){
                        tvLocation.setText(""+pays+" / Membre il y a "+(elapsedMonth)+" mois");
                    }
                    else{
                        tvLocation.setText(""+pays+" / Member since "+(elapsedMonth>1?elapsedMonth+" months":elapsedMonth+" month"));
                    }
                }
                else if(elapsedDays!=0){

                    if(is_fr){
                        tvLocation.setText(""+pays+" / Membre il y a "+(elapsedDays>1?elapsedDays+" jours":elapsedDays+" jour"));
                    }
                    else{
                        tvLocation.setText(""+pays+" / Member since "+(elapsedDays>1?elapsedDays+" days":elapsedDays+" day"));
                    }
                }
                else if(elapsedHours!=0){

                    if(is_fr){
                        tvLocation.setText(""+pays+" / Membre il y a "+(elapsedHours>1?elapsedHours+" heures":elapsedHours+" heure"));
                    }
                    else{
                        tvLocation.setText(""+pays+" / Member since "+(elapsedHours>1?elapsedHours+" hours":elapsedHours+" hour"));
                    }
                }
                else  if(elapsedMinutes!=0){

                    if(is_fr){
                        tvLocation.setText(""+pays+" / Membre il y a "+(elapsedMinutes>1?elapsedMinutes+" minutes":elapsedMinutes+" minute"));
                    }
                    else{
                        tvLocation.setText(""+pays+" / Member since "+(elapsedMinutes>1?elapsedMinutes+" minutes":elapsedMinutes+" minute"));
                    }
                }
                else  if(elapsedSeconds!=0){

                    if(is_fr){
                        tvLocation.setText(""+pays+" / Membre il y a "+(elapsedSeconds>1?elapsedSeconds+" secondes":" 0 seconde"));
                    }
                    else{
                        tvLocation.setText(""+pays+" / Member since "+(elapsedSeconds>1?elapsedSeconds+" seconds":" 0 second"));
                    }
                }
                else{

                    if(is_fr){
                        tvLocation.setText(""+pays+" / Membre il y a  0 seconde");
                    }
                    else{
                        tvLocation.setText(""+pays+" / Member since  0 second");
                    }
                }

            }
            catch(ParseException e){

            }
        }
    }

    private void setUpPager(ViewPager pager){
        ProfileFramenPagerAdapteur fragmentAdapteur=new ProfileFramenPagerAdapteur(getSupportFragmentManager());
        BottomPublicProfileVitrine profileVitrine=new BottomPublicProfileVitrine();
        Bundle bundle=new Bundle();
        bundle.putBoolean("public",true);
        bundle.putLong("id",id);
        profileVitrine.setArguments(bundle);
        fragmentAdapteur.add(profileVitrine,getResources().getString(R.string.profile_title_publication));
        pager.setAdapter(fragmentAdapteur);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
