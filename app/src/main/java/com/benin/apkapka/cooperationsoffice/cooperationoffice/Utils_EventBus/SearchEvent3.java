package com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus;

/**
 * Created by joel on 08/09/2016.
 */
public class SearchEvent3 {

    private String query;
    private int type;
    private String pays;

    public SearchEvent3(String query, int type, String pays) {
        this.query = query;
        this.type = type;
        this.pays = pays;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
