package com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus;

/**
 * Created by joel on 03/11/2016.
 */
public interface AdapteurBuyListener {
    public void onBuy(Object c);
    public  void onMore(Object c);
    public  void onShare(Object c);
    public  void onAddCard(Object c);
}
