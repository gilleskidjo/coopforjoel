package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

/**
 * Created by joel on 30/10/2016.
 */
public class HideShowFloating extends FloatingActionButton.Behavior {

    public  HideShowFloating(Context c, AttributeSet attrs)
    {

    }

    @Override
    public void onNestedScroll(CoordinatorLayout coordinatorLayout, FloatingActionButton child, View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
            if(dyConsumed>0)
            {
            CoordinatorLayout.LayoutParams layoutParams=(CoordinatorLayout.LayoutParams)child.getLayoutParams();
                    int fabmarginb=layoutParams.bottomMargin;
                child.animate().translationY(child.getHeight()+fabmarginb).setInterpolator(new LinearInterpolator()).start();
            }
            else if(dyConsumed<0)
            {
               child.animate().translationY(0).setInterpolator(new LinearInterpolator()).start();
            }

    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, FloatingActionButton child, View directTargetChild, View target, int nestedScrollAxes) {
        return  nestedScrollAxes== ViewCompat.SCROLL_AXIS_VERTICAL;
    }
}
