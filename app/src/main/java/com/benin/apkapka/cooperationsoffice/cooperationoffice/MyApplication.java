package com.benin.apkapka.cooperationsoffice.cooperationoffice;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.BaseActivity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.BottomProfileFavori;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ChatConversation;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Favorite;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.LocaleHelper;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SaveFavorite;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;
import io.objectbox.query.QueryBuilder;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by joel on 15/02/2016.
 */
public class MyApplication   extends MultiDexApplication {

    private RequestQueue mRequestQueue;
    private static  MyApplication instance;
    public  static Context context;
    BoxStore boxStore;
    private String bottomChatSelectedImage_path="";
    private boolean canSendSelectedImage=false;
    NotificationManager notificationManager;
    public static final boolean isRunning=false;
    ChatConversation conversation;
    boolean is_Active_conversation=false;
    List<Produit> mProduits=new ArrayList<>();


    @Override
    public void onCreate() {
        super.onCreate();
        context=getApplicationContext();
        instance=this;
        //Fabric.with(this,new Crashlytics());

        //Initialisation d'ObjectBox (permet de créer une base de donnée local) quand l'application est démarrée.
        boxStore= MyObjectBox.builder().androidContext(MyApplication.this).build();

        MobileAds.initialize(this,
                "ca-app-pub-5424708841083179~1666498449");

        notificationManager=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        /*
        On vérifie que la version d'Android est supérieur a 0
        Si cest le cas on applique la gestion de la notification de l'appareil.
        * */
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setUpChannel();
        }


        FirebaseMessaging.getInstance().subscribeToTopic("cooperation");
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TASK", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                       SessionManager sessionManager=new SessionManager(MyApplication.this);
                       User user=sessionManager.getUser(MyApplication.this);
                        if(user!=null && user.getId()!=0){
                            getUserToken(user.getId(),token);
                        }
                    }
                });
    }

    public BoxStore getBoxStore() {
        return boxStore;
    }

    public  static Context myContext()
    {
        return getInstance();
    }
    public  static  synchronized  MyApplication getInstance()
    {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base,"fr"));
        MultiDex.install(this);
    }

    public  RequestQueue  getRequestQueue()
    {
        if(mRequestQueue==null)
        {
            mRequestQueue= Volley.newRequestQueue(getApplicationContext());
        }
        return  mRequestQueue;
    }

    public <T>void addToRequestQueue(Request<T> request)
    {
        getRequestQueue().add(request);
    }
    public  void cancelAllRequest(Object c)
    {
        if(mRequestQueue!=null)
        {
            mRequestQueue.cancelAll(c);
        }
    }


   @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleHelper.setLocal(this);
    }


    /**
     * Gestion des notifications de cooperationsoffice
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setUpChannel(){
     String channel_id = getResources().getString(R.string.default_notification_channel_id);
     String channel_name = getResources().getResourceName(R.string.default_notification_channel_name);
     String channel_description = getResources().getString(R.string.notification_channel_description);

        //Déclaration du NotificationChannel
        NotificationChannel channel;

        //Initialisation de la notification avec comme paramètre l'id, le nom et le dégré d'importance de la notification
        channel = new NotificationChannel(channel_id,channel_name, NotificationManager.IMPORTANCE_LOW);
        channel.setDescription(channel_description);
        channel.enableLights(true);
        channel.setSound(null,null);
        channel.setLightColor(getColor(R.color.primary));

        if(notificationManager != null){
            notificationManager.createNotificationChannel(channel);
        }

    }


    /*
       Getter & Setter Chemin de l'image sélectionnée au niveau du chatbot -- Début
     */
    public String getBottomChatSelectedImage_path() {
        return bottomChatSelectedImage_path;
    }

    public void setBottomChatSelectedImage_path(String bottomChatSelectedImage_path) {
        this.bottomChatSelectedImage_path = bottomChatSelectedImage_path;
    }
    /*
     Getter & Setter Chemin de l'image sélectionnée au niveau du chatbot -- Fin
     */

    public boolean isCanSendSelectedImage() {
        return canSendSelectedImage;
    }

    public void setCanSendSelectedImage(boolean canSendSelectedImage) {
        this.canSendSelectedImage = canSendSelectedImage;
    }

    public ChatConversation getConversation() {
        return conversation;
    }

    public void setConversation(ChatConversation conversation) {
        this.conversation = conversation;
    }

    public boolean isIs_Active_conversation() {
        return is_Active_conversation;
    }

    public void setIs_Active_conversation(boolean is_Active_conversation) {
        this.is_Active_conversation = is_Active_conversation;
    }
    //Enregistre les favoris dans la base de donnée objectbox lorsque l'utilisateur se reconnecte apres deinstallation
    private void saveLocalFavorie(List<Produit> listP){
        List<Produit> favoritesList=new ArrayList<>();
        List<Long> saveFavorites=new ArrayList<>();
        Box<SaveFavorite> saveFavoriteBox=getBoxStore().boxFor(SaveFavorite.class);
        Box<Favorite> favoriteBox =getBoxStore().boxFor(Favorite.class);
        Gson gson=new Gson();
        QueryBuilder<SaveFavorite> builder=saveFavoriteBox.query();
        Query<SaveFavorite> query=builder.build();
        SaveFavorite unSaveFavorite=query.findFirst();
        if(unSaveFavorite!=null){
            String val=unSaveFavorite.saveFavorite;
            if(!val.equalsIgnoreCase("") && val!=null){
                Type type=new TypeToken<List<Long>>(){}.getType();
                saveFavorites=gson.fromJson(val,type);

            }
            else{
                saveFavorites=new ArrayList<>();
            }

        }
        else{
            saveFavorites=new ArrayList<>();
        }
        QueryBuilder<Favorite> builder1=favoriteBox.query();
        Query<Favorite> query1=builder1.build();
        Favorite favorite=query1.findFirst();
        if(favorite!=null){
            String val=favorite.mfavorite;
            if(!val.equalsIgnoreCase("") && val!=null){
                Type type=new TypeToken<List<Produit>>(){}.getType();
                favoritesList=gson.fromJson(val,type);

            }
            else{
                favoritesList=new ArrayList<>();
            }

        }
        else{
            favoritesList=new ArrayList<>();
        }

         if(listP!=null && listP.size()>0){

             for (int i=0;i<listP.size();i++) {
                 favoritesList.add(listP.get(i));

                 saveFavorites.add(listP.get(i).getId());
                 if(!favoritesList.contains(listP.get(i))){
                     favoritesList.add(listP.get(i));
                     saveFavorites.add(listP.get(i).getId());
                 }
             }
         }
        if(unSaveFavorite!=null){
            Type type=new TypeToken<List<Long>>(){}.getType();
            String UnfavoriteSaved=gson.toJson(saveFavorites,type);
            unSaveFavorite.saveFavorite=UnfavoriteSaved;
            saveFavoriteBox.put(unSaveFavorite);
        }
        else{

            unSaveFavorite=new SaveFavorite();
            Type type=new TypeToken<List<Long>>(){}.getType();
            String UnfavoriteSaved=gson.toJson(saveFavorites,type);
            unSaveFavorite.saveFavorite=UnfavoriteSaved;
            saveFavoriteBox.put(unSaveFavorite);
        }
        if(favorite!=null){
            Type type=new TypeToken<List<Produit>>(){}.getType();
            String favoriteSaved=gson.toJson(favoritesList,type);
            favorite.mfavorite=favoriteSaved;
            favoriteBox.put(favorite);
        }
        else{
            favorite=new Favorite();
            Type type=new TypeToken<List<Produit>>(){}.getType();
            String favoriteSaved=gson.toJson(favoritesList,type);
            favorite.mfavorite=favoriteSaved;
            favoriteBox.put(favorite);
        }

    }

    public void FetchFavori(long id,String zone){
        final Gson gson=new Gson();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.get_All_Favori_In_Login(""+id,zone);

        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {

                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {



                            } else {

                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {


                                } else {


                                    List<Produit> produits=ParseJson.parseProduit(o,getApplicationContext());
                                    if(produits!=null && produits.size()>0 ){
                                       mProduits=produits;
                                       new MyAsyncr().execute();
                                    }

                                }

                            }

                        } catch (Exception e) {


                        }

                    }//fin is success,
                    else {


                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

            }

        });
    }

    public void GetUserData(long useer_id){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.get_target_user_data(""+useer_id);

        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {


                            } else {

                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {

                                } else {


                                    User user=ParseJson.parseUserFromHome(MyApplication.this,o);
                                    SessionManager sessionManager=new SessionManager(MyApplication.this);
                                    sessionManager.setUser(MyApplication.this,user);
                                }
                            }

                        } catch (Exception e) {


                        }

                    }//fin is success,
                    else {

                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {


            }

        });
    }


    public void getUserToken(long useer_id,String token){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.get_user_token(""+useer_id,token);

        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {


                            } else {

                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {

                                } else {


                                }
                            }

                        } catch (Exception e) {


                        }

                    }//fin is success,
                    else {

                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {


            }

        });
    }

    class MyAsyncr extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {

          saveLocalFavorie(mProduits);
            return null;
        }
    }
}
