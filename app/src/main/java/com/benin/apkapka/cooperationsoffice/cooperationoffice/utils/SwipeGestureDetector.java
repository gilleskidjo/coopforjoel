package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.animation.AnimationUtils;
import android.widget.ViewFlipper;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;

/**
 * Created by joel on 15/09/2016.
 */
public  abstract class SwipeGestureDetector  implements  GestureDetector.OnGestureListener {
    ViewFlipper flipper;
    Context c;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
      public   SwipeGestureDetector(Context c,ViewFlipper flipper)
      {
          this.flipper=flipper;
          this.c=c;

      }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {


        try {
            // right to left swipe
            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                flipper.setInAnimation(AnimationUtils.loadAnimation(c, R.anim.slide_in_right));
                flipper.setOutAnimation(AnimationUtils.loadAnimation(c, R.anim.slide_out_left));
                flipper.showNext();
                return true;
            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                flipper.setInAnimation(AnimationUtils.loadAnimation(c, R.anim.slide_in_left));
                flipper.setOutAnimation(AnimationUtils.loadAnimation(c,R.anim.slide_out_right));
                flipper.showPrevious();
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }
}
