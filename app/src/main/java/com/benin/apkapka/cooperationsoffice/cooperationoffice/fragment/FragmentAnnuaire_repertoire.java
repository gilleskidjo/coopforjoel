package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.BusEventFragmentNetworkRequest2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.SearchEvent1;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.SearchEvent2;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Formulaire_annuaire;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.UnAnnuaire;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.AccueilInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushFamille;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushFamilleAsyncLoader;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushFamilleAsyncLoader2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Annuaire;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.EndlessRecyclerViewScrollListener;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.HelperActivity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ListDataHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MydateUtil;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ProduitDatabaseRelation;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;


/**
 * Created by joel on 05/08/2016.
 */
public class FragmentAnnuaire_repertoire  extends Fragment implements AccueilInterface,FragmentNetworkRequestNouvelArrivage.HandlerNetWorkRequestResponse,BusEventFragmentNetworkRequest2.HandlerNetWorkRequestResponse,LoaderManager.LoaderCallbacks<List<PushFamille>> {

    RecyclerView liste;
    LinearLayoutManager manager;
    ProgressBar progressBar;
    MyAdapteurAnnuaire adapteur;

    private  boolean is_fr=false;
    List<Produit> listP=new ArrayList<>();
    List<PushFamille> familles=new ArrayList<>();
    PushFamilleAsyncLoader2 loader;
    private boolean istablette;
    TextView vide;
    View linearvide;
    FragmentNetworkRequestNouvelArrivage    netWork;
    private int animate;
    String payss_code [];
    String pays="tout";
    SwipeRefreshLayout swipe;
    BusEventFragmentNetworkRequest2 net;
    boolean is_small,is_medium,is_large;
    boolean is_search;
    Box<ProduitDatabaseRelation> databaseRelationBox;
    ProduitDatabaseRelation produitDatabaseRelation;
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private SessionManager sessionSaverData;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            is_fr=getResources().getBoolean(R.bool.lang_fr);
        is_small=getResources().getBoolean(R.bool.is_small);
        is_medium=getResources().getBoolean(R.bool.is_medium);
        is_large=getResources().getBoolean(R.bool.is_large);
            payss_code=getResources().getStringArray(R.array.pays_code);
        View v=inflater.inflate(R.layout.fragment_annuaire_repertoire_layout,container,false);
        swipe=(SwipeRefreshLayout)v.findViewById(R.id.fragment_annuiare_repertoire_swipe);
        swipe.setRefreshing(false);
        swipe.setColorSchemeColors(Color.BLUE,Color.GREEN,Color.RED,Color.YELLOW);
        BoxStore boxStore=((MyApplication) getActivity().getApplicationContext()).getBoxStore();
        databaseRelationBox=boxStore.boxFor(ProduitDatabaseRelation.class);
        produitDatabaseRelation=new ProduitDatabaseRelation(14);
        liste=(RecyclerView)v.findViewById(R.id.fragment_annuiare_repertoire_RecycleView);
        progressBar=(ProgressBar)v.findViewById(R.id.fragment_annuiare_repertoire_Progress);
        linearvide=v.findViewById(R.id.fragment_annuiare_repertoire_linearvide);
        vide=(TextView)v.findViewById(R.id.fragment_annuiare_repertoire_textview_vide);
        animate=getResources().getInteger(android.R.integer.config_shortAnimTime);
        liste.setVisibility(View.GONE);
        manager=new LinearLayoutManager(getActivity());
        liste.setLayoutManager(manager);
        adapteur=new MyAdapteurAnnuaire(listP,familles,this,getActivity());
        endlessRecyclerViewScrollListener=new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(page);
            }
        };
        sessionSaverData = new SessionManager(getContext());

        liste.addOnScrollListener(endlessRecyclerViewScrollListener);
        FragmentManager fm=getFragmentManager();
        net=(BusEventFragmentNetworkRequest2)fm.findFragmentByTag(BusEventFragmentNetworkRequest2.TAG);
        if(net==null)
        {
            net=new BusEventFragmentNetworkRequest2();
            fm.beginTransaction().add(net,BusEventFragmentNetworkRequest2.TAG).commit();
        }
        else
        {
            if(net.isHasRequest())
            {
                net.setInterface(this);
                swipe.setRefreshing(true);
            }
        }
        netWork=(FragmentNetworkRequestNouvelArrivage) fm.findFragmentByTag(FragmentNetworkRequestNouvelArrivage.TAG);
        if(netWork==null)
        {

            netWork=new FragmentNetworkRequestNouvelArrivage();
            fm.beginTransaction().add(netWork,FragmentNetworkRequestNouvelArrivage.TAG).commit();
            netWork.setInterface(this);

            if(isConnected()){
              loadNextDataFromApi(0);
            }
            else {

                ProduitDatabaseRelation relation=databaseRelationBox.get(14);
                if(relation!=null){
                    listP=relation.list;
                }
                if(listP!=null && listP.size()>0){
                    adapteur=new MyAdapteurAnnuaire(listP,familles,FragmentAnnuaire_repertoire.this,getActivity());
                    liste.setAdapter(adapteur);
                    liste.setVisibility(View.VISIBLE);
                    linearvide.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                }
                else {
                    progressBar.setVisibility(View.GONE);
                    vide.setText(getResources().getString(R.string.error_frag_netw_parse));
                    liste.setVisibility(View.GONE);
                    linearvide.setVisibility(View.VISIBLE);
                }
            }
        }
        else
        {
           if(isConnected()){
               netWork.setInterface(this);
               if(net.isEmpty() || netWork.isEmpty){
                   vide.setText(netWork.getSms());
                   if(net.isReady() && net.getSms()!=null)
                   {
                       vide.setText(net.getSms());
                   }
                   liste.setVisibility(View.GONE);
                   linearvide.setVisibility(View.VISIBLE);
                   progressBar.setVisibility(View.GONE);
               }
               else {
                   listP=netWork.getListproduit();
                   if(net.isReady()){
                       listP=net.getListproduit();
                   }
                   if(listP!=null && listP.size()>0)
                   {
                       adapteur=new MyAdapteurAnnuaire(listP,familles,this,getActivity());
                       liste.setAdapter(adapteur);
                       progressBar.setVisibility(View.GONE);
                       linearvide.setVisibility(View.GONE);
                       liste.setVisibility(View.VISIBLE);

                   }
                   else
                   {

                       if(netWork.isMessage())
                       {
                           progressBar.setVisibility(View.GONE);
                           vide.setText(getResources().getString(R.string.error_frag_netw_parse));
                           liste.setVisibility(View.GONE);
                           linearvide.setVisibility(View.VISIBLE);
                       }
                   }
               }

           }
           else {
               ProduitDatabaseRelation relation=databaseRelationBox.get(14);
               if(relation!=null){
                   listP=relation.list;
               }
               if(listP!=null && listP.size()>0){
                   adapteur=new MyAdapteurAnnuaire(listP,familles,FragmentAnnuaire_repertoire.this,getActivity());
                   liste.setAdapter(adapteur);
                   liste.setVisibility(View.VISIBLE);
                   linearvide.setVisibility(View.GONE);
                   progressBar.setVisibility(View.GONE);
               }
               else {
                   progressBar.setVisibility(View.GONE);
                   vide.setText(getResources().getString(R.string.error_frag_netw_parse));
                   liste.setVisibility(View.GONE);
                   linearvide.setVisibility(View.VISIBLE);
               }
           }

        }


        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(false);
            }
        });

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(PushHandler.FRAGMENT2,null,this);
    }
    public void loadNextDataFromApi(int offset) {
        // Send an API request to retrieve appropriate paginated data
        //  --> Send the request including an offset value (i.e `page`) as a query parameter.
        //  --> Deserialize and construct new model objects from the API response
        //  --> Append the new data objects to the existing set of items inside the array of items
        //  --> Notify the adapter of the new items made with `notifyItemRangeInserted()
        //
        if(isConnected()){

            SessionManager sessionManager=new SessionManager(getActivity());
            User user=sessionManager.getUser(getActivity());
            String zone="";
            zone=user.getPays();
            if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
                zone="france";
            }
            netWork.doRequestProduitNouveauproduit_Group_By_Famille("repertoires",pays,is_fr==true?"fr":"en","annuaire",zone,offset);
        }
    }
    @Override
    public Loader<List<PushFamille>> onCreateLoader(int id, Bundle args) {
        loader=new PushFamilleAsyncLoader2(getContext(),PushHandler.type5,PushHandler.ANN_REP);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<PushFamille>> loader, List<PushFamille> data) {
        if(data!=null){
            familles=data;
            adapteur=new MyAdapteurAnnuaire(listP,familles,this,getActivity());
            liste.setAdapter(adapteur);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<PushFamille>> loader) {

    }

    @Override
    public void onResposeSuccess() {
        if(net.isReady())
        {

            if(is_search){
                is_search=false;
                if(net.getListproduit()!=null && net.getListproduit().size()>0){
                    if(net.getListproduit()!=null && net.getListproduit().size()==1){
                        Intent intent =new Intent(getActivity(),UnAnnuaire.class);
                        Produit  p=net.getListproduit().get(0);
                        HelperActivity.getInstance().setProduit(p);
                        intent.putExtra("id",p.getId());


                        intent.putExtra("reduction",p.getReduction());
                        intent.putExtra("nb_price_en",p.getNb_price_en().toString());
                        intent.putExtra("lien_catalogue",p.getLien_catalogue());
                        intent.putExtra("lien_description",p.getLien_description());
                        intent.putExtra("choixf",p.getChoixf());
                        intent.putExtra("url",p.getUrl());
                        intent.putExtra("url2",p.getUrl2());
                        intent.putExtra("from_co_direct","oui");
                        intent.putExtra("url3",p.getUrl3());
                        intent.putExtra("famille",p.getFamille());
                        intent.putExtra("pays",p.getPays());
                        intent.putExtra("category",p.getCategory());
                        intent.putExtra("type2",1);
                        intent.putExtra("date",p.getDate());
                        intent.putExtra("famille_en",p.getFamille_en());
                        intent.putExtra("lien_fichier",p.getLien_fichier());
                        startActivity(intent);

                    }
                    else {
                        listP.clear();
                        listP.addAll(net.getListproduit());
                        endlessRecyclerViewScrollListener.resetState();
                    }

                }
            }
            else {
                listP.clear();
                listP.addAll(net.getListproduit());
                endlessRecyclerViewScrollListener.resetState();
                if(listP!=null && listP.size()>0){
                    try {
                        ProduitDatabaseRelation relation=new ProduitDatabaseRelation(14);
                        databaseRelationBox.remove(relation);
                        databaseRelationBox.attach(relation);
                        relation.list.addAll(listP);
                        databaseRelationBox.put(relation);
                    }
                    finally {

                    }
                }
            }
        }
        else
        {
            listP.addAll(netWork.getListproduit());
            if(listP!=null && listP.size()>0){

               try {
                   ProduitDatabaseRelation relation=new ProduitDatabaseRelation(14);
                   databaseRelationBox.remove(relation);
                   databaseRelationBox.attach(relation);
                   relation.list.addAll(listP);
                   databaseRelationBox.put(relation);
               }
               finally {

               }
            }
        }

       adapteur.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
        linearvide.setVisibility(View.GONE);
        liste.setVisibility(View.VISIBLE);
        if(net.isHasRequest())
        {
            swipe.setRefreshing(false);
            net.setHasRequest(false);
        }
    }

    @Override
    public void onresponseFailAndPrintErrorResponse() {
        is_search=false;
        if(net.isEmpty() || netWork.isEmpty){
           if(endlessRecyclerViewScrollListener.currentPage<1){
               vide.setText(netWork.getSms());
               if(net.isHasRequest() && net.getSms()!=null)
               {
                   vide.setText(net.getSms());
               }
               liste.setVisibility(View.GONE);
               linearvide.setVisibility(View.VISIBLE);
               progressBar.setVisibility(View.GONE);
           }
        }
        else{

            ProduitDatabaseRelation relation=databaseRelationBox.get(14);
            if(relation!=null){
                listP=relation.list;
            }

            if(listP!=null && listP.size()>0){
                adapteur=new MyAdapteurAnnuaire(listP,familles,FragmentAnnuaire_repertoire.this,getActivity());
                liste.setAdapter(adapteur);
                liste.setVisibility(View.VISIBLE);
                linearvide.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            else {
                progressBar.setVisibility(View.GONE);
                vide.setText(getResources().getString(R.string.error_frag_netw_parse));
                liste.setVisibility(View.GONE);
                linearvide.setVisibility(View.VISIBLE);
            }

        }
        if(net.isHasRequest())
        {
            swipe.setRefreshing(false);
            net.setHasRequest(false);
        }
    }


    @Override
    public void itemClicked(int position) {

            Intent intent =new Intent(getActivity(),UnAnnuaire.class);
            Produit p=listP.get(position);
            HelperActivity.getInstance().setProduit(p);
            intent.putExtra("id",p.getId());

            intent.putExtra("reduction",p.getReduction());
            intent.putExtra("nb_price_en",p.getNb_price_en().toString());
            intent.putExtra("lien_catalogue",p.getLien_catalogue());
            intent.putExtra("lien_description",p.getLien_description());
            intent.putExtra("choixf",p.getChoixf());
        intent.putExtra("from_co_direct","oui");
            intent.putExtra("url",p.getUrl());
            intent.putExtra("url2",p.getUrl2());
            intent.putExtra("url3",p.getUrl3());
            intent.putExtra("famille",p.getFamille());
            intent.putExtra("pays",p.getPays());
            intent.putExtra("category",p.getCategory());
            intent.putExtra("type2",1);
            intent.putExtra("date",p.getDate());
            intent.putExtra("famille_en",p.getFamille_en());
        intent.putExtra("lien_fichier",p.getLien_fichier());
            startActivity(intent);

    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }
    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
    @Subscribe
    public void onEvent(SearchEvent2 event)
    {
        SessionManager sessionManager=new SessionManager(getActivity());
        User user=sessionManager.getUser(getActivity());
        String zone="";
        zone=user.getPays();
        if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
            zone="france";
        }
        net.setReady(true);
        swipe.setRefreshing(true);
        switch (event.getType())
        {
            case 1 :
                liste.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                linearvide.setVisibility(View.GONE);
                net.doRequestProduit_Group_By_Famille("repertoires",event.getQuery(),is_fr==true?"fr":"en","annuaire",zone,0);
                net.setInterface(this);
                net.setHasRequest(true);
                pays=event.getQuery();
                break;
            case 2:
                is_search=true;
                net.doRequestProduit_Search("repertoires",event.getPays(),event.getQuery(),is_fr==true?"fr":"en","annuaire",zone);
                net.setInterface(this);
                net.setHasRequest(true);
                break;
        }
    }
    @Override
    public void onPause() {
        super.onPause();
    }

    class  MyAdapteurAnnuaire extends  RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        public static final int VIEW_TYPE_UN = 0;
        public static final int VIEW_TYPE_TWO = 1;

        List<Produit> produitList;
        List<PushFamille> familleList;
        AccueilInterface accueilInterface;
        Context c;
        Produit annuaire;
        PushFamille f;
        private int lastPosition = -1;

        public MyAdapteurAnnuaire(List<Produit> produitList,List<PushFamille> familleList,AccueilInterface accueilInterface,Context c)

        {
            this.c=c;
            this.familleList=familleList;
            this.produitList=produitList;
            this.accueilInterface=accueilInterface;
        }
        public   class ViewHolder extends RecyclerView.ViewHolder
        {

            TextView title;

            TextView description;

            DinamicImageView image;
            TextView tvCount,txtFamille;
            TextView tvDate;
            TextView txtCountFamille;
            private boolean isNewFamille;
            LinearLayout parent;
            public ViewHolder(View v)
            {
                super(v);
                title=(TextView)v.findViewById(R.id.type_view_item_title);
                tvCount=(TextView)v.findViewById(R.id.type_view_item_Count);
                tvDate=(TextView)v.findViewById(R.id.type_view_item_date);
                description=(TextView)v.findViewById(R.id.type_view_item_description);
                image=(DinamicImageView) v.findViewById(R.id.type_view_item_image);
                txtFamille=(TextView)v.findViewById(R.id.famille_name);
                txtCountFamille=(TextView)v.findViewById(R.id.famille_count);
                parent=v.findViewById(R.id.parent);


            }
        }

        public class PrincipaleViewHolder  extends RecyclerView.ViewHolder
        {
            TextView  titlePrinciale,tvCount,tvDate,txtFamille;
            DinamicImageView imagePrincipale;
            ProgressBar  bar;
            TextView txtCountFamille;
            private boolean isNewFamille;
            public  PrincipaleViewHolder(View  v)
            {
                super(v);
                titlePrinciale=(TextView)v.findViewById(R.id.actualite_title_principale);
                tvCount=(TextView)v.findViewById(R.id.actualite_Count);
                tvDate=(TextView)v.findViewById(R.id.actualite_date);
                imagePrincipale=(DinamicImageView) v.findViewById(R.id.actualite_image_principale);
                bar=(ProgressBar)v.findViewById(R.id.actualite_progressBar1);
                txtFamille=(TextView)v.findViewById(R.id.famille_name);
                txtCountFamille=(TextView)v.findViewById(R.id.famille_count);
            }

        }

        @Override
        public int getItemCount() {
            return produitList.size();
        }

        @Override
        public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            switch (viewType)
            {
                case VIEW_TYPE_UN:

                    View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.annuaire_la_une_layout,parent,false);
                    final PrincipaleViewHolder holder=new PrincipaleViewHolder(v);
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            accueilInterface.itemClicked(holder.getAdapterPosition());
                        }
                    });
                    return holder;

                case  VIEW_TYPE_TWO:
                    View v1=LayoutInflater.from(parent.getContext()).inflate(R.layout.type_view_item_list_annuaire,parent,false);
                    final ViewHolder holder1=new ViewHolder(v1);
                    v1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            accueilInterface.itemClicked(holder1.getAdapterPosition());
                        }
                    });
                    return holder1;
            }


            return null;
        }


        @Override
        public int getItemViewType(int position) {
            return position==0?VIEW_TYPE_UN:VIEW_TYPE_TWO;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            annuaire=produitList.get(position);
            Predicate<PushFamille> predicate=new Predicate<PushFamille>() {
                @Override
                public boolean apply(@javax.annotation.Nullable PushFamille input) {

                    return input.getFamille().equalsIgnoreCase(annuaire.getFamille());
                }
            };
            if(familleList.size()>0){
                List<PushFamille> familles= Lists.newArrayList(Iterables.filter(familleList,predicate));
                if(familles!=null && familles.size()>0){
                    f=familles.get(0);
                    if(f!=null && f.getCount()>0)
                    {
                        annuaire.setCount(f.getCount());
                    }

                }
                else{
                    annuaire.setCount(0);
                }

            }
            else{
                annuaire.setCount(0);
            }

            if(holder instanceof PrincipaleViewHolder)
            {
               PrincipaleViewHolder holder1=(PrincipaleViewHolder)holder;
                setUpMainPrincipaleHoder(holder1,produitList.get(position));
            }
            else
            {
              ViewHolder holder2=(ViewHolder)holder;
                setUpListViewHolder(holder2,produitList.get(position));
                Config.setAnimation(holder2.parent,position,lastPosition,c);
            }




        }


        public void setUpMainPrincipaleHoder(final PrincipaleViewHolder holder, Produit article)
        {
            if(article.getNb_vue_famille()!=null && !article.getNb_vue_famille().trim().equalsIgnoreCase("") && !article.getNb_vue_famille().equalsIgnoreCase("null"))
            {
                holder.txtCountFamille.setText(article.getNb_vue_famille());
            }
            else{
                holder.txtCountFamille.setText("0");
            }
            holder.txtFamille.setText(article.getFamille().trim().toUpperCase());
            if(article.getCount()>0)
            {
                holder.tvCount.setText(String.valueOf(article.getCount()));
                holder.tvCount.setVisibility(View.VISIBLE);
            }
            else{
                holder.tvCount.setText("");
                holder.tvCount.setVisibility(View.GONE);
            }

            if(article.getAdmin_update().equalsIgnoreCase("oui")){
                holder.tvDate.setText(article.getExtend_proprety1().trim());
                holder.tvDate.setVisibility(View.VISIBLE);
            }
            else{
                holder.tvDate.setText(article.getExtend_proprety1());
                holder.tvDate.setVisibility(View.VISIBLE);
            }
            holder.titlePrinciale.setText(article.getTitle().trim());

            DisplayMetrics metrics=c.getResources().getDisplayMetrics();
            int deviceWidth = metrics.widthPixels;
            int deviceHieght=metrics.heightPixels;


            if (sessionSaverData.getKeySaverDataAnnuaire().equals("Oui")){
                Picasso.with(c)
                        .load(R.drawable.saverdatacoop)
                        .into(holder.imagePrincipale, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder.bar.setVisibility(View.GONE) ;
                                holder.imagePrincipale.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                holder.bar.setVisibility(View.GONE);
                                holder.imagePrincipale.setVisibility(View.VISIBLE);
                            }
                        });
            }

            if (sessionSaverData.getKeySaverDataAnnuaire().equals("Non")){
                Picasso.with(c)
                        .load(article.getUrl())
                        .into(holder.imagePrincipale, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder.bar.setVisibility(View.GONE) ;
                                holder.imagePrincipale.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                holder.bar.setVisibility(View.GONE);
                                holder.imagePrincipale.setVisibility(View.VISIBLE);
                            }
                        });
            }
        }

        public void setUpListViewHolder(ViewHolder  holder, final Produit annuaire)
        {
            if(annuaire.getNb_vue_famille()!=null && !annuaire.getNb_vue_famille().trim().equalsIgnoreCase("") && !annuaire.getNb_vue_famille().equalsIgnoreCase("null"))
            {
                holder.txtCountFamille.setText(annuaire.getNb_vue_famille());
            }
            else{
                holder.txtCountFamille.setText("0");
            }
            holder.txtFamille.setText(annuaire.getFamille().trim().toUpperCase());
            Predicate<PushFamille> predicate=new Predicate<PushFamille>() {
                @Override
                public boolean apply(@javax.annotation.Nullable PushFamille input) {

                    return input.getFamille().equalsIgnoreCase(annuaire.getFamille());
                }
            };
            if(familleList.size()>0){
                List<PushFamille> familles= Lists.newArrayList(Iterables.filter(familleList,predicate));
                if(familles!=null && familles.size()>0){
                    f=familles.get(0);
                    if(f!=null && f.getCount()>0)
                    {
                        annuaire.setCount(f.getCount());
                    }

                }
                else {
                    annuaire.setCount(0);
                }
            }
            else {
                annuaire.setCount(0);
            }
            if(annuaire.getCount()>0)
            {
                holder.tvCount.setText(String.valueOf(annuaire.getCount()));
                holder.tvCount.setVisibility(View.VISIBLE);
            }
            else{
                holder.tvCount.setText("");
                holder.tvCount.setVisibility(View.GONE);
            }

            String des="";

            holder.title.setText(annuaire.getTitle().trim());
            if(annuaire.getAdmin_update().equalsIgnoreCase("oui")){
                holder.tvDate.setText(annuaire.getExtend_proprety1().trim());
                des=annuaire.getDescription().trim();
                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des.length()>50)
                    {
                        des=des.substring(0,50)+"...";

                    }

                }
                else
                {
                    if(des.length()>110)
                    {
                        des=des.substring(0,110)+"...";
                    }
                }
            }
            else{
                holder.tvDate.setVisibility(View.VISIBLE);
                holder.tvDate.setText(annuaire.getExtend_proprety1());
                des=annuaire.getDescription().trim();
                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des.length()>50)
                    {
                        des=des.substring(0,50)+"...";

                    }

                }
                else
                {
                    if(des.length()>110)
                    {
                        des=des.substring(0,110)+"...";
                    }
                }
            }



            holder.description.setText(des);
            final  float scale=getResources().getDisplayMetrics().density;
            if(is_small)
            {
                if(sessionSaverData.getKeySaverDataAnnuaire().equals("Oui")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).resize((int)(120*scale+0.5f),(int)(100*scale+0.5f)).into(holder.image);
                }

                if(sessionSaverData.getKeySaverDataAnnuaire().equals("Non")){
                    Picasso.with(c).load(annuaire.getUrl()).resize((int)(120*scale+0.5f),(int)(100*scale+0.5f)).into(holder.image);
                }

            }
            else if(is_medium)
            {
                if(sessionSaverData.getKeySaverDataAnnuaire().equals("Oui")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).resize((int)(140*scale+0.5f),(int)(120*scale+0.5f)).into(holder.image);
                }

                if(sessionSaverData.getKeySaverDataAnnuaire().equals("Non")){
                    Picasso.with(c).load(annuaire.getUrl()).resize((int)(140*scale+0.5f),(int)(120*scale+0.5f)).into(holder.image);
                }

            }
            else if (is_large)
            {
                if(sessionSaverData.getKeySaverDataAnnuaire().equals("Oui")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).resize((int)(150*scale+0.5f),(int)(130*scale+0.5f)).into(holder.image);
                }

                if(sessionSaverData.getKeySaverDataAnnuaire().equals("Non")){
                    Picasso.with(c).load(annuaire.getUrl()).resize((int)(150*scale+0.5f),(int)(130*scale+0.5f)).into(holder.image);
                }

            }

        }


    }

    private boolean isConnected()
    {
        ConnectivityManager cm=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        if(info!=null && info.isConnected() && info.isAvailable())
        {
            return  true;
        }

        return  false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
