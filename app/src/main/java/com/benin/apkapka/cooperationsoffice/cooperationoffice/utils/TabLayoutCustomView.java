package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;

/**
 * Created by joel on 14/08/2017.
 */
public class TabLayoutCustomView extends RelativeLayout{
    ImageView tabIcon;
    TextView tabText;
    TextView tabSubtext;
    TextView tabCount;
    public TabLayoutCustomView(Context c){
        super(c);
        init(c);
    }

    private void init(Context c){
        View.inflate(c,R.layout.tablayout_custom_view,this);
        tabIcon=(ImageView)findViewById(R.id.tabIcon);
        tabText=(TextView)findViewById(R.id.tabText);
        tabSubtext=(TextView)findViewById(R.id.tabSubText);
        tabCount=(TextView)findViewById(R.id.tabCount);
        setTabText(null);
        setTabCount(0);
    }

    public void setTabIcon(int resId) {
        tabIcon.setImageResource(resId);
    }
    public void setTabIcon(Drawable drawable) {
        tabIcon.setImageDrawable(drawable);
        tabIcon.setVisibility(drawable!=null?View.VISIBLE:View.GONE);
    }
    public void setTabIcon(Bitmap drawable) {
        tabIcon.setImageBitmap(drawable);
        tabIcon.setVisibility(drawable!=null?View.VISIBLE:View.GONE);
    }
    public void setTabIcon(Uri drawable) {
        tabIcon.setImageURI(drawable);
        tabIcon.setVisibility(drawable!=null?View.VISIBLE:View.GONE);
    }

    public void setTabText(CharSequence text) {
        tabText.setText(text);
        tabText.setVisibility(TextUtils.isEmpty(text)?GONE:VISIBLE);
    }

    public void setTabSubtext(CharSequence text) {
        tabSubtext.setText(text);
        tabSubtext.setVisibility(TextUtils.isEmpty(text)?GONE:VISIBLE);
    }

    public void setTabCount(int count) {
        if(count>0){
            tabCount.setText(String.valueOf(count));
            tabCount.setVisibility(VISIBLE);
        }
        else{
            tabCount.setText(null);
            tabCount.setVisibility(GONE);
        }
    }

    public ImageView getTabIcon() {
        return tabIcon;
    }

    public TextView getTabText() {
        return tabText;
    }

    public TextView getTabSubtext() {
        return tabSubtext;
    }

    public TextView getTabCount() {
        return tabCount;
    }
}
