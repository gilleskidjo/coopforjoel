package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.CartItem;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ListDataHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ProductSale;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ProduitPanier;
import com.google.gson.Gson;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by joel on 04/11/2016.
 */
public class Activity_Panier extends BaseLanguageActivity  {
    List<ProduitPanier> itemList=new ArrayList<ProduitPanier>();
    Toolbar toolbar;
    ListView listView;
    TextView total;
    MyAdapteur adapteur;
    Button btn;
    boolean is_fr;
    View v;
   String val="";
    final String action2[]=new String[2];
    String title,sms;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.boutique_primary_dark));
        }

        setContentView(R.layout.activity_panier);
        action2[0]=getResources().getString(R.string.main_action_contacter1);
        action2[1]=getResources().getString(R.string.main_action_sms1);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        toolbar=(Toolbar)findViewById(R.id.panier_toolbar);
        listView=(ListView)findViewById(R.id.panier_listview);
        v=LayoutInflater.from(this).inflate(R.layout.list_footer,null);
        btn=(Button)v.findViewById(R.id.panier_vider);
        listView.addFooterView(v);

        setSupportActionBar(toolbar);


        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


           if(getIntent()!=null)
           {
             Bundle bundle=getIntent().getExtras();
               if(bundle!=null)
               {
             String  num=bundle.getString("num");
           title=bundle.getString("name");
             String re=bundle.getString("reduction");
             String price=bundle.getString("prix");
                val=bundle.getString("val");
          ProduitPanier p=new ProduitPanier(title,price,re,num);
          itemList.add(p);
               }
           }

        adapteur=new MyAdapteur(this,itemList);
        listView.setAdapter(adapteur);
            if(val.equals("2"))
            {
                    if(is_fr)
                    {
                     btn.setText("Louer".toUpperCase());
                    }
                    else
                    {

                     btn.setText("Rent".toUpperCase());
                    }

            }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProduitPanier article=itemList.get(0);

                final String num=article.getNumero();
                sms="Bonjour! \n Je suis intéressé(e) par votre annonce "+title.toUpperCase()+" postée sur Cooperations office. \n Merci de me renseigner davantage.";
                if(is_fr==false){
                    sms="Hello! \n" + " I am interested in your property "+title.toUpperCase()+" posted on Cooperations office.\n" + "Thank you for informing me more.";
                }
                AlertDialog.Builder builder=new AlertDialog.Builder(Activity_Panier.this);
                builder.setItems(action2, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which)
                        {
                            case 0:
                                Intent intent1=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+num));

                                if(intent1.resolveActivity(getPackageManager())!=null)
                                {
                                    startActivity(Intent.createChooser(intent1,getResources().getString(R.string.une_activity_appelez_nous)));
                                }
                                else
                                {
                                    Toast.makeText(Activity_Panier.this,getResources().getString(R.string.une_activity_installer_app_telephonie),Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case 1:
                                Intent intent2 = new Intent(Intent.ACTION_SENDTO);
                                intent2.addCategory(Intent.CATEGORY_DEFAULT);
                                intent2.setType("vnd.android-dir/mms-sms");
                                intent2.setData(Uri.parse("sms:" +num));
                                intent2.putExtra("sms_body", sms);

                                if(intent2.resolveActivity(getPackageManager())!=null)
                                {
                                    startActivity(intent2);
                                }
                                else
                                {
                                    Toast.makeText(Activity_Panier.this,getResources().getString(R.string.une_activity_installez_app_sms),Toast.LENGTH_SHORT).show();
                                }
                                break;


                        }

                    }
                }) ;
                builder.show();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       getMenuInflater().inflate(R.menu.card_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            case  R.id.autre_share:
                Intent intent2=new Intent(Intent.ACTION_SEND);
                intent2.setType("text/plain");
                String url= "https://play.google.com/store/apps/details?id=com.benin.apkapka.cooperationsoffice.cooperationoffice";
                intent2.putExtra(Intent.EXTRA_TEXT, url);
                intent2.putExtra(Intent.EXTRA_SUBJECT, "Cooperations office");
                if(intent2.resolveActivity(getPackageManager())!=null)
                {

                    String[] blacklist = new String[]{"com.google.android.gm","com.facebook.katana" };
                    startActivity(intent2);
                }
                else
                {
                    MyToast.show(this, getResources().getString(R.string.une_activity_installer_app_partage), false);
                }
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
    private Intent generateCustomChooserIntent(Intent prototype, String[] forbiddenChoices) {
        List<Intent> targetedShareIntents = new ArrayList<>();
        List<HashMap<String, String>> intentMetaInfo = new ArrayList<HashMap<String, String>>();
        Intent chooserIntent;

        Intent dummy = new Intent(prototype.getAction());
        dummy.setType(prototype.getType());
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(dummy, 0);

        if (!resInfo.isEmpty()) {
            for (ResolveInfo resolveInfo : resInfo) {
                if (resolveInfo.activityInfo == null || Arrays.asList(forbiddenChoices).contains(resolveInfo.activityInfo.packageName))
                    continue;

                HashMap<String, String> info = new HashMap<String, String>();
                info.put("packageName", resolveInfo.activityInfo.packageName);
                info.put("className", resolveInfo.activityInfo.name);
                info.put("simpleName", String.valueOf(resolveInfo.activityInfo.loadLabel(getPackageManager())));
                intentMetaInfo.add(info);
            }

            if (!intentMetaInfo.isEmpty()) {
                // sorting for nice readability
                Collections.sort(intentMetaInfo, new Comparator<HashMap<String, String>>() {
                    @Override
                    public int compare(HashMap<String, String> map, HashMap<String, String> map2) {
                        return map.get("simpleName").compareTo(map2.get("simpleName"));
                    }
                });

                // create the custom intent list
                for (HashMap<String, String> metaInfo : intentMetaInfo) {
                    Intent targetedShareIntent = (Intent) prototype.clone();
                    targetedShareIntent.setPackage(metaInfo.get("packageName"));
                    targetedShareIntent.setClassName(metaInfo.get("packageName"), metaInfo.get("className"));
                    targetedShareIntents.add(targetedShareIntent);
                }

                chooserIntent = Intent.createChooser(targetedShareIntents.remove(targetedShareIntents.size() - 1), getResources().getString(R.string.une_activity_partager_via));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
                return chooserIntent;
            }
        }

        return Intent.createChooser(prototype,getResources().getString(R.string.une_activity_partager_via));
    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    class  MyAdapteur extends BaseAdapter
    {


    List<ProduitPanier> list=new ArrayList<ProduitPanier>();
    Context c;
        public MyAdapteur(Context c,List<ProduitPanier> list)
        {
            this.c=c;
            this.list=list;

        }
        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }



        class ViewHolderHeader
        {
           TextView title;
            TextView price;

        }
        class  ViewHolderContent
        {
            TextView title;
            TextView price;

        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {


            ViewHolderContent holderContent=null;
            ViewHolderHeader holderHeader=null;
                if(convertView==null)
                {
                    LayoutInflater inflater=LayoutInflater.from(c);
                    convertView=inflater.inflate(R.layout.panier_item_list,null);
                    holderContent=new ViewHolderContent();
                    holderContent.title=(TextView) convertView.findViewById(R.id.panier_name);
                    holderContent.price=(TextView)convertView.findViewById(R.id.panier_price);
                    convertView.setTag(holderContent);



                }
                else
                {
                    holderContent=(ViewHolderContent)convertView.getTag();


                }
            ProduitPanier item=list.get(position);
            holderContent.title.setText(item.getTitle());

            BigDecimal taux=new BigDecimal(589.7833);
            BigDecimal reduction=new BigDecimal(item.getReduction());
            BigDecimal price=new BigDecimal(item.getPrice());
            BigDecimal re=new BigDecimal(0);

                    if (is_fr)
                    {

                        if (reduction.compareTo(BigDecimal.ZERO)==0)
                        {
                            re=price.setScale(2,BigDecimal.ROUND_HALF_UP);

                        }
                        else
                        {

                            re=price.subtract((price.multiply(reduction).divide( new BigDecimal(100))));
                            re=re.setScale(2,BigDecimal.ROUND_HALF_UP);

                        }
                        String s=re.toPlainString();
                        s=s.replace(".",",");
                        holderContent.price.setText(s+" FCFA");
                    }
                    else
                    {

                        if (reduction.compareTo(BigDecimal.ZERO)==0)
                        {
                            re=price.divide(taux,0,BigDecimal.ROUND_HALF_UP);
                            String i=re.toString();
                            int val1=(int)Double.parseDouble(i);
                            String s=""+val1;
                            holderContent.price.setText(s+" $");

                        }
                        else
                        {
                            re=price.divide(taux,2,BigDecimal.ROUND_HALF_UP);
                            re=re.subtract((re.multiply(reduction).divide( new BigDecimal(100))));
                            re=re.setScale(2,BigDecimal.ROUND_HALF_UP);
                            String b=re.toPlainString();
                            holderContent.price.setText(b+" $");

                        }


                    }


            return convertView;
        }
    }


}
