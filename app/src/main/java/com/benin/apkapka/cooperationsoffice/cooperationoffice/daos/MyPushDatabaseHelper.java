package com.benin.apkapka.cooperationsoffice.cooperationoffice.daos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by joel on 12/08/2017.
 */
public class MyPushDatabaseHelper  extends SQLiteOpenHelper {
    public static final String ROOT="cooperationofficedatabase";
    public static final class MYPUSH_HELPER_ENTREY implements BaseColumns{
        public static final String TABLE_NAME="cooperationofficepushdb";
        public static final String COLUMN_TYPE="ctype";
        public static final String COLUMN_CATEGORY="ccategory";
        public static final String COLUMN_FAMILLE="cfamille";
        public static final String COLUMN_REF="cref";
    }
    public static final String CREATE_TABLE="CREATE TABLE "+MYPUSH_HELPER_ENTREY.TABLE_NAME+
            "("+MYPUSH_HELPER_ENTREY._ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+MYPUSH_HELPER_ENTREY.COLUMN_TYPE+
            " TEXT , "+MYPUSH_HELPER_ENTREY.COLUMN_CATEGORY+" TEXT , "+MYPUSH_HELPER_ENTREY.COLUMN_FAMILLE+
            " TEXT, "+MYPUSH_HELPER_ENTREY.COLUMN_REF+" TEXT )";
    public static final String DELETE_TABLE=" DROP TABLE IF EXISTS "+MYPUSH_HELPER_ENTREY.TABLE_NAME;
    public static final int VERSION=2;
    public static final String DB_NAME=""+ROOT+"push_db.db";
    public  MyPushDatabaseHelper(Context c){
        super(c,DB_NAME,null,VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
   db.execSQL(DELETE_TABLE);
     onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
        onUpgrade(db,oldVersion,newVersion);
    }
}
