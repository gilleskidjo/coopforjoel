package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.HelperActivity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class Activity_Publish_Action extends AppCompatActivity implements View.OnClickListener {



    Button share;
    ShareDialog shareDialog;
    CallbackManager callbackManager;
    Toolbar toolbar;
    TextView tvFamilleName,tvTitre,tvDesc,tvFamilleCount,tvShare,tvPublish,tvEdit,tvHide;
    DinamicImageView dinamicImageView;
    long id;
    View familleContainer;
    String etat="attente";
    String user_share="";
    private  boolean is_fr=false;
    Produit produit;
    AlertDialog.Builder  builder;
    AlertDialog dialogPro;
    android.app.AlertDialog mAlertDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manageFacebook();
        setContentView(R.layout.publish_post_action_activity_layout);
        toolbar=(Toolbar)findViewById(R.id.publish_post_layout_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar =getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        dinamicImageView=(DinamicImageView)findViewById(R.id.publish_item_layout_image);
        tvFamilleName=(TextView)findViewById(R.id.famille_name);
        tvTitre=(TextView)findViewById(R.id.publish_item_layout_title);
        tvDesc=(TextView)findViewById(R.id.publish_item_layout_description);
        tvFamilleCount=(TextView)findViewById(R.id.famille_count);
        tvShare=(TextView)findViewById(R.id.publish_action_share);
        tvPublish=(TextView)findViewById(R.id.publish_action_publish);
        tvHide=(TextView)findViewById(R.id.publish_action_hide);

        tvEdit=(TextView)findViewById(R.id.publish_action_edit);
        familleContainer=findViewById(R.id.famille_container);
        familleContainer.setVisibility(View.INVISIBLE);
        tvShare.setOnClickListener(this);
        tvPublish.setOnClickListener(this);

        tvEdit.setOnClickListener(this);
        tvHide.setOnClickListener(this);
        tvEdit.setText(is_fr?"Modifier la publication":"Edit the publication");

          if(getIntent().getExtras()!=null){
              Bundle bundle=getIntent().getExtras();
              id=bundle.getLong("id");
              getAnnonce(id);
              updateShareState(id);
          }
        builder= new AlertDialog.Builder(Activity_Publish_Action.this);
        builder.setMessage(is_fr?"Cliquer sur publier pour rendre visible votre publication sur l'application":"Click on publish to make your publication visible on the application");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialogPro != null) {
                    dialogPro.dismiss();
                }


            }
        });
        dialogPro=builder.show();


    }

    @Override
    public void onClick(View v) {
           switch (v.getId()){
               case R.id.publish_action_share:
                   startbookShare();
                   break;
               case R.id.publish_action_publish:
                      if(user_share!=null){
                          if( user_share.equalsIgnoreCase("") || user_share.equalsIgnoreCase("non")){
                              builder= new AlertDialog.Builder(Activity_Publish_Action.this);
                              builder.setMessage(is_fr?"Pour rendre visible cette publication sur l'application, vous devez la partager avec vos amis":"To make this post visible on the app, you have to share it with your friends");
                              builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                  @Override
                                  public void onClick(DialogInterface dialog, int which) {
                                      if (dialogPro != null) {
                                          dialogPro.dismiss();
                                      }
                                  }
                              });
                              dialogPro=builder.show();
                          }
                          else{
                              if(etat!=null && !etat.isEmpty()){
                                  if(etat.equalsIgnoreCase("attente")){
                                      if(id!=0){
                                          updateAnnonceState(id,"publier");
                                      }
                                  }
                              }

                          }
                      }
                   break;
               case R.id.publish_action_hide:
                   if(etat!=null && !etat.isEmpty()){
                       if(etat.equalsIgnoreCase("publier")){
                           if(id!=0){
                               updateAnnonceState(id,"attente");
                           }
                       }
                   }

                   break;
              /* case R.id.publish_action_delete:
                  if(id!=0){
                      deletePublishAnnonce(id);
                  }
                   break;*/
               case R.id.publish_action_edit:
                   editPublication();
                   break;
           }
    }

    private void editPublication(){
        if(produit!=null){
            Intent intent=new Intent();
            Produit   p=produit;
            String type=p.getType();
            if(type.equals("actualite"))
            {
                intent=new Intent(this,Formulaire_actualite.class);
            }
            else if(type.equals("boutique")){
                intent =new Intent(this,Formulaire_boutique.class);
            }
            else if(type.equals("emploi")){
                intent =new Intent(this,Formulaire_emploi.class);
            }
            else if(type.equals("competence")){
                intent =new Intent(this,Formulaire_emploi.class);
            }
            else if(type.equals("bourse")){
                intent =new Intent(this,Formulaire_bourse.class);
            }
            else if(type.equals("annuaire")){

                intent =new Intent(this,Formulaire_annuaire.class);
            }
            else if(type.equals("service")){
                intent =new Intent(this,Formulaire_service.class);
            }
            else if(type.equals("location")){
                intent =new Intent(this,Formulaire_location.class);
            }
            else if(type.equals("partenariat")){
                intent =new Intent(this,Formulaire_partenariat.class);
            }
            else if(type.equals("projet")){
                intent =new Intent(this,Formulaire_Projet.class);
            }
            intent.putExtra("id",p.getId());
            intent.putExtra("category",p.getCategory());
            intent.putExtra("type",p.getType());
            intent.putExtra("title",p.getTitle());
            intent.putExtra("description",p.getExtend_proprety2());
            intent.putExtra("price",p.getNb_price().toString());
            intent.putExtra("reduction",p.getReduction());
            intent.putExtra("famille",p.getFamille());
            intent.putExtra("url",p.getUrl());
            intent.putExtra("edit","edit");

            startActivity(intent);
        }
        else{
            AlertDialog.Builder builder=new AlertDialog.Builder(this);
            builder.setMessage(is_fr?"Le contenu n'est pas encore coomplètement récupéré":"Content is not yet fully recovered");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (dialogPro != null) {
                        dialogPro.dismiss();
                    }

                }
            });
            dialogPro=builder.show();

        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        super.onBackPressed();
    }

    public void getAnnonce(long id){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.get_publish_annonce(""+id);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");

                            if (error) {

                            } else {

                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {



                                } else {

                                    List<Produit> list=ParseJson.parseProduit(o,getApplicationContext());
                                       if(list!=null && list.size()>0){
                                        produit=list.get(0);
                                           familleContainer.setVisibility(View.VISIBLE);
                                           tvFamilleName.setText(produit.getFamille());
                                           tvTitre.setText(produit.getTitle());
                                           tvDesc.setText(produit.getExtend_proprety2());
                                           if(produit.getNb_vue_famille()!=null && !produit.getNb_vue_famille().trim().equalsIgnoreCase("") && !produit.getNb_vue_famille().equalsIgnoreCase("null"))
                                           {
                                               tvFamilleCount.setText(produit.getNb_vue_famille());
                                           }
                                           else{
                                               tvFamilleCount.setText("0");
                                           }
                                           final  float scale=getResources().getDisplayMetrics().density;

                                           JSONArray   produits=o.getJSONArray("produits");
                                           JSONObject  p=produits.getJSONObject(0);
                                           etat=p.getString("etat");
                                           user_share=p.getString("user_share");

                                           if(etat.equalsIgnoreCase("attente")){
                                               tvPublish.setText(is_fr?"Publier":"Publish");
                                               tvHide.setText(is_fr?"Mettre en attente":"Put on hold");;
                                               tvHide.setEnabled(false);
                                               tvPublish.setEnabled(true);
                                           }
                                           else{
                                               tvPublish.setText(is_fr?"Publier":"Publish");
                                               tvHide.setText(is_fr?"Mettre en attente":"Put on hold");;
                                               tvHide.setEnabled(true);
                                               tvPublish.setEnabled(false);

                                           }
                                           Picasso.with(Activity_Publish_Action.this).load(produit.getUrl()).resize((int)(120*scale+0.5f),(int)(100*scale+0.5f)).into(dinamicImageView);
                                       }
                                }

                            }

                        } catch (Exception e) {


                        }

                    }//fin is success,
                    else {



                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

            }
        });
    }

    public void updateAnnonceState(long id,String state){
        final android.app.AlertDialog dialog1=new SpotsDialog(this,R.style.style_spot_actualite);
       if(dialog1!=null){
           dialog1.show();
       }
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.update_publish_annonce(""+id,state);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");

                            if (error) {
                                if(dialog1!=null){
                                    dialog1.dismiss();
                                }


                            } else {

                                if(dialog1!=null){
                                    dialog1.dismiss();
                                }

                                boolean isVide = o.getBoolean("vide");

                                if (isVide) {


                                } else {
                                    if(etat!=null && !etat.isEmpty()){
                                        if(etat.equalsIgnoreCase("attente")){
                                            builder= new AlertDialog.Builder(Activity_Publish_Action.this);
                                            builder.setMessage(is_fr?"Vitrine publiée":"Published showcase");
                                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (dialogPro != null) {
                                                        dialogPro.dismiss();
                                                    }


                                                }
                                            });
                                            dialogPro=builder.show();
                                            etat="publier";


                                            tvPublish.setText(is_fr?"Publier":"Publish");
                                            tvHide.setText(is_fr?"Mettre en attente":"Put on hold");;
                                            tvHide.setEnabled(true);
                                            tvPublish.setEnabled(false);

                                        }
                                        else {
                                            builder= new AlertDialog.Builder(Activity_Publish_Action.this);
                                            builder.setMessage(is_fr?"Publication mise en attente":"Publication put on hold");
                                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (dialogPro != null) {
                                                        dialogPro.dismiss();
                                                    }


                                                }
                                            });
                                            dialogPro=builder.show();
                                            etat="attente";
                                            tvPublish.setText(is_fr?"Publier":"Publish");
                                            tvHide.setText(is_fr?"Mettre en attente":"Put on hold");;
                                            tvHide.setEnabled(false);
                                            tvPublish.setEnabled(true);

                                        }
                                    }
                                }//fin

                            }

                        } catch (Exception e) {
                            if(dialog1!=null){
                                dialog1.dismiss();
                            }


                        }

                    }//fin is success,
                    else {
                        if(dialog1!=null){
                            dialog1.dismiss();
                        }

                    }//fin else is success

                }
                if(dialog1!=null){
                    dialog1.dismiss();
                }
            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {
              if(dialog1!=null){
                  dialog1.dismiss();
              }
            }
        });
    }

    public void deletePublishAnnonce(long id){
        final android.app.AlertDialog dialog1=new SpotsDialog(this,R.style.style_spot_actualite);
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.delete_publish_annonce(""+id);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");

                            if (error) {



                            } else {


                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {




                                } else {

                                    dialog1.dismiss();
                                    builder= new AlertDialog.Builder(Activity_Publish_Action.this);
                                    builder.setMessage(is_fr?"Publication supprimée":"Publication deleted");
                                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (dialogPro != null) {
                                                dialogPro.dismiss();
                                            }
                                            setResult(RESULT_OK);
                                            finish();

                                        }
                                    });
                                    dialogPro=builder.show();

                                }

                            }


                        } catch (Exception e) {


                        }

                    }//fin is success,
                    else {



                    }//fin else is success

                }

                dialog1.dismiss();

            }

            @Override
            public void onFailure(Call<String> call1 ,Throwable t) {
                dialog1.dismiss();
                if(!isFinishing())
                {


                }
            }
        });
    }


    public void updateShareState(long id){

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.updateAnnonceUserShareState(""+id);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");

                            if (error) {

                            } else {


                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {

                                } else {
                                    user_share="oui";

                                }

                            }

                        } catch (Exception e) {


                        }

                    }//fin is success,
                    else {



                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

                if(!isFinishing())
                {


                }
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                setResult(RESULT_OK);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void manageFacebook()
    {
        callbackManager= CallbackManager.Factory.create();
        shareDialog=new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                if(user_share.equalsIgnoreCase("") || user_share.equalsIgnoreCase("non")){
                     if(id!=0){
                         Toast.makeText(Activity_Publish_Action.this,is_fr?"Publication partagée":"Shared Publication",Toast.LENGTH_LONG).show();
                         updateShareState(id);
                     }
                }
            }

            @Override
            public void onCancel() {


            }

            @Override
            public void onError(FacebookException error) {

                Toast.makeText(Activity_Publish_Action.this,"Erreur lors du  partage",Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void startbookShare()
    {
        SessionManager sessionManager=new SessionManager(this);
        User user=sessionManager.getUser(this);
         mAlertDialog=new SpotsDialog(this,R.style.style_spot_actualite);
        mAlertDialog.show();
        Uri.Builder builder=Uri.parse(PushHandler.pushBaseUlr).buildUpon();

        String type=produit.getType();
        if(type.equals("actualite"))
        {
            builder.appendPath("actualite");
        }
        else if(type.equals("boutique")){
            builder.appendPath("boutique");
        }
        else if(type.equals("emploi")){
            builder.appendPath("emploi");
        }
        else if(type.equals("competence")){
            builder.appendPath("competence");
        }
        else if(type.equals("bourse")){
            builder.appendPath("bourse");
        }
        else if(type.equals("annuaire")){

            builder.appendPath("annuaire");
        }
        else if(type.equals("service")){
            builder.appendPath("service");
        }
        else if(type.equals("location")){
            builder.appendPath("location");
        }
        else if(type.equals("partenariat")){
            builder.appendPath("partenariat");
        }
        else if(type.equals("projet")){
            builder.appendPath("projet");
        }
        builder.appendQueryParameter("id",""+produit.getId());
        builder.appendQueryParameter("famille",produit.getFamille());
        builder.appendQueryParameter("type",produit.getType());
        builder.appendQueryParameter("category",produit.getCategory());
        Uri uri=builder.build();
        Task<ShortDynamicLink> shortDynamicLinkTask= FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(uri)
                .setDynamicLinkDomain("cooperations0ffices.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.benin.apkapka.cooperationsoffice.cooperationoffice")
                        .setFallbackUrl(Uri.parse("http://cooperationsoffice.net"))
                        .build())
                .setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder()
                        .setTitle(produit.getTitle())
                        .setDescription(is_fr?"CLIQUEZ".toUpperCase()+" pour "+"VISITER".toUpperCase()+" et "+"PARTAGER".toUpperCase()+" notre publication ":"CLICK".toUpperCase()+" to "+"VISIT".toUpperCase()+" and "+"SHARE".toUpperCase()+" our publication")
                        .setImageUrl(Uri.parse(""+produit.getUrl()))
                        .build())
                .buildShortDynamicLink()

                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        mAlertDialog.dismiss();
                        if(task.isSuccessful()){

                            Uri uri=task.getResult().getShortLink();
                            ShareLinkContent linkContent=new ShareLinkContent.Builder()
                                    .setContentUrl(uri)
                                    .build();
                            if(shareDialog.canShow(ShareLinkContent.class)){
                                shareDialog.show(linkContent);
                            }
                        }
                        else {

                           android.app.AlertDialog.Builder mBuilder1=new android.app.AlertDialog.Builder(Activity_Publish_Action.this)
                                    .setMessage(is_fr?"Impossible de publier l'image":"Can not publish image")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if(mAlertDialog!=null){
                                                mAlertDialog.dismiss();
                                            }

                                        }
                                    });
                            mAlertDialog=mBuilder1.show();
                        }

                    }
                });
    }

}
