package com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Annuaire;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Article;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Bourse;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Competence;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Emploi;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ListDataHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Partenariat;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Projet;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Service;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by joel on 09/09/2016.
 */
public class BusEventFragmentNetwokRequest1    extends Fragment{


        public  static  final  String TAG="com.cooperation.office.fragemn_networkbus1";

        private boolean hasRequest=false;

    public boolean isHasRequest() {
        return hasRequest;
    }

    public void setHasRequest(boolean hasRequest) {
        this.hasRequest = hasRequest;
    }
    public boolean isRealm;

    public void setRealm(boolean realm) {
        isRealm = realm;
    }

    public boolean isRealm() {
        return isRealm;
    }

    private boolean isRequestDo=false;
        private boolean isMessage=false;
        private String categorie;
        List<Produit> listproduit=new ArrayList<>();
        List<Article>  listeArticle=new ArrayList<>();
        List<Annuaire> listAnnuaire=new ArrayList<>();
        List<Bourse> listeBourse=new ArrayList<>();
        List<Competence> listCompetence=new ArrayList<>();
        List<Emploi>listEmploi=new ArrayList<>();
        List<Projet> listProjet=new ArrayList<>();
        List<Service> listeService=new ArrayList<>();
        List<Partenariat> listPartenariat=new ArrayList<>();
        Produit  produit;
        Article article;
        Annuaire annuaire;
        Bourse bourse;
        Competence competence;
        Emploi emploi;
        Projet  projet;
        Service  service;
        Partenariat partenariat;
        HandlerNetWorkRequestResponse handleResponse;
        private String sms="";
        RequestQueue mRequestQueue;
        boolean isEmpty;
        boolean isReady;

    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean ready) {
        isReady = ready;
    }

    public interface  HandlerNetWorkRequestResponse
        {

            public void onResposeSuccess();

            public  void onresponseFailAndPrintErrorResponse();

        }

        public void setArticle(Article article) {
            this.article = article;
        }

        public String getCategorie() {
            return categorie;
        }

        public void setCategorie(String categorie) {
            this.categorie = categorie;
        }

        public void setCompetence(Competence competence) {
            this.competence = competence;
        }

        public boolean isRequestDo() {
            return isRequestDo;
        }

        public boolean isMessage() {
            return isMessage;
        }

        public void setIsMessage(boolean isMessage) {
            this.isMessage = isMessage;
        }

        public String getSms() {
            return sms;
        }

    public boolean isEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }

    public  void setInterface(HandlerNetWorkRequestResponse handleResponse)
        {
            this.handleResponse=handleResponse;
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);


        }



        public void  doRequestProduit_Group_By_Famille(String category,String pays,String lang,String type,String zone,int page)
        {
            isEmpty=false;

            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(Config.GET_BASE_URL_RETROFIT)
                    .addConverterFactory(ScalarsConverterFactory.create())

                    .build();

            MyInterface2 service=retrofit.create(MyInterface2.class);

            Call<String> call=service.getAllProduct_Group_By_Category(category,pays,lang,type,zone,page);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                    String message = "";
                    if (response != null) {
                        if (response.isSuccessful()) {

                            try {
                                String rep = response.body();
                                JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                                boolean error = o.getBoolean("error");
                                if (error) {


                                    if(getActivity()!=null)
                                    {
                                        message=o.getString("message");
                                        sms=message;
                                        isMessage=true;
                                        if(handleResponse!=null)
                                        {
                                            handleResponse.onresponseFailAndPrintErrorResponse();
                                        }
                                    }

                                } else {

                                    boolean isVide = o.getBoolean("vide");
                                    if (isVide) {

                                        isEmpty=true;

                                        message = o.getString("message");
                                        sms = message;
                                        isMessage = true;
                                        if (handleResponse != null) {
                                            handleResponse.onresponseFailAndPrintErrorResponse();

                                        }

                                    } else {

                                        listproduit = ParseJson.parseProduit(o,getContext());
                                        if (handleResponse != null) {

                                            handleResponse.onResposeSuccess();
                                        }
                                    }

                                }

                            } catch (Exception e) {

                                if(getActivity()!=null)
                                {
                                    message=getResources().getString(R.string.error_frag_netw_parse);
                                    sms=message;
                                    isMessage=true;
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onresponseFailAndPrintErrorResponse();
                                    }
                                }
                            }

                        }//fin is success,
                        else {

                            if(getActivity()!=null)
                            {
                                message=getResources().getString(R.string.error_frag_netw_parse);
                                sms=message;
                                isMessage=true;
                                if(handleResponse!=null)
                                {
                                    handleResponse.onresponseFailAndPrintErrorResponse();
                                }
                            }
                        }//fin else is success

                    }

                    isRequestDo=true;
                }

                @Override
                public void onFailure(Call<String> call1,Throwable t) {

                    if(getActivity()!=null)
                    {
                        sms="";
                        sms=getResources().getString(R.string.error_frag_netw_error);
                        isMessage=true;
                        if(handleResponse!=null)
                        {
                            handleResponse.onresponseFailAndPrintErrorResponse();
                        }
                        isRequestDo = true;
                    }
                }
            });






        }

        public void  doRequestProduit_Search(String category,String pays,String word,String lang,String type,String zone)
        {

            isEmpty=false;
            Retrofit  retrofit=new Retrofit.Builder()
                    .baseUrl(Config.GET_BASE_URL_RETROFIT)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();

            MyInterface2 service=retrofit.create(MyInterface2.class);

            Call<String> call=service.getAllProduct_By_Word(category,pays,lang,word,type,zone);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                    String message = "";
                    if (response != null) {
                        if (response.isSuccessful()) {
                            try {
                                String rep = response.body();
                                JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                                boolean error = o.getBoolean("error");
                                if (error) {


                                    if(getActivity()!=null)
                                    {
                                        message=o.getString("message");
                                        sms=message;
                                        isMessage=true;
                                        if(handleResponse!=null)
                                        {
                                            handleResponse.onresponseFailAndPrintErrorResponse();
                                        }
                                    }
                                } else {
                                    boolean isVide = o.getBoolean("vide");
                                    if (isVide) {
                                        isEmpty=true;
                                        message = o.getString("message");
                                        sms = message;
                                        isMessage = true;
                                        if (handleResponse != null) {
                                            handleResponse.onresponseFailAndPrintErrorResponse();
                                        }

                                    } else {

                                        listproduit = ParseJson.parseProduit(o,getContext());
                                        if (handleResponse != null) {
                                            handleResponse.onResposeSuccess();
                                        }
                                    }

                                }

                            } catch (Exception e) {

                                if(getActivity()!=null)
                                {
                                    message=getResources().getString(R.string.error_frag_netw_parse);
                                    sms=message;
                                    isMessage=true;
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onresponseFailAndPrintErrorResponse();
                                    }
                                }
                            }

                        }//fin is success,
                        else {
                            if(getActivity()!=null)
                            {

                                message=getResources().getString(R.string.error_frag_netw_parse);
                                sms=message;
                                isMessage=true;
                                if(handleResponse!=null)
                                {
                                    handleResponse.onresponseFailAndPrintErrorResponse();
                                }
                            }
                        }//fin else is success

                    }

                    isRequestDo=true;
                }

                @Override
                public void onFailure(Call<String> call1,Throwable t) {
                    if(getActivity()!=null)
                    {
                        sms="";

                        sms=getResources().getString(R.string.error_frag_netw_error);
                        isMessage=true;
                        if(handleResponse!=null)
                        {
                            handleResponse.onresponseFailAndPrintErrorResponse();
                        }
                        isRequestDo = true;
                    }
                }
            });






        }


        public Partenariat getPartenariat() {
            return partenariat;
        }

        public void setPartenariat(Partenariat partenariat) {
            this.partenariat = partenariat;
        }

        public Produit getProduit() {
            return produit;
        }

        public void setProduit(Produit produit) {
            this.produit = produit;
        }

        public Article getArticle() {
            return article;
        }


        public void setAnnuaire(Annuaire annuaire) {
            this.annuaire = annuaire;
        }

        public Annuaire getAnnuaire() {
            return annuaire;
        }

        public Bourse getBourse() {
            return bourse;
        }

        public void setBourse(Bourse bourse) {
            this.bourse = bourse;
        }

        public List<Bourse> getListeBourse() {
            return listeBourse;
        }

        public List<Partenariat> getListPartenariat() {
            return listPartenariat;
        }



        public Competence getCompetence() {
            return competence;
        }



        public Emploi getEmploi() {
            return emploi;
        }

        public void setEmploi(Emploi emploi) {
            this.emploi = emploi;
        }

        public Projet getProjet() {
            return projet;
        }

        public void setProjet(Projet projet) {
            this.projet = projet;
        }

        public Service getService() {
            return service;
        }

        public void setService(Service service) {
            this.service = service;
        }




        public List<Competence> getListCompetence() {
            return listCompetence;
        }



        public List<Emploi> getListEmploi() {
            return listEmploi;
        }


        public List<Projet> getListProjet() {
            return listProjet;
        }


        public List<Service> getListeService() {
            return listeService;
        }




        public List<Produit> getListproduit() {
            return listproduit;
        }


        public List<Article> getListeArticle() {
            return listeArticle;
        }


        public List<Annuaire> getListAnnuaire() {
            return listAnnuaire;
        }



        @Override
        public void onDestroy() {
            super.onDestroy();


        }


}
