package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by joel on 04/12/2017.
 */
public class FragmentForNetworkSearchByTitleAndType extends Fragment {

    public  static  final  String TAG="com.cooperation.office.fragemnr.for_network_seach_by_title_and_type";
    private boolean isRequestDo=false;
    private boolean isMessage=false;
    List<Produit> listproduit=new ArrayList<>();
    Produit  produit;
    List<String> listTitle=new ArrayList<>();
    HandlerNetWorkRequestResponseHomeSearch handleResponse;
    String query="";
    String query2="";
    public interface  HandlerNetWorkRequestResponseHomeSearch
    {
        public void onResposeSuccessSearch(String s);

        public  void onresponseSearchFailAndPrintErrorResponse();

    }
    public boolean isRequestDo() {
        return isRequestDo;
    }

    public void setListTitle(List<String> listTitle) {
        this.listTitle = listTitle;
    }

    public List<String> getListTitle() {
        return listTitle;
    }

    public void setIsMessage(boolean isMessage) {
        this.isMessage = isMessage;
    }

    public List<Produit> getListproduit() {
        return listproduit;
    }

    public void setListproduit(List<Produit> listproduit) {
        this.listproduit = listproduit;
    }

    public  void setInterface(HandlerNetWorkRequestResponseHomeSearch handleResponse)
    {
        this.handleResponse=handleResponse;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);


    }




    public void  doRequestProduitSearch(final String word, final String lang,String zone)
    {

        query2=word;
        listTitle=new ArrayList<>();
        listproduit=new ArrayList<>();
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        MyInterface2 service=retrofit.create(MyInterface2.class);

        Call<String> call=service.getAllProduct_By_title(word,lang,zone);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {
                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {

                                if (getActivity()!=null)
                                {
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onresponseSearchFailAndPrintErrorResponse();
                                    }
                                }

                            } else {
                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {
                                    isMessage = true;
                                    if (handleResponse != null) {
                                        handleResponse.onresponseSearchFailAndPrintErrorResponse();
                                    }

                                } else {
                                    listproduit=new ArrayList<>();
                                    listproduit = ParseJson.parseProduitSearch(o,listTitle,lang,word,getContext());
                                    if (handleResponse != null ) {
                                        handleResponse.onResposeSuccessSearch(query2);
                                    }
                                }

                            }

                        } catch (Exception e) {
                            if (getActivity()!=null)
                            {
                                if(handleResponse!=null)
                                {
                                    handleResponse.onresponseSearchFailAndPrintErrorResponse();
                                }
                            }
                        }

                    }//fin is success,
                    else {
                        if (getActivity()!=null)
                        {
                            if(handleResponse!=null)
                            {
                                handleResponse.onresponseSearchFailAndPrintErrorResponse();
                            }
                        }
                    }//fin else is success

                }

                isRequestDo=true;
            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {
                if (getActivity()!=null)
                {
                    if(handleResponse!=null)
                    {
                        handleResponse.onresponseSearchFailAndPrintErrorResponse();
                    }
                    isRequestDo = true;
                }
            }
        });






    }
    public void  doRequestProduitSearchbytype(final String word, final String lang, String type, String category,String zone)
    {

        query=word;
        listTitle=new ArrayList<>();
        listproduit=new ArrayList<>();
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        MyInterface2 service=retrofit.create(MyInterface2.class);

        Call<String> call=service.getAllProduct_By_title2(word,type,category,lang,zone);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {
                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {

                                if (getActivity()!=null)
                                {
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onresponseSearchFailAndPrintErrorResponse();
                                    }
                                }

                            } else {
                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {
                                    isMessage = true;
                                    if (handleResponse != null) {
                                        handleResponse.onresponseSearchFailAndPrintErrorResponse();
                                    }

                                } else {
                                   listproduit=new ArrayList<>();
                                    listproduit = ParseJson.parseProduitSearch(o,listTitle,lang,word,getContext());
                                    if (handleResponse != null ) {
                                        handleResponse.onResposeSuccessSearch(query);
                                    }
                                }

                            }

                        } catch (Exception e) {
                            if (getActivity()!=null)
                            {
                                if(handleResponse!=null)
                                {
                                    handleResponse.onresponseSearchFailAndPrintErrorResponse();
                                }
                            }
                        }

                    }//fin is success,
                    else {
                        if (getActivity()!=null)
                        {
                            if(handleResponse!=null)
                            {
                                handleResponse.onresponseSearchFailAndPrintErrorResponse();
                            }
                        }
                    }//fin else is success

                }

                isRequestDo=true;
            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {
                if (getActivity()!=null)
                {
                    if(handleResponse!=null)
                    {
                        handleResponse.onresponseSearchFailAndPrintErrorResponse();
                    }
                    isRequestDo = true;
                }
            }
        });






    }


}
