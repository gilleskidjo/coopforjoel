package com.benin.apkapka.cooperationsoffice.cooperationoffice.daos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by joel on 12/08/2017.
 */
public class MyPushDatabaseDao {
   protected MyPushDatabaseHelper helper;
   protected SQLiteDatabase db;

    public MyPushDatabaseDao(Context c){
        if(helper==null){
           helper=new MyPushDatabaseHelper(c);
        }
        open();
    }
    public void open(){
        if(helper!=null){
            db=helper.getWritableDatabase();
        }
    }

    public void close(){
        if(helper!=null){
            helper.close();
        }
    }


}
