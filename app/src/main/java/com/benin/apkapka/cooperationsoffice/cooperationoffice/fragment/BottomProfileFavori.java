package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.SearchActivity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.UnAnnuaire;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Un_emploi_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Un_partenariat;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Un_produit_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Un_projet_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Un_service_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Une_actualite_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Une_bourse_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Une_competence_annuaire_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Une_location_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.BottomProfilInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushFamille;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.EndlessRecyclerViewScrollListener;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Favorite;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.HelperActivity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SaveFavorite;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.query.Query;
import io.objectbox.query.QueryBuilder;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class BottomProfileFavori extends Fragment implements BottomProfilInterface {


    RecyclerView recyclerView;
    ProgressBar progressBar;
    View linearVide;
    TextView tvVide;
    boolean is_small,is_medium,is_large;
    private boolean is_fr;
    List<Produit>produits=new ArrayList<>();
    MyAdatteurBottomProfileFavori myAdatteurBottomProfileFavori;
    Box<Favorite> favoriteBox;
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    LinearLayoutManager manager;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View  v=inflater.inflate(R.layout.bottom_profil_layout,container,false);
        recyclerView=(RecyclerView)v.findViewById(R.id.bottom_profil_recycleView);
        progressBar=(ProgressBar)v.findViewById(R.id.bottom_profil_progressBar2);
        linearVide=v.findViewById(R.id.bottom_profil_linearvide);
        tvVide=(TextView)v.findViewById(R.id.bottom_profil_textview_vide);
        recyclerView.setVisibility(View.GONE);
        linearVide.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        is_small=getResources().getBoolean(R.bool.is_small);
        is_medium=getResources().getBoolean(R.bool.is_medium);
        is_large=getResources().getBoolean(R.bool.is_large);
        myAdatteurBottomProfileFavori=new MyAdatteurBottomProfileFavori(getActivity(),produits,this);
         manager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(myAdatteurBottomProfileFavori);
        endlessRecyclerViewScrollListener=new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(page);
            }
        };
        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
        favoriteBox =((MyApplication)getActivity().getApplicationContext()).getBoxStore().boxFor(Favorite.class);


         if(isConnected()){
          loadNextDataFromApi(0);
         }
         else {
             HandleNoConnexion();
         }
        return v;
    }

    public void loadNextDataFromApi(int offset) {
        // Send an API request to retrieve appropriate paginated data
        //  --> Send the request including an offset value (i.e `page`) as a query parameter.
        //  --> Deserialize and construct new model objects from the API response
        //  --> Append the new data objects to the existing set of items inside the array of items
        //  --> Notify the adapter of the new items made with `notifyItemRangeInserted()
        //
        SessionManager sessionManager=new SessionManager(getActivity());
        User user=sessionManager.getUser(getActivity());
        String zone="";
        zone=user.getPays();
        if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
            zone="france";
        }
        FetchFavori(user.getId(),zone,offset);
    }
    private void HandleNoConnexion(){
        final Gson gson=new Gson();
        QueryBuilder<Favorite> builder1=favoriteBox.query();
        Query<Favorite> query1=builder1.build();
        Favorite favorite=query1.findFirst();
        if(favorite!=null){
            String val=favorite.mfavorite;
            if(!val.equalsIgnoreCase("") && val!=null){
                Type type=new TypeToken<List<Produit>>(){}.getType();
                produits=gson.fromJson(val,type);
                if(produits!=null){

                    if(produits.size()>0){
                        recyclerView.setVisibility(View.VISIBLE);
                        linearVide.setVisibility(View.GONE);
                        tvVide.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        myAdatteurBottomProfileFavori=new MyAdatteurBottomProfileFavori(getActivity(),produits,BottomProfileFavori.this);
                        recyclerView.setAdapter(myAdatteurBottomProfileFavori);
                    }
                    else{
                        recyclerView.setVisibility(View.GONE);
                        linearVide.setVisibility(View.VISIBLE);
                        tvVide.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        tvVide.setText(is_fr?"Enregistrez de nouveaux favoris":"Save new favorites");
                    }
                }
                else {
                    recyclerView.setVisibility(View.GONE);
                    linearVide.setVisibility(View.VISIBLE);
                    tvVide.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    tvVide.setText(is_fr?"Impossible de récupérer les favoris":"Can not retrieve favorites");
                }

            }
            else{
                recyclerView.setVisibility(View.GONE);
                linearVide.setVisibility(View.VISIBLE);
                tvVide.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                tvVide.setText(is_fr?"Impossible de récupérer les favoris":"Can not retrieve favorites");
            }

        }
        else{
            recyclerView.setVisibility(View.GONE);
            linearVide.setVisibility(View.VISIBLE);
            tvVide.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            tvVide.setText(is_fr?"Impossible de récupérer les favoris":"Can not retrieve favorites");
        }
    }

    private void saveLocalFavorie(List<Produit> listP){
        List<Produit> favoritesList=new ArrayList<>();
        List<Long> saveFavorites=new ArrayList<>();
        Box<SaveFavorite>saveFavoriteBox=((MyApplication)getActivity().getApplicationContext()).getBoxStore().boxFor(SaveFavorite.class);
        Box<Favorite> favoriteBox =((MyApplication)getActivity().getApplicationContext()).getBoxStore().boxFor(Favorite.class);

        Gson  gson=new Gson();
        QueryBuilder<SaveFavorite> builder=saveFavoriteBox.query();
        Query<SaveFavorite> query=builder.build();
        SaveFavorite unSaveFavorite=query.findFirst();
        if(unSaveFavorite!=null){
            String val=unSaveFavorite.saveFavorite;
            if(!val.equalsIgnoreCase("") && val!=null){
                Type type=new TypeToken<List<Long>>(){}.getType();
                saveFavorites=gson.fromJson(val,type);

            }
            else{
                saveFavorites=new ArrayList<>();
            }

        }
        else{
            saveFavorites=new ArrayList<>();
        }
        QueryBuilder<Favorite> builder1=favoriteBox.query();
        Query<Favorite> query1=builder1.build();
        Favorite favorite=query1.findFirst();
        if(favorite!=null){
            String val=favorite.mfavorite;
            if(!val.equalsIgnoreCase("") && val!=null){
                Type type=new TypeToken<List<Produit>>(){}.getType();
                favoritesList=gson.fromJson(val,type);

            }
            else{
                favoritesList=new ArrayList<>();
            }

        }
        else{
            favoritesList=new ArrayList<>();
        }

        for (int i=0;i<listP.size();i++) {
            favoritesList.add(listP.get(i));
            saveFavorites.add(listP.get(i).getId());
            if(!favoritesList.contains(listP.get(i))){
                favoritesList.add(listP.get(i));
                saveFavorites.add(listP.get(i).getId());
            }
        }
        if(unSaveFavorite!=null){
            Type type=new TypeToken<List<Long>>(){}.getType();
            String UnfavoriteSaved=gson.toJson(saveFavorites,type);
            unSaveFavorite.saveFavorite=UnfavoriteSaved;
            saveFavoriteBox.put(unSaveFavorite);
        }
        if(favorite!=null){
            Type type=new TypeToken<List<Produit>>(){}.getType();
            String favoriteSaved=gson.toJson(favoritesList,type);
            favorite.mfavorite=favoriteSaved;
            favoriteBox.put(favorite);
        }

    }

    public void FetchFavori(long id,String zone,int page){
       final Gson gson=new Gson();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.get_All_Favori(""+id,zone,page);

        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {

                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {
                                Log.e("Error",o.toString());

                                recyclerView.setVisibility(View.GONE);
                                linearVide.setVisibility(View.VISIBLE);
                                tvVide.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);
                                QueryBuilder<Favorite> builder1=favoriteBox.query();
                                Query<Favorite> query1=builder1.build();
                                Favorite favorite=query1.findFirst();
                                if(favorite!=null){
                                    String val=favorite.mfavorite;
                                    if(!val.equalsIgnoreCase("") && val!=null){
                                        Type type=new TypeToken<List<Produit>>(){}.getType();
                                        produits=gson.fromJson(val,type);
                                        if(produits!=null){
                                            if(produits.size()>0){
                                                recyclerView.setVisibility(View.VISIBLE);
                                                linearVide.setVisibility(View.GONE);
                                                tvVide.setVisibility(View.GONE);
                                                progressBar.setVisibility(View.GONE);
                                                List<Produit> mList=new ArrayList<>(produits);
                                                Collections.reverse(mList);
                                                myAdatteurBottomProfileFavori=new MyAdatteurBottomProfileFavori(getActivity(),mList,BottomProfileFavori.this);
                                                recyclerView.setAdapter(myAdatteurBottomProfileFavori);
                                            }
                                            else {
                                                recyclerView.setVisibility(View.GONE);
                                                linearVide.setVisibility(View.VISIBLE);
                                                tvVide.setVisibility(View.VISIBLE);
                                                progressBar.setVisibility(View.GONE);
                                                tvVide.setText(is_fr?"Enregistrez de nouveaux favoris":"Save new favorites");
                                            }
                                        }
                                        else{
                                            recyclerView.setVisibility(View.GONE);
                                            linearVide.setVisibility(View.VISIBLE);
                                            tvVide.setVisibility(View.VISIBLE);
                                            progressBar.setVisibility(View.GONE);
                                            tvVide.setText(is_fr?"Impossible de récupérer les favoris":"Can not retrieve favorites");
                                        }

                                    }
                                    else{
                                        recyclerView.setVisibility(View.GONE);
                                        linearVide.setVisibility(View.VISIBLE);
                                        tvVide.setVisibility(View.VISIBLE);
                                        progressBar.setVisibility(View.GONE);
                                        tvVide.setText(is_fr?"Impossible de récupérer les favoris":"Can not retrieve favorites");
                                    }

                                }
                                else{
                                    recyclerView.setVisibility(View.GONE);
                                    linearVide.setVisibility(View.VISIBLE);
                                    tvVide.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                    tvVide.setText(is_fr?"Impossible de récupérer les favoris":"Can not retrieve favorites");
                                }

                            } else {

                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {

                                    recyclerView.setVisibility(View.GONE);
                                    linearVide.setVisibility(View.VISIBLE);
                                    tvVide.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                    tvVide.setText(is_fr?"Enregistrez de nouveaux favoris":"Save new favorites");
                                } else {

                                    recyclerView.setVisibility(View.VISIBLE);
                                    linearVide.setVisibility(View.GONE);
                                    tvVide.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                    produits.addAll( ParseJson.parseProduit(o,getContext()));

                                     if(produits!=null && produits.size()>0 ){

                                     }
                                   myAdatteurBottomProfileFavori.notifyDataSetChanged();
                                }

                            }

                        } catch (Exception e) {

                            recyclerView.setVisibility(View.GONE);
                            linearVide.setVisibility(View.VISIBLE);
                            tvVide.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            tvVide.setText(is_fr?"Impossible de récupérer les favoris":"Can not retrieve favorites");
                        }

                    }//fin is success,
                    else {

                        recyclerView.setVisibility(View.GONE);
                        linearVide.setVisibility(View.VISIBLE);
                        tvVide.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        tvVide.setText(is_fr?"Impossible de récupérer les favoris":"Can not retrieve favorites");
                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

                QueryBuilder<Favorite> builder1=favoriteBox.query();
                Query<Favorite> query1=builder1.build();
                Favorite favorite=query1.findFirst();
                if(favorite!=null){
                    String val=favorite.mfavorite;
                    if(!val.equalsIgnoreCase("") && val!=null){
                        Type type=new TypeToken<List<Produit>>(){}.getType();
                        produits=gson.fromJson(val,type);
                        if(produits!=null){

                            if(produits.size()>0){
                                recyclerView.setVisibility(View.VISIBLE);
                                linearVide.setVisibility(View.GONE);
                                tvVide.setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);
                                myAdatteurBottomProfileFavori=new MyAdatteurBottomProfileFavori(getActivity(),produits,BottomProfileFavori.this);
                                recyclerView.setAdapter(myAdatteurBottomProfileFavori);
                            }
                            else{
                                recyclerView.setVisibility(View.GONE);
                                linearVide.setVisibility(View.VISIBLE);
                                tvVide.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);
                                tvVide.setText(is_fr?"Enregistrez de nouveaux favoris":"Save new favorites");
                            }
                        }
                        else {
                            recyclerView.setVisibility(View.GONE);
                            linearVide.setVisibility(View.VISIBLE);
                            tvVide.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            tvVide.setText(is_fr?"Impossible de récupérer les favoris":"Can not retrieve favorites");
                        }

                    }
                    else{
                        recyclerView.setVisibility(View.GONE);
                        linearVide.setVisibility(View.VISIBLE);
                        tvVide.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        tvVide.setText(is_fr?"Impossible de récupérer les favoris":"Can not retrieve favorites");
                    }

                }
                else{
                    recyclerView.setVisibility(View.GONE);
                    linearVide.setVisibility(View.VISIBLE);
                    tvVide.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    tvVide.setText(is_fr?"Impossible de récupérer les favoris":"Can not retrieve favorites");
                }
            }

        });
    }



    @Override
    public void onBottomProfilItemClick(int position) {
        Produit p=produits.get(position);
        HelperActivity.getInstance().setProduit(p);
        Intent intent=new Intent();
        String type=p.getType();
        if(type.equals("actualite"))
        {
            intent=new Intent(getContext(),Une_actualite_activity.class);
        }
        else if(type.equals("boutique")){
            intent =new Intent(getContext(),Un_produit_activity.class);
        }
        else if(type.equals("emploi")){
            intent =new Intent(getContext(),Un_emploi_activity.class);
        }
        else if(type.equals("competence")){
            intent =new Intent(getContext(),Une_competence_annuaire_activity.class);
        }
        else if(type.equals("bourse")){
            intent =new Intent(getContext(),Une_bourse_activity.class);
        }
        else if(type.equals("annuaire")){

            intent =new Intent(getContext(),UnAnnuaire.class);
        }
        else if(type.equals("service")){
            intent =new Intent(getContext(),Un_service_activity.class);
        }
        else if(type.equals("location")){
            intent =new Intent(getContext(),Une_location_activity.class);
        }
        else if(type.equals("partenariat")){
            intent =new Intent(getContext(),Un_partenariat.class);
        }
        else if(type.equals("projet")){
            intent =new Intent(getContext(),Un_projet_activity.class);
        }
        intent.putExtra("id",p.getId());

        intent.putExtra("lien_catalogue",p.getLien_catalogue());
        intent.putExtra("lien_description",p.getLien_description());
        intent.putExtra("choixf",p.getChoixf());
        intent.putExtra("url",p.getUrl());
        intent.putExtra("url2",p.getUrl2());
        intent.putExtra("url3",p.getUrl3());
        intent.putExtra("type",p.getType());
        intent.putExtra("famille",p.getFamille());
        intent.putExtra("pays",p.getPays());
        intent.putExtra("category",p.getCategory());
        intent.putExtra("type2",0);
        intent.putExtra("from_co_direct","oui");
        intent.putExtra("date",p.getDate());
        intent.putExtra("famille_en",p.getFamille_en());
        intent.putExtra("lien_fichier",p.getLien_fichier());
        startActivity(intent);
    }

    class MyAdatteurBottomProfileFavori  extends RecyclerView.Adapter<MyAdatteurBottomProfileFavori.MyBottomProfilFavoriViewholder> {

        List<Produit> listF = new ArrayList<>();
        BottomProfilInterface profilInterface;
        Produit p;
        Context c;

        public MyAdatteurBottomProfileFavori(Context c, List<Produit> listF, BottomProfilInterface bottomProfilInterface) {
            this.listF = listF;
            this.profilInterface = bottomProfilInterface;
            this.c = c;
        }


        class MyBottomProfilFavoriViewholder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView description, txtFamille;
            DinamicImageView image;
            TextView title;
            TextView tvCount;
            TextView tvDate;
            TextView tvFamilleCount;

            public MyBottomProfilFavoriViewholder(View v) {
                super(v);
                v.setOnClickListener(this);
                title = (TextView)v.findViewById(R.id.actualite_item_layout_title);
                image = (DinamicImageView)v.findViewById(R.id.actualite_item_layout_image);
                tvCount = (TextView)v.findViewById(R.id.actualite_Count);
                tvDate = (TextView)v.findViewById(R.id.actualite_date);
                description = (TextView)v.findViewById(R.id.actualite_item_layout_description);
                txtFamille = (TextView)v.findViewById(R.id.famille_name);
                tvFamilleCount = (TextView)v.findViewById(R.id.famille_count);
            }

            @Override
            public void onClick(View v) {
                profilInterface.onBottomProfilItemClick(getAdapterPosition());
            }
        }

        @Override
        public int getItemCount() {
            return listF.size();
        }


        @Override
        public MyBottomProfilFavoriViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;

            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.actualite_item_layout, parent, false);

            MyBottomProfilFavoriViewholder holder = new MyBottomProfilFavoriViewholder(v);

            return holder;
        }

        @Override
        public void onBindViewHolder(MyBottomProfilFavoriViewholder holder, int position) {
            p = listF.get(position);
            if (is_fr) {
                holder.txtFamille.setText(p.getFamille().trim().toUpperCase());
            } else {
                holder.txtFamille.setText(p.getFamille_en().trim().toUpperCase());
            }
            if(p.getNb_vue_famille()!=null && !p.getNb_vue_famille().trim().equalsIgnoreCase("") && !p.getNb_vue_famille().equalsIgnoreCase("null"))
            {
                holder.tvFamilleCount.setText(p.getNb_vue_famille());
            }
            else{
                holder.tvFamilleCount.setText("0");
            }


            holder.tvCount.setVisibility(View.GONE);

            String des = "";
            holder.title.setText(p.getTitle().trim());

            if(p.getAdmin_update().equalsIgnoreCase("oui")){
                des = p.getDescription().trim();
                holder.tvDate.setText(p.getExtend_proprety1().trim());
            }
            else{
                holder.tvDate.setVisibility(View.VISIBLE);
                holder.tvDate.setText(p.getExtend_proprety1());
                des = p.getDescription().trim();
            }
            if (c.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                if (des.length() > 50) {
                    des = des.substring(0, 50) + "...";

                }

            } else {
                if (des.length() > 156) {
                    des = des.substring(0, 110) + "...";
                }
            }


            holder.description.setText(des);
            final float scale = getResources().getDisplayMetrics().density;
            if (is_small) {
                Picasso.with(c).load(p.getUrl()).resize((int) (120 * scale + 0.5f), (int) (100 * scale + 0.5f)).into(holder.image);
            } else if (is_medium) {
                Picasso.with(c).load(p.getUrl()).resize((int) (140 * scale + 0.5f), (int) (120 * scale + 0.5f)).into(holder.image);
            } else if (is_large) {
                Picasso.with(c).load(p.getUrl()).resize((int) (150 * scale + 0.5f), (int) (130 * scale + 0.5f)).into(holder.image);
            }


        }

    }
    private boolean isConnected()
    {
        ConnectivityManager cm=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        if(info!=null && info.isConnected() && info.isAvailable())
        {
            return  true;
        }

        return  false;
    }
}
