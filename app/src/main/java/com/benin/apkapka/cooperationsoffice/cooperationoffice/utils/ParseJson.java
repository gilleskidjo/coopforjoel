package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by joel on 26/01/2016.
 */
public class ParseJson {



    public static User parseUser(Context c,JSONObject o) throws Exception
    {
        User  user = new User(o.getLong("id"), o.getString("name"),  o.getString("email"), o.getString("id_2"));
            user.setPhoto_url(o.getString("photo"));
            user.setPrenom(o.getString("prenom"));
            user.setPays(o.getString("pays"));
            user.setLatitude(o.getString("latitude"));
            user.setLongitude(o.getString("longitude"));
            user.setCreate_at(o.getString("create_at"));

    return user;
    }
    public static User parseUserFromHome(Context c,JSONObject o) throws Exception
    {
        User  user = new User(o.getLong("id"), o.getString("name"),  o.getString("email"), o.getString("id_2"));
        user.setPhoto_url(o.getString("photo"));
        user.setPrenom(o.getString("prenom"));
        user.setPays(o.getString("pays"));
        user.setLatitude(o.getString("latitude"));
        user.setLongitude(o.getString("longitude"));
        user.setCreate_at(o.getString("create_at"));
        user.setPrefix(o.getString("prefix"));
        user.setNumero(o.getString("tel"));
        user.setNaissance(o.getString("date_naissance"));
        SessionManager sessionManager=new SessionManager(c);

        sessionManager.setCompteGoogle(o.getString("email_google"));
        if(sessionManager.getLogginType()==1 && (o.getString("email_google").trim()).isEmpty()){
            sessionManager.setCompteWhatsapp(o.getString("email"));
        }
        sessionManager.setCompteFacebook(o.getString("email_facebook"));
        if(sessionManager.getLogginType()==2 && (o.getString("email_facebook").trim()).isEmpty()){
            sessionManager.setCompteWhatsapp(o.getString("email"));
        }
        sessionManager.setCompteWhatsapp(o.getString("num_whatsapp"));
                if(sessionManager.getLogginType()==3 && (o.getString("num_whatsapp").trim()).isEmpty()){
                    sessionManager.setCompteWhatsapp(o.getString("email"));
                }


        return user;
    }


    public  static List<Produit>parseProduit(JSONObject object,Context ctx)throws  Exception
    {
        List<Produit>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("produits");


                for (int i=0;i<produits.length();i++)
                {
                 JSONObject  o=produits.getJSONObject(i);

                    SharedPreferences preferences=ctx.getSharedPreferences("les_options",MODE_PRIVATE);
                    SharedPreferences.Editor editor=preferences.edit();

                    final Gson gson2=new Gson();
                    final String json=preferences.getString("MES_MENUS",null);
                    List<MenuVitrine> data=null;
                    if(json != null) {
                        MenuVitrine[] menuVitrines = gson2.fromJson(json, MenuVitrine[].class);
                        data = Arrays.asList(menuVitrines);
                        ArrayList<String> str = new ArrayList<>();
                        for (int j = 0; j < data.size(); j++) {
                            str.add(data.get(j).getType());
                        }

                        if (!str.isEmpty()) {

                            Log.e("STROB", o.getString("type"));

                            if (str.contains(o.getString("type")))
                            {

                                String n1=o.getString("nb_price").trim();

                                n1=n1.replace(",",".");
                                if(!TextUtils.isEmpty(n1)){
                                    n1=""+Double.parseDouble(n1);
                                }
                                String n2=o.getString("nb_price_en").trim();
                                n2=n2.replace(",",".");
                                String n3=o.getString("reduction").trim();

                                n3=n3.replace(",",".");


                                Produit p=new Produit(o.getLong("id"),o.optString("title"), o.getString("title_en"),o.getString("description"), o.getString("description_en"), o.getString("lien_description"), o.getString("price"), o.optString("price_en"), n1,n2 , o.getString("text_reduction"),o.getString("text_reduction_en"),n3 , o.getString("lien_catalogue"), o.getString("extend_proprety1"), o.getString("extend_proprety1_en"), o.getString("extend_proprety2"), o.getString("extend_proprety2_en"), o.getString("nb_share"), o.getString("nb_read_catalogue"), o.getString("choixf"), o.getString("type"), o.getString("famille"), o.getString("pays"), o.getString("category"),o.getString("url"), o.getString("url2"), o.getString("url3"),0,o.getString("create_at"),o.getString("famille_en"),o.getString("lien_fichier"),o.getString("auteur"));
                                p.setLien_bonus(o.getString("lien_bonus"));
                                p.setNb_vue(o.getString("nb_vue"));

                                p.setNb_vue_famille(o.getString("nb_vue_famille"));

                                p.setUser_id(o.getLong("user_id"));
                                p.setLatitude(o.getString("latitude"));
                                p.setLongitude(o.getString("longitude"));
                                p.setEmail(o.getString("email"));
                                p.setAdmin_update(o.getString("admin_update"));
                                p.setContent_type(o.getString("content_type"));
                                p.setLien_gift(o.getString("lien_gift"));
                                p.setVideo_id(o.getString("id_video"));
                                list.add(p);

                            }


                        }


                    }




                }



        return list;
    }

    public  static List<Divertissement>parseDivertissement(JSONObject object)throws  Exception
    {
        List<Divertissement>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("divertissements");


        for (int i=0;i<produits.length();i++)
        {
            JSONObject  o=produits.getJSONObject(i);
            Divertissement divertissement=new Divertissement();
            divertissement.setId(o.getLong("id"));
            divertissement.setTitle(o.getString("title"));
            divertissement.setTitle_en(o.getString("title_en"));
            divertissement.setDescription(o.getString("description"));
            divertissement.setDescription_en(o.getString("description_en"));
            divertissement.setFamille(o.getString("famille"));
            divertissement.setFamille_en(o.getString("famille_en"));
            divertissement.setCreate_at(o.getString("create_at"));
            divertissement.setUrl(o.getString("url"));
            divertissement.setLink_id(o.getString("link_id"));
            list.add(divertissement);
        }

        return list;
    }
    public  static List<Produit>parseProduitFom(JSONObject object,List<Produit>produitList ,long id)throws  Exception
    {
        List<Produit>list=new ArrayList<>();
         list=produitList;
        JSONArray   produits=object.getJSONArray("produits");

        for (int i=0;i<produits.length();i++)
        {


            JSONObject  o=produits.getJSONObject(i);
            long _id=o.getLong("id");
             if(_id!=id){
                 String n1=o.getString("nb_price").trim();

                 n1=n1.replace(",",".");
                 if(!TextUtils.isEmpty(n1)){
                     n1=""+Double.parseDouble(n1);
                 }
                 String n2=o.getString("nb_price_en").trim();

                 n2=n2.replace(",",".");

                 String n3=o.getString("reduction").trim();

                 n3=n3.replace(",",".");

                 Produit p=new Produit(o.getLong("id"),o.optString("title"), o.getString("title_en"),o.getString("description"), o.getString("description_en"), o.getString("lien_description"), o.getString("price"), o.optString("price_en"), n1,n2 , o.getString("text_reduction"),o.getString("text_reduction_en"),n3 , o.getString("lien_catalogue"), o.getString("extend_proprety1"), o.getString("extend_proprety1_en"), o.getString("extend_proprety2"), o.getString("extend_proprety2_en"), o.getString("nb_share"), o.getString("nb_read_catalogue"), o.getString("choixf"), o.getString("type"), o.getString("famille"), o.getString("pays"), o.getString("category"),o.getString("url"), o.getString("url2"), o.getString("url3"),0,o.getString("create_at"),o.getString("famille_en"),o.getString("lien_fichier"),o.getString("auteur"));
                 p.setLien_bonus(o.getString("lien_bonus"));
                 p.setNb_vue(o.getString("nb_vue"));
                 p.setNb_vue_famille(o.getString("nb_vue_famille"));
                 p.setUser_id(o.getLong("user_id"));
                 p.setLatitude(o.getString("latitude"));
                 p.setLongitude(o.getString("longitude"));
                 p.setEmail(o.getString("email"));
                 p.setContent_type(o.getString("content_type"));
                 p.setLien_gift(o.getString("lien_gift"));
                 p.setVideo_id(o.getString("id_video"));
                 p.setAdmin_update(o.getString("admin_update"));

                 list.add(p);
             }
        }

        return list;
    }
    public  static List<Produit>parseProduitSearch(JSONObject object,List<String>liste,String lang,String word,Context ctx)throws  Exception
    {
        List<Produit>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("produits");

        for (int i=0;i<produits.length();i++)
        {
            JSONObject  o=produits.getJSONObject(i);

            SharedPreferences preferences=ctx.getSharedPreferences("les_options",MODE_PRIVATE);
            SharedPreferences.Editor editor=preferences.edit();

            final Gson gson2=new Gson();
            final String json=preferences.getString("MES_MENUS",null);
            List<MenuVitrine> data=null;
            if(json != null) {
                MenuVitrine[] menuVitrines = gson2.fromJson(json, MenuVitrine[].class);
                data = Arrays.asList(menuVitrines);
                ArrayList<String> str = new ArrayList<>();
                for (int j = 0; j < data.size(); j++) {
                    str.add(data.get(j).getType());
                }

                if (!str.isEmpty()) {

                    Log.e("STROB", o.getString("type"));

                    if (str.contains(o.getString("type")))
                    {
                        String n1=o.getString("nb_price").trim();

                        n1=n1.replace(",",".");
                        n1=""+Double.parseDouble(n1);
                        String n2=o.getString("nb_price_en").trim();

                        n2=n2.replace(",",".");

                        String n3=o.getString("reduction").trim();

                        n3=n3.replace(",",".");

                        Produit p=new Produit(o.getLong("id"),o.optString("title"), o.getString("title_en"),o.getString("description"), o.getString("description_en"), o.getString("lien_description"), o.getString("price"), o.optString("price_en"), n1,n2 , o.getString("text_reduction"),o.getString("text_reduction_en"),n3 , o.getString("lien_catalogue"), o.getString("extend_proprety1"), o.getString("extend_proprety1_en"), o.getString("extend_proprety2"), o.getString("extend_proprety2_en"), o.getString("nb_share"), o.getString("nb_read_catalogue"), o.getString("choixf"), o.getString("type"), o.getString("famille"), o.getString("pays"), o.getString("category"),o.getString("url"), o.getString("url2"), o.getString("url3"),0,o.getString("create_at"),o.getString("famille_en"),o.getString("lien_fichier"),o.getString("auteur"));
                        p.setLien_bonus(o.getString("lien_bonus"));
                        p.setNb_vue(o.getString("nb_vue"));
                        p.setNb_vue_famille(o.getString("nb_vue_famille"));
                        p.setUser_id(o.getLong("user_id"));
                        p.setLatitude(o.getString("latitude"));
                        p.setLongitude(o.getString("longitude"));
                        p.setEmail(o.getString("email"));
                        p.setAdmin_update(o.getString("admin_update"));
                        p.setContent_type(o.getString("content_type"));
                        p.setLien_gift(o.getString("lien_gift"));
                        p.setVideo_id(o.getString("id_video"));
                        if(liste!=null){
                            if(o.getString("title").toLowerCase().contains(word.toLowerCase())){
                                if(!liste.contains(o.getString("title"))){
                                    liste.add(o.getString("title"));

                                    list.add(p);

                                }

                            }
                            else if(o.getString("famille").toLowerCase().contains(word.toLowerCase())){
                                if(!liste.contains(o.getString("famille"))){
                                    liste.add(o.getString("famille"));
                                    list.add(p);


                                }

                            }
                            else if(o.getString("extend_proprety2").toLowerCase().contains(word.toLowerCase())){

                                if(!liste.contains(o.getString("extend_proprety2"))){
                                    liste.add(o.getString("extend_proprety2"));
                                    list.add(p);

                                }

                            }


                        }
                    }
                }
            }






        }


        return list;
    }


    public  static List<Article>parseArticle(JSONObject object)throws  Exception
    {
        List<Article>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("articles");


        for (int i=0;i<produits.length();i++)
        {

            JSONObject  o=produits.getJSONObject(i);
            String n1=o.getString("nb_price").trim();
            n1=n1.replace(",",".");
            n1=""+Double.parseDouble(n1);
            String n2=o.getString("nb_price_en").trim();
            n2=n2.replace(",",".");

            String n3=o.getString("reduction").trim();
            n3=n3.replace(",",".");
            n3=""+Double.parseDouble(n3);
            Article article=new Article(o.getLong("id"), o.getString("title"), o.getString("title_en"), o.getString("description"), o.getString("description_en"), o.getString("lien_description"), o.getString("lien_catalogue"), o.getString("extend_proprety1"), o.getString("extend_proprety1_en"), o.getString("extend_proprety2"), o.getString("extend_proprety2_en"), o.getString("nb_share"), o.getString("nb_read_catalogue"), o.getString("choixf"), o.getString("famille"), o.getString("pays"), o.getString("category"), o.getString("create_at"),o.getString("price"), o.optString("price_en"),new BigDecimal(n1),new BigDecimal(n2), o.getString("text_reduction"), o.getString("text_reduction_en"), n3, o.getString("url"), o.getString("url2"), o.getString("url3"),0,o.getString("create_at"),o.getString("famille_en"),o.getString("lien_fichier"));

            list.add(article);
        }

        return list;
    }

    public  static List<Home>parseHome(JSONObject object)throws  Exception
    {
        List<Home>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("homes");

        for (int i=0;i<produits.length();i++)
        {
            JSONObject  o=produits.getJSONObject(i);
            Home article=new Home();
            if(o.getString("content_type").equalsIgnoreCase("affiche")){
               article=new Home(o.getLong("id"), o.getString("title"), o.getString("title_en"), o.getString("description"), o.getString("description_en"), o.getString("url"), o.getString("url2"), o.getString("url3"),"","",o.getString("content_type"));
            }
            else{
               article=new Home(o.getLong("id"), o.getString("title"), o.getString("title_en"), o.getString("description"), o.getString("description_en"), "", "", "",o.getString("preview_image"),o.getString("link_video"),o.getString("content_type"));
            }

            list.add(article);
        }

        return list;
    }

    public  static List<Emploi>parseEmploi(JSONObject object)throws  Exception
    {
        List<Emploi>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("emplois");

        for (int i=0;i<produits.length();i++)
        {
            JSONObject  o=produits.getJSONObject(i);
            String n1=o.getString("nb_price").trim();
            n1=n1.replace(",",".");
            n1=""+Double.parseDouble(n1);
            String n2=o.getString("nb_price_en").trim();
            n2=n2.replace(",",".");

            String n3=o.getString("reduction").trim();
            n3=n3.replace(",",".");
            n3=""+Double.parseDouble(n3);
            Emploi article=new  Emploi(o.getLong("id"), o.getString("title"), o.getString("title_en"), o.getString("description"),o.getString("description_en"), o.getString("lien_description"), o.getString("lien_catalogue"), o.getString("extend_proprety1"), o.getString("extend_proprety1_en"), o.getString("extend_proprety2"), o.getString("extend_proprety2_en"), o.getString("nb_share"), o.getString("nb_read_catalogue"), o.getString("choixf"), o.getString("famille"), o.getString("pays"), o.getString("category"), o.getString("price"), o.optString("price_en"),new BigDecimal(n1),new BigDecimal(n2), o.getString("text_reduction"), o.getString("text_reduction_en"), n3, o.getString("url"), o.getString("url2"),o.getString("url3"),0,o.getString("create_at"),o.getString("famille_en"),o.getString("lien_fichier")) ;

            list.add(article);
        }

        return list;
    }
    public  static List<Competence>parseCompetence(JSONObject object)throws  Exception
    {
        List<Competence>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("competences");

        for (int i=0;i<produits.length();i++)
        {
            JSONObject  o=produits.getJSONObject(i);
            String n1=o.getString("nb_price").trim();
            n1=n1.replace(",",".");
            n1=""+Double.parseDouble(n1);
            String n2=o.getString("nb_price_en").trim();
            n2=n2.replace(",",".");

            String n3=o.getString("reduction").trim();
            n3=n3.replace(",",".");
            n3=""+Double.parseDouble(n3);
            Competence article=new Competence(o.getLong("id"), o.getString("title"), o.getString("title_en"), o.getString("description"), o.getString("description_en"), o.getString("lien_description"), o.getString("lien_catalogue"), o.getString("extend_proprety1"), o.getString("extend_proprety1_en"), o.getString("extend_proprety2"), o.getString("extend_proprety2_en"), o.getString("nb_share"), o.getString("nb_read_catalogue"), o.getString("choixf"), o.getString("famille"), o.getString("pays"), o.getString("category"), o.getString("price"), o.getString("price_en"), new BigDecimal(n1),new BigDecimal(n2), o.getString("text_reduction"), o.getString("text_reduction_en"), n3,o.getString("url"),o.getString("url2"), o.getString("url3"),0,o.getString("create_at"),o.getString("famille_en"),o.getString("lien_fichier"));

            list.add(article);
        }

        return list;
    }
    public  static List<Bourse>parseBourse(JSONObject object)throws  Exception
    {
        List<Bourse>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("bourses");

        for (int i=0;i<produits.length();i++)
        {
            JSONObject  o=produits.getJSONObject(i);
            String n1=o.getString("nb_price").trim();
            n1=n1.replace(",",".");
            n1=""+Double.parseDouble(n1);
            String n2=o.getString("nb_price_en").trim();
            n2=n2.replace(",",".");
            String n3=o.getString("reduction").trim();
            n3=n3.replace(",",".");
            n3=""+Double.parseDouble(n3);
            Bourse article=new Bourse(o.getLong("id"), o.getString("title"), o.getString("title_en"), o.getString("description"), o.getString("description_en"), o.getString("lien_description"), o.getString("lien_catalogue"), o.getString("extend_proprety1"), o.getString("extend_proprety1_en"), o.getString("extend_proprety2"), o.getString("extend_proprety2_en"), o.getString("nb_share"), o.getString("nb_read_catalogue"), o.getString("choixf"), o.getString("famille"), o.getString("pays"), o.getString("category"), o.getString("price"), o.getString("price_en"), new BigDecimal(n1),new BigDecimal(n2), o.getString("text_reduction"), o.getString("text_reduction_en"), n3, o.getString("url"), o.getString("url2"), o.getString("url3"),0,o.getString("create_at"),o.getString("famille_en"),o.getString("lien_fichier")) ;

            list.add(article);
        }

        return list;
    }
    public  static List<Annuaire>parseAnnuaire(JSONObject object)throws  Exception
    {
        List<Annuaire>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("annuaires");

        for (int i=0;i<produits.length();i++)
        {
            JSONObject  o=produits.getJSONObject(i);
            String n1=o.getString("nb_price").trim();
            n1=n1.replace(",",".");
            n1=""+Double.parseDouble(n1);
            String n2=o.getString("nb_price_en").trim();
            n2=n2.replace(",",".");
            String n3=o.getString("reduction").trim();
            n3=n3.replace(",",".");
            n3=""+Double.parseDouble(n3);
            Annuaire article=new Annuaire(o.getLong("id"), o.getString("title"), o.getString("title_en"), o.getString("description"), o.getString("description_en"), o.getString("lien_description"), o.getString("lien_catalogue"), o.getString("extend_proprety1"), o.getString("extend_proprety1_en"), o.getString("extend_proprety2"), o.getString("extend_proprety2_en"), o.getString("nb_share"), o.getString("nb_read_catalogue"), o.getString("choixf"), o.getString("famille"), o.getString("pays"), o.getString("category"), o.getString("price"), o.getString("price_en"),new BigDecimal(n1),new BigDecimal(n2), o.getString("text_reduction"), o.getString("text_reduction_en"), n3, o.getString("url"), o.getString("url2"), o.getString("url3"),0,o.getString("create_at"),o.getString("famille_en"),o.getString("lien_fichier"));

            list.add(article);
        }

        return list;
    }
    public  static List<Service>parseService(JSONObject object)throws  Exception
    {
        List<Service>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("projets");

        for (int i=0;i<produits.length();i++)
        {
            JSONObject  o=produits.getJSONObject(i);
            String n1=o.getString("nb_price").trim();
            n1=n1.replace(",",".");
            n1=""+Double.parseDouble(n1);
            String n2=o.getString("nb_price_en").trim();
            n2=n2.replace(",",".");
            String n3=o.getString("reduction").trim();
            n3=n3.replace(",",".");
            n3=""+Double.parseDouble(n3);
            Service article=new Service(o.getLong("id"), o.getString("title"), o.getString("title_en"), o.getString("description"), o.getString("description_en"), o.getString("lien_description"), o.getString("lien_catalogue"), o.getString("extend_proprety1"), o.getString("extend_proprety1_en"), o.getString("extend_proprety2"), o.getString("extend_proprety2_en"), o.getString("nb_share"), o.getString("nb_read_catalogue"), o.getString("choixf"), o.getString("famille"), o.getString("pays"), o.getString("category"), o.getString("type"), o.getString("price"), o.getString("price_en"),new BigDecimal(n1), new BigDecimal(n2), o.getString("text_reduction"), o.getString("text_reduction_en"), n3, o.getString("url"), o.getString("url2"), o.getString("url3"),0,o.getString("create_at"),o.getString("famille_en"),o.getString("lien_fichier"));

            list.add(article);
        }

        return list;
    }

    public  static List<Partenariat>parsePartenariat(JSONObject object)throws  Exception
    {
        List<Partenariat>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("projets");

        for (int i=0;i<produits.length();i++)
        {
            //new BigDecimal(format.format( Double.parseDouble(o.getString("nb_price_en"))))

            JSONObject  o=produits.getJSONObject(i);
            String n1=o.getString("nb_price").trim();
            n1=n1.replace(",",".");
            n1=""+Double.parseDouble(n1);
            String n2=o.getString("nb_price_en").trim();
            n2=n2.replace(",",".");
            String n3=o.getString("reduction").trim();
            n3=n3.replace(",",".");
            n3=""+Double.parseDouble(n3);
            Partenariat article=new Partenariat(o.getLong("id"), o.getString("title"), o.getString("title_en"), o.getString("description"), o.getString("description_en"), o.getString("lien_description"), o.getString("lien_catalogue"), o.getString("extend_proprety1"), o.getString("extend_proprety1_en"), o.getString("extend_proprety2"), o.getString("extend_proprety2_en"), o.getString("nb_share"), o.getString("nb_read_catalogue"), o.getString("choixf"), o.getString("famille"), o.getString("pays"), o.getString("category"), o.getString("type"), o.getString("price"), o.getString("price_en"),new BigDecimal(n1),new BigDecimal(n2), o.getString("text_reduction"), o.getString("text_reduction_en"), n3, o.getString("url"), o.getString("url2"), o.getString("url3"),0,o.getString("create_at"),o.getString("famille_en"),o.getString("lien_fichier"));

            list.add(article);
        }

        return list;
    }
    public  static List<Projet>parseProjet(JSONObject object)throws  Exception
    {
        List<Projet>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("projets");

        for (int i=0;i<produits.length();i++)
        {
            JSONObject  o=produits.getJSONObject(i);
            String n1=o.getString("nb_price").trim();
             n1=n1.replace(",",".");
             n1=""+Double.parseDouble(n1);
            String n2=o.getString("nb_price_en").trim();
            n2=n2.replace(",",".");
            String n3=o.getString("reduction").trim();
            n3=n3.replace(",",".");
            n3=""+Double.parseDouble(n3);
            Projet article=new Projet(o.getLong("id"), o.getString("title"), o.getString("title_en"), o.getString("description"), o.getString("description_en"), o.getString("lien_description"), o.getString("lien_catalogue"), o.getString("extend_proprety1"), o.getString("extend_proprety1_en"), o.getString("extend_proprety2"), o.getString("extend_proprety2_en"), o.getString("nb_share"), o.getString("nb_read_catalogue"), o.getString("choixf"), o.getString("famille"), o.getString("pays"), o.getString("category"), o.getString("type"), o.getString("price"), o.getString("price_en"),new BigDecimal(n1),new BigDecimal(n2), o.getString("text_reduction"), o.getString("text_reduction_en"), n3, o.getString("url"), o.getString("url2"), o.getString("url3"),0,o.getString("create_at"),o.getString("extend_proprety1_en"),o.getString("lien_fichier"));

            list.add(article);
        }

        return list;
    }
    public  static List<ChatConversation>parseConversation(JSONObject object)throws  Exception
    {
        List<ChatConversation>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("data");

        for (int i=0;i<produits.length();i++)
        {
            JSONObject  o=produits.getJSONObject(i);

            ChatConversation c=new ChatConversation();

            c.setFrom_user_id(o.getString("from_user_id"));
            c.setId(o.getLong("id"));
            c.setLast_message(o.getString("last_message"));
            c.setType(o.getString("type"));
            c.setPrenom(o.getString("prenom"));
            c.setStatut(o.getString("statut"));
            c.setTo_user_nom(o.getString("to_user_prenom"));
            c.setCreate_at(o.getString("create_at"));
            list.add(c);
        }

        return list;
    }

    public  static List<ChatMessage>parseChatMessage(JSONObject object)throws  Exception
    {
        List<ChatMessage>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("data");

        for (int i=0;i<produits.length();i++)
        {
            JSONObject  o=produits.getJSONObject(i);

            ChatMessage c=new ChatMessage();
              c.setSend(false);
              c.setPhoto(o.getString("photo"));
              c.setMessage(o.getString("message"));
              c.setType(o.getString("type"));
              c.setId(o.getLong("id"));
              c.setSendPrenom(o.getString("prenom"));
              c.setFromUser(o.getLong("from_user_id"));
              c.setCreate_at(o.getString("create_at"));
              c.setFetchFromServer(true);
            list.add(c);
            if(!o.getString("type").equalsIgnoreCase("message") && o.getString("message").equalsIgnoreCase("")){

            }
        }

        return list;
    }

    public  static List<String>parseFamille(JSONObject object)throws  Exception
    {
        List<String>list=new ArrayList<>();

        JSONArray   produits=object.getJSONArray("data");

        for (int i=0;i<produits.length();i++)
        {
            JSONObject  o=produits.getJSONObject(i);

            list.add(o.getString("famille"));
        }

        return list;
    }

}
