package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.res.Resources;

/**
 * Created by joel on 30/11/2017.
 */
public class LanguageNotifier {



    public  interface LanguageNotifierInterface{
        public  void languageChange(Resources resources,String langague);
    }
    public static   LanguageNotifier languageNotifier;
    public  LanguageNotifierInterface notifierInterface;

    public static LanguageNotifier getInstance(){
        if(languageNotifier==null){
            languageNotifier=new LanguageNotifier();
        }
        return languageNotifier;
    }

    public void setNotifierInterface(LanguageNotifierInterface notifierInterface){
        this.notifierInterface=notifierInterface;
    }

    public  void alertNotification(Resources resources,String s){
        if(notifierInterface!=null){
            notifierInterface.languageChange(resources,s);
        }
    }
}
