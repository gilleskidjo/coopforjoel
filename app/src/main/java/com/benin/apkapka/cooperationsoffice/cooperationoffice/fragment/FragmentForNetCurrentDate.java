package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by joel on 09/10/2017.
 */
public class FragmentForNetCurrentDate extends Fragment {
    public  static  final  String TAG="com.cooperation.office.fragemntnetworkrequest_current_date";
    public interface  HandlerNetWorkRequestResponseDate
    {

        public void onResposeSuccessDate(String date);

        public  void onresponseFailAndPrintErrorResponseDate();

    }

    HandlerNetWorkRequestResponseDate handleResponse;
    private boolean isRequestDo=false;
    private boolean isMessage=false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    public  void setInterface(HandlerNetWorkRequestResponseDate handleResponse)
    {
        this.handleResponse=handleResponse;
    }

    public void doRequestDate()
    {

        String url= Config.URL_CURRENT_DATE;


        JsonObjectRequest request=new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(response!=null)
                {
                    try {
                        boolean error = response.getBoolean("error");
                        if(error)
                        {
                        }
                        else
                        {

                            if(handleResponse!=null)
                            {
                                handleResponse.onResposeSuccessDate(response.getString("c_date"));
                            }
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
                else
                {
                }

                isRequestDo=true;
            }//fin onResponse
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isRequestDo=true;
                    }
                });


        MyApplication.getInstance().addToRequestQueue(request);
    }

}
