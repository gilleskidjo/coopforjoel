package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.LoginActivity;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

/**
 * Created by joel on 20/12/2017.
 */

public class MyTrackDateIntenteService extends IntentService {
    public static final String TIME_SERVER = "time-a.nist.gov";
    public MyTrackDateIntenteService(){
        super("");
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        NTPUDPClient client=new NTPUDPClient();
        try{
            InetAddress address=InetAddress.getByName(TIME_SERVER);
            TimeInfo timeInfo=client.getTime(address);
            long servertime=timeInfo.getMessage().getTransmitTimeStamp().getTime();
            MyDateServeur.getInstance().initDate(new Date(servertime),getApplicationContext());
            MyDateServeur.getInstance().setCanReque(true,getApplicationContext());

        }
        catch (UnknownHostException e){

        }
        catch (IOException e){

        }
        stopSelf();
    }
}
