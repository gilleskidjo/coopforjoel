package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.res.Resources;

/**
 * Created by joel on 29/11/2017.
 */
public class LanguageChaneEvent {
    Resources resources;
    String lang;

    public LanguageChaneEvent(Resources resources, String lang) {
        this.resources = resources;
        this.lang = lang;
    }

    public Resources getResources() {
        return resources;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public void setResources(Resources resources) {
        this.resources = resources;
    }

    public String getLang() {
        return lang;
    }
}
