package com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ExpandableModele;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

/**
 * Created by joel on 19/11/2017.
 */
public class ExpandableDrawerAdapter extends BaseExpandableListAdapter {
    private Context c;
    List<ExpandableModele> listDataHeader;
    private HashMap<ExpandableModele,List<ExpandableModele>> listChild;
    public  ExpandableDrawerAdapter(Context c,List<ExpandableModele> listDataHeader,HashMap<ExpandableModele,List<ExpandableModele>>listChild){
        this.c=c;
        this.listDataHeader=listDataHeader;
        this.listChild=listChild;
    }

    @Override
    public int getChildrenCount(int i) {
        return listChild.get(listDataHeader.get(i)).size();
    }

    @Override
    public int getGroupCount() {
        return listDataHeader.size();
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public Object getChild(int i, int i1) {
        return listChild.get(listDataHeader.get(i)).get(i1);
    }

    @Override
    public Object getGroup(int i) {
        return listDataHeader.get(i);
    }

    class ExpandableViewHolder{
        TextView textView;
        ImageView simpleDraweeView;
    }
    @Override
    public View getChildView(int i, int i1, boolean b, View convertView, ViewGroup viewGroup) {
         ExpandableViewHolder holder;
        if(convertView==null){
            LayoutInflater inflater=LayoutInflater.from(c);
            convertView=inflater.inflate(R.layout.ex_list_iem,null);
            holder=new ExpandableViewHolder();
            holder.textView=(TextView) convertView.findViewById(R.id.ex_list_item_title);
            holder.simpleDraweeView=(ImageView) convertView.findViewById(R.id.ex_List_item_image);
            convertView.setTag(holder);
        }
        else {
           holder=(ExpandableViewHolder)convertView.getTag();
        }

        ExpandableModele modele=(ExpandableModele) getChild(i,i1);
        holder.textView.setText(modele.getTitle());
    
        Glide.with(c).load("").placeholder(modele.getImage()).into(holder.simpleDraweeView);
        return convertView;
    }

    @Override
    public View getGroupView(int i, boolean b, View convertView, ViewGroup viewGroup) {
        if(convertView==null){
            LayoutInflater inflater=LayoutInflater.from(c);
            convertView=inflater.inflate(R.layout.ex_list_header,null);
        }
        TextView t=(TextView)convertView.findViewById(R.id.listh_title);
        ImageView imageView=(ImageView)convertView.findViewById(R.id.ex_headerImage);
        ImageView imageHeader=(ImageView)convertView.findViewById(R.id.ex_arrow_image);
        t.setTypeface(null, Typeface.BOLD);
        ExpandableModele modele=listDataHeader.get(i);
        t.setText(modele.getTitle());
        imageView.setImageResource(modele.getImage());
        if(b){

            imageHeader.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
        }
        else{
            imageHeader.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
        }
        return convertView;

    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
