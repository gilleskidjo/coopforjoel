package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

public class MyDateFragment extends DialogFragment  implements   DatePickerDialog.OnDateSetListener{

    private int year,month,day;

    public interface MyDatePickerInterface{
        public void onDatePick(int day,int month,int year);
    }
    MyDatePickerInterface myDatePickerInterface;
    public void setMyDatePickerInterface(MyDatePickerInterface myDatePickerInterface){
        this.myDatePickerInterface=myDatePickerInterface;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar=Calendar.getInstance();
        year= calendar.get(Calendar.YEAR);
        month= calendar.get(Calendar.MONTH);
        day=calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), android.app.AlertDialog.THEME_HOLO_LIGHT,this,year,month,day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        setYear(year);
         if(month<10){
             month=Integer.valueOf(String.format("%02d",month));
         }
        if(dayOfMonth<10){
            dayOfMonth=Integer.valueOf(String.format("%02d",dayOfMonth));
        }
        setMonth(month+1);
        setDay(dayOfMonth);
        myDatePickerInterface.onDatePick(getDay(),getMonth(),getYear());

    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
