package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentHome;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.SpotFragment;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Conf;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Home;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.gms.ads.mediation.admob.AdMobExtras;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import dmax.dialog.SpotsDialog;

/**
 * Created by joel on 25/11/2017.
 */
public class SpotActivity  extends BaseLanguageActivity {


    TextView title,description;
    Button share;
    ShareDialog shareDialog;
    CallbackManager callbackManager;

    boolean is_fr;
    Home home;
    Toolbar toolbar;
    ActionBar actionBar;
    String type="";
    String id="";
    SpotFragment fragment;
    private boolean isFromVitrine=false;
    private  static final String TAG_FRAGMENT="com.cooperaton.spot.tag.fragmeng";
    String mtitle="",mdescription="",mimage="";
    AlertDialog mAlertDialog1=null;
    private AdView mAdView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.primary_dark));
        }
        manageFacebook();
        setContentView(R.layout.spot_activity_view);
        toolbar=(Toolbar)findViewById(R.id.toolbar_spot);
        setSupportActionBar(toolbar);
        actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        title=(TextView)findViewById(R.id.home_title);
        description=(TextView)findViewById(R.id.home_description);
        share=(Button)findViewById(R.id.un_modele_view_partager);

        /*MobileAds.initialize(this,
                "ca-app-pub-5424708841083179~1666498449");*/

        //AdMobExtras extras = new AdMobExtras(savedInstanceState);
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        /*AdLoader adLoader = new AdLoader.Builder(this, "ca-app-pub-3940256099942544/2247696110")
                .forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                    @Override
                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                        // Assumes you have a placeholder FrameLayout in your View layout
                        // (with id fl_adplaceholder) where the ad is to be placed.
                        FrameLayout frameLayout =
                                findViewById(R.id.fl_adplaceholder);
                        // Assumes that your ad layout is in a file call ad_unified.xml
                        // in the res/layout folder
                        UnifiedNativeAdView adView = (UnifiedNativeAdView) getLayoutInflater()
                                .inflate(R.layout.item_pub_famille, null);
                        // This method sets the text, images and the native ad, etc into the ad
                        // view.
                        populateUnifiedNativeAdView(unifiedNativeAd, adView);
                        frameLayout.removeAllViews();
                        frameLayout.addView(adView);
                    }
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        Toast.makeText(SpotActivity.this, "Une menace a été détectée : " + errorCode, Toast.LENGTH_SHORT).show();
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());*/

        if(getIntent()!=null)
        {
            Bundle bundle=getIntent().getExtras();
            if(bundle!=null)
            {


               type=bundle.getString("type","home");
                if(!type.equalsIgnoreCase("home")){
                    title.setVisibility(View.GONE);
                    description.setVisibility(View.GONE);
                    id=bundle.getString("id");

                    mimage=bundle.getString("image");
                    mtitle=bundle.getString("title");
                    mdescription=bundle.getString("invitaion");
                   isFromVitrine=true;
                   SpotActivity.this.setTitle(mtitle);
                }
                else{
                    home  =new Home(bundle.getLong("id"),bundle.getString("title"),bundle.getString("title_en"),bundle.getString("description"),bundle.getString("description_en"),bundle.getString("url"),bundle.getString("url2"),bundle.getString("url3"),bundle.getString("preview_image"),bundle.getString("link_video"),bundle.getString("content_type"));
                    title.setText(home.getTitle());
                    description.setText(home.getDescription());
                     if(bundle.getString("title")!=null && !bundle.getString("title").trim().equalsIgnoreCase("")){
                         SpotActivity.this.setTitle(bundle.getString("title"));
                     }

                }

            }
            else{


            }

        }
        else{
        }


        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startbookShare();
            }
        });

        if(!isFinishing()){
             fragment=new SpotFragment();
            Bundle  bundle=new Bundle();
            bundle.putString("id",type.equalsIgnoreCase("home")?home.getLink_video().trim():id.trim());
            fragment.setArguments(bundle);
            FragmentManager manager=getSupportFragmentManager();
            manager.beginTransaction()
                    .replace(R.id.main,fragment,TAG_FRAGMENT)
                    .commit();

        }

    }

    /*private void populateUnifiedNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
        // Set the media view. Media content will be automatically populated in the media view once
        // adView.setNativeAd() is called.
        MediaView mediaView = adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);

        // Set other ad assets.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setPriceView(adView.findViewById(R.id.ad_price));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

        // The headline is guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }

        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        // This method tells the Google Mobile Ads SDK that you have finished populating your
        // native ad view with this native ad. The SDK will populate the adView's MediaView
        // with the media content from this native ad.
        adView.setNativeAd(nativeAd);
    }*/

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void manageFacebook()
    {
        callbackManager= CallbackManager.Factory.create();
        shareDialog=new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

                Toast.makeText(SpotActivity.this,"Erreur lors du  partage",Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void startbookShare()
    {
         if(isFromVitrine){
             final android.app.AlertDialog mAlertDialog=new SpotsDialog(SpotActivity.this,R.style.style_spot_actualite);
             mAlertDialog.show();
             Task<ShortDynamicLink> shortDynamicLinkTask= FirebaseDynamicLinks.getInstance().createDynamicLink()
                     .setLink(Uri.parse("http://cooperationsoffice.net"))
                     .setDynamicLinkDomain("cooperations0ffices.page.link")
                     .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.benin.apkapka.cooperationsoffice.cooperationoffice")
                             .setFallbackUrl(Uri.parse("http://cooperationsoffice.net"))
                             .build())
                     .setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder()
                             .setTitle(""+(mtitle))
                             .setDescription(is_fr?"CLIQUEZ".toUpperCase()+" pour "+"VISITER".toUpperCase()+" et "+"PARTAGER".toUpperCase()+" notre publication ":"CLICK".toUpperCase()+" to "+"VISIT".toUpperCase()+" and "+"SHARE".toUpperCase()+" our publication")
                             .setImageUrl(Uri.parse(""+mimage))
                             .build())
                     .buildShortDynamicLink()

                     .addOnCompleteListener(SpotActivity.this, new OnCompleteListener<ShortDynamicLink>() {
                         @Override
                         public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                             mAlertDialog.dismiss();
                             if(task.isSuccessful()){

                                 Uri uri=task.getResult().getShortLink();
                                 ShareLinkContent linkContent=new ShareLinkContent.Builder()
                                         .setContentUrl(uri)
                                         .build();
                                 if(shareDialog.canShow(ShareLinkContent.class)){
                                     shareDialog.show(linkContent);
                                 }
                             }
                             else {

                                 AlertDialog.Builder mBuilder1=new AlertDialog.Builder(SpotActivity.this)
                                         .setMessage(is_fr?"Impossible de publier l'image":"Can not publish image")
                                         .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                             @Override
                                             public void onClick(DialogInterface dialogInterface, int i) {
                                                 if(mAlertDialog1!=null){
                                                     mAlertDialog1.dismiss();
                                                 }

                                             }
                                         });
                                 mAlertDialog1=mBuilder1.show();
                             }

                         }
                     });

         }
         else{
             final android.app.AlertDialog mAlertDialog=new SpotsDialog(SpotActivity.this,R.style.style_spot_actualite);
             mAlertDialog.show();
             Task<ShortDynamicLink> shortDynamicLinkTask= FirebaseDynamicLinks.getInstance().createDynamicLink()
                     .setLink(Uri.parse("http://cooperationsoffice.net"))
                     .setDynamicLinkDomain("tnn58.app.goo.gl")
                     .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.benin.apkapka.cooperationsoffice.cooperationoffice")
                             .setFallbackUrl(Uri.parse("http://cooperationsoffice.net"))
                             .build())
                     .setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder()
                             .setTitle(""+(is_fr?""+home.getTitle():""+home.getTitle_en()))
                             .setDescription(is_fr?"CLIQUEZ".toUpperCase()+" pour "+"VISITER".toUpperCase()+" et "+"PARTAGER".toUpperCase()+" notre publication ":"CLICK".toUpperCase()+" to "+"VISIT".toUpperCase()+" and "+"SHARE".toUpperCase()+" our publication")
                             .setImageUrl(Uri.parse(""+home.getUrl()))
                             .build())
                     .buildShortDynamicLink()

                     .addOnCompleteListener(SpotActivity.this, new OnCompleteListener<ShortDynamicLink>() {
                         @Override
                         public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                             mAlertDialog.dismiss();
                             if(task.isSuccessful()){

                                 Uri uri=task.getResult().getShortLink();
                                 ShareLinkContent linkContent=new ShareLinkContent.Builder()
                                         .setContentUrl(uri)
                                         .build();
                                 if(shareDialog.canShow(ShareLinkContent.class)){
                                     shareDialog.show(linkContent);
                                 }
                             }
                             else {

                                 AlertDialog.Builder mBuilder1=new AlertDialog.Builder(SpotActivity.this)
                                         .setMessage(is_fr?"Impossible de publier l'image":"Can not publish image")
                                         .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                             @Override
                                             public void onClick(DialogInterface dialogInterface, int i) {
                                                 if(mAlertDialog1!=null){
                                                     mAlertDialog1.dismiss();
                                                 }
                                             }
                                         });
                                 mAlertDialog1=mBuilder1.show();
                             }

                         }
                     });

         }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case android.R.id.home:

                finish();
                return true;
            case R.id.autre_share:
                Intent intent2=new Intent(Intent.ACTION_SEND);
                intent2.setType("text/plain");
                String url= "https://play.google.com/store/apps/details?id=com.benin.apkapka.cooperationsoffice.cooperationoffice";
                intent2.putExtra(Intent.EXTRA_TEXT, url);
                intent2.putExtra(Intent.EXTRA_SUBJECT, "Cooperations office");
                if(intent2.resolveActivity(getPackageManager())!=null)
                {

                    String[] blacklist = new String[]{"com.google.android.gm","com.facebook.katana" };
                    startActivity(generateCustomChooserIntent(intent2, blacklist));
                }
                else
                {
                    MyToast.show(this, getResources().getString(R.string.une_activity_installer_app_partage), false);
                }
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private Intent generateCustomChooserIntent(Intent prototype, String[] forbiddenChoices) {
        List<Intent> targetedShareIntents = new ArrayList<>();
        List<HashMap<String, String>> intentMetaInfo = new ArrayList<HashMap<String, String>>();
        Intent chooserIntent;

        Intent dummy = new Intent(prototype.getAction());
        dummy.setType(prototype.getType());
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(dummy, 0);

        if (!resInfo.isEmpty()) {
            for (ResolveInfo resolveInfo : resInfo) {
                if (resolveInfo.activityInfo == null || Arrays.asList(forbiddenChoices).contains(resolveInfo.activityInfo.packageName))
                    continue;

                HashMap<String, String> info = new HashMap<String, String>();
                info.put("packageName", resolveInfo.activityInfo.packageName);
                info.put("className", resolveInfo.activityInfo.name);
                info.put("simpleName", String.valueOf(resolveInfo.activityInfo.loadLabel(getPackageManager())));
                intentMetaInfo.add(info);
            }

            if (!intentMetaInfo.isEmpty()) {
                // sorting for nice readability
                Collections.sort(intentMetaInfo, new Comparator<HashMap<String, String>>() {
                    @Override
                    public int compare(HashMap<String, String> map, HashMap<String, String> map2) {
                        return map.get("simpleName").compareTo(map2.get("simpleName"));
                    }
                });

                // create the custom intent list
                for (HashMap<String, String> metaInfo : intentMetaInfo) {
                    Intent targetedShareIntent = (Intent) prototype.clone();
                    targetedShareIntent.setPackage(metaInfo.get("packageName"));
                    targetedShareIntent.setClassName(metaInfo.get("packageName"), metaInfo.get("className"));
                    targetedShareIntents.add(targetedShareIntent);
                }

                chooserIntent = Intent.createChooser(targetedShareIntents.remove(targetedShareIntents.size() - 1), getResources().getString(R.string.une_activity_partager_via));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
                return chooserIntent;
            }
        }

        return Intent.createChooser(prototype,getResources().getString(R.string.une_activity_partager_via) );
    }



}
