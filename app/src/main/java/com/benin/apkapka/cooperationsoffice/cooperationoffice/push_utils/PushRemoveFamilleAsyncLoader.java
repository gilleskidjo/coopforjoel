package com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.PushDataEvent;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushDataDao;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by joel on 22/08/2017.
 */
public class PushRemoveFamilleAsyncLoader  extends AsyncTaskLoader<Void> {

    EventBus bus;
    PushDataDao dao;
    String type,category,famille;
    public PushRemoveFamilleAsyncLoader(Context c,String type,String category,String famille){
        super(c);
        dao=new PushDataDao(getContext());
        this.type=type;
        this.category=category;
        this.famille=famille;
        bus=EventBus.getDefault();
    }

    @Override
    public Void loadInBackground() {

        dao.deletePushDataFamille(type,category,famille);
         bus.post(new PushDataEvent());
        return null;
    }
}
