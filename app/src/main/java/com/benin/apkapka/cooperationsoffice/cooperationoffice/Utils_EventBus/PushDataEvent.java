package com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus;

/**
 * Created by joel on 28/08/2017.
 */
public class PushDataEvent {
private String message;

    public PushDataEvent() {
    }

    public PushDataEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
