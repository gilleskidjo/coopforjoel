package com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.PushDataEvent;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.MyPushDatabaseHelper;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushDataDao;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joel on 15/08/2017.
 */
public class PushTypeAsyncLoader extends AsyncTaskLoader<List<PushType>> {
  List<PushType> list1=new ArrayList<PushType>();
  PushDataDao pushDataDao;
 EventBus bus;
    public PushTypeAsyncLoader(Context c){
        super(c);
        pushDataDao=new PushDataDao(getContext());
        bus=EventBus.getDefault();
        bus.register(this);
    }

    @Subscribe
    public void onEvent(PushDataEvent event){
        onContentChanged();

    }

    @Override
    protected void onStartLoading() {
        if(list1.size()>0){
            deliverResult(list1);
        }
        if(list1.size()<1 || takeContentChanged()){
            forceLoad();
        }
    }

    @Override
    public List<PushType> loadInBackground() {
        List<PushType> list=new ArrayList<PushType>();
        Cursor c= pushDataDao.getPushDataTypeCount();
        if(c!=null){
            while (c.moveToNext()){
               String mtype=c.getString(c.getColumnIndexOrThrow(MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_TYPE));
               int count=c.getInt(c.getColumnIndexOrThrow("count"));
                PushType pushType=new PushType(mtype,count);
                if (count>0)
                {
                    list.add(pushType);
                }
            }
            c.close();
        }
        list1=list;
        return list;
    }


    @Override
    public void deliverResult(List<PushType> data) {
        if(isReset()){
            return;
        }
        super.deliverResult(data);
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        bus.unregister(this);
        super.onReset();
    }
}
