package com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter;

public interface BottomProfilInterface {
    public void onBottomProfilItemClick(int position);
}
