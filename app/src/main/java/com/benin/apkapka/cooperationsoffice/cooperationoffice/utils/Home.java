package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToOne;


/**
 * Created by joel on 13/10/2016.
 */
@Entity
public class Home {

    @Id
    public long id;
    public ToOne<HomeDatabaseRelation> homeDatabaseRelation;
    public long Id2;

  private String title;
  private String title_en;
  private String description;
  private String description_en;
  private String url;
  private String url2;
  private  String url3;
  private String preview_image;
  private String link_video;
  private  String content_type;
    public Home() {


    }

    public Home(long id) {
        this.id = id;
    }

    public Home(long id, String title, String title_en, String description, String description_en, String url, String url2, String url3, String preview_image, String link_video, String content_type) {
        this.Id2 = id;
        this.title = title;
        this.title_en = title_en;
        this.description = description;
        this.description_en = description_en;
        this.url = url;
        this.url2 = url2;
        this.url3 = url3;
        this.preview_image = preview_image;
        this.link_video = link_video;
        this.content_type = content_type;
    }

    public long getId2() {
        return Id2;
    }

    public void setId2(long id2) {
        Id2 = id2;
    }

    public String getContent_type() {
        return content_type;
    }

    public String getLink_video() {
        return link_video;
    }

    public String getPreview_image() {
        return preview_image;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public void setLink_video(String link_video) {
        this.link_video = link_video;
    }

    public void setPreview_image(String preview_image) {
        this.preview_image = preview_image;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_en() {
        return title_en;
    }

    public void setTitle_en(String title_en) {
        this.title_en = title_en;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription_en() {
        return description_en;
    }

    public void setDescription_en(String description_en) {
        this.description_en = description_en;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl2() {
        return url2;
    }

    public void setUrl2(String url2) {
        this.url2 = url2;
    }

    public String getUrl3() {
        return url3;
    }

    public void setUrl3(String url3) {
        this.url3 = url3;
    }
}
