package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.AccueilInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Divertissement;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.EndlessRecyclerViewScrollListener;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class BottomDivertissementActivity extends AppCompatActivity  implements  AccueilInterface{
    Toolbar toolbar;
    View linearVide;
    TextView tvVide;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    private boolean is_fr=false;
    boolean is_small,is_medium,is_large;
    LinearLayoutManager manager;
    MyAdapteurDivertissement myAdapteurDivertissement;
    List<Divertissement> divertissementList=new ArrayList<>();
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;

    private SessionManager sessionSaverData;
    private android.support.v7.widget.SwitchCompat switchCompat;
    private boolean getSwitchValue;
    private AlertDialog mDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottom_divertissement_layout);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        is_small=getResources().getBoolean(R.bool.is_small);
        is_medium=getResources().getBoolean(R.bool.is_medium);
        is_large=getResources().getBoolean(R.bool.is_large);
        toolbar=(android.support.v7.widget.Toolbar)findViewById(R.id.activity_divertissement_layout_toolbar);
        recyclerView=(RecyclerView)findViewById(R.id.divertissement_RecycleView);
        progressBar=(ProgressBar)findViewById(R.id.divertissement_Progress);
        linearVide=findViewById(R.id.divertissement_linearvide);
        tvVide=(TextView)findViewById(R.id.divertissement_textview_vide);
        sessionSaverData = new SessionManager(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_navigate_before_black_24dp);
        manager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        myAdapteurDivertissement=new MyAdapteurDivertissement(divertissementList,this,this);
        recyclerView.setAdapter(myAdapteurDivertissement);
        endlessRecyclerViewScrollListener=new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(page);
            }
        };
        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
        loadNextDataFromApi(0);
    }

    public void loadNextDataFromApi(int offset) {
        // Send an API request to retrieve appropriate paginated data
        //  --> Send the request including an offset value (i.e `page`) as a query parameter.
        //  --> Deserialize and construct new model objects from the API response
        //  --> Append the new data objects to the existing set of items inside the array of items
        //  --> Notify the adapter of the new items made with `notifyItemRangeInserted()
        //
        SessionManager sessionManager=new SessionManager(getApplicationContext());
        User user=sessionManager.getUser(getApplicationContext());
        String zone="";
        zone=user.getPays();
        if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
            zone="france";
        }
        getAllDivetissement(zone,offset);
    }

    @Override
    public void itemClicked(int position) {
      if(position<=(divertissementList.size()-1) && position>-1){
          Divertissement divertissement=divertissementList.get(position);
          Intent  intent=new Intent(this,BottomDivertissementSingleActvity.class);
          intent.putExtra("famille",divertissement.getFamille());
          startActivity(intent);
      }
    }
    public  void getAllDivetissement(String zone,int page){
        OkHttpClient client=new OkHttpClient();



        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(client)
                .build();

        MyInterface service=retrofit.create(MyInterface.class);
        Call<String> call=service.get_All_Divertissement_famille("famille",zone,page);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,Response<String> response) {
                if(response!=null)
                {
                    if(response.isSuccessful())
                    {
                        try {
                            String rep = response.body() != null ? response.body() : "";

                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error=o.getBoolean("error");
                            if(error==true)
                            {

                                progressBar.setVisibility(View.GONE);
                                linearVide.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                tvVide.setVisibility(View.VISIBLE);
                                tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));
                            }
                            else
                            {
                                progressBar.setVisibility(View.GONE);
                                linearVide.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                divertissementList.addAll(ParseJson.parseDivertissement(o));
                               if(divertissementList!=null && divertissementList.size()>0){
                                   myAdapteurDivertissement.notifyDataSetChanged();
                               }

                            }

                        }
                        catch(Exception e)
                        {

                            progressBar.setVisibility(View.GONE);
                            linearVide.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                            tvVide.setVisibility(View.VISIBLE);
                            tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));
                        }



                    }
                    else
                    {

                    }
                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

                progressBar.setVisibility(View.GONE);
                linearVide.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                tvVide.setVisibility(View.VISIBLE);
                tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));

            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.bottom_divertiss_menu,menu);

        MenuItem menuItem1=(MenuItem)menu.findItem(R.id.action_saverdata_divertiss);
        switchCompat = (SwitchCompat) MenuItemCompat.getActionView(menuItem1);
        switchCompat.setTextOn("Oui");
        switchCompat.setTextOff("Non");
        if(sessionSaverData.getKeySaverDataDivertissement().equals("Oui")){
            switchCompat.setChecked(true);
        }else{
            switchCompat.setChecked(false);
        }

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                if (isChecked){
                    getSwitchValue = isChecked;

                    if(getDialogStatus()){
                        mDialog.hide();
                    }else{
                        AlertDialog.Builder mBuilder = new AlertDialog.Builder(BottomDivertissementActivity.this);
                        View mView = getLayoutInflater().inflate(R.layout.layout_lite_option, null);
                        CheckBox mCheckBox = mView.findViewById(R.id.checkBoxLiteOption);
                        mBuilder.setView(mView);
                        mBuilder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                sessionSaverData.putSaverDataDivertissement("Oui");
                                dialogInterface.dismiss();
                                switchCompat.setChecked(isChecked);
                                recreate();
                            }
                        });

                        mBuilder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sessionSaverData.putSaverDataDivertissement("Non");
                                dialog.dismiss();
                                switchCompat.setChecked(false);
                            }
                        });

                        mDialog = mBuilder.create();
                        mDialog.show();
                        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if(compoundButton.isChecked()){
                                    storeDialogStatus(true);
                                }else{
                                    storeDialogStatus(false);
                                }
                            }
                        });
                        mDialog.show();
                    }
                }else if(!isChecked){
                    sessionSaverData.putSaverDataDivertissement("Non");
                    switchCompat.setChecked(false);
                    recreate();
                }
            }
        });

        return true;
    }

    private void storeDialogStatus(boolean isChecked){
        SharedPreferences mSharedPreferences = getSharedPreferences("CheckItem", MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putBoolean("item", isChecked);
        mEditor.apply();
    }

    private boolean getDialogStatus(){
        SharedPreferences mSharedPreferences = getSharedPreferences("CheckItem", MODE_PRIVATE);
        return mSharedPreferences.getBoolean("item", false);
    }

    class  MyAdapteurDivertissement extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

        List<Divertissement> divertissements;
        AccueilInterface accueilInterface;
        Context c;
        Divertissement ar;

        public MyAdapteurDivertissement(List<Divertissement> divertissements, AccueilInterface accueilInterface, Context c) {

            this.divertissements = divertissements;
            this.accueilInterface = accueilInterface;
            this.c = c;

        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v1= LayoutInflater.from(parent.getContext()).inflate(R.layout.actualite_item_layout,parent,false);
            ListViewHolder holder1=new ListViewHolder(v1);

            return holder1;


        }


        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ar=divertissements.get(position);
            ListViewHolder holder2=(ListViewHolder)holder;
            setUpListViewHolder(holder2,divertissements.get(position));
        }



        public void setUpListViewHolder(ListViewHolder holder, Divertissement article)
        {
            if(is_fr){
                holder.txtFamille.setText(article.getFamille().trim().toUpperCase());
            }
            else{
                holder.txtFamille.setText(article.getFamille_en().trim().toUpperCase());
            }
            holder.tvCount.setText("");
            holder.tvCount.setVisibility(View.GONE);
            String des="";
            //holder.tvDate.setText(MydateUtil.getDate(article.getDate(),is_fr));
            //holder.tvDate.setText(is_fr?article.getExtend_proprety1().trim():article.getExtend_proprety1_en().trim());
            holder.tvDate.setVisibility(View.GONE);
            if(is_fr)
            {
                holder.title.setText(article.getTitle().toUpperCase().trim());
                des=article.getDescription().trim();

                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des.length()>50)
                    {
                        des=des.substring(0,50)+"...";

                    }

                }
                else
                {
                    if(des.length()>110)
                    {
                        des=des.substring(0,110)+"...";
                    }
                }
            }
            else
            {
                holder.title.setText(article.getTitle_en().toUpperCase().trim());
                des=article.getDescription_en().trim();

                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des.length()>50)
                    {
                        des=des.substring(0,50)+"...";

                    }

                }
                else
                {
                    if(des.length()>110)
                    {
                        des=des.substring(0,110)+"...";
                    }
                }

            }


            holder.description.setText(des);

            final  float scale=getResources().getDisplayMetrics().density;
            if(is_small)
            {
                //Picasso.with(c).load(article.getUrl()).resize((int)(120*scale+0.5f),(int)(100*scale+0.5f)).into(holder.image);
                if (sessionSaverData.getKeySaverDataDivertissement().equals("Oui")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).resize((int)(120*scale+0.5f),(int)(100*scale+0.5f)).into(holder.image);
                }
                if(sessionSaverData.getKeySaverDataDivertissement().equals("Non")){
                    Picasso.with(c).load(article.getUrl()).resize((int)(120*scale+0.5f),(int)(100*scale+0.5f)).into(holder.image);
                }
            }
            else if(is_medium)
            {
                //Picasso.with(c).load(article.getUrl()).resize((int)(140*scale+0.5f),(int)(120*scale+0.5f)).into(holder.image);
                if (sessionSaverData.getKeySaverDataDivertissement().equals("Oui")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).resize((int)(140*scale+0.5f),(int)(120*scale+0.5f)).into(holder.image);
                }
                if(sessionSaverData.getKeySaverDataDivertissement().equals("Non")){
                    Picasso.with(c).load(article.getUrl()).resize((int)(140*scale+0.5f),(int)(120*scale+0.5f)).into(holder.image);
                }

            }
            else if (is_large)
            {
                //Picasso.with(c).load(article.getUrl()).resize((int)(150*scale+0.5f),(int)(130*scale+0.5f)).into(holder.image);
                if (sessionSaverData.getKeySaverDataDivertissement().equals("Oui")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).resize((int)(150*scale+0.5f),(int)(130*scale+0.5f)).into(holder.image);
                }
                if(sessionSaverData.getKeySaverDataDivertissement().equals("Non")){
                    Picasso.with(c).load(article.getUrl()).resize((int)(150*scale+0.5f),(int)(130*scale+0.5f)).into(holder.image);
                }

            }

        }



        @Override
        public int getItemCount() {
            return divertissements.size();
        }



        public  class ListViewHolder extends  RecyclerView.ViewHolder
        {
            TextView  title,tvCount,tvDate;
            TextView description;
            DinamicImageView  image;
            TextView  txtFamille;
            AppCompatImageView compatImageViewIcon;
            public  ListViewHolder(View v)
            {
                super(v);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        accueilInterface.itemClicked(getAdapterPosition());
                    }
                });
                title=(TextView)v.findViewById(R.id.actualite_item_layout_title);
                tvDate=(TextView)v.findViewById(R.id.actualite_date);
                tvCount=(TextView)v.findViewById(R.id.actualite_Count);
                description=(TextView)v.findViewById(R.id.actualite_item_layout_description);
                image=(DinamicImageView) v.findViewById(R.id.actualite_item_layout_image);
                compatImageViewIcon=(AppCompatImageView) v.findViewById(R.id.famille_list_vue_icon);
                compatImageViewIcon.setVisibility(View.GONE);
                txtFamille=(TextView)v.findViewById(R.id.famille_name);
            }


        }


    }
}
