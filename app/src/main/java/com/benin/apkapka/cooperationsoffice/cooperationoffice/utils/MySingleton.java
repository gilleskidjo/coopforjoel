package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by joel on 26/01/2016.
 */
public class MySingleton {
    private static MySingleton mySingleton;
    private RequestQueue mQueue;
    private static Context mContext;
    private ImageLoader mLoader;
    private MySingleton(Context c)
    {
        this.mContext=c;
        mQueue=getRequestQueue();
        mLoader=new ImageLoader(mQueue, new ImageLoader.ImageCache() {


            private  final LruCache<String,Bitmap> cache=new LruCache<String, Bitmap>(20);
            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
            cache.put(url,bitmap);
            }
        });

    }


    public static synchronized  MySingleton getInstance(Context c)
    {
        if(mySingleton==null)
        {
            mySingleton=new MySingleton(c);
        }
        return mySingleton;
    }

    public RequestQueue  getRequestQueue()
    {
        if(mQueue==null)
        {
            mQueue= Volley.newRequestQueue(mContext);
        }
        return mQueue;
    }



    public <T> void addToRequestQueue(Request<T> req)
    {
        getRequestQueue().add(req);

    }
    public void cancelPendingRequests(Object tag) {
        if (getRequestQueue() != null) {
            getRequestQueue().cancelAll(tag);
        }
    }

    public ImageLoader getImageLoader() {
        return mLoader;
    }
}
