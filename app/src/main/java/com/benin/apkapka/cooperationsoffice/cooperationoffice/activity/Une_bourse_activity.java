package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.Manifest;
import android.app.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;


import com.android.volley.toolbox.ImageLoader;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.AdapteurBuyListener;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.SearchEvent1;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.SearchEvent2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.SearchEvent3;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.AccueilInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.DialogFragment;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetUpdateStatistique;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetworkRequest;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetworkRequest2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetworkSearchOnHome;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForRetainData;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushRemoveFamilleAsyncLoader;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.*;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Bourse;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Emploi;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaychang.st.OnTextClickListener;
import com.jaychang.st.Range;
import com.jaychang.st.SimpleText;
import com.lapism.searchview.SearchView;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;

import com.squareup.picasso.Picasso;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import dmax.dialog.SpotsDialog;
import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;
import io.objectbox.query.QueryBuilder;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by joel on 30/01/2016.
 */
public class Une_bourse_activity extends BaseLanguageActivity implements View.OnClickListener,AccueilInterface,FragmentForNetworkRequest2.HandlerNetWorkRequestResponse,AdapteurBuyListener,LoaderManager.LoaderCallbacks<Void>,FragmentForNetworkSearchOnHome.HandlerNetWorkRequestResponseHomeSearch{





    String bad_url="https://cooperationsoffice.net/cooperation/images/";
    String bad_pdf="https://cooperationsoffice.net/cooperation/pdf/";
    private GestureDetectorCompat detector;
    PushRemoveFamilleAsyncLoader loader;

    RecyclerView liste;
    LinearLayoutManager manager;
    ProgressBar  bar2;
    TextView vide;
    View   linearvide;
    FragmentForRetainData f;
    private boolean is_fr;
    FragmentForNetUpdateStatistique netWorkupdate;
    MyAdapteur  adapteur;
    FragmentForNetworkRequest2 netWork;
    List<Produit> listP=new ArrayList<>();
    DialogFragment df;
    Toolbar toolbar;
    FragmentManager fm;
    FragmentForNetworkSearchOnHome forNetworkSearchOnHome;
    List<String> listSearch=new ArrayList<>();

    private static PayPalConfiguration payPalConfiguration=new PayPalConfiguration()
            .environment(Conf.PAYPAL_ENVIRONMENT)
            .clientId(Conf.PAYPAL_CLIENT_ID);
    List<PayPalItem> items=new ArrayList<PayPalItem>();
    DialogFragment dialogFragment;

    ShareDialog shareDialog;
    CallbackManager callbackManager;
    int position1;
    AlertDialog.Builder pro;
    boolean isDialogShow=false;
    ImageLoader imageLoader;
    PersistentSeachView searchView;
    int type2;
    final String action2[]=new String[3];
    Produit bourse;
    long current_id;
    List  listP1;
    Produit sharep;
    AlertDialog dialog;
    Box<FamilleRelation> familleRelationBox;
    FamilleRelation familleRelation;
    AlertDialog searchalertDialog;
    AlertDialog mAlertDialog1=null;
    String sms;
    AlertDialog alertDialog;
    private boolean CanBonusShare=false;
    BottomNavigationView navigationView;
    Produit produitBonus;
    ActionBar actionBar;
    EndlessRecyclerViewScrollListener  endlessRecyclerViewScrollListener;
    private  SessionManager sessionSaverData;

    public abstract class EndlessRecyclerViewScrollListener extends RecyclerView.OnScrollListener
    {
        // The minimum amount of items to have below your current scroll position
        // before loading more.
        private int visibleThreshold = 5;
        // The current offset index of data you have loaded
        public int currentPage = 0;
        // The total number of items in the dataset after the last load
        private int previousTotalItemCount = 0;
        // True if we are still waiting for the last set of data to load.
        private boolean loading = true;
        // Sets the starting page index
        private int startingPageIndex = 0;
        private int NextPost=0;

        RecyclerView.LayoutManager mLayoutManager;

        public EndlessRecyclerViewScrollListener(LinearLayoutManager layoutManager) {
            this.mLayoutManager = layoutManager;
        }

        public EndlessRecyclerViewScrollListener(GridLayoutManager layoutManager) {
            this.mLayoutManager = layoutManager;
            visibleThreshold = visibleThreshold * layoutManager.getSpanCount();
        }

        public EndlessRecyclerViewScrollListener(StaggeredGridLayoutManager layoutManager) {
            this.mLayoutManager = layoutManager;
            visibleThreshold = visibleThreshold * layoutManager.getSpanCount();
        }

        public int getLastVisibleItem(int[] lastVisibleItemPositions) {
            int maxSize = 0;
            for (int i = 0; i < lastVisibleItemPositions.length; i++) {
                if (i == 0) {
                    maxSize = lastVisibleItemPositions[i];
                }
                else if (lastVisibleItemPositions[i] > maxSize) {
                    maxSize = lastVisibleItemPositions[i];
                }
            }
            return maxSize;
        }

        // This happens many times a second during a scroll, so be wary of the code you place here.
        // We are given a few useful parameters to help us work out if we need to load some more data,
        // but first we check if we are waiting for the previous load to finish.
        @Override
        public void onScrolled(RecyclerView view, int dx, int dy) {

            LinearLayoutManager linearLayoutManager=null;
            int post=-1;
            if(mLayoutManager instanceof LinearLayoutManager){
                linearLayoutManager=(LinearLayoutManager)mLayoutManager;
            }
            if(linearLayoutManager!=null){
                post=linearLayoutManager.findLastVisibleItemPosition();

            }
            if(dy>0){

                if(post==NextPost){
                    Produit article=listP.get(post);
                    if(article!=null){
                        if(netWorkupdate!=null){
                            netWorkupdate.doRequestUpdateStatistiqueItem(article.getId(),"produit2");
                        }
                    }
                    NextPost=NextPost+1;
                }
            }
            else if(dy<0){

                if(post==NextPost){
                    Produit article=listP.get(post);
                    if(article!=null){
                        if(netWorkupdate!=null){
                            netWorkupdate.doRequestUpdateStatistiqueItem(article.getId(),"produit2");
                        }
                    }
                    NextPost=NextPost+1;
                }
            }
            else{

                if(post==NextPost){
                    Produit article=listP.get(post);
                    if(article!=null){
                        if(netWorkupdate!=null){
                            netWorkupdate.doRequestUpdateStatistiqueItem(article.getId(),"produit2");
                        }
                    }
                    NextPost=NextPost+1;
                }
            }
            int lastVisibleItemPosition = 0;
            int totalItemCount = mLayoutManager.getItemCount();

            if (mLayoutManager instanceof StaggeredGridLayoutManager) {
                int[] lastVisibleItemPositions = ((StaggeredGridLayoutManager) mLayoutManager).findLastVisibleItemPositions(null);
                // get maximum element within the list
                lastVisibleItemPosition = getLastVisibleItem(lastVisibleItemPositions);
            } else if (mLayoutManager instanceof GridLayoutManager) {
                lastVisibleItemPosition = ((GridLayoutManager) mLayoutManager).findLastVisibleItemPosition();
            } else if (mLayoutManager instanceof LinearLayoutManager) {
                lastVisibleItemPosition = ((LinearLayoutManager) mLayoutManager).findLastVisibleItemPosition();
            }

            // If it’s still loading, we check to see if the dataset count has
            // changed, if so we conclude it has finished loading and update the current page
            // number and total item count.
            if (loading && (totalItemCount > previousTotalItemCount)) {
                loading = false;
                previousTotalItemCount = totalItemCount;
            }

            // If it isn’t currently loading, we check to see if we have breached
            // the visibleThreshold and need to reload more data.
            // If we do need to reload some more data, we execute onLoadMore to fetch the data.
            // threshold should reflect how many total columns there are too
            if (!loading && (lastVisibleItemPosition + visibleThreshold) > totalItemCount) {
                currentPage++;
                onLoadMore(currentPage, totalItemCount, view);
                loading = true;
            }
        }

        // Call whenever performing new searches
        public void resetState() {
            this.currentPage = this.startingPageIndex;
            this.previousTotalItemCount = 0;
            this.loading = true;
        }

        // Defines the process for actually loading more data based on page
        public abstract void onLoadMore(int page, int totalItemsCount, RecyclerView view);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.emploi_primary_dar));
        }
        manageFacebook();
        setContentView(R.layout.une_bource_item_layout);
        sessionSaverData = new SessionManager(this);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        final String action1[]={getResources().getString(R.string.main_action_contacter),getResources().getString(R.string.main_action_sms),getResources().getString(R.string.main_action_email)};
        action2[0]=getResources().getString(R.string.main_action_contacter1);
        action2[1]=getResources().getString(R.string.main_action_sms1);
        action2[2]=getResources().getString(R.string.main_action_email);
        navigationView=(BottomNavigationView)findViewById(R.id.bottom_navigation_view);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){

                    case R.id.bootom_chat:
                        Intent intent=new Intent(Une_bourse_activity.this,BottomChatConversation_Activity.class);
                        startActivity(intent);
                        return true;
                    case R.id.bootom_profile:
                        Intent intent1=new Intent(Une_bourse_activity.this,BottomProfileActivity.class);
                        startActivity(intent1);
                        return  true;
                    case R.id.bootom_entertement:
                        Intent intent2=new Intent(Une_bourse_activity.this,BottomDivertissementActivity.class);
                        startActivity(intent2);
                        return true;
                }
                return false;
            }
        });
        liste=(RecyclerView) findViewById(R.id.un_modele_view_RecycleView);
        bar2=(ProgressBar)findViewById(R.id.un_modele_view_progressBar2);
        linearvide=findViewById(R.id.un_modele_view_linearvide);
        vide=(TextView)findViewById(R.id.un_modele_view_textview_vide);
        BoxStore boxStore=((MyApplication)getApplicationContext()).getBoxStore();
        familleRelationBox=boxStore.boxFor(FamilleRelation.class);
        familleRelation=new FamilleRelation(8);
        liste.setVisibility(View.GONE);
        manager=new LinearLayoutManager(this);
        liste.setLayoutManager(manager);
        endlessRecyclerViewScrollListener=new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(page);
            }
        };
        liste.addOnScrollListener(endlessRecyclerViewScrollListener);
        toolbar=(Toolbar)findViewById(R.id.une_bourse_toolbar);
        setSupportActionBar(toolbar);
        actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        FragmentManager fm=getSupportFragmentManager();

        f=(FragmentForRetainData) fm.findFragmentByTag(FragmentForRetainData.TAG);
        if(f==null)
        {
            f=new FragmentForRetainData();
            fm.beginTransaction().add(f,FragmentForRetainData.TAG).commit();

            if(getIntent()!=null)
            {
                Bundle  extra=getIntent().getExtras();
                if(extra!=null)
                {
                    if(extra.containsKey("from_co_direct")){
                        bourse=HelperActivity.getInstance().getProduit();
                        current_id=bourse.getId();
                        listP.add(bourse);
                        adapteur=new MyAdapteur(listP,this,this,this,liste);
                        liste.setAdapter(adapteur);
                        actionBar.setTitle(extra.getString("famille"));
                    }

                }
            }


        }


        if(getIntent()!=null)
        {
            Bundle bundle=getIntent().getExtras();
            if(bundle!=null)
            {
                if(bundle.containsKey("from_co_direct")){
                    type2=bundle.getInt("type2",0);
                }

            }
        }

        FragmentManager fm1=getSupportFragmentManager();
        netWork=(FragmentForNetworkRequest2) fm1.findFragmentByTag(FragmentForNetworkRequest2.TAG);
        if(netWork==null)
        {

            netWork=new FragmentForNetworkRequest2();
            fm1.beginTransaction().add(netWork,FragmentForNetworkRequest.TAG).commit();
            netWork.setInterface(this);

            if(isConnected()){
                if(getIntent().getExtras()!=null) {
                    Bundle bundle = getIntent().getExtras();

                    if (bundle.containsKey("from_co_direct")) {
                        loadNextDataFromApi(0);
                    }
                    else {

                        if(getIntent().getExtras().containsKey("locale_uri")){

                            Uri uri=getIntent().getData();
                            if(uri!=null){
                                String id=uri.getQueryParameter("id");
                                getAnnonce(id);
                            }

                        }
                        else{

                            FirebaseDynamicLinks.getInstance()
                                    .getDynamicLink(getIntent())
                                    .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                                        @Override
                                        public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                                            // Get deep link from result (may be null if no link is found)
                                            Uri deepLink = null;
                                            if (pendingDynamicLinkData != null) {

                                                deepLink = pendingDynamicLinkData.getLink();
                                                String id=deepLink.getQueryParameter("id");
                                                getAnnonce(id);
                                            }



                                            // Handle the deep link. For example, open the linked
                                            // content, or apply promotional credit to the user's
                                            // account.
                                            // ...

                                            // ...
                                        }
                                    })
                                    .addOnFailureListener(this, new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {

                                        }
                                    });
                        }
                    }
                }

            }
            else{
                Snackbar snackbar=Snackbar.make(findViewById(android.R.id.content),is_fr?"Aucune connexion internet détectée":"No internet connection detected",Snackbar.LENGTH_LONG);
                snackbar.show();
               /* ProduitDatabaseRelation relation=databaseRelationQuery.findFirst();
                if(relation!=null){
                    listP=relation.getList();
                }
                */
                if(listP!=null && listP.size()>0){
                    adapteur=new MyAdapteur(listP,Une_bourse_activity.this,Une_bourse_activity.this,Une_bourse_activity.this,liste);
                    liste.setAdapter(adapteur);
                    bar2.setVisibility(View.GONE);
                    linearvide.setVisibility(View.GONE);
                    liste.setVisibility(View.VISIBLE);
                }
                else {
                    bar2.setVisibility(View.GONE);
                    vide.setText(getResources().getString(R.string.error_frag_netw_parse));
                    liste.setVisibility(View.GONE);
                    linearvide.setVisibility(View.VISIBLE);
                }
            }

        }
        else
        {
            netWork.setInterface(this);
            listP=netWork.getListproduit();

            if(listP!=null && listP.size()>0)
            {
                adapteur=new MyAdapteur(listP,this,this,this,liste);
                liste.setAdapter(adapteur);
                bar2.setVisibility(View.GONE);
                linearvide.setVisibility(View.GONE);
                liste.setVisibility(View.VISIBLE);

            }
            else
            {


                if(netWork.isMessage())
                {
                    bar2.setVisibility(View.GONE);
                    vide.setText(netWork.getSms());
                    liste.setVisibility(View.VISIBLE);
                    linearvide.setVisibility(View.GONE);
                }
            }

        }

        imageLoader=MySingleton.getInstance(this).getImageLoader();
        Loader<Void> loader=getSupportLoaderManager().getLoader(0);
        if(loader==null){
            getSupportLoaderManager().initLoader(0,null,this);
        }
        else{
            getSupportLoaderManager().restartLoader(0,null,this);
        }
         fm=getSupportFragmentManager();
         netWorkupdate=(FragmentForNetUpdateStatistique) fm.findFragmentByTag(FragmentForNetUpdateStatistique.TAG);
        if(netWorkupdate==null){
            netWorkupdate=new FragmentForNetUpdateStatistique();
            fm.beginTransaction().add(netWorkupdate,FragmentForNetUpdateStatistique.TAG).commit();
            netWorkupdate.doRequestUpdateStatistique(12);
            if(bourse!=null){
                netWorkupdate.doRequestUpdateStatistiqueItemFamille("produit2",bourse.getType()==null?"":bourse.getType(),bourse.getCategory()==null?"":bourse.getCategory(),bourse.getFamille()==null?"":bourse.getFamille());
            }

        }

        fm=getSupportFragmentManager();
        forNetworkSearchOnHome=(FragmentForNetworkSearchOnHome) fm.findFragmentByTag(FragmentForNetworkSearchOnHome.TAG);
        if(forNetworkSearchOnHome==null){
            forNetworkSearchOnHome=new FragmentForNetworkSearchOnHome();
            fm.beginTransaction().add(forNetworkSearchOnHome,FragmentForNetworkSearchOnHome.TAG).commit();
            forNetworkSearchOnHome.setInterface(this);
        }
        else{
            forNetworkSearchOnHome.setInterface(this);
        }
        searchView=(PersistentSeachView) findViewById(R.id.search_view);
        searchView.setHint(getResources().getString(R.string.main_menu_search));
        searchView.setVersionMargins(SearchView.VERSION_MARGINS_MENU_ITEM);
        searchView.setVersion(SearchView.VERSION_MENU_ITEM);
        searchView.setVoice(true);
        searchView.setOnVoiceClickListener(new SearchView.OnVoiceClickListener() {
            @Override
            public void onVoiceClick() {
                requestRecordePermission();
            }
        });

      //  searchView.setNavigationIconAnimation(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                if(searchView!=null){

                    if(adapteur!=null){
                        adapteur.getFilter().filter(newText);
                    }
                }
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                if(searchView!=null){
                    searchView.close(true);
                    if(adapteur!=null){
                        adapteur.getFilter().filter(query);
                    }
                }
                return false;
            }
        });


    }
    public void loadNextDataFromApi(int offset) {
        // Send an API request to retrieve appropriate paginated data
        //  --> Send the request including an offset value (i.e `page`) as a query parameter.
        //  --> Deserialize and construct new model objects from the API response
        //  --> Append the new data objects to the existing set of items inside the array of items
        //  --> Notify the adapter of the new items made with `notifyItemRangeInserted()
        //
        SessionManager sessionManager=new SessionManager(getApplicationContext());
        User user=sessionManager.getUser(getApplicationContext());
        String zone="";
        zone=user.getPays();
        if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
            zone="france";
        }
        netWork.doRequestProduit_By_Category(bourse.getCategory(),bourse.getPays(),bourse.getFamille(),is_fr==true?"fr":"en",bourse.getType(),type2,listP,current_id,zone,offset);
    }
    public void getAnnonce(String id){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.get_publish_annonce(""+id);

        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");

                            if (error) {

                            } else {

                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {



                                } else {

                                    List<Produit> list=ParseJson.parseProduit(o,getApplicationContext());
                                    if(list!=null && list.size()>0){
                                        bourse=list.get(0);
                                        current_id=bourse.getId();
                                        listP.add(bourse);
                                        adapteur=new MyAdapteur(listP,Une_bourse_activity.this,Une_bourse_activity.this,Une_bourse_activity.this,liste);
                                        liste.setAdapter(adapteur);
                                        actionBar.setTitle(bourse.getFamille());
                                        loadNextDataFromApi(0);

                                    }
                                }

                            }

                        } catch (Exception e) {


                        }

                    }//fin is success,
                    else {



                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

            }
        });
    }
    private void requestRecordePermission(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.RECORD_AUDIO)){

                AlertDialog.Builder builder=new AlertDialog.Builder(this);
                builder.setTitle(is_fr?"Autorisation d'enregistrement":"Registration Authorization ");
                builder.setMessage(is_fr?"Vous devez activer la recherche vocale pour utiliser cette fonctionnalité ":"You must enable voice search to use this feature");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        searchalertDialog.dismiss();
                    }
                });
                builder.setNegativeButton(is_fr?"Annuler":"Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        searchalertDialog.dismiss();
                    }
                });
                searchalertDialog=builder.show();

            }
            else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.RECORD_AUDIO}, Conf.RECORD_REQUEST_CODE);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case Conf.RECORD_REQUEST_CODE:
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){

                }
                else {

                }
                return;
        }
    }



    private boolean isConnected()
    {
        ConnectivityManager cm=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        if(info!=null && info.isConnected() && info.isAvailable())
        {
            return  true;
        }

        return  false;
    }

    @Override
    public void onResposeSuccessSearch(String s) {
        List<String> list=forNetworkSearchOnHome.getListTitle();
        if(list!=null && list.size()>0){


        }
    }

    @Override
    public void onresponseSearchFailAndPrintErrorResponse() {

    }

    @Override
    public Loader<Void> onCreateLoader(int id, Bundle args) {
       if(bourse!=null){
           loader=new PushRemoveFamilleAsyncLoader(this, PushHandler.type4,bourse.getCategory(),bourse.getFamille());
           loader.forceLoad();
           return loader;
       }
       return new PushRemoveFamilleAsyncLoader(this, "","","");
    }

    @Override
    public void onLoadFinished(Loader<Void> loader, Void data) {

    }

    @Override
    public void onLoaderReset(Loader<Void> loader) {

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putBoolean("dialog",isDialogShow);
    }

    @Override
    public void onMore(Object c) {
        Produit article=(Produit) c;

        if(!article.getChoixf().equals(""))
        {
            if(article.getChoixf().trim().equals("pdf"))
            {
                if (!article.getLien_catalogue().equals(bad_pdf))
                {
                    Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(article.getLien_catalogue()));
                    if (intent.resolveActivity(getPackageManager())!=null)
                    {
                        startActivity(intent);
                    }
                    else
                    {
                        MyToast.show(Une_bourse_activity.this,getResources().getString(R.string.install_application),false);

                    }

                }
                else if(!article.getLien_fichier().equals(""))
                {
                    Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(article.getLien_fichier()));
                    if (intent.resolveActivity(getPackageManager())!=null)
                    {
                        startActivity(intent);
                    }
                    else
                    {
                        MyToast.show(Une_bourse_activity.this,getResources().getString(R.string.install_application),false);

                    }

                }
                else
                {
                    MyToast.show(Une_bourse_activity.this,is_fr==true?"Auncun element ajouter":"Nothing found",false);
                }
            }
            else if (article.getChoixf().trim().equals("lien"))
            {
              if(canparseLink(((Produit) c).getLien_fichier()).equalsIgnoreCase("")){
                  if (!article.getLien_fichier().equals(""))
                  {
                      Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(article.getLien_fichier()));
                      if (intent.resolveActivity(getPackageManager())!=null)
                      {
                          startActivity(intent);
                      }
                      else
                      {
                          MyToast.show(Une_bourse_activity.this,getResources().getString(R.string.install_application),false);

                      }
                  }
                  else if(!article.getLien_catalogue().equals(bad_pdf))
                  {
                      Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(article.getLien_catalogue()));
                      if (intent.resolveActivity(getPackageManager())!=null)
                      {
                          startActivity(intent);
                      }
                      else
                      {
                          MyToast.show(Une_bourse_activity.this,getResources().getString(R.string.install_application),false);

                      }
                  }
                  else
                  {
                      MyToast.show(Une_bourse_activity.this,is_fr==true?"Auncun element ajouter":"Nothing found",false);
                  }
              }
              else{
                  Intent intent=new Intent(this,SpotActivity.class);
                  intent.putExtra("id",canparseLink(((Produit) c).getLien_fichier()));
                  intent.putExtra("type","vitrine");
                  startActivity(intent);
              }



            }
        }
        else
        {
            if(!article.getLien_catalogue().equals(bad_pdf))
            {
                Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(article.getLien_catalogue()));
                if (intent.resolveActivity(getPackageManager())!=null)
                {
                    startActivity(intent);
                }
                else
                {
                    MyToast.show(Une_bourse_activity.this,getResources().getString(R.string.install_application),false);

                }
            }
            else if(!article.getLien_fichier().equals(""))
            {
                Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(article.getLien_fichier()));
                if (intent.resolveActivity(getPackageManager())!=null)
                {
                    startActivity(intent);
                }
                else
                {
                    MyToast.show(Une_bourse_activity.this,getResources().getString(R.string.install_application),false);

                }
            }
            else
            {
                MyToast.show(Une_bourse_activity.this,is_fr==true?"Auncun element ajouter":"Nothing found",false);
            }


        }
    }

    @Override
    public void onShare(Object c) {
       final Produit  article=(Produit) c;

        if(article.getLien_bonus()!=null && !article.getLien_bonus().trim().equalsIgnoreCase("") && !article.getLien_bonus().trim().equalsIgnoreCase("null")){

            SessionManager sessionManager=new SessionManager(this);
            User user=sessionManager.getUser(this);
            if(sessionManager.isUserDataSet()){
                AlertDialog.Builder builder=  builder= new AlertDialog.Builder(this);

                builder.setMessage(is_fr?"Invitez vos amis à visiter cette publication avant d'accéder au bonus.":"Invite your friends to visit this publication before accessing the bonus.");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (alertDialog != null) {
                            alertDialog.dismiss();
                        }

                        ViewBonus(article);
                    }
                });
                alertDialog=builder.show();

            }
            else{
                MyToast.show(this,is_fr?"Completer votre profil":"Complete your profile",true);
            }
        }
        else{
            MyToast.show(this,is_fr?"Aucun bonus disponible":"No bonus available",true);
        }
    }

    private void ViewBonus(final  Produit p){
        ShareBonus(p);
        produitBonus=p;
        CanBonusShare=true;

    }

    private void ShareBonus(Produit article)
    {
        SessionManager sessionManager=new SessionManager(this);
        User user=sessionManager.getUser(this);
        sharep=article;
        Uri.Builder builder1=Uri.parse(PushHandler.pushBaseUlr).buildUpon();
        builder1.appendPath(PushHandler.type4);
        builder1.appendQueryParameter("id",""+sharep.getId());
        builder1.appendQueryParameter("famille",sharep.getFamille());
        builder1.appendQueryParameter("type",sharep.getType());
        builder1.appendQueryParameter("category",sharep.getCategory());
        Uri uri=builder1.build();
        final android.app.AlertDialog mAlertDialog=new SpotsDialog(Une_bourse_activity.this,R.style.style_spot_emploi);
        mAlertDialog.show();
        Task<ShortDynamicLink> shortDynamicLinkTask= FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(uri)
                .setDynamicLinkDomain("cooperations0ffices.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.benin.apkapka.cooperationsoffice.cooperationoffice")
                        .setFallbackUrl(Uri.parse("http://cooperationsoffice.net"))
                        .build())
                .setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder()
                        .setTitle(sharep.getTitle())
                        .setDescription(is_fr?"CLIQUEZ".toUpperCase()+" pour "+"VISITER".toUpperCase()+" et "+"PARTAGER".toUpperCase()+" notre publication ":"CLICK".toUpperCase()+" to "+"VISIT".toUpperCase()+" and "+"SHARE".toUpperCase()+" our publication")
                        .setImageUrl(Uri.parse(""+sharep.getUrl()))
                        .build())
                .buildShortDynamicLink()

                .addOnCompleteListener(Une_bourse_activity.this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        mAlertDialog.dismiss();
                        if(task.isSuccessful()){

                            Uri uri=task.getResult().getShortLink();
                            ShareLinkContent linkContent=new ShareLinkContent.Builder()
                                    .setContentUrl(uri)
                                    .build();
                            if(shareDialog.canShow(ShareLinkContent.class)){
                                shareDialog.show(linkContent);

                            }
                        }
                        else {

                            android.support.v7.app.AlertDialog.Builder mBuilder1=new android.support.v7.app.AlertDialog.Builder(Une_bourse_activity.this)
                                    .setMessage(is_fr?"Impossible de publier l'image":"Can not publish image")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if(mAlertDialog1!=null){
                                                mAlertDialog1.dismiss();
                                            }
                                        }
                                    });
                            mAlertDialog1=mBuilder1.show();
                        }

                    }
                });

    }
    @Override
    public void onBuy(Object c) {
       final   Produit article=(Produit) c;

        final String num=article.getNb_price_en();
        sms="Bonjour! \n Je suis intéressé(e) par votre annonce "+((Produit) c).getTitle().toUpperCase()+" postée sur Cooperations office. \n Merci de me renseigner davantage.";
        if(is_fr==false){
            sms="Hello! \n" + " I am interested in your property "+ ((Produit)c).getTitle_en().toUpperCase() +" posted on Cooperations office.\n" + "Thank you for informing me more.";
        }
        AlertDialog.Builder builder=new AlertDialog.Builder(Une_bourse_activity.this);
        builder.setItems(action2, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which)
                {
                    case 0:
                        Intent intent1=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+num));

                        if(intent1.resolveActivity(getPackageManager())!=null)
                        {
                            startActivity(Intent.createChooser(intent1,getResources().getString(R.string.une_activity_appelez_nous)));
                        }
                        else
                        {
                            Toast.makeText(Une_bourse_activity.this,getResources().getString(R.string.une_activity_installer_app_telephonie),Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 1:
                        Intent intent2 = new Intent(Intent.ACTION_SENDTO);
                        intent2.addCategory(Intent.CATEGORY_DEFAULT);
                        intent2.setType("vnd.android-dir/mms-sms");
                        intent2.setData(Uri.parse("sms:" +num));
                        intent2.putExtra("sms_body", sms);
                        if(intent2.resolveActivity(getPackageManager())!=null)
                        {
                            startActivity(intent2);
                        }
                        else
                        {
                            Toast.makeText(Une_bourse_activity.this,getResources().getString(R.string.une_activity_installez_app_sms),Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 2:
                        if(article.getEmail()!=null && !article.getEmail().equalsIgnoreCase("") && !article.getEmail().equalsIgnoreCase("vide")){

                            Intent intent = new Intent(Intent.ACTION_SENDTO , Uri.parse("mailto:"+""+article.getEmail()));
                            intent.putExtra(Intent.EXTRA_SUBJECT, sms);
                            if (intent.resolveActivity(getPackageManager()) != null) {
                                startActivity(intent);
                            }
                            else{
                                Toast.makeText(Une_bourse_activity.this,getResources().getString(R.string.une_activity_installez_app_sms),Toast.LENGTH_SHORT).show();
                            }
                        }
                        else{
                            MyToast.show(getApplicationContext(),is_fr?"Impossible de contacter l'auteur par email":"Could not contact the author by email",true);
                        }
                        break;


                }

            }
        }) ;
        builder.show();

    }

    @Override
    public void onAddCard(Object c) {

    }

    private void manageFacebook()
    {
        callbackManager=CallbackManager.Factory.create();
        shareDialog=new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {


                Toast.makeText(Une_bourse_activity.this,getResources().getString(R.string.error_frag_netw_error),Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void startbookShare()
    {
        if(shareDialog.canShow(ShareLinkContent.class))
        {
            ShareLinkContent content=new ShareLinkContent.Builder()

                    .setContentTitle(f.getC().getTitle())
                    .setContentDescription(f.getC().getDescription())
                    .setContentUrl(Uri.parse(Config.URL_PLAY_STORE))
                    .setImageUrl(Uri.parse(f.getC().getUrl()))
                    .build();
            shareDialog.show(content);

        }
    }
    private void startbookShare(Produit article)
    {
        SessionManager sessionManager=new SessionManager(this);
        final User user=sessionManager.getUser(this);
        sharep=article;
        Uri.Builder builder1=Uri.parse(PushHandler.pushBaseUlr).buildUpon();
        builder1.appendPath(PushHandler.type4);
        builder1.appendQueryParameter("id",""+sharep.getId());
        builder1.appendQueryParameter("famille",sharep.getFamille());
        builder1.appendQueryParameter("type",sharep.getType());
        builder1.appendQueryParameter("category",sharep.getCategory());
        final Uri uri=builder1.build();
        android.support.v7.app.AlertDialog.Builder builder=new android.support.v7.app.AlertDialog.Builder(this);
        builder.setCustomTitle(LayoutInflater.from(this).inflate(R.layout.custom_dialog_title,null));
        View v= LayoutInflater.from(this).inflate(R.layout.dialog_view,null);
        builder.setView(v);
        ImageView view1=(ImageView) v.findViewById(R.id.image1);
        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                final android.app.AlertDialog mAlertDialog=new SpotsDialog(Une_bourse_activity.this,R.style.style_spot_bourse);
                mAlertDialog.show();
                Task<ShortDynamicLink> shortDynamicLinkTask= FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLink(uri)
                        .setDynamicLinkDomain("cooperations0ffices.page.link")
                        .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.benin.apkapka.cooperationsoffice.cooperationoffice")
                                .setFallbackUrl(Uri.parse("http://cooperationsoffice.net"))
                                .build())
                        .setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder()
                                .setTitle(sharep.getTitle())
                                .setDescription(is_fr?"CLIQUEZ".toUpperCase()+" pour "+"VISITER".toUpperCase()+" et "+"PARTAGER".toUpperCase()+" notre publication ":"CLICK".toUpperCase()+" to "+"VISIT".toUpperCase()+" and "+"SHARE".toUpperCase()+" our publication")
                                .setImageUrl(Uri.parse(""+sharep.getUrl()))
                                .build())
                        .buildShortDynamicLink()

                        .addOnCompleteListener(Une_bourse_activity.this, new OnCompleteListener<ShortDynamicLink>() {
                            @Override
                            public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                                mAlertDialog.dismiss();
                                if(task.isSuccessful()){

                                    Uri uri=task.getResult().getShortLink();
                                    ShareLinkContent linkContent=new ShareLinkContent.Builder()
                                            .setContentUrl(uri)
                                            .build();
                                    if(shareDialog.canShow(ShareLinkContent.class)){
                                        shareDialog.show(linkContent);
                                    }
                                }
                                else {

                                    AlertDialog.Builder mBuilder1=new AlertDialog.Builder(Une_bourse_activity.this)
                                            .setMessage(is_fr?"Impossible de publier l'image":"Can not publish image")
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    if(mAlertDialog1!=null){
                                                        mAlertDialog1.dismiss();
                                                    }
                                                }
                                            });
                                    mAlertDialog1=mBuilder1.show();
                                }

                            }
                        });

            }
        });

        ImageView view2=(ImageView) v.findViewById(R.id.image2);
        view2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                Intent intent=new Intent(Une_bourse_activity.this,PhoneContactActivity.class);
                startActivity(intent);
            }
        });

        dialog=builder.create();
        dialog.show();

    }

    private void  startPayPalService()
    {
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,payPalConfiguration);
        startService(intent);
    }
    private  String canparseLink(String link){
        if(link.startsWith("https://www.youtube.com")){
            if(link.contains("?v=")){
                int position=link.lastIndexOf("=");
                String p=link.substring(position+1);

                if(p.length()>0)
                {
                    return p;

                }


            }

        }

        return "";
    }
    private void verifyPayment(long id,final  String paymentId,final  String paymentClient,final  String category ,final  String lang)
    {
       df.showDialogue(is_fr==true?"Vérification":"Checking");
        OkHttpClient client=new OkHttpClient();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(client)
                .build();

        MyInterface2 service=retrofit.create(MyInterface2.class);

        Call<String> call=service.verifyPayment(id,paymentId,paymentClient,"bourse",lang);
        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,Response<String> response) {
                if(response!=null)
                {
                    if(response.isSuccessful())
                    {
                        try {
                            String rep = response.body() != null ? response.body() : "";

                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error=o.getBoolean("error");
                            String message=o.getString("message");

                            if(error==true)
                            {
                                 df.hideDialog();
                                df.showDialogPro(""+message);
                            }
                            else
                            {   df.hideDialog();
                                df.showDialogPro(""+message);

                            }

                        }
                        catch(Exception e)
                        {
                            e.printStackTrace();
                          df.hideDialog();
                        }



                    }
                    else
                    {
                        Log.e("Reponse pas de succesc","Pas");
                    }
                }
                else
                {

                }
                df.hideDialog();
            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {
                dialogFragment.hideDialog();
                MyToast.show(Une_bourse_activity.this,""+t.getMessage(),false);
            }
        });

    }


    private void beganPayment()
    {

        BigDecimal taux=new BigDecimal(589.7833);
        BigDecimal re=new BigDecimal(0);
        BigDecimal reduction=new BigDecimal(f.getC().getReduction());

        BigDecimal price=new BigDecimal(f.getC().getNb_price());
        if (reduction.compareTo(new BigDecimal(0.0))==0)
        {
            re=price.divide(taux,2,BigDecimal.ROUND_HALF_UP);

        }
        else
        {
            re=price.divide(taux,2,BigDecimal.ROUND_HALF_UP);
            re=re.subtract((re.multiply(reduction).divide( new BigDecimal(100))));

        }
        re=re.setScale(2,BigDecimal.ROUND_HALF_UP);
        PayPalItem item=new PayPalItem(f.getC().getTitle(),1,re,Conf.DEFAULT_CURRENCY,""+f.getC().getId());
        items.add(item);
        PayPalPayment payment=preparedcard();
        Intent intent=new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,payPalConfiguration);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT,payment);
        startActivityForResult(intent,Conf.REQUEST_CODE_PAYEMENT);

    }
    private void beganPayment(Produit article)
    {

        BigDecimal taux=new BigDecimal(589.7833);
        BigDecimal reduction=new BigDecimal(article.getReduction());
        BigDecimal price=new BigDecimal(article.getNb_price());
        BigDecimal re=new BigDecimal(0);
        if (reduction.compareTo(new BigDecimal(0.0))==0)
        {
            re=price.divide(taux,2,BigDecimal.ROUND_HALF_UP);

        }
        else
        {
            re=price.divide(taux,2,BigDecimal.ROUND_HALF_UP);
            re=re.subtract((re.multiply(reduction).divide( new BigDecimal(100))));

        }
        re=re.setScale(2,BigDecimal.ROUND_HALF_UP);
        PayPalItem item=new PayPalItem(article.getTitle(),1,re,Conf.DEFAULT_CURRENCY,""+article.getId());
        items.add(item);
        PayPalPayment payment=preparedcard();
        Intent intent=new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,payPalConfiguration);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT,payment);
        startActivityForResult(intent,Conf.REQUEST_CODE_PAYEMENT);


    }

    private PayPalPayment preparedcard()
    {
        PayPalItem [] payPalItems=new PayPalItem[items.size()];
        payPalItems=items.toArray(payPalItems);

        BigDecimal total=PayPalItem.getItemTotal(payPalItems);
        String list_name="";
         for(int i=0;i<items.size();i++)
         {String name=items.get(i).getName();
            if(!Arrays.asList(items).contains(name))
              list_name=list_name+name+" ";

         }

        PayPalPayment payment=new PayPalPayment(total,Conf.DEFAULT_CURRENCY,list_name,Conf.PAYMENT_INTENTE);
        payment.items(payPalItems);
        payment.enablePayPalShippingAddressesRetrieval(true);
        return payment;
    }

    @Override
    public void onResposeSuccess() {

        if(listP!=null && listP.size()>0){


             try {
                 FamilleRelation relation=new FamilleRelation(8);
                 familleRelationBox.remove(familleRelation);
                 familleRelationBox.attach(relation);
                 relation.produits.addAll(listP);
                 familleRelationBox.put(relation);
             }
             finally {

             }
            adapteur.notifyDataSetChanged();
            bar2.setVisibility(View.GONE);
            linearvide.setVisibility(View.GONE);
            liste.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onresponseFailAndPrintErrorResponse() {
        bar2.setVisibility(View.GONE);
        liste.setVisibility(View.VISIBLE);
        linearvide.setVisibility(View.GONE);
    }

    @Override
        public void itemClicked(int position) {
        RecyclerView.ViewHolder holder=liste.findViewHolderForAdapterPosition(position);
        if(holder instanceof MyAdapteur.ListViewHolderVideo){
            Produit article=listP.get(position);
            Intent intent=new Intent(this,SpotActivity.class);
            intent.putExtra("id",article.getVideo_id());
            intent.putExtra("type","vitrine");
            if(article!=null){

                intent.putExtra("image",""+article.getUrl());
                intent.putExtra("title",is_fr?article.getTitle():article.getTitle_en());
                intent.putExtra("invitaion",article.getExtend_proprety1());
            }
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
         if(searchView.isSearchOpen()){
             searchView.close(true);
         }
         else {
             super.onBackPressed();
         }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==SearchView.SPEECH_REQUEST_CODE && resultCode==RESULT_OK){
            List<String> results=data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if(results!=null && results.size()>0){
                String query=results.get(0);
                if(!TextUtils.isEmpty(query)){
                    searchView.setTextOnly(query);
                }
            }
        }
        callbackManager.onActivityResult(requestCode,resultCode,data);

        if(CanBonusShare){
            CanBonusShare=false;
            Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(produitBonus.getLien_bonus()));
            if (intent.resolveActivity(getPackageManager())!=null)
            {
                startActivity(intent);
            }
            else
            {
                MyToast.show(Une_bourse_activity.this,getResources().getString(R.string.install_application),false);

            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_autre, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            case  R.id.action_search:
                searchView.open(true);
                return true;
            case R.id.autre_share:
                Intent intent2=new Intent(Intent.ACTION_SEND);
                intent2.setType("text/plain");
                String url= "https://play.google.com/store/apps/details?id=com.benin.apkapka.cooperationsoffice.cooperationoffice";
                intent2.putExtra(Intent.EXTRA_TEXT, url);
                intent2.putExtra(Intent.EXTRA_SUBJECT, "Cooperations office");
                if(intent2.resolveActivity(getPackageManager())!=null)
                {

                    String[] blacklist = new String[]{"com.google.android.gm","com.facebook.katana" };
                    startActivity(generateCustomChooserIntent(intent2, blacklist));
                }
                else
                {
                    MyToast.show(this, getResources().getString(R.string.une_activity_installer_app_partage), false);
                }
                return true;



        }

        return super.onOptionsItemSelected(item);
    }
    private Intent generateCustomChooserIntent(Intent prototype, String[] forbiddenChoices) {
        List<Intent> targetedShareIntents = new ArrayList<>();
        List<HashMap<String, String>> intentMetaInfo = new ArrayList<HashMap<String, String>>();
        Intent chooserIntent;

        Intent dummy = new Intent(prototype.getAction());
        dummy.setType(prototype.getType());
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(dummy, 0);

        if (!resInfo.isEmpty()) {
            for (ResolveInfo resolveInfo : resInfo) {
                if (resolveInfo.activityInfo == null || Arrays.asList(forbiddenChoices).contains(resolveInfo.activityInfo.packageName))
                    continue;

                HashMap<String, String> info = new HashMap<String, String>();
                info.put("packageName", resolveInfo.activityInfo.packageName);
                info.put("className", resolveInfo.activityInfo.name);
                info.put("simpleName", String.valueOf(resolveInfo.activityInfo.loadLabel(getPackageManager())));
                intentMetaInfo.add(info);
            }

            if (!intentMetaInfo.isEmpty()) {
                // sorting for nice readability
                Collections.sort(intentMetaInfo, new Comparator<HashMap<String, String>>() {
                    @Override
                    public int compare(HashMap<String, String> map, HashMap<String, String> map2) {
                        return map.get("simpleName").compareTo(map2.get("simpleName"));
                    }
                });

                // create the custom intent list
                for (HashMap<String, String> metaInfo : intentMetaInfo) {
                    Intent targetedShareIntent = (Intent) prototype.clone();
                    targetedShareIntent.setPackage(metaInfo.get("packageName"));
                    targetedShareIntent.setClassName(metaInfo.get("packageName"), metaInfo.get("className"));
                    targetedShareIntents.add(targetedShareIntent);
                }

                chooserIntent = Intent.createChooser(targetedShareIntents.remove(targetedShareIntents.size() - 1), getResources().getString(R.string.une_activity_partager_via));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
                return chooserIntent;
            }
        }

        return Intent.createChooser(prototype, getResources().getString(R.string.une_activity_partager_via));
    }
    @Override
    public void onClick(View v) {

    }



    class  MyAdapteur extends  RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable{
        public static final int VIEW_TYPE_UN = 0;
        public static final int VIEW_TYPE_TWO = 1;

        RecyclerView recyclerView;
        List<Produit> articlestList;
        AccueilInterface accueilInterface;
        Context c;

        AdapteurBuyListener listener;
        Produit bourse;
        RecyclerView.ViewHolder holder2;
        ListViewHolder holder1,holder;
        List<Produit> filterArticle;
        List<Produit> favoritesList=new ArrayList<>();
        List<Long> saveFavorites;
        Box<Favorite> favoriteBox;
        Box<SaveFavorite>saveFavoriteBox;
        Animation scaleAnimation;
        Box<Historique> historiqueBox;
        List<Produit>  historiqueList=new ArrayList<>();
        public static final int VITRINE_TYPE_SIMPLE=1;
        public static final int VITRINE_TYPE_GIFT=2;
        public static final int VITRINE_TYPE_VIDEO=3;
        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    String s=constraint.toString();
                    if(s.isEmpty()){
                        articlestList=filterArticle;
                    }
                    else{
                        List<Produit> articles=new ArrayList<>();
                        for (Produit row:filterArticle){
                            if(is_fr){
                                if(row.getTitle().toLowerCase().contains(s.toLowerCase())){
                                    articles.add(row);
                                }
                            }
                            else{
                                if(row.getTitle_en().toLowerCase().contains(s.toLowerCase())){
                                    articles.add(row);
                                }
                            }

                        }
                        articlestList=articles;

                    }
                    FilterResults results=new FilterResults();
                    results.values=articlestList;

                    return results;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    articlestList=(ArrayList<Produit>)results.values;
                    notifyDataSetChanged();
                    if(articlestList.isEmpty()){
                        liste.setVisibility(View.GONE);
                        linearvide.setVisibility(View.VISIBLE);
                        vide.setVisibility(View.VISIBLE);
                        vide.setText(getResources().getString(R.string.empty_liste));
                    }
                    else{
                        liste.setVisibility(View.VISIBLE);
                        linearvide.setVisibility(View.GONE);
                        vide.setVisibility(View.GONE);
                        vide.setText("");
                    }
                }
            };
        }
        public MyAdapteur(List<Produit> articlestList, AccueilInterface accueilInterface, Context c,AdapteurBuyListener listener,RecyclerView recyclerView) {

            this.articlestList = articlestList;
            this.accueilInterface = accueilInterface;
            this.c = c;
            this.filterArticle=articlestList;
            this.listener=listener;
            this.recyclerView=recyclerView;
            scaleAnimation= new ScaleAnimation(0.7f, 1.0f, 0.7f, 1.0f, Animation.RELATIVE_TO_SELF, 0.7f, Animation.RELATIVE_TO_SELF, 0.7f);
            scaleAnimation.setDuration(500);
            BounceInterpolator bounceInterpolator = new BounceInterpolator();
            scaleAnimation.setInterpolator(bounceInterpolator);
            saveFavoriteBox =((MyApplication)getApplicationContext()).getBoxStore().boxFor(SaveFavorite.class);
            favoriteBox =((MyApplication)getApplicationContext()).getBoxStore().boxFor(Favorite.class);
            historiqueBox=((MyApplication)getApplicationContext()).getBoxStore().boxFor(Historique.class);
            geALltFavorite(favoriteBox,saveFavoriteBox);
        }

        public  void addFavorite(Produit p){
            Gson gson=new Gson();
            favoritesList.add(p);
            saveFavorites.add(p.getId());
            String s="";
            //Favorite;
            QueryBuilder<Favorite> builder1=favoriteBox.query();
            Query<Favorite> query1=builder1.build();
            Favorite favorite=query1.findFirst();
            if(favorite!=null){
                Type type=new TypeToken<List<Produit>>(){}.getType();
                String favoriteSaved=gson.toJson(favoritesList,type);

                favorite.mfavorite=favoriteSaved;
                favoriteBox.put(favorite);


            }
            else{
                Favorite favorite1=new Favorite();
                Type type=new TypeToken<List<Produit>>(){}.getType();
                String favoriteSaved=gson.toJson(favoritesList,type);
                favorite1.mfavorite=favoriteSaved;
                favoriteBox.put(favorite1);

            }
            QueryBuilder<SaveFavorite> unSaveFavoriteQueryBuilder=saveFavoriteBox.query();
            Query<SaveFavorite> query2=unSaveFavoriteQueryBuilder.build();
            SaveFavorite unSaveF=query2.findFirst();
            if(unSaveF!=null){
                Type type=new TypeToken<List<Long>>(){}.getType();
                String UnfavoriteSaved=gson.toJson(saveFavorites,type);
                unSaveF.saveFavorite=UnfavoriteSaved;
                s=UnfavoriteSaved;
                saveFavoriteBox.put(unSaveF);

            }
            else {
                SaveFavorite unSaveFavorite=new SaveFavorite();
                Type type=new TypeToken<List<Long>>(){}.getType();
                String UnfavoriteSaved=gson.toJson(saveFavorites,type);
                unSaveFavorite.saveFavorite=UnfavoriteSaved;
                s=UnfavoriteSaved;
                saveFavoriteBox.put(unSaveFavorite);


            }

            SessionManager sessionManager=new SessionManager(getApplicationContext());
            User user=sessionManager.getUser(getApplicationContext());
            SaveFavori("action",s,user.getId());
            //J'upload la nouvelle  liste des favoris

        }
        public void deleteFavorite(final Produit p,RecyclerView.ViewHolder holder){
            Gson  gson=new Gson();

            String s="";
            Predicate<Produit> predicate2=new Predicate<Produit>() {

                public boolean apply(@javax.annotation.Nullable Produit input) {
                    return p.getId()==(input.getId());
                }
            };

            List<Produit> produits= Lists.newArrayList(Iterables.filter(favoritesList,predicate2));
            if(produits!=null && produits.size()>0){
                Produit f=produits.get(0);
                if(f!=null)
                {
                    favoritesList.remove(f);
                    if(holder instanceof ListViewHolder){
                        ListViewHolder viewHolder=(ListViewHolder)holder;
                        viewHolder.checkFavorite.setChecked(false);
                    }
                    if(holder instanceof ListViewHolderGift){
                        ListViewHolderGift viewHolder=(ListViewHolderGift)holder;
                        viewHolder.checkFavorite.setChecked(false);
                    }
                    if(holder instanceof ListViewHolderVideo){
                        ListViewHolderVideo viewHolder=(ListViewHolderVideo)holder;
                        viewHolder.checkFavorite.setChecked(false);
                    }
                }

            }
            int index=saveFavorites.indexOf(p.getId());
            if(index!=-1){
                saveFavorites.remove(index);
            }



            //Favorite;
            QueryBuilder<Favorite> builder1=favoriteBox.query();
            Query<Favorite> query1=builder1.build();
            Favorite favorite=query1.findFirst();
            if(favorite!=null){
                Type type=new TypeToken<List<Produit>>(){}.getType();
                String favoriteSaved=gson.toJson(favoritesList);
                favorite.mfavorite=favoriteSaved;
                favoriteBox.put(favorite);

            }
            QueryBuilder<SaveFavorite> unSaveFavoriteQueryBuilder=saveFavoriteBox.query();
            Query<SaveFavorite> query2=unSaveFavoriteQueryBuilder.build();
            SaveFavorite unSaveF=query2.findFirst();
            if(unSaveF!=null){
                Type type=new TypeToken<List<Long>>(){}.getType();
                String UnfavoriteSaved=gson.toJson(saveFavorites,type);
                unSaveF.saveFavorite=UnfavoriteSaved;
                s=UnfavoriteSaved;
                saveFavoriteBox.put(unSaveF);

            }

            SessionManager sessionManager=new SessionManager(getApplicationContext());
            User user=sessionManager.getUser(getApplicationContext());
            SaveFavori("action",s,user.getId());
            //J'upload la nouvelle  liste des favoris
        }
        public void SaveFavori(String action,String favoris,long id){
            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(Config.GET_BASE_URL_RETROFIT)
                    .addConverterFactory(ScalarsConverterFactory.create())

                    .build();

            MyInterface service=retrofit.create(MyInterface.class);

            Call<String> call=service.saveFavori(action,favoris,id);

            call.enqueue(new retrofit2.Callback<String>() {
                @Override
                public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                    String message = "";
                    if (response != null) {
                        if (response.isSuccessful()) {

                            try {
                                String rep = response.body();
                                JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                                boolean error = o.getBoolean("error");
                                if (error) {

                                } else {

                                    boolean isVide = o.getBoolean("vide");
                                    if (isVide) {

                                    } else {

                                    }

                                }

                            } catch (Exception e) {

                            }

                        }//fin is success,
                        else {

                        }//fin else is success

                    }


                }

                @Override
                public void onFailure(Call<String> call1,Throwable t) {

                }
            });
        }
        public void SaveSignalemnt(long produit_id, long id, String message, final android.app.AlertDialog  mAlertDialog){


            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(Config.GET_BASE_URL_RETROFIT)
                    .addConverterFactory(ScalarsConverterFactory.create())

                    .build();

            MyInterface service=retrofit.create(MyInterface.class);

            Call<String> call=service.createSignalement(id,produit_id,message);

            call.enqueue(new retrofit2.Callback<String>() {
                @Override
                public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                    String message = "";
                    if (response != null) {
                        if (response.isSuccessful()) {

                            try {
                                String rep = response.body();
                                JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                                boolean error = o.getBoolean("error");
                                if (error) {
                                    if(mAlertDialog!=null){
                                        mAlertDialog.dismiss();
                                    }
                                    AlertDialog.Builder builder=new AlertDialog.Builder(Une_bourse_activity.this);
                                    builder.setMessage(getResources().getString(R.string.error_frag_netw_parse));
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if(alertDialog!=null){
                                                alertDialog.dismiss();
                                            }
                                        }
                                    });
                                    dialog=builder.show();

                                } else {

                                    boolean isVide = o.getBoolean("vide");
                                    if (isVide) {

                                    } else {
                                        if(mAlertDialog!=null){
                                            mAlertDialog.dismiss();
                                        }
                                        AlertDialog.Builder builder=new AlertDialog.Builder(Une_bourse_activity.this);
                                        builder.setMessage(is_fr?"Votre signalement a bien été pris en compte":"Your report has been taken into account");
                                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if(alertDialog!=null){
                                                    alertDialog.dismiss();
                                                }
                                            }
                                        });
                                        alertDialog =builder.show();

                                    }

                                }

                            } catch (Exception e) {

                            }

                        }//fin is success,
                        else {
                            if(mAlertDialog!=null){
                                mAlertDialog.dismiss();
                            }
                            AlertDialog.Builder builder=new AlertDialog.Builder(Une_bourse_activity.this);
                            builder.setMessage(getResources().getString(R.string.error_frag_netw_parse));
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if(alertDialog!=null){
                                        alertDialog.dismiss();
                                    }
                                }
                            });
                            dialog=builder.show();

                        }//fin else is success

                    }


                }

                @Override
                public void onFailure(Call<String> call1,Throwable t) {
                    if(mAlertDialog!=null){
                        mAlertDialog.dismiss();
                    }
                    AlertDialog.Builder builder=new AlertDialog.Builder(Une_bourse_activity.this);
                    builder.setMessage(getResources().getString(R.string.error_frag_netw_parse));
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(alertDialog!=null){
                                alertDialog.dismiss();
                            }
                        }
                    });
                    dialog=builder.show();

                }

            });
        }

        private void addToHistorique(final Produit p){
            Gson  gson=new Gson();
            Predicate<Produit> predicate2=new Predicate<Produit>() {

                public boolean apply(@javax.annotation.Nullable Produit input) {
                    return p.getId()==(input.getId());
                }
            };

            List<Produit> produits= Lists.newArrayList(Iterables.filter(historiqueList,predicate2));
            if(produits!=null && produits.size()>0){
                Produit f=produits.get(0);
                if(f!=null)
                {

                }
                else {
                    historiqueList.add(p);
                    QueryBuilder<Historique> builder1=historiqueBox.query();
                    Query<Historique> query1=builder1.build();
                    Historique historique=query1.findFirst();
                    if(historique!=null){
                        Type type=new TypeToken<List<Produit>>(){}.getType();
                        String mhistorique=gson.toJson(historiqueList);

                        historique.mHistorique=mhistorique;
                        historiqueBox.put(historique);

                    }
                }

            }
            else{
                historiqueList.add(p);
                QueryBuilder<Historique> builder1=historiqueBox.query();
                Query<Historique> query1=builder1.build();
                Historique historique=query1.findFirst();
                if(historique!=null){
                    Type type=new TypeToken<List<Produit>>(){}.getType();
                    String mhistorique=gson.toJson(historiqueList);

                    historique.mHistorique=mhistorique;
                    historiqueBox.put(historique);

                }
            }

        }
        public void geALltFavorite(Box<Favorite> favoriteBox,Box<SaveFavorite> unSaveFavoriteBox){
            Gson  gson=new Gson();
            QueryBuilder<SaveFavorite> builder=unSaveFavoriteBox.query();
            Query<SaveFavorite> query=builder.build();
            SaveFavorite unSaveFavorite=query.findFirst();
            if(unSaveFavorite!=null){
                String val=unSaveFavorite.saveFavorite;
                if(!val.equalsIgnoreCase("") && val!=null){
                    Type type=new TypeToken<List<Long>>(){}.getType();
                    saveFavorites=gson.fromJson(val,type);

                }
                else{
                    saveFavorites=new ArrayList<>();
                }

            }
            else{
                saveFavorites=new ArrayList<>();
            }

            //Historique
            QueryBuilder<Historique> builder2=historiqueBox.query();
            Query<Historique> query2=builder2.build();
            Historique historique=query2.findFirst();
            if(historique!=null){
                String val=historique.mHistorique;
                if(!val.equalsIgnoreCase("") && val!=null){
                    Type type=new TypeToken<List<Produit>>(){}.getType();
                    historiqueList=gson.fromJson(val,type);

                }
                else{
                    historiqueList=new ArrayList<>();
                }

            }
            else{
                historiqueList=new ArrayList<>();
            }
            //Favorite;
            QueryBuilder<Favorite> builder1=favoriteBox.query();
            Query<Favorite> query1=builder1.build();
            Favorite favorite=query1.findFirst();
            if(favorite!=null){
                String val=favorite.mfavorite;
                if(!val.equalsIgnoreCase("") && val!=null){
                    Type type=new TypeToken<List<Produit>>(){}.getType();
                    favoritesList=gson.fromJson(val,type);

                }
                else{
                    favoritesList=new ArrayList<>();
                }

            }
            else{
                favoritesList=new ArrayList<>();
            }


        }
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            switch (viewType){
                case VITRINE_TYPE_SIMPLE:
                    View v1=LayoutInflater.from(parent.getContext()).inflate(R.layout.type_view_item_bourse,parent,false);
                    final RecyclerView.ViewHolder holderSimple=new ListViewHolder(v1);
                    v1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            accueilInterface.itemClicked(holderSimple.getAdapterPosition());
                        }
                    });
                    return holderSimple;
                case VITRINE_TYPE_GIFT:
                    View v2=LayoutInflater.from(parent.getContext()).inflate(R.layout.type_view_item_gift,parent,false);
                    final RecyclerView.ViewHolder holderGif=new ListViewHolderGift(v2);
                    v2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            accueilInterface.itemClicked(holderGif.getAdapterPosition());
                        }
                    });
                    return holderGif;
                case VITRINE_TYPE_VIDEO:
                    View v3=LayoutInflater.from(parent.getContext()).inflate(R.layout.type_view_item_video,parent,false);
                    final RecyclerView.ViewHolder holderVideo=new ListViewHolderVideo(v3);
                    v3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            accueilInterface.itemClicked(holderVideo.getAdapterPosition());
                        }
                    });
                    return holderVideo;
            }
            return null;
        }


        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder,final int position) {

            if(holder instanceof ListViewHolder){
                ListViewHolder  viewHolder=(ListViewHolder)holder;
                viewHolder.bin(position);
                setUpListViewHolder(viewHolder,articlestList.get(position),position);
                bourse=articlestList.get(position);
                viewHolder.flipper.stopFlipping();
                viewHolder.flipper.setAutoStart(false);
                viewHolder.flipper.removeAllViews();
                setSlide1(viewHolder.flipper,bourse,viewHolder);
            }
            else if(holder instanceof ListViewHolderGift){
                ListViewHolderGift    holderGift=(ListViewHolderGift)holder;
                holderGift.bin(position);
                setUpListViewHolderGift(holderGift,articlestList.get(position),position);
            }
            else if(holder instanceof ListViewHolderVideo){
                ListViewHolderVideo   holderVideo=(ListViewHolderVideo)holder;
                holderVideo.bin(position);
                setUpListViewHolderVideo(holderVideo,articlestList.get(position),position);

            }

        }
        private void setSlide1(final ViewFlipper fliper, final Produit article, ListViewHolder holder1)
        {
            if(article.getUrl().equals(bad_url)){

            }
            else{
                LayoutInflater inflater=LayoutInflater.from(c);
                View view=inflater.inflate(R.layout.aa_pager_item2,null);
                final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                if (sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).into(imageView);
                }else if(sessionSaverData.getKeySaverDataBourse().equals("Non")){
                    Picasso.with(c).load(article.getUrl()).into(imageView);
                }

                holder1.setItemCount(1);
                fliper.addView(view);
                holder1.switchCompatVitrineBourse.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            if(sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                                Picasso.with(c).load(article.getUrl()).into(imageView);
                            }else{
                                Picasso.with(c).load(R.drawable.saverdatacoop).into(imageView);
                            }

                        }else{
                            if(sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                                Picasso.with(c).load(R.drawable.saverdatacoop).into(imageView);
                            }else{
                                Picasso.with(c).load(article.getUrl()).into(imageView);
                            }


                        }
                    }
                });
            }
            if(!article.getUrl2().equals(bad_url)){
                LayoutInflater inflater=LayoutInflater.from(c);
                View view=inflater.inflate(R.layout.aa_pager_item2,null);
                final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                if (sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).into(imageView);
                }else if(sessionSaverData.getKeySaverDataBourse().equals("Non")){
                    Picasso.with(c).load(article.getUrl2()).into(imageView);
                }

                holder1.setItemCount(2);
                fliper.addView(view);
                holder1.switchCompatVitrineBourse.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            if(sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                                fliper.removeAllViews();
                                if(!article.getUrl().equals(bad_url)){
                                    LayoutInflater inflater=LayoutInflater.from(c);
                                    View view=inflater.inflate(R.layout.aa_pager_item2,null);
                                    final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                                    fliper.addView(view, 0);
                                    Picasso.with(c).load(article.getUrl()).into(imageView);
                                }
                                if(!article.getUrl2().equals(bad_url)){
                                    LayoutInflater inflater=LayoutInflater.from(c);
                                    View view=inflater.inflate(R.layout.aa_pager_item2,null);
                                    final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                                    fliper.addView(view, 0);
                                    Picasso.with(c).load(article.getUrl2()).into(imageView);
                                }
                            }else{
                                fliper.removeAllViews();
                                LayoutInflater inflater=LayoutInflater.from(c);
                                View view=inflater.inflate(R.layout.aa_pager_item2,null);
                                final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                                fliper.addView(view);
                                Picasso.with(c).load(R.drawable.saverdatacoop).into(imageView);
                            }


                        }else {
                            if(sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                                fliper.removeAllViews();
                                LayoutInflater inflater=LayoutInflater.from(c);
                                View view=inflater.inflate(R.layout.aa_pager_item2,null);
                                final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                                fliper.addView(view);
                                Picasso.with(c).load(R.drawable.saverdatacoop).into(imageView);
                            }else{
                                fliper.removeAllViews();
                                if(!article.getUrl().equals(bad_url)){
                                    LayoutInflater inflater=LayoutInflater.from(c);
                                    View view=inflater.inflate(R.layout.aa_pager_item2,null);
                                    final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                                    fliper.addView(view, 0);
                                    Picasso.with(c).load(article.getUrl()).into(imageView);
                                }
                                if(!article.getUrl2().equals(bad_url)){
                                    LayoutInflater inflater=LayoutInflater.from(c);
                                    View view=inflater.inflate(R.layout.aa_pager_item2,null);
                                    final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                                    fliper.addView(view, 0);
                                    Picasso.with(c).load(article.getUrl2()).into(imageView);
                                }
                            }


                        }
                    }
                });
            }

            if(!article.getUrl3().equals(bad_url)){
                LayoutInflater inflater=LayoutInflater.from(c);
                View view=inflater.inflate(R.layout.aa_pager_item2,null);
                final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                if (sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).into(imageView);
                }else if(sessionSaverData.getKeySaverDataBourse().equals("Non")){
                    Picasso.with(c).load(article.getUrl3()).into(imageView);
                }

                holder1.setItemCount(3);
                fliper.addView(view);

                holder1.switchCompatVitrineBourse.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            if(sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                                fliper.removeAllViews();
                                if(!article.getUrl().equals(bad_url)){
                                    LayoutInflater inflater=LayoutInflater.from(c);
                                    View view=inflater.inflate(R.layout.aa_pager_item2,null);
                                    final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                                    fliper.addView(view);
                                    Picasso.with(c).load(article.getUrl()).into(imageView);
                                }
                                if(!article.getUrl2().equals(bad_url)){
                                    LayoutInflater inflater=LayoutInflater.from(c);
                                    View view=inflater.inflate(R.layout.aa_pager_item2,null);
                                    final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                                    fliper.addView(view);
                                    Picasso.with(c).load(article.getUrl2()).into(imageView);
                                }
                                if(!article.getUrl3().equals(bad_url)){
                                    LayoutInflater inflater=LayoutInflater.from(c);
                                    View view=inflater.inflate(R.layout.aa_pager_item2,null);
                                    final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                                    fliper.addView(view);
                                    Picasso.with(c).load(article.getUrl3()).into(imageView);
                                }
                            }else{
                                fliper.removeAllViews();
                                LayoutInflater inflater=LayoutInflater.from(c);
                                View view=inflater.inflate(R.layout.aa_pager_item2,null);
                                final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                                fliper.addView(view);
                                Picasso.with(c).load(R.drawable.saverdatacoop).into(imageView);
                            }


                        }else{
                            if(sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                                fliper.removeAllViews();
                                LayoutInflater inflater=LayoutInflater.from(c);
                                View view=inflater.inflate(R.layout.aa_pager_item2,null);
                                final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                                fliper.addView(view);
                                Picasso.with(c).load(R.drawable.saverdatacoop).into(imageView);
                            }else{
                                fliper.removeAllViews();
                                if(!article.getUrl().equals(bad_url)){
                                    LayoutInflater inflater=LayoutInflater.from(c);
                                    View view=inflater.inflate(R.layout.aa_pager_item2,null);
                                    final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                                    fliper.addView(view);
                                    Picasso.with(c).load(article.getUrl()).into(imageView);
                                }
                                if(!article.getUrl2().equals(bad_url)){
                                    LayoutInflater inflater=LayoutInflater.from(c);
                                    View view=inflater.inflate(R.layout.aa_pager_item2,null);
                                    final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                                    fliper.addView(view);
                                    Picasso.with(c).load(article.getUrl2()).into(imageView);
                                }
                                if(!article.getUrl3().equals(bad_url)){
                                    LayoutInflater inflater=LayoutInflater.from(c);
                                    View view=inflater.inflate(R.layout.aa_pager_item2,null);
                                    final DinamicImageView imageView=view.findViewById(R.id.un_modele_view_image_slide);
                                    fliper.addView(view);
                                    Picasso.with(c).load(article.getUrl3()).into(imageView);
                                }
                            }

                        }
                    }
                });

            }
            if(holder1.itemCount>1){
                holder1.left.setVisibility(View.VISIBLE);
                holder1.right.setVisibility(View.VISIBLE);
                fliper.setAutoStart(true);
                fliper.startFlipping();
                fliper.setFlipInterval(4000);
                fliper.setInAnimation(AnimationUtils.loadAnimation(Une_bourse_activity.this, R.anim.main_slide_in_left));
                fliper.setOutAnimation(AnimationUtils.loadAnimation(Une_bourse_activity.this,R.anim.main_slide_out_left));
            }
            else {
                holder1.left.setVisibility(View.GONE);
                holder1.right.setVisibility(View.GONE);
            }
        }

        public void setUpListViewHolder( final ListViewHolder  holder, final Produit article,final int position)
        {
            String des="";
            if(articlestList.get(position).getNb_vue()!=null && !articlestList.get(position).getNb_vue().trim().equalsIgnoreCase("")){
                holder.tv_vitrine_nb_vue.setText(""+articlestList.get(position).getNb_vue());
            }
            else{
                holder.tv_vitrine_nb_vue.setText("0");
            }
            holder.image_action_get_profile.setTag(position);
            holder.image_action_get_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    if(""+p.getUser_id()!=null && !(""+p.getUser_id()).equalsIgnoreCase("")){
                        Intent intent=new Intent(Une_bourse_activity.this,PublicProfileActivity.class);
                        intent.putExtra("id",p.getUser_id());
                        startActivity(intent);
                    }
                    else{
                        MyToast.show(Une_bourse_activity.this,is_fr?"Impossible de consulter le profil du vendeur":"Could not view seller's profile",true);
                    }
                }
            });

            holder.image_action_get_location.setTag(position);
            holder.image_action_get_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    if((p.getLatitude()!=null && p.getLongitude()!=null) && (!p.getLatitude().equalsIgnoreCase("") && !p.getLongitude().equalsIgnoreCase(""))){
                        Uri gmmIntentUri = Uri.parse("geo:"+p.getLatitude().trim()+","+p.getLongitude().trim());
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        if (mapIntent.resolveActivity(getPackageManager()) != null) {
                            startActivity(mapIntent);
                        }
                        else{
                            MyToast.show(Une_bourse_activity.this,getResources().getString(R.string.install_application),false);
                        }
                    }
                }
            });
            holder.more.setTag(position);
            holder.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    listener.onMore(p);
                    addToHistorique(p);
                }
            });
            holder.share.setTag(position);
            holder.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    listener.onShare(p);
                    addToHistorique(p);
                }
            });

            holder.buy.setTag(position);
            holder.buy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    listener.onBuy(p);
                    addToHistorique(p);
                }
            });
            holder.imageViewActionShare.setTag(position);
            holder.imageViewActionShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    startbookShare(p);
                }
            });
            holder.imageViewActionRepport.setTag(position);
            holder.imageViewActionRepport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    final   Produit p=articlestList.get(position);
                    SessionManager sessionManager = new SessionManager(getApplicationContext());
                    final User user=sessionManager.getUser(getApplicationContext());

                    AlertDialog.Builder builder=new AlertDialog.Builder(Une_bourse_activity.this);
                    LayoutInflater inflater=getLayoutInflater();
                    View view=inflater.inflate(R.layout.repport_dialog_layout,null);
                    final EditText editText=(EditText)view.findViewById(R.id.edit_repportd_dialog);
                    builder.setView(view);

                    builder.setPositiveButton(is_fr?"ENVOYER":"SEND", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(editText.getText().toString().equalsIgnoreCase("")){
                                Toast.makeText(Une_bourse_activity.this,is_fr?"Entrez un text":"Enter a text",Toast.LENGTH_SHORT).show();
                            }
                            else{
                                final android.app.AlertDialog dialog1=new SpotsDialog(Une_bourse_activity.this,R.style.style_spot_location);
                                dialog1.show();
                                SaveSignalemnt(p.getId(),user.getId(),editText.getText().toString(),dialog1);
                            }
                        }
                    });
                    builder.setNegativeButton(is_fr?"Annuler":"Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog=builder.show();
                }
            });
            ((ListViewHolder) holder).checkFavorite.setOnCheckedChangeListener(null);

            ((ListViewHolder) holder).checkFavorite.setChecked(articlestList.get(position).isFavorite);

            Predicate<Produit> predicate2=new Predicate<Produit>() {

                public boolean apply(@javax.annotation.Nullable Produit input) {
                    return articlestList.get(position).getId()==(input.getId());
                }
            };

            List<Produit> produits= Lists.newArrayList(Iterables.filter(favoritesList,predicate2));
            if(produits!=null && produits.size()>0){
                Produit f=produits.get(0);
                if(f!=null)
                {
                    Produit mproduit=articlestList.get(position);
                    mproduit.isFavorite=true;
                    ((ListViewHolder) holder).checkFavorite.setChecked(mproduit.isFavorite);

                }
                else{
                    ((ListViewHolder) holder).checkFavorite.setChecked(false);
                }

            }
            ((ListViewHolder) holder).checkFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                    buttonView.startAnimation(scaleAnimation);
                    articlestList.get(position).setFavorite(isChecked);
                    if(isChecked){
                        addFavorite(articlestList.get(position));

                    }
                    else {
                        deleteFavorite(articlestList.get(position),holder);
                    }


                }
            });

            holder.tvDate.setText(MydateUtil.getDate(article.getDate(),is_fr)+""+(article.getAuteur().trim().equals("")?"":" / "+article.getAuteur()));
            if(is_fr)
            {
                if(article.getExtend_proprety1().equals(""))
                {
                    holder.extend.setVisibility(View.GONE);
                }
                else
                {
                    holder.extend.setText(article.getExtend_proprety1());
                }
                if( article.getExtend_proprety2().equals("") )
                {
                    holder.extend2.setVisibility(View.GONE);

                }
                else
                {

                    holder.extend2.setText(article.getExtend_proprety2());
                }

                holder.title.setText(article.getTitle().toUpperCase());
                des=article.getDescription();

                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des.length()>50)
                    {
                        des=des.substring(0,50)+"...";

                    }

                }
                else
                {
                    if(des.length()>110)
                    {
                        des=des.substring(0,110)+"...";
                    }
                }
            }
            else
            {
                if(article.getExtend_proprety1_en().equals(""))
                {
                    holder.extend.setVisibility(View.GONE);
                }
                else
                {
                    holder.extend.setText(article.getExtend_proprety1_en());
                }
                if( article.getExtend_proprety2_en().equals("") )
                {
                    holder.extend2.setVisibility(View.GONE);

                }
                else
                {

                    holder.extend2.setText(article.getExtend_proprety2_en());
                }
                holder.title.setText(article.getTitle_en().toUpperCase());
                des=article.getDescription_en();

                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des.length()>50)
                    {
                        des=des.substring(0,50)+"...";

                    }

                }
                else
                {
                    if(des.length()>110)
                    {
                        des=des.substring(0,110)+"...";
                    }
                }

            }

            final String lien_des=article.getLien_description();
            holder.description.setText(des+" ");
            holder.description.setLinkTextColor(Color.BLUE);
            if(!lien_des.equals(""))
            {

                SimpleText simpleText=SimpleText.from(des+" "+(is_fr?"Cliquez ici.".toUpperCase():"CLICK HERE.".toUpperCase()))
                        .last((is_fr?"Cliquez ici.".toUpperCase():"CLICK HERE.".toUpperCase()))
                        .bold()
                        .textColor(R.color.linkColor)
                        .onClick(holder.description, new OnTextClickListener() {
                            @Override
                            public void onClicked(CharSequence text, Range range, Object tag) {
                                if(canparseLink(lien_des).equalsIgnoreCase("")){
                                    Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(lien_des));
                                    if(intent.resolveActivity(getPackageManager())!=null){
                                        startActivity(intent);
                                    }
                                    else{
                                        MyToast.show(Une_bourse_activity.this, getResources().getString(R.string.une_activity_installer_app_partage), false);
                                    }
                                }
                                else{
                                    Intent intent=new Intent(Une_bourse_activity.this,SpotActivity.class);
                                    intent.putExtra("id",canparseLink(lien_des));
                                    intent.putExtra("type","vitrine");
                                    if(article!=null){

                                        intent.putExtra("image",""+article.getUrl());
                                        intent.putExtra("title",is_fr?article.getTitle():article.getTitle_en());
                                        intent.putExtra("invitaion",is_fr?article.getExtend_proprety1():article.getExtend_proprety1_en());
                                    }
                                    startActivity(intent);
                                }
                            }
                        });
                holder.description.setText(simpleText);

            }

        }

        public void setUpListViewHolderGift( final ListViewHolderGift  holder, final Produit article,final int position)
        {
            String des="";
            if(articlestList.get(position).getNb_vue()!=null && !articlestList.get(position).getNb_vue().trim().equalsIgnoreCase("")){
                holder.tv_vitrine_nb_vue.setText(""+articlestList.get(position).getNb_vue());
            }
            else{
                holder.tv_vitrine_nb_vue.setText("0");
            }
            holder.image_action_get_profile.setTag(position);
            holder.image_action_get_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    if(""+p.getUser_id()!=null && !(""+p.getUser_id()).equalsIgnoreCase("")){
                        Intent intent=new Intent(Une_bourse_activity.this,PublicProfileActivity.class);
                        intent.putExtra("id",p.getUser_id());
                        startActivity(intent);
                    }
                    else{
                        MyToast.show(Une_bourse_activity.this,is_fr?"Impossible de consulter le profil du vendeur":"Could not view seller's profile",true);
                    }
                }
            });

            holder.image_action_get_location.setTag(position);
            holder.image_action_get_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    if((p.getLatitude()!=null && p.getLongitude()!=null) && (!p.getLatitude().equalsIgnoreCase("") && !p.getLongitude().equalsIgnoreCase(""))){
                        Uri gmmIntentUri = Uri.parse("geo:"+p.getLatitude().trim()+","+p.getLongitude().trim());
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        if (mapIntent.resolveActivity(getPackageManager()) != null) {
                            startActivity(mapIntent);
                        }
                        else{
                            MyToast.show(Une_bourse_activity.this,getResources().getString(R.string.install_application),false);
                        }
                    }
                }
            });
            holder.more.setTag(position);
            holder.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    listener.onMore(p);
                    addToHistorique(p);
                }
            });
            holder.share.setTag(position);
            holder.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    listener.onShare(p);
                    addToHistorique(p);
                }
            });

            holder.buy.setTag(position);
            holder.buy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    listener.onBuy(p);
                    addToHistorique(p);
                }
            });
            holder.imageViewActionShare.setTag(position);
            holder.imageViewActionShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    startbookShare(p);
                }
            });
            holder.imageViewActionRepport.setTag(position);
            holder.imageViewActionRepport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    final   Produit p=articlestList.get(position);
                    SessionManager sessionManager = new SessionManager(getApplicationContext());
                    final User user=sessionManager.getUser(getApplicationContext());

                    AlertDialog.Builder builder=new AlertDialog.Builder(Une_bourse_activity.this);
                    LayoutInflater inflater=getLayoutInflater();
                    View view=inflater.inflate(R.layout.repport_dialog_layout,null);
                    final EditText editText=(EditText)view.findViewById(R.id.edit_repportd_dialog);
                    builder.setView(view);

                    builder.setPositiveButton(is_fr?"ENVOYER":"SEND", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(editText.getText().toString().equalsIgnoreCase("")){
                                Toast.makeText(Une_bourse_activity.this,is_fr?"Entrez un text":"Enter a text",Toast.LENGTH_SHORT).show();
                            }
                            else{
                                final android.app.AlertDialog dialog1=new SpotsDialog(Une_bourse_activity.this,R.style.style_spot_location);
                                dialog1.show();
                                SaveSignalemnt(p.getId(),user.getId(),editText.getText().toString(),dialog1);
                            }
                        }
                    });
                    builder.setNegativeButton(is_fr?"Annuler":"Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog=builder.show();
                }
            });
            ((ListViewHolderGift) holder).checkFavorite.setOnCheckedChangeListener(null);

            ((ListViewHolderGift) holder).checkFavorite.setChecked(articlestList.get(position).isFavorite);

            Predicate<Produit> predicate2=new Predicate<Produit>() {

                public boolean apply(@javax.annotation.Nullable Produit input) {
                    return articlestList.get(position).getId()==(input.getId());
                }
            };

            List<Produit> produits= Lists.newArrayList(Iterables.filter(favoritesList,predicate2));
            if(produits!=null && produits.size()>0){
                Produit f=produits.get(0);
                if(f!=null)
                {
                    Produit mproduit=articlestList.get(position);
                    mproduit.isFavorite=true;
                    ((ListViewHolderGift) holder).checkFavorite.setChecked(mproduit.isFavorite);

                }
                else{
                    ((ListViewHolderGift) holder).checkFavorite.setChecked(false);
                }

            }
            ((ListViewHolderGift) holder).checkFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                    buttonView.startAnimation(scaleAnimation);
                    articlestList.get(position).setFavorite(isChecked);
                    if(isChecked){
                        addFavorite(articlestList.get(position));

                    }
                    else {
                        deleteFavorite(articlestList.get(position),holder);
                    }


                }
            });

            holder.tvDate.setText(MydateUtil.getDate(article.getDate(),is_fr)+""+(article.getAuteur().trim().equals("")?"":" / "+article.getAuteur()));
            if(is_fr)
            {
                if(article.getExtend_proprety1().equals(""))
                {
                    holder.extend.setVisibility(View.GONE);
                }
                else
                {
                    holder.extend.setText(article.getExtend_proprety1());
                }
                if( article.getExtend_proprety2().equals("") )
                {
                    holder.extend2.setVisibility(View.GONE);

                }
                else
                {

                    holder.extend2.setText(article.getExtend_proprety2());
                }

                holder.title.setText(article.getTitle().toUpperCase());
                des=article.getDescription();

                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des.length()>50)
                    {
                        des=des.substring(0,50)+"...";

                    }

                }
                else
                {
                    if(des.length()>110)
                    {
                        des=des.substring(0,110)+"...";
                    }
                }
            }
            else
            {
                if(article.getExtend_proprety1_en().equals(""))
                {
                    holder.extend.setVisibility(View.GONE);
                }
                else
                {
                    holder.extend.setText(article.getExtend_proprety1_en());
                }
                if( article.getExtend_proprety2_en().equals("") )
                {
                    holder.extend2.setVisibility(View.GONE);

                }
                else
                {

                    holder.extend2.setText(article.getExtend_proprety2_en());
                }
                holder.title.setText(article.getTitle_en().toUpperCase());
                des=article.getDescription_en();

                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des.length()>50)
                    {
                        des=des.substring(0,50)+"...";

                    }

                }
                else
                {
                    if(des.length()>110)
                    {
                        des=des.substring(0,110)+"...";
                    }
                }

            }

            final String lien_des=article.getLien_description();
            holder.description.setText(des+" ");
            holder.description.setLinkTextColor(Color.BLUE);
            if(!lien_des.equals(""))
            {

                SimpleText simpleText=SimpleText.from(des+" "+(is_fr?"Cliquez ici.".toUpperCase():"CLICK HERE.".toUpperCase()))
                        .last((is_fr?"Cliquez ici.".toUpperCase():"CLICK HERE.".toUpperCase()))
                        .bold()
                        .textColor(R.color.linkColor)
                        .onClick(holder.description, new OnTextClickListener() {
                            @Override
                            public void onClicked(CharSequence text, Range range, Object tag) {
                                if(canparseLink(lien_des).equalsIgnoreCase("")){
                                    Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(lien_des));
                                    if(intent.resolveActivity(getPackageManager())!=null){
                                        startActivity(intent);
                                    }
                                    else{
                                        MyToast.show(Une_bourse_activity.this, getResources().getString(R.string.une_activity_installer_app_partage), false);
                                    }
                                }
                                else{
                                    Intent intent=new Intent(Une_bourse_activity.this,SpotActivity.class);
                                    intent.putExtra("id",canparseLink(lien_des));
                                    intent.putExtra("type","vitrine");
                                    if(article!=null){

                                        intent.putExtra("image",""+article.getUrl());
                                        intent.putExtra("title",is_fr?article.getTitle():article.getTitle_en());
                                        intent.putExtra("invitaion",is_fr?article.getExtend_proprety1():article.getExtend_proprety1_en());
                                    }
                                    startActivity(intent);
                                }
                            }
                        });
                holder.description.setText(simpleText);

            }
            if(sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                Glide.with(getApplicationContext()).load(R.drawable.saverdatacoop).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.giftImage);

            }else{
                Glide.with(getApplicationContext()).load(article.getLien_gift()).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.giftImage);
            }

            holder.switchCompatVitrineActuGift.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        if(sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                            Glide.with(getApplicationContext()).load(article.getLien_gift()).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.giftImage);

                        }else{
                            Glide.with(getApplicationContext()).load(R.drawable.saverdatacoop).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.giftImage);

                        }
                    }else{
                        if(sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                            Glide.with(getApplicationContext()).load(R.drawable.saverdatacoop).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.giftImage);

                        }else{
                            Glide.with(getApplicationContext()).load(article.getLien_gift()).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.giftImage);

                        }
                    }
                }
            });        }
        public void setUpListViewHolderVideo( final ListViewHolderVideo  holder, final Produit article,final int position)
        {
            String des="";
            if(articlestList.get(position).getNb_vue()!=null && !articlestList.get(position).getNb_vue().trim().equalsIgnoreCase("")){
                holder.tv_vitrine_nb_vue.setText(""+articlestList.get(position).getNb_vue());
            }
            else{
                holder.tv_vitrine_nb_vue.setText("0");
            }
            holder.image_action_get_profile.setTag(position);
            holder.image_action_get_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    if(""+p.getUser_id()!=null && !(""+p.getUser_id()).equalsIgnoreCase("")){
                        Intent intent=new Intent(Une_bourse_activity.this,PublicProfileActivity.class);
                        intent.putExtra("id",p.getUser_id());
                        startActivity(intent);
                    }
                    else{
                        MyToast.show(Une_bourse_activity.this,is_fr?"Impossible de consulter le profil du vendeur":"Could not view seller's profile",true);
                    }
                }
            });

            holder.image_action_get_location.setTag(position);
            holder.image_action_get_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    if((p.getLatitude()!=null && p.getLongitude()!=null) && (!p.getLatitude().equalsIgnoreCase("") && !p.getLongitude().equalsIgnoreCase(""))){
                        Uri gmmIntentUri = Uri.parse("geo:"+p.getLatitude().trim()+","+p.getLongitude().trim());
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        if (mapIntent.resolveActivity(getPackageManager()) != null) {
                            startActivity(mapIntent);
                        }
                        else{
                            MyToast.show(Une_bourse_activity.this,getResources().getString(R.string.install_application),false);
                        }
                    }
                }
            });
            holder.more.setTag(position);
            holder.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    listener.onMore(p);
                    addToHistorique(p);
                }
            });
            holder.share.setTag(position);
            holder.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    listener.onShare(p);
                    addToHistorique(p);
                }
            });

            holder.buy.setTag(position);
            holder.buy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    listener.onBuy(p);
                    addToHistorique(p);
                }
            });
            holder.imageViewActionShare.setTag(position);
            holder.imageViewActionShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    Produit p=articlestList.get(position);
                    startbookShare(p);
                }
            });
            holder.imageViewActionRepport.setTag(position);
            holder.imageViewActionRepport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                    final   Produit p=articlestList.get(position);
                    SessionManager sessionManager = new SessionManager(getApplicationContext());
                    final User user=sessionManager.getUser(getApplicationContext());

                    AlertDialog.Builder builder=new AlertDialog.Builder(Une_bourse_activity.this);
                    LayoutInflater inflater=getLayoutInflater();
                    View view=inflater.inflate(R.layout.repport_dialog_layout,null);
                    final EditText editText=(EditText)view.findViewById(R.id.edit_repportd_dialog);
                    builder.setView(view);

                    builder.setPositiveButton(is_fr?"ENVOYER":"SEND", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(editText.getText().toString().equalsIgnoreCase("")){
                                Toast.makeText(Une_bourse_activity.this,is_fr?"Entrez un text":"Enter a text",Toast.LENGTH_SHORT).show();
                            }
                            else{
                                final android.app.AlertDialog dialog1=new SpotsDialog(Une_bourse_activity.this,R.style.style_spot_location);
                                dialog1.show();
                                SaveSignalemnt(p.getId(),user.getId(),editText.getText().toString(),dialog1);
                            }
                        }
                    });
                    builder.setNegativeButton(is_fr?"Annuler":"Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog=builder.show();
                }
            });
            ((ListViewHolderVideo) holder).checkFavorite.setOnCheckedChangeListener(null);

            ((ListViewHolderVideo) holder).checkFavorite.setChecked(articlestList.get(position).isFavorite);

            Predicate<Produit> predicate2=new Predicate<Produit>() {

                public boolean apply(@javax.annotation.Nullable Produit input) {
                    return articlestList.get(position).getId()==(input.getId());
                }
            };

            List<Produit> produits= Lists.newArrayList(Iterables.filter(favoritesList,predicate2));
            if(produits!=null && produits.size()>0){
                Produit f=produits.get(0);
                if(f!=null)
                {
                    Produit mproduit=articlestList.get(position);
                    mproduit.isFavorite=true;
                    ((ListViewHolderVideo) holder).checkFavorite.setChecked(mproduit.isFavorite);

                }
                else{
                    ((ListViewHolderVideo) holder).checkFavorite.setChecked(false);
                }

            }
            ((ListViewHolderVideo) holder).checkFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                    buttonView.startAnimation(scaleAnimation);
                    articlestList.get(position).setFavorite(isChecked);
                    if(isChecked){
                        addFavorite(articlestList.get(position));

                    }
                    else {
                        deleteFavorite(articlestList.get(position),holder);
                    }


                }
            });

            holder.tvDate.setText(MydateUtil.getDate(article.getDate(),is_fr)+""+(article.getAuteur().trim().equals("")?"":" / "+article.getAuteur()));
            if(is_fr)
            {
                if(article.getExtend_proprety1().equals(""))
                {
                    holder.extend.setVisibility(View.GONE);
                }
                else
                {
                    holder.extend.setText(article.getExtend_proprety1());
                }
                if( article.getExtend_proprety2().equals("") )
                {
                    holder.extend2.setVisibility(View.GONE);

                }
                else
                {

                    holder.extend2.setText(article.getExtend_proprety2());
                }

                holder.title.setText(article.getTitle().toUpperCase());
                des=article.getDescription();

                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des.length()>50)
                    {
                        des=des.substring(0,50)+"...";

                    }

                }
                else
                {
                    if(des.length()>110)
                    {
                        des=des.substring(0,110)+"...";
                    }
                }
            }
            else
            {
                if(article.getExtend_proprety1_en().equals(""))
                {
                    holder.extend.setVisibility(View.GONE);
                }
                else
                {
                    holder.extend.setText(article.getExtend_proprety1_en());
                }
                if( article.getExtend_proprety2_en().equals("") )
                {
                    holder.extend2.setVisibility(View.GONE);

                }
                else
                {

                    holder.extend2.setText(article.getExtend_proprety2_en());
                }
                holder.title.setText(article.getTitle_en().toUpperCase());
                des=article.getDescription_en();

                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des.length()>50)
                    {
                        des=des.substring(0,50)+"...";

                    }

                }
                else
                {
                    if(des.length()>110)
                    {
                        des=des.substring(0,110)+"...";
                    }
                }

            }

            final String lien_des=article.getLien_description();
            holder.description.setText(des+" ");
            holder.description.setLinkTextColor(Color.BLUE);
            if(!lien_des.equals(""))
            {

                SimpleText simpleText=SimpleText.from(des+" "+(is_fr?"Cliquez ici.".toUpperCase():"CLICK HERE.".toUpperCase()))
                        .last((is_fr?"Cliquez ici.".toUpperCase():"CLICK HERE.".toUpperCase()))
                        .bold()
                        .textColor(R.color.linkColor)
                        .onClick(holder.description, new OnTextClickListener() {
                            @Override
                            public void onClicked(CharSequence text, Range range, Object tag) {
                                if(canparseLink(lien_des).equalsIgnoreCase("")){
                                    Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(lien_des));
                                    if(intent.resolveActivity(getPackageManager())!=null){
                                        startActivity(intent);
                                    }
                                    else{
                                        MyToast.show(Une_bourse_activity.this, getResources().getString(R.string.une_activity_installer_app_partage), false);
                                    }
                                }
                                else{
                                    Intent intent=new Intent(Une_bourse_activity.this,SpotActivity.class);
                                    intent.putExtra("id",canparseLink(lien_des));
                                    intent.putExtra("type","vitrine");
                                    if(article!=null){

                                        intent.putExtra("image",""+article.getUrl());
                                        intent.putExtra("title",is_fr?article.getTitle():article.getTitle_en());
                                        intent.putExtra("invitaion",is_fr?article.getExtend_proprety1():article.getExtend_proprety1_en());
                                    }
                                    startActivity(intent);
                                }
                            }
                        });
                holder.description.setText(simpleText);

            }
            if(sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                Glide.with(getApplicationContext()).load(R.drawable.saverdatacoop).into(holder.previewVideo);
            }else{
                Glide.with(getApplicationContext()).load(article.getUrl()).into(holder.previewVideo);
            }



            holder.switchCompatVitrineActuVideo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        if(sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                            Glide.with(getApplicationContext()).load(article.getUrl()).into(holder.previewVideo);
                        }else{
                            Glide.with(getApplicationContext()).load(R.drawable.saverdatacoop).into(holder.previewVideo);
                        }
                    }else{
                        if(sessionSaverData.getKeySaverDataBourse().equals("Oui")){
                            Glide.with(getApplicationContext()).load(R.drawable.saverdatacoop).into(holder.previewVideo);
                        }else{
                            Glide.with(getApplicationContext()).load(article.getUrl()).into(holder.previewVideo);
                        }
                    }
                }
            });
        }


        @Override
        public int getItemViewType(int position) {

            Produit produit=articlestList.get(position);
            switch (produit.getContent_type()){
                case "vitrine_simple":
                    return VITRINE_TYPE_SIMPLE;
                case "vitrine_gift":
                    return VITRINE_TYPE_GIFT;
                case "vitrine_video":
                    return VITRINE_TYPE_VIDEO;
            }
            return position;

        }

        @Override
        public int getItemCount() {
            return articlestList.size();
        }



        public  class ListViewHolder extends  RecyclerView.ViewHolder
        {
            TextView  title;
            TextView tvDate;
            TextView description;
            TextView extend,extend2;
            ImageView left;
            ImageView right;
            AppCompatImageView imageViewToggleDate,imageViewActionShare,imageViewActionRepport;
            AppCompatCheckBox checkFavorite;
            ExpandableLayout expandableLinearLayoutdate,expandableLinearLayoutreadmore;
            LinearLayout read_more_linearlayout;
            ViewFlipper flipper;
            Button more,share,buy;
            int itemCount;
            Handler handler;
            Runnable runnable;
            TextView tv_vitrine_nb_vue;
            View view_vitrine_nb_vue_parent;
            AppCompatImageView image_action_get_profile;
            AppCompatImageView image_action_get_location;
            public  void stopRunnable(){
                handler.removeCallbacks(runnable);
            }
            public  void runHandler(){
                handler.postDelayed(runnable,4000);
            }
            public void setItemCount(int itemCount){
                this.itemCount=itemCount;
            }
            private int position;
            public void bin(int position){
                this.position=position;
            }
            SwitchCompat switchCompatVitrineBourse;
            public  ListViewHolder(View v)
            {
                super(v);
                tv_vitrine_nb_vue=(TextView)v.findViewById(R.id.vitrine_nb_vue);
                view_vitrine_nb_vue_parent=v.findViewById(R.id.vitrine_nb_vue_parent);
                image_action_get_profile=(AppCompatImageView)v.findViewById(R.id.action_get_profile);
                image_action_get_location=(AppCompatImageView)v.findViewById(R.id.action_get_location);
                handler=new Handler(Looper.getMainLooper());
                title=(TextView)v.findViewById(R.id.un_modele_view_title);
                checkFavorite=(AppCompatCheckBox) v.findViewById(R.id.checkfavorite);
                imageViewActionShare=(AppCompatImageView)v.findViewById(R.id.action_partage);
                imageViewActionRepport=(AppCompatImageView)v.findViewById(R.id.action_signalement);
                tvDate=(TextView)v.findViewById(R.id.un_modele_view_date);
                description=(TextView) v.findViewById(R.id.un_modele_view_description);
                extend=(TextView)v.findViewById(R.id.un_modele_view_extend);
                extend2=(TextView)v.findViewById(R.id.un_modele_view_extend2);
                more=(Button)v.findViewById(R.id.un_modele_view_plus);
                share=(Button)v.findViewById(R.id.un_modele_view_partager);
                buy=(Button)v.findViewById(R.id.un_modele_view_acheter);
                flipper=(ViewFlipper)v.findViewById(R.id.un_modele_view_flipper);
                left=(ImageView)v.findViewById(R.id.un_modele_view_swipe_left);
                right=(ImageView)v.findViewById(R.id.un_modele_view_swipe_right);
                imageViewToggleDate=(AppCompatImageView) v.findViewById(R.id.action_get_time);
                expandableLinearLayoutdate=(ExpandableLayout) v.findViewById(R.id.expandable);
                expandableLinearLayoutreadmore=(ExpandableLayout) v.findViewById(R.id.expandable2);
                read_more_linearlayout=(LinearLayout)v.findViewById(R.id.read_more_linearlayout);
                switchCompatVitrineBourse = (SwitchCompat) v.findViewById(R.id.switchVitrineBourse);
                runnable=new Runnable() {
                    @Override
                    public void run() {
                        flipper.setAutoStart(false);
                        flipper.stopFlipping();
                        flipper.setFlipInterval(4000);
                        flipper.setInAnimation(AnimationUtils.loadAnimation(Une_bourse_activity.this, R.anim.main_slide_in_left));
                        flipper.setOutAnimation(AnimationUtils.loadAnimation(Une_bourse_activity.this,R.anim.main_slide_out_left));
                        flipper.setAutoStart(true);
                        flipper.startFlipping();
                    }
                };
                read_more_linearlayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ListViewHolder holder=(ListViewHolder) recyclerView.findViewHolderForAdapterPosition(position);
                        if(holder.expandableLinearLayoutreadmore.isExpanded()){
                            expandableLinearLayoutreadmore.collapse();
                        }
                        else {
                            expandableLinearLayoutreadmore.expand();
                        }
                    }
                });
                imageViewToggleDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ListViewHolder holder=(ListViewHolder) recyclerView.findViewHolderForAdapterPosition(position);
                        if(holder.expandableLinearLayoutdate.isExpanded()){
                            expandableLinearLayoutdate.collapse();
                            view_vitrine_nb_vue_parent.setVisibility(View.VISIBLE);
                            image_action_get_location.setVisibility(View.VISIBLE);
                            image_action_get_profile.setVisibility(View.VISIBLE);
                        }
                        else {
                            expandableLinearLayoutdate.expand();
                            view_vitrine_nb_vue_parent.setVisibility(View.GONE);
                            image_action_get_location.setVisibility(View.GONE);
                            image_action_get_profile.setVisibility(View.GONE);
                        }
                    }
                });

                left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        stopRunnable();
                        flipper.stopFlipping();
                        flipper.setAutoStart(false);

                        flipper.setInAnimation(AnimationUtils.loadAnimation(Une_bourse_activity.this, R.anim.main_slide_in_left));;
                        flipper.setOutAnimation(AnimationUtils.loadAnimation(Une_bourse_activity.this,R.anim.main_slide_out_left));
                        flipper.showNext();
                        runHandler();

                    }
                });
                right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        stopRunnable();
                        flipper.stopFlipping();
                        flipper.setAutoStart(false);

                        flipper.setInAnimation(AnimationUtils.loadAnimation(Une_bourse_activity.this, R.anim.main_slide_in_right));
                        flipper.setOutAnimation(AnimationUtils.loadAnimation(Une_bourse_activity.this, R.anim.main_slide_out_right));
                        flipper.showPrevious();
                        runHandler();
                    }
                });

            }


        }

        public  class ListViewHolderVideo extends  RecyclerView.ViewHolder
        {
            TextView  title;
            TextView tvDate;
            TextView description;
            TextView extend,extend2;
            ImageView left;
            ImageView right;
            AppCompatImageView imageViewToggleDate,imageViewActionShare,imageViewActionRepport;
            AppCompatCheckBox checkFavorite;
            ExpandableLayout expandableLinearLayoutdate,expandableLinearLayoutreadmore;
            LinearLayout read_more_linearlayout;
            DinamicImageView previewVideo;
            Button more,share,buy;
            int itemCount;
            Handler handler;
            Runnable runnable;
            TextView tv_vitrine_nb_vue;
            View view_vitrine_nb_vue_parent;
            AppCompatImageView image_action_get_profile;
            AppCompatImageView image_action_get_location;
            public  void stopRunnable(){
                handler.removeCallbacks(runnable);
            }
            public  void runHandler(){
                handler.postDelayed(runnable,4000);
            }
            public void setItemCount(int itemCount){
                this.itemCount=itemCount;
            }
            private int position;
            public void bin(int position){
                this.position=position;
            }
            private SwitchCompat switchCompatVitrineActuVideo;
            public  ListViewHolderVideo(View v)
            {
                super(v);
                tv_vitrine_nb_vue=(TextView)v.findViewById(R.id.vitrine_nb_vue);
                view_vitrine_nb_vue_parent=v.findViewById(R.id.vitrine_nb_vue_parent);
                image_action_get_profile=(AppCompatImageView)v.findViewById(R.id.action_get_profile);
                image_action_get_location=(AppCompatImageView)v.findViewById(R.id.action_get_location);
                handler=new Handler(Looper.getMainLooper());
                title=(TextView)v.findViewById(R.id.un_modele_view_title);
                checkFavorite=(AppCompatCheckBox) v.findViewById(R.id.checkfavorite);
                imageViewActionShare=(AppCompatImageView)v.findViewById(R.id.action_partage);
                imageViewActionRepport=(AppCompatImageView)v.findViewById(R.id.action_signalement);
                tvDate=(TextView)v.findViewById(R.id.un_modele_view_date);
                description=(TextView) v.findViewById(R.id.un_modele_view_description);
                extend=(TextView)v.findViewById(R.id.un_modele_view_extend);
                extend2=(TextView)v.findViewById(R.id.un_modele_view_extend2);
                more=(Button)v.findViewById(R.id.un_modele_view_plus);
                share=(Button)v.findViewById(R.id.un_modele_view_partager);
                buy=(Button)v.findViewById(R.id.un_modele_view_acheter);
                buy.setBackground(ContextCompat.getDrawable(v.getContext(),R.drawable.shape_bourse_en_savoir_buy));
                more.setBackground(ContextCompat.getDrawable(v.getContext(),R.drawable.shape_bourse_en_savoir_buy));
                previewVideo=(DinamicImageView) v.findViewById(R.id.un_modele_view_flipper);
                left=(ImageView)v.findViewById(R.id.un_modele_view_swipe_left);
                right=(ImageView)v.findViewById(R.id.un_modele_view_swipe_right);
                imageViewToggleDate=(AppCompatImageView) v.findViewById(R.id.action_get_time);
                expandableLinearLayoutdate=(ExpandableLayout) v.findViewById(R.id.expandable);
                expandableLinearLayoutreadmore=(ExpandableLayout) v.findViewById(R.id.expandable2);
                read_more_linearlayout=(LinearLayout)v.findViewById(R.id.read_more_linearlayout);
                switchCompatVitrineActuVideo = (SwitchCompat) v.findViewById(R.id.switchVitrineActuVideo);
                read_more_linearlayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ListViewHolderVideo holder=(ListViewHolderVideo) recyclerView.findViewHolderForAdapterPosition(position);
                        if(holder.expandableLinearLayoutreadmore.isExpanded()){
                            expandableLinearLayoutreadmore.collapse();
                        }
                        else {
                            expandableLinearLayoutreadmore.expand();
                        }
                    }
                });
                imageViewToggleDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ListViewHolderVideo holder=(ListViewHolderVideo) recyclerView.findViewHolderForAdapterPosition(position);
                        if(holder.expandableLinearLayoutdate.isExpanded()){
                            expandableLinearLayoutdate.collapse();
                            view_vitrine_nb_vue_parent.setVisibility(View.VISIBLE);
                            image_action_get_location.setVisibility(View.VISIBLE);
                            image_action_get_profile.setVisibility(View.VISIBLE);
                        }
                        else {
                            expandableLinearLayoutdate.expand();
                            view_vitrine_nb_vue_parent.setVisibility(View.GONE);
                            image_action_get_location.setVisibility(View.GONE);
                            image_action_get_profile.setVisibility(View.GONE);
                        }
                    }
                });


            }




        }

        public  class ListViewHolderGift extends  RecyclerView.ViewHolder
        {
            TextView  title;
            TextView tvDate;
            TextView description;
            TextView extend,extend2;
            ImageView left;
            ImageView right;
            AppCompatImageView imageViewToggleDate,imageViewActionShare,imageViewActionRepport;
            AppCompatCheckBox checkFavorite;
            ExpandableLayout expandableLinearLayoutdate,expandableLinearLayoutreadmore;
            LinearLayout read_more_linearlayout;
            DinamicImageView giftImage;
            Button more,share,buy;
            int itemCount;
            Handler handler;
            Runnable runnable;
            TextView tv_vitrine_nb_vue;
            View view_vitrine_nb_vue_parent;
            AppCompatImageView image_action_get_profile;
            AppCompatImageView image_action_get_location;
            public  void stopRunnable(){
                handler.removeCallbacks(runnable);
            }
            public  void runHandler(){
                handler.postDelayed(runnable,4000);
            }
            public void setItemCount(int itemCount){
                this.itemCount=itemCount;
            }
            private int position;
            public void bin(int position){
                this.position=position;
            }
            private SwitchCompat switchCompatVitrineActuGift;
            public  ListViewHolderGift(View v)
            {
                super(v);
                tv_vitrine_nb_vue=(TextView)v.findViewById(R.id.vitrine_nb_vue);
                view_vitrine_nb_vue_parent=v.findViewById(R.id.vitrine_nb_vue_parent);
                image_action_get_profile=(AppCompatImageView)v.findViewById(R.id.action_get_profile);
                image_action_get_location=(AppCompatImageView)v.findViewById(R.id.action_get_location);
                handler=new Handler(Looper.getMainLooper());
                title=(TextView)v.findViewById(R.id.un_modele_view_title);
                checkFavorite=(AppCompatCheckBox) v.findViewById(R.id.checkfavorite);
                imageViewActionShare=(AppCompatImageView)v.findViewById(R.id.action_partage);
                imageViewActionRepport=(AppCompatImageView)v.findViewById(R.id.action_signalement);
                tvDate=(TextView)v.findViewById(R.id.un_modele_view_date);
                description=(TextView) v.findViewById(R.id.un_modele_view_description);
                extend=(TextView)v.findViewById(R.id.un_modele_view_extend);
                extend2=(TextView)v.findViewById(R.id.un_modele_view_extend2);
                more=(Button)v.findViewById(R.id.un_modele_view_plus);
                share=(Button)v.findViewById(R.id.un_modele_view_partager);
                buy=(Button)v.findViewById(R.id.un_modele_view_acheter);
                buy.setBackground(ContextCompat.getDrawable(v.getContext(),R.drawable.shape_bourse_en_savoir_buy));
                more.setBackground(ContextCompat.getDrawable(v.getContext(),R.drawable.shape_bourse_en_savoir_buy));
                giftImage=(DinamicImageView) v.findViewById(R.id.un_modele_view_flipper);
                left=(ImageView)v.findViewById(R.id.un_modele_view_swipe_left);
                right=(ImageView)v.findViewById(R.id.un_modele_view_swipe_right);
                imageViewToggleDate=(AppCompatImageView) v.findViewById(R.id.action_get_time);
                expandableLinearLayoutdate=(ExpandableLayout) v.findViewById(R.id.expandable);
                expandableLinearLayoutreadmore=(ExpandableLayout) v.findViewById(R.id.expandable2);
                read_more_linearlayout=(LinearLayout)v.findViewById(R.id.read_more_linearlayout);
                switchCompatVitrineActuGift = (SwitchCompat) v.findViewById(R.id.switchVitrineActuGift);
                read_more_linearlayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ListViewHolderGift holder=(ListViewHolderGift) recyclerView.findViewHolderForAdapterPosition(position);
                        if(holder.expandableLinearLayoutreadmore.isExpanded()){
                            expandableLinearLayoutreadmore.collapse();
                        }
                        else {
                            expandableLinearLayoutreadmore.expand();
                        }
                    }
                });
                imageViewToggleDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ListViewHolderGift holder=(ListViewHolderGift) recyclerView.findViewHolderForAdapterPosition(position);
                        if(holder.expandableLinearLayoutdate.isExpanded()){
                            expandableLinearLayoutdate.collapse();
                            view_vitrine_nb_vue_parent.setVisibility(View.VISIBLE);
                            image_action_get_location.setVisibility(View.VISIBLE);
                            image_action_get_profile.setVisibility(View.VISIBLE);
                        }
                        else {
                            expandableLinearLayoutdate.expand();
                            view_vitrine_nb_vue_parent.setVisibility(View.GONE);
                            image_action_get_location.setVisibility(View.GONE);
                            image_action_get_profile.setVisibility(View.GONE);
                        }
                    }
                });

            }


        }


    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
