package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

/**
 * Created by joel on 23/10/2016.
 */
public class PicassoTransformation  implements Transformation {
    int size;
    boolean isScale;
    public  PicassoTransformation(int widthorheight,boolean isScaleHeight)
    {
     this.size=widthorheight;
      this.isScale=isScaleHeight;

    }
    @Override
    public Bitmap transform(Bitmap source) {

        Bitmap scalebitmap=null;

           scalebitmap=A_ScaleUtils.createScaledBitmap(source,400,500, A_ScaleUtils.ScalingLogic.FIT);
        if(scalebitmap!=source)
        {
            source.recycle();
        }


        return scalebitmap;
    }

    @Override
    public String key() {
        return "scaleRespectRatio"+size+isScale;
    }
}
