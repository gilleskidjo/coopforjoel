package com.benin.apkapka.cooperationsoffice.cooperationoffice.daos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.PushData;

import java.util.List;

/**
 * Created by joel on 12/08/2017.
 */
public class PushDataDao extends MyPushDatabaseDao {


    public PushDataDao(Context c){
        super(c);
    }
    //Nous ajoutons les donnée reçus lors des notifications
    public void addPushData(PushData data){
        ContentValues values=new ContentValues();
        values.put(MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_TYPE,data.getType());
        values.put(MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_CATEGORY,data.getCategory());
        values.put(MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_FAMILLE,data.getFamille());
        values.put(MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_REF,data.getRef());
        db.insert(MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.TABLE_NAME,null, values);
    }
    //Recupere le nombre de donné de notification reçu pour une famille
    //Je recupere le type et le nombre d'element
    public Cursor getPushDataTypeCount(){

       String selectionArgs []={};
        String query="SELECT "+ MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_TYPE+", COUNT("+ MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_TYPE+
                ")as count FROM "+ MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.TABLE_NAME+" GROUP BY "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_TYPE;
       Cursor c=db.rawQuery(query,selectionArgs);

      return c;
    }
    //Recupere le nombre de famille dans lesquelle il ya de nouvelles données (d'un type et d'une category)(Pour les tablayout)
    //Je recupere la category et le nombre d'element
    public Cursor getPushDataCategoryFamilleCount(String type,String category,String category2,String category3){
        String query="SELECT DISTINCT "+ MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_CATEGORY+", COUNT( DISTINCT "+ MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_FAMILLE+" ) as count FROM  "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.TABLE_NAME+" WHERE "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_TYPE+" =? AND ( "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_CATEGORY+" =? OR "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_CATEGORY+" =? OR "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_CATEGORY+" =? ) GROUP BY  "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_CATEGORY;
        String selectionArgs[]={type,category,category2,category3};
        Cursor c=db.rawQuery(query,selectionArgs);
        return c;
    }
    //recupere le nombre de nouveaux elements dans une famille d'un type et d'une category(a utiliser pour les liste)
    // Je recupere la famille et le nombre d'element
    public Cursor getPushDataFamilleCount(String type,String category){
        String query="SELECT "+ MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_FAMILLE+", COUNT("+ MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_FAMILLE+") as count FROM  "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.TABLE_NAME+" WHERE "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_TYPE+" =? AND "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_CATEGORY+" =? GROUP BY "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_FAMILLE;
        String selectionArgs []={type,category};
        Cursor c=db.rawQuery(query,selectionArgs);
        return c;
    }
    public Cursor testeFamilleCount(String type,String category){
        String query="SELECT "+ MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_FAMILLE+", COUNT("+ MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_FAMILLE+") as count FROM  "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.TABLE_NAME+" WHERE "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_TYPE+" =? AND "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_CATEGORY+" =? GROUP BY "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_FAMILLE;
        String selectionArgs []={type,category};
        Cursor c=db.rawQuery(query,selectionArgs);
        return c;
    }
  //Suprime les elements d'une famille donnée
    public  void deletePushDataFamille(String type,String category, String famille){
        String selection=""+ MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_TYPE+" =? AND "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_CATEGORY+" = ? AND "+
                MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_FAMILLE+"=? ";
        String selectionArgs[]={type,category,famille};
        db.delete(MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.TABLE_NAME,selection,selectionArgs);
    }
    //Check before insert data
   public   boolean chechPushData(long id){
       String selections=""+ MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_REF+"=?";
       String selectionArgs []={""+id};
       long val= DatabaseUtils.queryNumEntries(db, MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.TABLE_NAME,selections,selectionArgs);
        if(val>0)
           return true;
      return false;
   }
}
