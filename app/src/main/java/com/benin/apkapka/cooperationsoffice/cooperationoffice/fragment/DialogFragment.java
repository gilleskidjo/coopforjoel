package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.app.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;


import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;

/**
 * Created by joel on 23/09/2016.
 */
public class DialogFragment  extends Fragment {
    public  static  final  String TAG="dialog_fragment_tag";
    ProgressDialog dialog1;
    Context c;
    AlertDialog.Builder builder;
    AlertDialog alertDialog;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        c=context;

    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    setRetainInstance(true);
        dialog1=new ProgressDialog(c);
       builder=new AlertDialog.Builder(c);

    }
    public  void showDialogPro(String message)
    {
        if(getActivity()!=null){
            if(!getActivity().isFinishing()){
                builder.setMessage(message);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                          if(alertDialog!=null){
                              alertDialog.dismiss();
                          }
                    }
                });
               alertDialog= builder.show();
            }
        }
    }
    public  void showDialogue(String message)
    {
        if(getActivity()!=null){
            if(!getActivity().isFinishing()){
                dialog1.setCancelable(false);
                dialog1.setMessage(message);
                if(!dialog1.isShowing())
                    dialog1.show();
            }
        }
    }



    public  void hideDialog()
    {
       if(getActivity()!=null){
           if(!getActivity().isFinishing()){
               if(dialog1!=null && dialog1.isShowing())
                   dialog1.dismiss();

           }
       }
    }


}
