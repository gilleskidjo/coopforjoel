package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.ContactAsyncLoader;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Contact;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyCheckBoxInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joel on 28/08/2017.
 */
public class PhoneContactActivity  extends BaseLanguageActivity implements MyCheckBoxInterface,LoaderManager.LoaderCallbacks<List<Contact>>{
    ListView listView;
    Button btn;
    Toolbar toolbar;
    ActionBar bar;
    MyAdapter adapter;
    List<Contact> list=new ArrayList<>();
    ContactAsyncLoader loader;
    MenuItem menuItemCount;
    CheckBox checkBoxCount;
    private int count;
    private static final int READ_CONTACT_REUEST_CODE=1;
    TextView vide;
    View linearvide;
    ProgressBar progress;
    private static final int LOADER_ID=6;
    private boolean is_fr;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.primary_dark));
        }
        setContentView(R.layout.user_contact_activity_view);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        listView=(ListView)findViewById(R.id.liste_contact);
        linearvide=findViewById(R.id.user_contact_linearvide);
        vide=(TextView)findViewById(R.id.user_contact_textview_vide);
        progress=(ProgressBar)findViewById(R.id.user_contact_Progress);
        toolbar=(Toolbar)findViewById(R.id.my_toolbar);
        checkBoxCount=(CheckBox)toolbar.findViewById(R.id.checkCount);
        checkBoxCount.setEnabled(false);
        checkBoxCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkBoxCount.isChecked()){
                    count=list.size();
                    for (int i=0;i<list.size();i++){
                        Contact c=list.get(i);
                        c.setChecked(true);
                    }
                }
                else{
                    count=0;
                    for (int i=0;i<list.size();i++){
                        Contact c=list.get(i);
                        c.setChecked(false);
                    }
                }
                menuItemCount.setTitle(""+count);
                adapter.notifyDataSetChanged();
            }
        });
        setSupportActionBar(toolbar);
        bar=getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);

        try{
            bar.setDisplayShowTitleEnabled(false);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        adapter=new MyAdapter(this,list,this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CheckBox checkBox=(CheckBox) view.findViewById(R.id.check);
                Contact c=list.get(i);
                c.setChecked(!c.isChecked());
                checkBox.setChecked(c.isChecked());
                if(c.isChecked()){
                    count+=1;
                }
                else{

                    if(count>0)
                        count-=1;
                    else
                        count=0;
                }
                menuItemCount.setTitle(""+count);
            }
        });

        if( ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS )!= PackageManager.PERMISSION_GRANTED){

            if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_CONTACTS))
            {
                AlertDialog.Builder builder=new AlertDialog.Builder(this);
                builder.setMessage(R.string.contact_permission);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                   PhoneContactActivity.this.finish();
                    }
                });
            }
            else {
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_CONTACTS},READ_CONTACT_REUEST_CODE);
            }


        }
        else {
            getSupportLoaderManager().initLoader(LOADER_ID,null,this);
        }

    }

    @Override
    public void notifyCheckBokCliked(boolean clicked) {

        if(clicked){
            count+=1;
        }
        else{

            if(count>0)
                count-=1;
            else
                count=0;
        }
        menuItemCount.setTitle(""+count);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.user_contact_menu,menu);
        menuItemCount=menu.findItem(R.id.menu_item_cout);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_send:
               int count=0;
                String val="";
                for (int i=0;i<list.size();i++){
                    Contact c=list.get(i);
                    if(c.isChecked()){
                        val=val+c.getNumber()+";";
                        count=count+1;
                    }
                }
                if(count>0){
                    Intent intent=new Intent(Intent.ACTION_SENDTO);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.setType("vnd.android-dir/mms-sms");
                    intent.setData(Uri.parse("sms:"+val));
                    intent.putExtra("sms_body", is_fr?"Telecharger cooperationsOffice sur Google Play: https://play.google.com/store/apps/details?id=com.benin.apkapka.cooperationsoffice.cooperationoffice&hl=fr":"Download cooperationsOffice on Google Play: https://play.google.com/store/apps/details?id=com.benin.apkapka.cooperationsoffice.cooperationoffice&hl=fr");
                    if(intent.resolveActivity(getPackageManager())!=null){
                        startActivity(intent);
                    }
                }
                else {
                    Toast.makeText(this,""+getResources().getString(R.string.user_contact_add),Toast.LENGTH_SHORT).show();
                }

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<List<Contact>> onCreateLoader(int id, Bundle args) {
        loader=new ContactAsyncLoader(this);
        loader.forceLoad();

        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<Contact>> loader, List<Contact> data) {
      switch (loader.getId()){
          case LOADER_ID:
              progress.setVisibility(View.GONE);
              checkBoxCount.setEnabled(true);
              if(data!=null)
              {
                  list=data;
                  if(data.size()>0){
                      list=data;
                      adapter=new MyAdapter(this,list,this);
                      listView.setAdapter(adapter);
                      listView.setVisibility(View.VISIBLE);
                  }
                  else {
                      linearvide.setVisibility(View.VISIBLE);
                      listView.setVisibility(View.GONE);
                  }

              }
              return;
      }
    }

    @Override
    public void onLoaderReset(Loader<List<Contact>> loader) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case READ_CONTACT_REUEST_CODE:
                if(grantResults.length>0 &&grantResults [0]==PackageManager.PERMISSION_GRANTED)
                {
                    getSupportLoaderManager().initLoader(LOADER_ID,null,this);
                }
                else {
                    linearvide.setVisibility(View.VISIBLE);
                    vide.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                    progress.setVisibility(View.GONE);

                }
                return;
        }
    }

    class MyAdapter extends BaseAdapter {
        List<Contact> list;
        Context context;

        MyCheckBoxInterface boxInterface;
        public  MyAdapter(Context c, List<Contact> list,MyCheckBoxInterface boxInterface)
        {
            this.list=list;
            this.context=c;
            this.boxInterface=boxInterface;
        }
        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }
        class ViewHolder{
            TextView name;
            TextView number;
            CheckBox checkBox;
        }
        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            ViewHolder holder=null;


            if(convertView==null){
                LayoutInflater inflater=LayoutInflater.from(context);
                convertView=inflater.inflate(R.layout.user_contact_activity_list_item,null);
                holder=new ViewHolder();
                holder.name=(TextView)convertView.findViewById(R.id.text_name);
                holder.number=(TextView)convertView.findViewById(R.id.text_number);
                holder.checkBox=(CheckBox)convertView.findViewById(R.id.check);

                holder.checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CheckBox check=(CheckBox)view;

                        Contact c=(Contact)check.getTag();
                        c.setChecked(!c.isChecked());
                        boxInterface.notifyCheckBokCliked(c.isChecked());
                    }
                });
                convertView.setTag(holder);
            }
            else {
                holder=(ViewHolder)convertView.getTag();
            }
            Contact contact=list.get(i);
            holder.name.setText(contact.getName());
            holder.number.setText(contact.getNumber());
            holder.checkBox.setChecked(contact.isChecked());
            holder.checkBox.setTag(contact);

            return convertView;
        }


    }
}
