package com.benin.apkapka.cooperationsoffice.cooperationoffice.daos;

import javax.xml.transform.sax.SAXResult;

/**
 * Created by joel on 16/08/2017.
 */
public  final class PushHandler {
  public static final String pushBaseUlr="https://cooperationsoffice.net";
  //type
  public static final String type_push="push";
  public  static final String type="actualite";
  public  static  final String type2="boutique";
  public static  final String type3="emploi";
  public static  final String type4="bourse";
  public static  final String type5="annuaire";
  public static  final String type6="service";
  public static  final String type7="location";
  public static final String type8="partenariat";
  public static final String type9="projet";
  //category
  public static final  String ACT_INFO="information";
  public static  final String ACT_KIOSQUE="kiosque";
  public static final String ACT_BIBLIO="bibliotheque";

  public static final String B_STOCK="stock";
  public static final String B_AUTO="automobile";
  public static final String B_IMM0="immobilier";

  public static final String EMP_OFFRE="emploi";
  public static final String EMP_CV="cv_en_ligne";
  public static final String EMP_FREE="freelance";

  public static final String BOURSE_FORMA="formation";
  public static final String BOURSE_VOY="voyage";
  public static final String BOURSE_VAL="valeur";

  public static final String ANN_ADR="adresses";
  public static final String ANN_REP="repertoires";
  public static final String ANN_TROM="trombinoscope";

  public static final String SER_PUB="publics";
  public static final String SER_ENTRE="entreprises";
  public static final String SER_CLIENT="clients";

  public static final String LO_IMMO="immobilier";
  public static final String LO_AUTO="automobile";
  public static final String LO_AUTRE="autre";

  public static final String PART_COMME="commercial";
  public static final String PART_INDUST="industriel";
  public static final String PART_TECH="technologique";

  public static final String PRO_OFFRE="appel_offre";
  public static final String PRO_FIN="financement";
  public static final String PRO_DEV="developpement";

  public static final int FRAGMENT1=1;
  public static final int FRAGMENT2=2;
  public static final int FRAGMENT3=3;
  public static final int LOADER_FRAGMENT_ACTIVITY_ID=4;
  public static final int LOADER_MAIN_ACTIVITY_ID=5;
}
