package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.ChatConversationInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ChatConversation;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MydateUtil;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class BottomChatConversation_Activity extends AppCompatActivity implements ChatConversationInterface{

    Toolbar toolbar;
    RecyclerView recyclerView;
    FloatingActionButton floatingActionButton;
    ProgressBar progressBar;
    TextView tvVide;
    View linearvide;
    boolean is_fr;
    List<ChatConversation>chatConversations=new ArrayList<>();
    MyConversationAdapateur adapateur;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottom_chat_conversation_layout);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        toolbar=(Toolbar)findViewById(R.id.conversation_toolbar);
        recyclerView=(RecyclerView)findViewById(R.id.conversation_recyclerview);
        floatingActionButton=(FloatingActionButton)findViewById(R.id.convesation_fab);
        progressBar=(ProgressBar)findViewById(R.id.chat_conversation_Progress);
        linearvide=findViewById(R.id.chat_conversation_linearvide);
        tvVide=(TextView)findViewById(R.id.chat_conversation_textview_vide);
        recyclerView.setVisibility(View.GONE);

        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle(is_fr?"Chat avec Cooperations office":"Chat with Cooperations office");
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(BottomChatConversation_Activity.this,BottomChatNouveauMessageActvity.class);
                startActivityForResult(intent,10);
            }
        });
        if(!isConnected()){
            floatingActionButton.setVisibility(View.GONE);
        }
        adapateur=new MyConversationAdapateur(chatConversations,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapateur);
        getAllConversation();


    }


    public void getAllConversation(){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);
        SessionManager sessionManager = new SessionManager(this);
        User user= sessionManager.getUser(this);
        Call<String> call=service.get_All_conversation(user.getId());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));


                            boolean error = o.getBoolean("error");
                            if (error) {

                                if(!isFinishing()){
                                    progressBar.setVisibility(View.GONE);
                                    linearvide.setVisibility(View.VISIBLE);
                                    tvVide.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                    tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));
                                }

                            } else {


                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {

                                    if(is_fr){
                                        progressBar.setVisibility(View.GONE);
                                        linearvide.setVisibility(View.VISIBLE);
                                        recyclerView.setVisibility(View.GONE);
                                        tvVide.setVisibility(View.VISIBLE);
                                        tvVide.setText("Aucune conversation trouvée");
                                    }
                                    else{
                                        progressBar.setVisibility(View.GONE);
                                        linearvide.setVisibility(View.VISIBLE);
                                        tvVide.setVisibility(View.VISIBLE);
                                        recyclerView.setVisibility(View.GONE);
                                        tvVide.setText("No conversation found");
                                    }

                                } else {


                                    progressBar.setVisibility(View.GONE);
                                    linearvide.setVisibility(View.GONE);
                                    tvVide.setVisibility(View.GONE);

                                    recyclerView.setVisibility(View.VISIBLE);
                                   chatConversations = ParseJson.parseConversation(o);
                                   adapateur.notifyDataSetChanged();
                                   adapateur=new MyConversationAdapateur(chatConversations,BottomChatConversation_Activity.this);
                                   recyclerView.setAdapter(adapateur);


                                }

                            }

                        } catch (Exception e) {

                           if(!isFinishing()){
                               progressBar.setVisibility(View.GONE);
                               linearvide.setVisibility(View.VISIBLE);
                               tvVide.setVisibility(View.VISIBLE);
                               recyclerView.setVisibility(View.GONE);
                               tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));
                           }
                        }

                    }//fin is success,
                    else {

                        progressBar.setVisibility(View.GONE);
                        linearvide.setVisibility(View.VISIBLE);
                        tvVide.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));

                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {


                if(!isFinishing())
                {
                    progressBar.setVisibility(View.GONE);
                    linearvide.setVisibility(View.VISIBLE);
                    tvVide.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));

                }
            }
        });
    }



    @Override
    public void onClickConversation(int position) {
    ChatConversation conversation=chatConversations.get(position);

    long conversation_id=conversation.getId();
    Intent intent=new Intent(BottomChatConversation_Activity.this,BottomChat_Activity.class);
    intent.putExtra("conversation_id",conversation_id);
    startActivityForResult(intent,100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       getAllConversation();
    }
    private boolean isConnected()
    {
        ConnectivityManager cm=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        if(info!=null && info.isConnected() && info.isAvailable())
        {
            return  true;
        }

        return  false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;



        }
        return super.onOptionsItemSelected(item);
    }

    class MyConversationAdapateur extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
        public static final int VIEW_TYPE_ONE=1;
        public static final int VIEW_TYPE_TWO=2;
        List<ChatConversation> listCon=new ArrayList<>();
        ChatConversationInterface conversationInterface;
        public MyConversationAdapateur(List<ChatConversation> chatConversations,ChatConversationInterface chatConversationInterface){
            this.listCon=chatConversations;
            this.conversationInterface=chatConversationInterface;
        }

        class ConversationMessageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            TextView avatar;
            TextView tvNom;
            TextView messageboby;
            TextView tvdate;
            public ConversationMessageViewHolder(View v){
                super(v);
                avatar=(TextView)v.findViewById(R.id.conversation_avatar);
                tvNom=(TextView)v.findViewById(R.id.conversation_name);
                messageboby=(TextView)v.findViewById(R.id.conversation_message_body);
                tvdate=(TextView)v.findViewById(R.id.conversation_date);
                v.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                conversationInterface.onClickConversation(getAdapterPosition());
            }
        }

        class ConversationPhotoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            TextView avatar;
            TextView tvNom;
            TextView tvdate;
            public ConversationPhotoViewHolder(View v){
                super(v);
                avatar=(TextView)v.findViewById(R.id.conversation_avatar);
                tvNom=(TextView)v.findViewById(R.id.conversation_name);
                tvdate=(TextView)v.findViewById(R.id.conversation_date);
                v.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                conversationInterface.onClickConversation(getAdapterPosition());
            }
        }
        @Override
        public int getItemCount() {
            return listCon.size();
        }

        @Override
        public int getItemViewType(int position) {
            ChatConversation conversation=listCon.get(position);
                    int type=0;
                    if(conversation.getType().equalsIgnoreCase("message")){
                        type=VIEW_TYPE_ONE;
                    }
                    else {
                        type=VIEW_TYPE_TWO;
                    }
            return type;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

              switch (viewType){
                  case VIEW_TYPE_ONE:
                      View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.bottom_chat_conversation_item_message,parent,false);
                      ConversationMessageViewHolder textMessageMyViewHolder=new ConversationMessageViewHolder(v);
                      return  textMessageMyViewHolder;

                  case VIEW_TYPE_TWO:
                      View v1= LayoutInflater.from(parent.getContext()).inflate(R.layout.bottom_chat_conversation_item_photo,parent,false);
                      ConversationPhotoViewHolder conversationPhotoViewHolder=new ConversationPhotoViewHolder(v1);
                      return  conversationPhotoViewHolder;


              }
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
              ChatConversation conversation=listCon.get(position);

              if(holder instanceof ConversationMessageViewHolder){
                 ConversationMessageViewHolder messageViewHolder=(ConversationMessageViewHolder)holder;
                  messageViewHolder.tvdate.setText(""+ MydateUtil.getDateFromChat(conversation.getCreate_at(),is_fr));
                  messageViewHolder.avatar.setText("CO");
                  messageViewHolder.tvNom.setText("Cooperationsoffice");
                  if(conversation.getStatut().equalsIgnoreCase("attente")){
                      messageViewHolder.avatar.setText("CO");
                      messageViewHolder.tvNom.setText("Cooperationsoffice");
                  }
                  else{
                      messageViewHolder.avatar.setText(conversation.getTo_user_nom().length()>0?""+conversation.getTo_user_nom().charAt(0):"CO");
                      if(conversation.getTo_user_nom()!=null && !conversation.getTo_user_nom().equalsIgnoreCase("") && !conversation.getTo_user_nom().equalsIgnoreCase("null")){
                       messageViewHolder.tvNom.setText(conversation.getTo_user_nom());
                      }
                      else{
                          messageViewHolder.tvNom.setText("Cooperationsoffice");
                      }
                  }

                  if(conversation.getLast_message()!=null){
                      messageViewHolder.messageboby.setText(conversation.getLast_message());
                  }
                  else{

                  }
                }
                else if(holder instanceof ConversationPhotoViewHolder){
                    ConversationPhotoViewHolder photoViewHolder=(ConversationPhotoViewHolder)holder;
                  photoViewHolder.tvdate.setText(""+ MydateUtil.getDateFromChat(conversation.getCreate_at(),is_fr));
                  photoViewHolder.avatar.setText("CO");
                  photoViewHolder.tvNom.setText("Cooperationsoffice");
                  if(conversation.getStatut().equalsIgnoreCase("attente")){
                      photoViewHolder.avatar.setText("CO");
                      photoViewHolder.tvNom.setText("Cooperationsoffice");
                  }
                  else{
                      photoViewHolder.avatar.setText(conversation.getTo_user_nom().length()>0?""+conversation.getTo_user_nom().charAt(0):"CO");

                      if(conversation.getTo_user_nom()!=null && !conversation.getTo_user_nom().equalsIgnoreCase("") && !conversation.getTo_user_nom().equalsIgnoreCase("null")){
                          photoViewHolder.tvNom.setText(conversation.getTo_user_nom());
                      }
                      else{
                          photoViewHolder.tvNom.setText("Cooperationsoffice");
                      }
                  }


                }

        }
    }
}
