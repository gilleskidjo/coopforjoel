package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;

/**
 * Created by joel on 24/02/2016.
 */
public class MyToast {



    public static  void show(Context c,String texte,boolean isLong)
    {
        LayoutInflater inflater=LayoutInflater.from(c);
        View v=inflater.inflate(R.layout.mytoast_layout, null);
        TextView t=(TextView)v.findViewById(R.id.toastexte);
        t.setText(texte);
        Toast toast=new Toast(c);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.setDuration(isLong?Toast.LENGTH_LONG:Toast.LENGTH_SHORT);
        toast.setView(v);
        toast.show();


    }
}
