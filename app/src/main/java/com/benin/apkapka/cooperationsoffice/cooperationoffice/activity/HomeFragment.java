package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v4.view.GestureDetectorCompat;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewFlipper;


import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.AccueilInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.AdapterMenuRecycler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetBackImage;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetworkRequest;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushType;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushTypeAsyncLoader;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Home;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.HomeDatabaseRelation;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.LanguageNotifier;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MenuVitrine;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.signature.StringSignature;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

import static android.content.Context.MODE_PRIVATE;


public class HomeFragment extends Fragment implements AccueilInterface,FragmentForNetworkRequest.HandlerNetWorkRequestResponse,LoaderManager.LoaderCallbacks<List<PushType>>,FragmentForNetBackImage.HandlerNetWorkRequestResponseImage,LanguageNotifier.LanguageNotifierInterface{


    ProgressBar bar;
    View flipperLayout;
    ViewFlipper flipper;
   ImageView left,right;
    FragmentForNetworkRequest netWork;

    List<Home> listP=new ArrayList<Home>();
    FragmentManager fm;

      HomeDatabaseRelation relation;

    LinearLayout conteneur;
    private boolean is_fr=false;
    private String  [] pays;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    Animation anim;
    PushTypeAsyncLoader loader;
    TextView ntTextview1,ntTextview2,ntTextview3,ntTextview4,ntTextview5,ntTextview6,ntTextview7,ntTextview8,ntTextview9;

    boolean is_small,is_medium,is_large,is_landescape;

    Button  btnActualite,btnBoutique,btnLocation,btnemploi,btnService,btnbourse,btnPartenaire,btnAnnuaire,btnProjet;
    Bitmap btm;
    GestureDetectorCompat detectorCompat;
    IntentFilter filter;
    MyReceiverApp receiverApp;
    MenuItem menuItemCount;
    private int contHeight;
    FragmentForNetBackImage fNetImage;
    List<Target> targets=new ArrayList<>();
    RecyclerView recycler;
    AdapterMenuRecycler adapterMenuRecycler;
    List<PushType> notif;






        View.OnClickListener  clickListener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent=new Intent();
                switch (v.getId())
                {
                    case R.id.btn_actualite:
                        btnActualite.startAnimation(anim);
                        intent=new Intent(getContext(),Activity_Actualite.class);
                        startActivity(intent);
                       break;
                    case R.id.btn_boutique:
                        intent=new Intent(getContext(),Activity_Boutique.class);
                        startActivity(intent);
                        btnBoutique.startAnimation(anim);
                        break;
                    case R.id.btn_emploi:
                        intent=new Intent(getContext(),Emploi.class);
                        startActivity(intent);
                        btnemploi.startAnimation(anim);
                        break;
                    case R.id.btn_bourse:
                        btnbourse.startAnimation(anim);
                        intent=new Intent(getContext(),Bourse.class);
                        startActivity(intent);
                        break;

                    case R.id.btn_annuaire:
                        btnAnnuaire.startAnimation(anim);
                        intent=new Intent(getContext(),Annuaire.class);
                        startActivity(intent);
                        break;
                    case R.id.btn_service:
                        btnService.startAnimation(anim);
                        intent=new Intent(getContext(),Activity_Service.class);
                        startActivity(intent);
                        break;
                    case R.id.btn_location_voiture:
                        btnLocation.startAnimation(anim);
                        intent=new Intent(getContext(),Activity_Location_Voiture.class);
                        startActivity(intent);
                        break;
                    case R.id.btn_partenaire:
                        btnPartenaire.startAnimation(anim);
                        intent=new Intent(getContext(),Activity_Partenariat.class);
                        startActivity(intent);
                        break;
                    case  R.id.btn_projet:
                        btnProjet.startAnimation(anim);
                        intent=new Intent(getContext(),Projet.class);
                        startActivity(intent);
                        break;

                }

            }
        };




   public class MyReceiverApp extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(Intent.ACTION_SCREEN_OFF)){

                    if(flipper!=null){
                        flipper.stopFlipping();
                    }
                }
                else if(intent.getAction().equals(Intent.ACTION_SCREEN_ON)){

                    if(flipper!=null){
                        flipper.setAutoStart(false);
                        flipper.startFlipping();
                        flipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.main_slide_in_left));
                        flipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.main_slide_out_left));
                    }
                }


        }
    }
    Box<HomeDatabaseRelation> databaseRelationBox;
    Query<HomeDatabaseRelation> databaseRelationQuery;


    @android.support.annotation.Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @android.support.annotation.Nullable ViewGroup container, @android.support.annotation.Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.activity_home,container,false);;
        LanguageNotifier.getInstance().setNotifierInterface(this);
        anim= AnimationUtils.loadAnimation(getContext(),R.anim.home_imageview_anim);

        preferences=getContext().getSharedPreferences("les_options",MODE_PRIVATE);
        editor=preferences.edit();

        flipper=(ViewFlipper)v.findViewById(R.id.flipper);
        bar=(ProgressBar)v.findViewById(R.id.progress);

        BoxStore boxStore=((MyApplication)getActivity().getApplicationContext()).getBoxStore();
        databaseRelationBox=boxStore.boxFor(HomeDatabaseRelation.class);
        relation=new HomeDatabaseRelation(1);

        setUpFlipper(v);

        is_fr=getResources().getBoolean(R.bool.lang_fr);
        is_small=getResources().getBoolean(R.bool.is_small);
        is_medium=getResources().getBoolean(R.bool.is_medium);
        is_large=getResources().getBoolean(R.bool.is_large);
        is_landescape=getResources().getBoolean(R.bool.is_landscape);
        pays=getResources().getStringArray(R.array.pays_code);

        conteneur=(LinearLayout) v.findViewById(R.id.conteneur);

        ntTextview1=(TextView)v.findViewById(R.id.notification_tv_bloc1);
        ntTextview2=(TextView)v.findViewById(R.id.notification_tv_bloc2);
        ntTextview3=(TextView)v.findViewById(R.id.notification_tv_bloc3);
        ntTextview4=(TextView)v.findViewById(R.id.notification_tv_bloc4);
        ntTextview5=(TextView)v.findViewById(R.id.notification_tv_bloc5);
        ntTextview6=(TextView)v.findViewById(R.id.notification_tv_bloc6);
        ntTextview7=(TextView)v.findViewById(R.id.notification_tv_bloc7);
        ntTextview8=(TextView)v.findViewById(R.id.notification_tv_bloc8);
        ntTextview9=(TextView)v.findViewById(R.id.notification_tv_bloc9);



        fm=getFragmentManager();

        fNetImage=(FragmentForNetBackImage) fm.findFragmentByTag(FragmentForNetBackImage.TAG);
        if(fNetImage==null){
            fNetImage=new FragmentForNetBackImage();
            fm.beginTransaction().add(fNetImage,FragmentForNetBackImage.TAG).commit();
            fNetImage.setInterface(this);
        }
        else{
            fNetImage.setInterface(this);
        }

        fNetImage.doRequestImage();
        fm=getFragmentManager();

        setupView(v);



        contHeight=getDisplayContentHeight();
        FragmentManager fm=getFragmentManager();
        netWork=(FragmentForNetworkRequest) fm.findFragmentByTag(FragmentForNetworkRequest.TAG);
        if(netWork==null)
        {

            netWork=new FragmentForNetworkRequest();
            fm.beginTransaction().add(netWork,FragmentForNetworkRequest.TAG).commit();
            netWork.setInterface(this);

            if(isConnect()){
                SessionManager sessionManager=new SessionManager(getActivity());
                User user=sessionManager.getUser(getContext());
                String zone="";
                zone=user.getPays();
                if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
                    zone="france";
                }
                netWork.doRequestHome(zone);
            }
            else{


                relation=databaseRelationBox.get(1);
                if(relation!=null){
                    listP=relation.list;


                }
                //home_data
                if (listP != null && listP.size() > 0) {

                    setUpSlide();
                }
                else{

                }

            }
        }
        else
        {

            netWork.setInterface(this);


            if(isConnect()){
                listP=netWork.getListhome();
                if(listP!=null && listP.size()>0)
                {

                    setUpSlide();
                }
                else
                {

                    //home_data
                    if (listP != null && listP.size() > 0) {
                        setUpSlide();
                    }

                }
            }
            else {

                relation=databaseRelationBox.get(1);
                if(relation!=null){
                    listP=relation.list;


                }
                //home_data
                if (listP != null && listP.size() > 0) {

                    setUpSlide();
                }
                else{

                }
            }

        }

        receiverApp=new MyReceiverApp();
        filter=new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);

        Loader<List<PushType>> loader=getLoaderManager().getLoader(PushHandler.LOADER_MAIN_ACTIVITY_ID);
        if(loader==null){
            getLoaderManager().initLoader(PushHandler.LOADER_MAIN_ACTIVITY_ID,null,this);
        }
        else{
            getLoaderManager().restartLoader(PushHandler.LOADER_MAIN_ACTIVITY_ID,null,this);
        }
        return v;
    }//fin onCreateView


    private void setUpFlipper(View v){
        flipperLayout=v.findViewById(R.id.flipper_layout);
        flipperLayout.setVisibility(View.GONE);
        bar.setVisibility(View.VISIBLE);

        final Handler handler=new Handler(Looper.getMainLooper());
        final Runnable runnable=new Runnable() {
            @Override
            public void run() {
               if(flipper!=null){
                   flipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.main_slide_in_left));
                   flipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.main_slide_out_left));
                   flipper.setAutoStart(true);
                   flipper.startFlipping();
               }
            }
        };
        left=(ImageView)v.findViewById(R.id.main_left);
        right=(ImageView)v.findViewById(R.id.main_right);
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(runnable);
                flipper.setAutoStart(false);
                flipper.stopFlipping();
                flipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.main_slide_in_left));
                flipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.main_slide_out_left));
                flipper.showNext();
                flipper.setAutoStart(true);
                flipper.startFlipping();
            }
        });

        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(runnable);
                flipper.setAutoStart(false);
                flipper.stopFlipping();
                flipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.main_slide_in_right));
                flipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.main_slide_out_right));
                flipper.showPrevious();

                handler.postDelayed(runnable, 4000);

            }
        });


    }


    private void setupView(View v){

        btnActualite=(Button) v.findViewById(R.id.btn_actualite);
        btnLocation=(Button)v.findViewById(R.id.btn_location_voiture);
        btnBoutique=(Button)v.findViewById(R.id.btn_boutique);
        btnemploi=(Button)v.findViewById(R.id.btn_emploi);
        btnbourse=(Button)v.findViewById(R.id.btn_bourse);
        btnService=(Button)v.findViewById(R.id.btn_service);
        btnAnnuaire=(Button)v.findViewById(R.id.btn_annuaire);
        btnPartenaire=(Button)v.findViewById(R.id.btn_partenaire);
        btnProjet=(Button)v.findViewById(R.id.btn_projet);

        recycler=v.findViewById(R.id.recycler);

        btnLocation.setVisibility(View.GONE);
        btnBoutique.setVisibility(View.GONE);
        btnbourse.setVisibility(View.GONE);
        btnemploi.setVisibility(View.GONE);
        btnService.setVisibility(View.GONE);
        btnAnnuaire.setVisibility(View.GONE);
        btnPartenaire.setVisibility(View.GONE);
        btnProjet.setVisibility(View.GONE);
        btnActualite.setVisibility(View.GONE);

        //on click handle
        btnActualite.setOnClickListener(clickListener);
        btnLocation.setOnClickListener(clickListener);
        btnBoutique.setOnClickListener(clickListener);
        btnemploi.setOnClickListener(clickListener);
        btnbourse.setOnClickListener(clickListener);
        btnService.setOnClickListener(clickListener);
        btnAnnuaire.setOnClickListener(clickListener);
        btnPartenaire.setOnClickListener(clickListener);
        btnProjet.setOnClickListener(clickListener);
    }







    @Override
    public void languageChange(Resources resources, String langague) {
        btnActualite.setText(resources.getString(R.string.main_actu));
        btnBoutique.setText(resources.getString(R.string.main_bout));
        btnemploi.setText(resources.getString(R.string.main_emploi));
        btnbourse.setText(resources.getString(R.string.main_bourse));
        btnAnnuaire.setText(resources.getString(R.string.main_annuaire));
        btnService.setText(resources.getString(R.string.main_service));
        btnLocation.setText(resources.getString(R.string.main_location));
        btnPartenaire.setText(resources.getString(R.string.main_partenariat));
        btnProjet.setText(resources.getString(R.string.main_projet));


        updateUiView();


    }

    private  void updateUiView(){
        flipper.removeAllViews();
        listP=netWork.getListhome();
        if(listP!=null)
        {
            for(int i=0;i<listP.size();i++){
                Home home=listP.get(i);
                if(home.getContent_type().equals("spot")){


                    View v=getLayoutInflater().inflate(R.layout.main_flipper_video_item,null);
                    DinamicImageView image=(DinamicImageView) v.findViewById(R.id.image);
                    TextView t=(TextView)v.findViewById(R.id.text);
                    t.setVisibility(View.VISIBLE);
                    final  float scale=getResources().getDisplayMetrics().density;
                    if(getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE){

                        if(is_small){
                            Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-60).into(image);
                        }
                        else if(is_medium)
                        {
                            Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(image);
                        }
                        else if(is_large){
                            Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(image);
                        }



                    }
                    else{
                        if(is_small){
                            Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels,(int)(230*scale+0.5f)+15).into(image);
                        }
                        else if(is_medium){
                            Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(image);
                        }
                        else if(is_large){
                            Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(image);
                        }
                        t.setVisibility(View.GONE);
                    }

                    if(is_fr){
                        t.setText(home.getTitle());
                    }
                    else{
                        t.setText(home.getTitle_en());
                    }
                    image.setTag(home);
                    image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DinamicImageView image=(DinamicImageView)v;
                            Home p=(Home) image.getTag();
                            Intent  intent =new Intent(getContext(),SpotActivity.class);
                            intent.putExtra("id",p.getId());
                            if(is_fr==true)
                            {
                                intent.putExtra("title",p.getTitle());
                                intent.putExtra("title_en",p.getTitle_en());
                                intent.putExtra("description",p.getDescription());
                                intent.putExtra("description_en",p.getDescription_en());


                            }
                            else
                            {
                                intent.putExtra("title",p.getTitle_en());
                                intent.putExtra("description",p.getDescription_en());
                                intent.putExtra("title_en",p.getTitle_en());
                                intent.putExtra("description_en",p.getDescription_en());
                            }
                            intent.putExtra("url",p.getPreview_image());
                            intent.putExtra("url2",p.getUrl2());
                            intent.putExtra("url3",p.getUrl3());
                            intent.putExtra("preview_image",p.getPreview_image());
                            intent.putExtra("link_video",p.getLink_video());
                            intent.putExtra("content_type",p.getContent_type());

                            startActivity(intent);
                        }
                    });
                    flipper.addView(v);

                }//if spot
                else{
                    View v=getLayoutInflater().inflate(R.layout.main_flipper_item_layout,null);
                    DinamicImageView image=(DinamicImageView) v.findViewById(R.id.image);
                    TextView t=(TextView)v.findViewById(R.id.text);
                    t.setVisibility(View.VISIBLE);
                    final  float scale=getResources().getDisplayMetrics().density;
                    if(getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE){

                        if(is_small){
                            Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-60).into(image);
                        }
                        else if(is_medium)
                        {
                            Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(image);
                        }
                        else if(is_large){
                            Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(image);
                        }


                    }
                    else{
                        if(is_small){
                            Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels,(int)(230*scale+0.5f)+15).into(image);
                        }
                        else if(is_medium){
                            Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(image);
                        }
                        else if(is_large){
                            Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(image);
                        }
                        t.setVisibility(View.GONE);
                    }
                    if(is_fr){

                    }
                    else{
                        t.setText(home.getTitle_en());
                    }


                    image.setTag(home);
                    image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DinamicImageView image=(DinamicImageView)v;
                            Home p=(Home) image.getTag();
                            Intent  intent =new Intent(getContext(),Home_Activity.class);
                            intent.putExtra("id",p.getId());
                            if(is_fr==true)
                            {
                                intent.putExtra("title",p.getTitle());
                                intent.putExtra("title_en",p.getTitle_en());
                                intent.putExtra("description",p.getDescription());
                                intent.putExtra("description_en",p.getDescription_en());


                            }
                            else
                            {
                                intent.putExtra("title",p.getTitle_en());
                                intent.putExtra("description",p.getDescription_en());
                                intent.putExtra("title_en",p.getTitle_en());
                                intent.putExtra("description_en",p.getDescription_en());
                            }
                            intent.putExtra("url",p.getUrl());
                            intent.putExtra("url2",p.getUrl2());
                            intent.putExtra("url3",p.getUrl3());
                            intent.putExtra("preview_image",p.getPreview_image());
                            intent.putExtra("link_video",p.getLink_video());
                            intent.putExtra("content_type",p.getContent_type());

                            startActivity(intent);
                        }
                    });
                    flipper.addView(v);
                }
            }//fin boucle for


        }
    }

    @Override
    public void onResposeSuccessImage(String url, String date) {
        final Palette.PaletteAsyncListener paletteAsyncListener=new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                Palette.Swatch swatch=palette.getLightVibrantSwatch();



            }
        };

        Glide.with(this).load(url).asBitmap().signature(new StringSignature(url+date)).into(new SimpleTarget<Bitmap>(400,400) {
            @Override
            public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                btm=bitmap;
                conteneur.setBackground(new BitmapDrawable(HomeFragment.this.getResources(),bitmap));

            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                conteneur.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.launch_color));
            }
        });
    }



    @Override
    public void onresponseFailAndPrintErrorResponseImage() {

    }

    public int getDisplayContentHeight() {
        DisplayMetrics dm = getResources().getDisplayMetrics();
        int screen_w = dm.widthPixels;
        int screen_h = dm.heightPixels;
        StringBuilder sb=new StringBuilder();

        int resId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resId > 0) {
            screen_h -= getResources().getDimensionPixelSize(resId);
        }
        TypedValue typedValue = new TypedValue();
        if(getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, typedValue, true)){
            screen_h -= getResources().getDimensionPixelSize(typedValue.resourceId);
        }
        return screen_h;
    }
    @Override
    public void onResposeSuccess() {



        listP=netWork.getListhome();
                if(listP!=null && listP.size()>0)
                {

                    relation=new HomeDatabaseRelation(1);
                     databaseRelationBox.removeAll();
                     databaseRelationBox.attach(relation);
                     relation.list.addAll(listP);

                     databaseRelationBox.put(relation);

                  setUpSlide();
                }

    }
    private void setUpSlide(){


        for(int i=0;i<listP.size();i++){

            Home home=listP.get(i);
            if(home.getContent_type().equals("spot")){


                View v=getLayoutInflater().inflate(R.layout.main_flipper_video_item,null);
                final DinamicImageView image=(DinamicImageView) v.findViewById(R.id.image);
                TextView t=(TextView)v.findViewById(R.id.text);
                t.setVisibility(View.VISIBLE);
                final  float scale=getResources().getDisplayMetrics().density;
                Target  target=new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                        image.setImageBitmap(bitmap);
                        targets.remove(this);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                };
                targets.add(target);
                if(getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE){

                    if(is_small){
                        Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-60).into(target);
                    }
                    else if(is_medium)
                    {
                        Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(target);
                    }
                    else if(is_large){
                        Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(target);
                    }



                }
                else{
                    if(is_small){
                        Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels,(int)(230*scale+0.5f)+15).into(target);
                    }
                    else if(is_medium){
                        Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(target);
                    }
                    else if(is_large){
                        Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(target);
                    }
                    t.setVisibility(View.GONE);
                }
                if(is_fr){
                    t.setText(home.getTitle());
                }
                else{
                    t.setText(home.getTitle_en());
                }

                image.setTag(home);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DinamicImageView image=(DinamicImageView)v;
                        Home p=(Home) image.getTag();
                        Intent  intent =new Intent(getContext(),SpotActivity.class);
                        intent.putExtra("id",p.getId());
                        if(is_fr==true)
                        {
                            intent.putExtra("title",p.getTitle());
                            intent.putExtra("title_en",p.getTitle_en());
                            intent.putExtra("description",p.getDescription());
                            intent.putExtra("description_en",p.getDescription_en());


                        }
                        else
                        {
                            intent.putExtra("title",p.getTitle_en());
                            intent.putExtra("description",p.getDescription_en());
                            intent.putExtra("title_en",p.getTitle_en());
                            intent.putExtra("description_en",p.getDescription_en());
                        }
                        intent.putExtra("url",p.getPreview_image());
                        intent.putExtra("url2",p.getUrl2());
                        intent.putExtra("url3",p.getUrl3());
                        intent.putExtra("preview_image",p.getPreview_image());
                        intent.putExtra("link_video",p.getLink_video());
                        intent.putExtra("content_type",p.getContent_type());

                        startActivity(intent);
                    }
                });
                flipper.addView(v);

            }//if spot
            else{
                View v=getLayoutInflater().inflate(R.layout.main_flipper_item_layout,null);
                final DinamicImageView image=(DinamicImageView) v.findViewById(R.id.image);
                TextView t=(TextView)v.findViewById(R.id.text);
                t.setVisibility(View.VISIBLE);
                final  float scale=getResources().getDisplayMetrics().density;
               Target   target=new Target() {
                   @Override
                   public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                       image.setImageBitmap(bitmap);
                       targets.remove(this);
                   }

                   @Override
                   public void onBitmapFailed(Drawable errorDrawable) {

                   }

                   @Override
                   public void onPrepareLoad(Drawable placeHolderDrawable) {

                   }
               } ;
               targets.add(target);
                if(getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE){

                    if(is_small){
                        Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-60).into(target);
                    }
                    else if(is_medium)
                    {
                        Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(target);
                    }
                    else if(is_large){
                        Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(target);
                    }



                }
                else{
                    if(is_small){
                        Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels,(int)(230*scale+0.5f)+15).into(target);
                    }
                    else if(is_medium){
                        Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(target);
                    }
                    else if(is_large){
                        Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(target);
                    }
                    t.setVisibility(View.GONE);
                }

                if(is_fr){
                    t.setText(home.getTitle());
                }
                else{
                    t.setText(home.getTitle_en());
                }

                image.setTag(home);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DinamicImageView image=(DinamicImageView)v;
                        Home p=(Home) image.getTag();
                        Intent  intent =new Intent(getContext(),Home_Activity.class);
                        intent.putExtra("id",p.getId());
                        if(is_fr==true)
                        {
                            intent.putExtra("title",p.getTitle());
                            intent.putExtra("title_en",p.getTitle_en());
                            intent.putExtra("description",p.getDescription());
                            intent.putExtra("description_en",p.getDescription_en());


                        }
                        else
                        {
                            intent.putExtra("title",p.getTitle_en());
                            intent.putExtra("description",p.getDescription_en());
                            intent.putExtra("title_en",p.getTitle_en());
                            intent.putExtra("description_en",p.getDescription_en());
                        }
                        intent.putExtra("url",p.getUrl());
                        intent.putExtra("url2",p.getUrl2());
                        intent.putExtra("url3",p.getUrl3());
                        intent.putExtra("preview_image",p.getPreview_image());
                        intent.putExtra("link_video",p.getLink_video());
                        intent.putExtra("content_type",p.getContent_type());

                        startActivity(intent);
                    }
                });
                flipper.addView(v);
            }
        }//fin boucle for
        flipperLayout.setVisibility(View.VISIBLE);
        bar.setVisibility(View.GONE);
    }




    @Override
    public void onresponseFailAndPrintErrorResponse() {

       //home_data

        relation=databaseRelationBox.get(1);
          if(relation!=null){

            listP=relation.list;

          }

        if (listP != null && listP.size() > 0) {
            setUpSlide();

        }
        else {

        }

    }


    private boolean isConnect()
    {
        ConnectivityManager cm=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        if(info!=null&& info.isConnected() )
            return true;


        return false;
    }






    @Override
    public Loader<List<PushType>> onCreateLoader(int id, Bundle args) {
      loader=new PushTypeAsyncLoader(getContext());

        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<PushType>> loader, List<PushType> data) {


        switch (loader.getId()){
            case PushHandler.LOADER_MAIN_ACTIVITY_ID:

                 if(data!=null){

                     if(data.size()>0){

                         Predicate<PushType> predicate1=new Predicate<PushType>() {
                             @Override
                             public boolean apply(@Nullable PushType input) {
                                 return input.getType().equalsIgnoreCase(PushHandler.type);
                             }
                         };
                         List<PushType> types1=Lists.newArrayList(Iterables.filter(data,predicate1));
                         if(types1!=null && types1.size()>0){

                             PushType f=types1.get(0);
                             if(f!=null && f.getCount()>0)
                             {

                                 ntTextview1.setText(""+f.getCount());
                                 ntTextview1.setVisibility(View.VISIBLE);
                             }

                         }
                         else {

                             ntTextview1.setText("");
                             ntTextview1.setVisibility(View.GONE);
                         }

                         Predicate<PushType> predicate2=new Predicate<PushType>() {
                             @Override
                             public boolean apply(@Nullable PushType input) {
                                 return input.getType().equalsIgnoreCase(PushHandler.type2);
                             }
                         };
                         List<PushType> types2=Lists.newArrayList(Iterables.filter(data,predicate2));
                         if(types2!=null && types2.size()>0){

                             PushType f=types2.get(0);
                             if(f!=null && f.getCount()>0)
                             {

                                 ntTextview2.setText(""+f.getCount());
                                 ntTextview2.setVisibility(View.VISIBLE);
                             }

                         }
                         else {

                             ntTextview2.setText("");
                             ntTextview2.setVisibility(View.GONE);
                         }


                         //3
                         Predicate<PushType> predicate3=new Predicate<PushType>() {
                             @Override
                             public boolean apply(@Nullable PushType input) {
                                 return input.getType().equalsIgnoreCase(PushHandler.type3);
                             }
                         };
                         List<PushType> types3=Lists.newArrayList(Iterables.filter(data,predicate3));
                         if(types3!=null && types3.size()>0){
                             PushType f=types3.get(0);
                             if(f!=null && f.getCount()>0)
                             {

                                 ntTextview3.setText(""+f.getCount());
                                 ntTextview3.setVisibility(View.VISIBLE);
                             }

                         }
                         else {

                             ntTextview3.setText("");
                             ntTextview3.setVisibility(View.GONE);
                         }

                         //4
                         Predicate<PushType> predicate4=new Predicate<PushType>() {
                             @Override
                             public boolean apply(@Nullable PushType input) {
                                 return input.getType().equalsIgnoreCase(PushHandler.type4);
                             }
                         };
                         List<PushType> types4=Lists.newArrayList(Iterables.filter(data,predicate4));
                         if(types4!=null && types4.size()>0){
                             PushType f=types4.get(0);
                             if(f!=null && f.getCount()>0)
                             {

                                 ntTextview4.setText(""+f.getCount());
                                 ntTextview4.setVisibility(View.VISIBLE);
                             }

                         }
                         else {

                             ntTextview4.setText("");
                             ntTextview4.setVisibility(View.GONE);
                         }
                         //5
                         Predicate<PushType> predicate5=new Predicate<PushType>() {
                             @Override
                             public boolean apply(@Nullable PushType input) {
                                 return input.getType().equalsIgnoreCase(PushHandler.type5);
                             }
                         };
                         List<PushType> types5=Lists.newArrayList(Iterables.filter(data,predicate5));
                         if(types5!=null && types5.size()>0){
                             PushType f=types5.get(0);
                             if(f!=null && f.getCount()>0)
                             {

                                 ntTextview5.setText(""+f.getCount());
                                 ntTextview5.setVisibility(View.VISIBLE);
                             }

                         }
                         else {

                             ntTextview5.setText("");
                             ntTextview5.setVisibility(View.GONE);
                         }
                         //6
                         Predicate<PushType> predicate6=new Predicate<PushType>() {
                             @Override
                             public boolean apply(@Nullable PushType input) {
                                 return input.getType().equalsIgnoreCase(PushHandler.type6);
                             }
                         };
                         List<PushType> types6=Lists.newArrayList(Iterables.filter(data,predicate6));
                         if(types6!=null && types6.size()>0){
                             PushType f=types6.get(0);
                             if(f!=null && f.getCount()>0)
                             {

                                 ntTextview6.setText(""+f.getCount());
                                 ntTextview6.setVisibility(View.VISIBLE);
                             }

                         }
                         else {

                             ntTextview6.setText("");
                             ntTextview6.setVisibility(View.GONE);
                         }

                         //7
                         Predicate<PushType> predicate7=new Predicate<PushType>() {
                             @Override
                             public boolean apply(@Nullable PushType input) {
                                 return input.getType().equalsIgnoreCase(PushHandler.type7);
                             }
                         };
                         List<PushType> types7=Lists.newArrayList(Iterables.filter(data,predicate7));
                         if(types7!=null && types7.size()>0){
                             PushType f=types7.get(0);
                             if(f!=null && f.getCount()>0)
                             {

                                 ntTextview7.setText(""+f.getCount());
                                 ntTextview7.setVisibility(View.VISIBLE);
                             }

                         }
                         else {

                             ntTextview7.setText("");
                             ntTextview7.setVisibility(View.GONE);
                         }

                         //8
                         Predicate<PushType> predicate8=new Predicate<PushType>() {
                             @Override
                             public boolean apply(@Nullable PushType input) {
                                 return input.getType().equalsIgnoreCase(PushHandler.type8);
                             }
                         };
                         List<PushType> types8=Lists.newArrayList(Iterables.filter(data,predicate8));
                         if(types8!=null && types8.size()>0){
                             PushType f=types8.get(0);
                             if(f!=null && f.getCount()>0)
                             {

                                 ntTextview8.setText(""+f.getCount());
                                 ntTextview8.setVisibility(View.VISIBLE);
                             }

                         }
                         else {

                             ntTextview8.setText("");
                             ntTextview8.setVisibility(View.GONE);
                         }
                         Predicate<PushType> predicate9=new Predicate<PushType>() {
                             @Override
                             public boolean apply(@Nullable PushType input) {
                                 return input.getType().equalsIgnoreCase(PushHandler.type9);
                             }
                         };
                         List<PushType> types9=Lists.newArrayList(Iterables.filter(data,predicate9));
                         if(types9!=null && types9.size()>0){
                             PushType f=types9.get(0);
                             if(f!=null && f.getCount()>0)
                             {

                                 ntTextview9.setText(""+f.getCount());
                                 ntTextview9.setVisibility(View.VISIBLE);
                             }

                         }
                         else {

                             ntTextview9.setText("");
                             ntTextview9.setVisibility(View.GONE);
                         }






                     }
                     else
                     {
                        ntTextview1.setText("");
                        ntTextview1.setVisibility(View.GONE);
                         ntTextview2.setText("");
                         ntTextview2.setVisibility(View.GONE);
                         ntTextview3.setText("");
                         ntTextview3.setVisibility(View.GONE);
                         ntTextview4.setText("");
                         ntTextview4.setVisibility(View.GONE);
                         ntTextview5.setText("");
                         ntTextview5.setVisibility(View.GONE);
                         ntTextview6.setText("");
                         ntTextview6.setVisibility(View.GONE);
                         ntTextview7.setText("");
                         ntTextview7.setVisibility(View.GONE);
                         ntTextview8.setText("");
                         ntTextview8.setVisibility(View.GONE);
                         ntTextview9.setText("");
                         ntTextview9.setVisibility(View.GONE);
                     }
                 }
                return;
        }
    }

    @Override
    public void onLoaderReset(Loader<List<PushType>> loader) {

    }



    @Override
    public void itemClicked(int position) {



    }


    @Override
    public void onStart() {
        super.onStart();
        flipper.setAutoStart(true);
        flipper.setFlipInterval(4000);
        flipper.setInAnimation(getContext(),R.anim.main_slide_in_left);
        flipper.setOutAnimation(getContext(),R.anim.main_slide_out_left);
        flipper.startFlipping();

        final Gson gson=new Gson();
        final String json=preferences.getString("MES_MENUS",null);
        if(json != null) {
            MenuVitrine[] menuVitrines = gson.fromJson(json, MenuVitrine[].class);
            final List<MenuVitrine> datas = Arrays.asList(menuVitrines);

            List<MenuVitrine> data=new ArrayList<>();
            for (int k=0;k<datas.size();k++)
            {
                MenuVitrine menuVitrine = null;
                if (datas.get(k).getType().equalsIgnoreCase(PushHandler.type))
                {
                    menuVitrine =new MenuVitrine(datas.get(k).getId(),datas.get(k).getLabel(),datas.get(k).getImg(),datas.get(k).getType(),ntTextview1.getText().toString());
                }

                if (datas.get(k).getType().equalsIgnoreCase(PushHandler.type2))
                {
                    menuVitrine =new MenuVitrine(datas.get(k).getId(),datas.get(k).getLabel(),datas.get(k).getImg(),datas.get(k).getType(),ntTextview2.getText().toString());
                }

                if (datas.get(k).getType().equalsIgnoreCase(PushHandler.type3))
                {
                    menuVitrine =new MenuVitrine(datas.get(k).getId(),datas.get(k).getLabel(),datas.get(k).getImg(),datas.get(k).getType(),ntTextview3.getText().toString());
                }

                if (datas.get(k).getType().equalsIgnoreCase(PushHandler.type4))
                {
                    menuVitrine =new MenuVitrine(datas.get(k).getId(),datas.get(k).getLabel(),datas.get(k).getImg(),datas.get(k).getType(),ntTextview4.getText().toString());
                }


                if (datas.get(k).getType().equalsIgnoreCase(PushHandler.type5))
                {
                    menuVitrine =new MenuVitrine(datas.get(k).getId(),datas.get(k).getLabel(),datas.get(k).getImg(),datas.get(k).getType(),ntTextview5.getText().toString());
                }

                if (datas.get(k).getType().equalsIgnoreCase(PushHandler.type6))
                {
                    menuVitrine =new MenuVitrine(datas.get(k).getId(),datas.get(k).getLabel(),datas.get(k).getImg(),datas.get(k).getType(),ntTextview6.getText().toString());
                }


                if (datas.get(k).getType().equalsIgnoreCase(PushHandler.type7))
                {
                    menuVitrine =new MenuVitrine(datas.get(k).getId(),datas.get(k).getLabel(),datas.get(k).getImg(),datas.get(k).getType(),ntTextview7.getText().toString());
                }


                if (datas.get(k).getType().equalsIgnoreCase(PushHandler.type8))
                {
                    menuVitrine =new MenuVitrine(datas.get(k).getId(),datas.get(k).getLabel(),datas.get(k).getImg(),datas.get(k).getType(),ntTextview8.getText().toString());
                }


                if (datas.get(k).getType().equalsIgnoreCase(PushHandler.type9))
                {
                    menuVitrine =new MenuVitrine(datas.get(k).getId(),datas.get(k).getLabel(),datas.get(k).getImg(),datas.get(k).getType(),ntTextview9.getText().toString());
                }

                data.add(menuVitrine);


            }


            GridLayoutManager layoutManager = null;


            if (datas.size()==1)
            {
                LinearLayoutManager manager=new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
                recycler.setLayoutManager(manager);
                adapterMenuRecycler=new AdapterMenuRecycler(getContext(),data);
                recycler.setAdapter(adapterMenuRecycler);
            }else{
                if (datas.size() == 2){

                    layoutManager  = new GridLayoutManager(getContext(), 2);
                }

                if (datas.size() >= 3 && datas.size()<=5){
                    layoutManager  = new GridLayoutManager(getContext(), 3);
                    layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int i) {
                            if (i==0 || i==1 || i==2)
                            {

                                return 1;
                            }

                            if (i==3)
                            {
                                if (datas.size() == 5)
                                {
                                    return 2;
                                }else{
                                    return 3;
                                }

                            }

                            if (i==4)
                            {

                                return 1;
                            }

                            if (i==5)
                            {

                                return 1;
                            }



                            Log.e("TOTAL", String.valueOf(i));
                            Log.e("TOTA", String.valueOf(datas.size()));
                            throw new IllegalStateException("Error interne");
                        }
                    });
                }

                if (datas.size() == 6)
                {
                    layoutManager  = new GridLayoutManager(getContext(), 3);
                }

                if (datas.size() >= 7 && datas.size()<=8)
                {
                    layoutManager  = new GridLayoutManager(getContext(), 3);
                    layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int i) {

                            if (i==0 || i==1 || i==2 || i==3 || i==4 || i==5)
                            {

                                return 1;
                            }



                            if (i==6)
                            {
                                if (datas.size() == 8)
                                {
                                    return 2;
                                }else{
                                    return 3;
                                }
                            }

                            if (i==7)
                            {
                                return 1;
                            }
                            throw new IllegalStateException("Error interne");
                        }
                    });


                }

                if (datas.size() == 9)
                {
                    layoutManager  = new GridLayoutManager(getContext(), 3);
                }


                recycler.setLayoutManager(layoutManager);

                adapterMenuRecycler=new AdapterMenuRecycler(getContext(),data);
                recycler.setAdapter(adapterMenuRecycler);
            }



        }






    }




    @Override
    public void onResume() {
        super.onResume();

       getActivity().registerReceiver(receiverApp,filter);

    }


    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiverApp);
    }



}
