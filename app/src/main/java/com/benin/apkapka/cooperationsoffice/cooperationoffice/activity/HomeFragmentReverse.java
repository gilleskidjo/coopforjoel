package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.AccueilInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetBackImage;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetworkRequestReverseHomeFragment;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.HelperActivity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Home;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.HomeClickInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.HomeDatabaseRelation;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.LanguageNotifier;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MenuVitrine;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ProduitDatabaseRelation;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragmentReverse extends Fragment implements AccueilInterface,HomeClickInterface,FragmentForNetworkRequestReverseHomeFragment.HandlerNetWorkRequestResponse,LanguageNotifier.LanguageNotifierInterface,FragmentForNetworkRequestReverseHomeFragment.HandlerNetWorkRequetFetchProductAtHome {

    ProgressBar bar,bar2;
    View flipperLayout;
    ViewFlipper flipper;
    ImageView left,right;
    FragmentForNetworkRequestReverseHomeFragment netWork;
    List<Home> listP=new ArrayList<Home>();
    FragmentManager fm;
    HomeDatabaseRelation relation;


    private boolean is_fr=false;
    private String  [] pays;
    Animation anim;

    boolean is_small,is_medium,is_large,is_landescape;
    RecyclerView  recyclerView;

    Bitmap btm;
    GestureDetectorCompat detectorCompat;
    IntentFilter filter;
    MyReceiverApp receiverApp;
    MenuItem menuItemCount;
    private int contHeight;
    FragmentForNetBackImage fNetImage;
    List<Target> targets=new ArrayList<>();
    List<Produit> produits=new ArrayList<>();
    MyAdapteurHomeProduct myAdapteurHomeProduct;
    Box<ProduitDatabaseRelation> produitDatabaseRelationBox;
    ProduitDatabaseRelation produitDatabaseRelation;
    public class MyReceiverApp extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(Intent.ACTION_SCREEN_OFF)){

                if(flipper!=null){
                    flipper.stopFlipping();
                }
            }
            else if(intent.getAction().equals(Intent.ACTION_SCREEN_ON)){

                if(flipper!=null){
                    flipper.setAutoStart(false);
                    flipper.startFlipping();
                    flipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.main_slide_in_left));
                    flipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.main_slide_out_left));
                }
            }


        }
    }
    Box<HomeDatabaseRelation> databaseRelationBox;
    Query<HomeDatabaseRelation> databaseRelationQuery;

    @android.support.annotation.Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @android.support.annotation.Nullable ViewGroup container, @android.support.annotation.Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.activity_home_reverse,container,false);;
        anim= AnimationUtils.loadAnimation(getContext(),R.anim.home_imageview_anim);

        flipper=(ViewFlipper)v.findViewById(R.id.flipper);
        bar=(ProgressBar)v.findViewById(R.id.progress);
        bar2=(ProgressBar)v.findViewById(R.id.progress2);
        recyclerView=(RecyclerView)v.findViewById(R.id.home_recylcerview);
        recyclerView.setVisibility(View.GONE);
        bar2.setVisibility(View.VISIBLE);
        myAdapteurHomeProduct=new MyAdapteurHomeProduct(produits,this,getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(myAdapteurHomeProduct);
        BoxStore boxStore=((MyApplication)getActivity().getApplicationContext()).getBoxStore();
        databaseRelationBox=boxStore.boxFor(HomeDatabaseRelation.class);
        relation=new HomeDatabaseRelation(1);
        produitDatabaseRelationBox=boxStore.boxFor(ProduitDatabaseRelation.class);
        produitDatabaseRelation=new ProduitDatabaseRelation(30);
        setUpFlipper(v);

        is_fr=getResources().getBoolean(R.bool.lang_fr);
        is_small=getResources().getBoolean(R.bool.is_small);
        is_medium=getResources().getBoolean(R.bool.is_medium);
        is_large=getResources().getBoolean(R.bool.is_large);
        is_landescape=getResources().getBoolean(R.bool.is_landscape);
        pays=getResources().getStringArray(R.array.pays_code);





        fm=getFragmentManager();
        fm=getFragmentManager();
        contHeight=getDisplayContentHeight();
        FragmentManager fm=getFragmentManager();
        netWork=(FragmentForNetworkRequestReverseHomeFragment) fm.findFragmentByTag(FragmentForNetworkRequestReverseHomeFragment.TAG);
        if(netWork==null)
        {

            netWork=new FragmentForNetworkRequestReverseHomeFragment();
            fm.beginTransaction().add(netWork,FragmentForNetworkRequestReverseHomeFragment.TAG).commit();
            netWork.setInterface(this);
            netWork.setInterfaceHomeProduct(this);

            if(isConnect()){
                SessionManager sessionManager=new SessionManager(getActivity());
                User user=sessionManager.getUser(getActivity());
                String zone="";
                zone=user.getPays();
                if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
                    zone="france";
                }
                netWork.doRequestHome(zone);
                netWork.doRequestFectProductAtHome(zone);

            }
            else{

                relation=databaseRelationBox.get(1);
                if(relation!=null){
                    listP=relation.list;
                }
                //home_data
                if (listP != null && listP.size() > 0) {
                    setUpSlide();
                }
                else{

                }
                ProduitDatabaseRelation relation=produitDatabaseRelationBox.get(30);
                if(relation!=null){
                    produits=relation.list;
                }
                if(produits!=null && produits.size()>0){
                    myAdapteurHomeProduct=new MyAdapteurHomeProduct(produits,this,getActivity());
                    recyclerView.setAdapter(myAdapteurHomeProduct);

                }
                bar2.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

            }
        }
        else
        {

            netWork.setInterface(this);
            netWork.setInterfaceHomeProduct(this);
            if(isConnect()){

                listP=netWork.getListhome();
                produits=netWork.getListproduit();
                if(listP!=null && listP.size()>0)
                {
                    setUpSlide();
                }
                else
                {

                    //home_data
                    if (listP != null && listP.size() > 0) {
                        setUpSlide();
                    }

                }
                if(produits!=null && produits.size()>0){

                }

                produits=netWork.getListproduit();
                if(produits!=null && produits.size()>0){
                    myAdapteurHomeProduct=new MyAdapteurHomeProduct(produits,this,getActivity());
                    recyclerView.setAdapter(myAdapteurHomeProduct);
                }
                bar2.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

            }
            else {


                relation=databaseRelationBox.get(1);
                if(relation!=null){
                    listP=relation.list;


                }
                //home_data
                if (listP != null && listP.size() > 0) {

                    setUpSlide();
                }
                else{

                }
                ProduitDatabaseRelation relation=produitDatabaseRelationBox.get(30);
                if(relation!=null){
                    produits=relation.list;
                }
                if(produits!=null && produits.size()>0){
                    myAdapteurHomeProduct=new MyAdapteurHomeProduct(produits,this,getActivity());
                    recyclerView.setAdapter(myAdapteurHomeProduct);

                }
                bar2.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }

        }
        receiverApp=new MyReceiverApp();
        filter=new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);


        return v;
    }//fin onCreateView

    @Override
    public void onFetchProductAtHomeFail() {

        ProduitDatabaseRelation relation=produitDatabaseRelationBox.get(30);
        if(relation!=null){
            produits=relation.list;
        }
        if(produits!=null && produits.size()>0){
            myAdapteurHomeProduct=new MyAdapteurHomeProduct(produits,this,getActivity());
            recyclerView.setAdapter(myAdapteurHomeProduct);

        }
        bar2.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

    }

    @Override
    public void onFetchProductAtHomeSuccess() {


        produits=netWork.getListproduit();
        if(produits!=null && produits.size()>0){
            ProduitDatabaseRelation relation=new ProduitDatabaseRelation(30);
            produitDatabaseRelationBox.attach(relation);
            produitDatabaseRelationBox.remove(relation);
            relation.list.addAll(produits);
            produitDatabaseRelationBox.put(relation);
            myAdapteurHomeProduct=new MyAdapteurHomeProduct(produits,this,getActivity());
            recyclerView.setAdapter(myAdapteurHomeProduct);

        }
        bar2.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

    }

    @Override
    public void onProductClick(int position) {
        Produit f=produits.get(position);
        HelperActivity.getInstance().setProduit(f);
        Intent intent=new Intent();
        Produit   p=f;
        String type=p.getType();
        if(type.equals("actualite"))
        {
            intent=new Intent(getContext(),Une_actualite_activity.class);
        }
        else if(type.equals("boutique")){
            intent =new Intent(getContext(),Un_produit_activity.class);
        }
        else if(type.equals("emploi")){
            intent =new Intent(getContext(),Un_emploi_activity.class);
        }
        else if(type.equals("competence")){
            intent =new Intent(getContext(),Une_competence_annuaire_activity.class);
        }
        else if(type.equals("bourse")){
            intent =new Intent(getContext(),Une_bourse_activity.class);
        }
        else if(type.equals("annuaire")){

            intent =new Intent(getContext(),UnAnnuaire.class);
        }
        else if(type.equals("service")){
            intent =new Intent(getContext(),Un_service_activity.class);
        }
        else if(type.equals("location")){
            intent =new Intent(getContext(),Une_location_activity.class);
        }
        else if(type.equals("partenariat")){
            intent =new Intent(getContext(),Un_partenariat.class);
        }
        else if(type.equals("projet")){
            intent =new Intent(getContext(),Un_projet_activity.class);
        }
        intent.putExtra("id",p.getId());

        intent.putExtra("lien_catalogue",p.getLien_catalogue());
        intent.putExtra("lien_description",p.getLien_description());
        intent.putExtra("choixf",p.getChoixf());
        intent.putExtra("url",p.getUrl());
        intent.putExtra("url2",p.getUrl2());
        intent.putExtra("url3",p.getUrl3());
        intent.putExtra("type",p.getType());
        intent.putExtra("famille",p.getFamille());
        intent.putExtra("pays",p.getPays());
        intent.putExtra("category",p.getCategory());
        intent.putExtra("type2",0);
        intent.putExtra("date",p.getDate());
        intent.putExtra("from_co_direct","oui");
        intent.putExtra("famille_en",p.getFamille_en());
        intent.putExtra("lien_fichier",p.getLien_fichier());
        startActivity(intent);
    }

    private void setUpFlipper(View v){
        flipperLayout=v.findViewById(R.id.flipper_layout);
        flipperLayout.setVisibility(View.GONE);
        bar.setVisibility(View.VISIBLE);

        final Handler handler=new Handler(Looper.getMainLooper());
        final Runnable runnable=new Runnable() {
            @Override
            public void run() {
                if(flipper!=null){
                    flipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.main_slide_in_left));
                    flipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.main_slide_out_left));
                    flipper.setAutoStart(true);
                    flipper.startFlipping();
                }
            }
        };
        left=(ImageView)v.findViewById(R.id.main_left);
        right=(ImageView)v.findViewById(R.id.main_right);
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(runnable);
                flipper.setAutoStart(false);
                flipper.stopFlipping();
                flipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.main_slide_in_left));
                flipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.main_slide_out_left));
                flipper.showNext();
                flipper.setAutoStart(true);
                flipper.startFlipping();
            }
        });

        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(runnable);
                flipper.setAutoStart(false);
                flipper.stopFlipping();
                flipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.main_slide_in_right));
                flipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.main_slide_out_right));
                flipper.showPrevious();

                handler.postDelayed(runnable, 4000);

            }
        });


    }


    @Override
    public void languageChange(Resources resources, String langague) {

        updateUiView();
    }

    private  void updateUiView(){
        flipper.removeAllViews();
        listP=netWork.getListhome();
        if(listP!=null)
        {
            for(int i=0;i<listP.size();i++){
                Home home=listP.get(i);
                if(home.getContent_type().equals("spot")){


                    View v=getLayoutInflater().inflate(R.layout.main_flipper_video_item,null);
                    DinamicImageView image=(DinamicImageView) v.findViewById(R.id.image);
                    TextView t=(TextView)v.findViewById(R.id.text);
                    t.setVisibility(View.VISIBLE);
                    final  float scale=getResources().getDisplayMetrics().density;
                    if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE){

                        if(is_small){
                            Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-60).into(image);
                        }
                        else if(is_medium)
                        {
                            Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(image);
                        }
                        else if(is_large){
                            Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(image);
                        }



                    }
                    else{
                        if(is_small){
                            Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels,(int)(230*scale+0.5f)+15).into(image);
                        }
                        else if(is_medium){
                            Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(image);
                        }
                        else if(is_large){
                            Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(image);
                        }
                        t.setVisibility(View.GONE);
                    }

                    if(is_fr){
                        t.setText(home.getTitle());
                    }
                    else{
                        t.setText(home.getTitle_en());
                    }
                    image.setTag(home);
                    image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DinamicImageView image=(DinamicImageView)v;
                            Home p=(Home) image.getTag();
                            Intent  intent =new Intent(getContext(),SpotActivity.class);
                            intent.putExtra("id",p.getId());
                            if(is_fr==true)
                            {
                                intent.putExtra("title",p.getTitle());
                                intent.putExtra("title_en",p.getTitle_en());
                                intent.putExtra("description",p.getDescription());
                                intent.putExtra("description_en",p.getDescription_en());


                            }
                            else
                            {
                                intent.putExtra("title",p.getTitle_en());
                                intent.putExtra("description",p.getDescription_en());
                                intent.putExtra("title_en",p.getTitle_en());
                                intent.putExtra("description_en",p.getDescription_en());
                            }
                            intent.putExtra("url",p.getPreview_image());
                            intent.putExtra("url2",p.getUrl2());
                            intent.putExtra("url3",p.getUrl3());
                            intent.putExtra("preview_image",p.getPreview_image());
                            intent.putExtra("link_video",p.getLink_video());
                            intent.putExtra("content_type",p.getContent_type());

                            startActivity(intent);
                        }
                    });
                    flipper.addView(v);

                }//if spot
                else{
                    View v=getLayoutInflater().inflate(R.layout.main_flipper_item_layout,null);
                    DinamicImageView image=(DinamicImageView) v.findViewById(R.id.image);
                    TextView t=(TextView)v.findViewById(R.id.text);
                    t.setVisibility(View.VISIBLE);
                    final  float scale=getResources().getDisplayMetrics().density;
                    if(getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE){

                        if(is_small){
                            Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-60).into(image);
                        }
                        else if(is_medium)
                        {
                            Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(image);
                        }
                        else if(is_large){
                            Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(image);
                        }


                    }
                    else{
                        if(is_small){
                            Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels,(int)(230*scale+0.5f)+15).into(image);
                        }
                        else if(is_medium){
                            Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(image);
                        }
                        else if(is_large){
                            Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(image);
                        }
                        t.setVisibility(View.GONE);
                    }
                    if(is_fr){
                        t.setText(home.getTitle());
                    }
                    else{
                        t.setText(home.getTitle_en());
                    }


                    image.setTag(home);
                    image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DinamicImageView image=(DinamicImageView)v;
                            Home p=(Home) image.getTag();
                            Intent  intent =new Intent(getContext(),Home_Activity.class);
                            intent.putExtra("id",p.getId());
                            if(is_fr==true)
                            {
                                intent.putExtra("title",p.getTitle());
                                intent.putExtra("title_en",p.getTitle_en());
                                intent.putExtra("description",p.getDescription());
                                intent.putExtra("description_en",p.getDescription_en());


                            }
                            else
                            {
                                intent.putExtra("title",p.getTitle_en());
                                intent.putExtra("description",p.getDescription_en());
                                intent.putExtra("title_en",p.getTitle_en());
                                intent.putExtra("description_en",p.getDescription_en());
                            }
                            intent.putExtra("url",p.getUrl());
                            intent.putExtra("url2",p.getUrl2());
                            intent.putExtra("url3",p.getUrl3());
                            intent.putExtra("preview_image",p.getPreview_image());
                            intent.putExtra("link_video",p.getLink_video());
                            intent.putExtra("content_type",p.getContent_type());

                            startActivity(intent);
                        }
                    });
                    flipper.addView(v);
                }
            }//fin boucle for


        }
        flipper.setAutoStart(true);
        flipper.setFlipInterval(4000);
        flipper.setInAnimation(getContext(),R.anim.main_slide_in_left);
        flipper.setOutAnimation(getContext(),R.anim.main_slide_out_left);
    }

    public int getDisplayContentHeight() {
        DisplayMetrics dm = getResources().getDisplayMetrics();
        int screen_w = dm.widthPixels;
        int screen_h = dm.heightPixels;
        StringBuilder sb=new StringBuilder();

        int resId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resId > 0) {
            screen_h -= getResources().getDimensionPixelSize(resId);
        }
        TypedValue typedValue = new TypedValue();
        if(getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, typedValue, true)){
            screen_h -= getResources().getDimensionPixelSize(typedValue.resourceId);
        }
        return screen_h;
    }
    @Override
    public void onResposeSuccess() {
        listP=netWork.getListhome();
        if(listP!=null && listP.size()>0)
        {

            relation=new HomeDatabaseRelation(1);
            databaseRelationBox.removeAll();
            databaseRelationBox.attach(relation);
            relation.list.addAll(listP);

            databaseRelationBox.put(relation);

            setUpSlide();
        }

    }
    private void setUpSlide(){

        for(int i=0;i<listP.size();i++){

            Home home=listP.get(i);
            if(home.getContent_type().equals("spot")){


                View v=getLayoutInflater().inflate(R.layout.main_flipper_video_item,null);
                final DinamicImageView image=(DinamicImageView) v.findViewById(R.id.image);
                TextView t=(TextView)v.findViewById(R.id.text);
                t.setVisibility(View.VISIBLE);
                final  float scale=getResources().getDisplayMetrics().density;
                Target  target=new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                        image.setImageBitmap(bitmap);
                        targets.remove(this);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                };
                targets.add(target);
                if(getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE){

                    if(is_small){
                        Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-60).into(target);
                    }
                    else if(is_medium)
                    {
                        Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(target);
                    }
                    else if(is_large){
                        Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(target);
                    }



                }
                else{
                    if(is_small){
                        Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels,(int)(230*scale+0.5f)+15).into(target);
                    }
                    else if(is_medium){
                        Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(target);
                    }
                    else if(is_large){
                        Picasso.with(getContext()).load(home.getPreview_image()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(target);
                    }
                    t.setVisibility(View.GONE);
                }
                if(is_fr){
                    t.setText(home.getTitle());
                }
                else{
                    t.setText(home.getTitle_en());
                }

                image.setTag(home);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DinamicImageView image=(DinamicImageView)v;
                        Home p=(Home) image.getTag();
                        Intent  intent =new Intent(getContext(),SpotActivity.class);
                        intent.putExtra("id",p.getId());
                        if(is_fr==true)
                        {
                            intent.putExtra("title",p.getTitle());
                            intent.putExtra("title_en",p.getTitle_en());
                            intent.putExtra("description",p.getDescription());
                            intent.putExtra("description_en",p.getDescription_en());


                        }
                        else
                        {
                            intent.putExtra("title",p.getTitle_en());
                            intent.putExtra("description",p.getDescription_en());
                            intent.putExtra("title_en",p.getTitle_en());
                            intent.putExtra("description_en",p.getDescription_en());
                        }
                        intent.putExtra("url",p.getPreview_image());
                        intent.putExtra("url2",p.getUrl2());
                        intent.putExtra("url3",p.getUrl3());
                        intent.putExtra("preview_image",p.getPreview_image());
                        intent.putExtra("link_video",p.getLink_video());
                        intent.putExtra("content_type",p.getContent_type());

                        startActivity(intent);
                    }
                });
                flipper.addView(v);

            }//if spot
            else{
                View v=getLayoutInflater().inflate(R.layout.main_flipper_item_layout,null);
                final DinamicImageView image=(DinamicImageView) v.findViewById(R.id.image);
                TextView t=(TextView)v.findViewById(R.id.text);
                t.setVisibility(View.VISIBLE);
                final  float scale=getResources().getDisplayMetrics().density;
                Target   target=new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        image.setImageBitmap(bitmap);
                        targets.remove(this);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                } ;
                targets.add(target);
                if(getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE){

                    if(is_small){
                        Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-60).into(target);
                    }
                    else if(is_medium)
                    {
                        Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(target);
                    }
                    else if(is_large){
                        Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels/2,getDisplayContentHeight()-55).into(target);
                    }



                }
                else{
                    if(is_small){
                        Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels,(int)(230*scale+0.5f)+15).into(target);
                    }
                    else if(is_medium){
                        Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(target);
                    }
                    else if(is_large){
                        Picasso.with(getContext()).load(home.getUrl()).resize(getResources().getDisplayMetrics().widthPixels,(int)(390*scale+0.5f)).into(target);
                    }
                    t.setVisibility(View.GONE);
                }

                if(is_fr){
                    t.setText(home.getTitle());
                }
                else{
                    t.setText(home.getTitle_en());
                }

                image.setTag(home);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DinamicImageView image=(DinamicImageView)v;
                        Home p=(Home) image.getTag();
                        Intent  intent =new Intent(getContext(),Home_Activity.class);
                        intent.putExtra("id",p.getId());
                        if(is_fr==true)
                        {
                            intent.putExtra("title",p.getTitle());
                            intent.putExtra("title_en",p.getTitle_en());
                            intent.putExtra("description",p.getDescription());
                            intent.putExtra("description_en",p.getDescription_en());


                        }
                        else
                        {
                            intent.putExtra("title",p.getTitle_en());
                            intent.putExtra("description",p.getDescription_en());
                            intent.putExtra("title_en",p.getTitle_en());
                            intent.putExtra("description_en",p.getDescription_en());
                        }
                        intent.putExtra("url",p.getUrl());
                        intent.putExtra("url2",p.getUrl2());
                        intent.putExtra("url3",p.getUrl3());
                        intent.putExtra("preview_image",p.getPreview_image());
                        intent.putExtra("link_video",p.getLink_video());
                        intent.putExtra("content_type",p.getContent_type());

                        startActivity(intent);
                    }
                });
                flipper.addView(v);
            }
        }//fin boucle for
        flipperLayout.setVisibility(View.VISIBLE);
        bar.setVisibility(View.GONE);
    }





    @Override
    public void onresponseFailAndPrintErrorResponse() {

        //home_data

        relation=databaseRelationBox.get(1);
        if(relation!=null){

            listP=relation.list;

        }

        if (listP != null && listP.size() > 0) {
            setUpSlide();

        }
        else {

        }

    }


    private boolean isConnect()
    {
        ConnectivityManager cm=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        if(info!=null&& info.isConnected() )
            return true;


        return false;
    }










    @Override
    public void itemClicked(int position) {


    }


    @Override
    public void onStart() {
        super.onStart();
        flipper.setAutoStart(true);
        flipper.setFlipInterval(4000);
        flipper.setInAnimation(getContext(),R.anim.main_slide_in_left);
        flipper.setOutAnimation(getContext(),R.anim.main_slide_out_left);
        flipper.startFlipping();







    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().registerReceiver(receiverApp,filter);

    }


    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiverApp);
    }
    class  MyAdapteurHomeProduct extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

        List<Produit> produitList;
        HomeClickInterface accueilInterface;
        Context c;
        Produit ar;
        SharedPreferences preferences;
        SharedPreferences.Editor editor;

        public MyAdapteurHomeProduct(List<Produit> produitList, HomeClickInterface accueilInterface, Context c) {

            this.produitList = produitList;
            this.accueilInterface = accueilInterface;
            this.c = c;
            preferences=getContext().getSharedPreferences("les_options",MODE_PRIVATE);
            editor=preferences.edit();


        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v1= LayoutInflater.from(parent.getContext()).inflate(R.layout.actualite_item_layout,parent,false);
            ListViewHolder holder1=new ListViewHolder(v1);
            return holder1;
        }


        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            final Gson gson2=new Gson();

            final String json=preferences.getString("MES_MENUS",null);
            List<MenuVitrine> data=null;
            if(json != null) {
                MenuVitrine[] menuVitrines = gson2.fromJson(json, MenuVitrine[].class);
                data = Arrays.asList(menuVitrines);
                ArrayList<String> str = new ArrayList<>();
                for (int i = 0; i < data.size(); i++) {
                    str.add(data.get(i).getType());
                }

                if (!str.isEmpty()) {
                    if (str.contains(produitList.get(position).getType())) {
                        ar=produitList.get(position);
                        ListViewHolder holder2=(ListViewHolder)holder;
                        setUpListViewHolder(holder2,produitList.get(position));
                    }
                }
            }


        }



        public void setUpListViewHolder(ListViewHolder holder, Produit article)
        {

            if(article.getNb_vue_famille()!=null && !article.getNb_vue_famille().trim().equalsIgnoreCase(""))
            {
                holder.txtCountFamille.setText(article.getNb_vue_famille());
            }
            else{
                holder.txtCountFamille.setText("0");
            }

            holder.txtFamille.setText(article.getFamille().trim().toUpperCase());
            holder.tvCount.setText("");
            holder.tvCount.setVisibility(View.GONE);
            String des="";
            //holder.tvDate.setText(MydateUtil.getDate(article.getDate(),is_fr));
            //

            holder.title.setText(article.getTitle().toUpperCase().trim());
            des=article.getDescription().trim();
            holder.tvDate.setText(article.getExtend_proprety1().trim());
            holder.tvDate.setVisibility(View.VISIBLE);


            if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
            {
                if(des.length()>50)
                {
                    des=des.substring(0,50)+"...";

                }

            }
            else
            {
                if(des.length()>110)
                {
                    des=des.substring(0,110)+"...";
                }
            }


            holder.description.setText(des);

            final  float scale=getResources().getDisplayMetrics().density;
            if(is_small)
            {
                Picasso.with(c).load(article.getUrl()).resize((int)(120*scale+0.5f),(int)(100*scale+0.5f)).into(holder.image);
            }
            else if(is_medium)
            {
                Picasso.with(c).load(article.getUrl()).resize((int)(140*scale+0.5f),(int)(120*scale+0.5f)).into(holder.image);
            }
            else if (is_large)
            {
                Picasso.with(c).load(article.getUrl()).resize((int)(150*scale+0.5f),(int)(130*scale+0.5f)).into(holder.image);
            }

        }



        @Override
        public int getItemCount() {
            return produitList.size();
        }



        public  class ListViewHolder extends  RecyclerView.ViewHolder
        {
            TextView  title,tvCount,tvDate;
            TextView description;
            DinamicImageView  image;
            TextView  txtFamille;
            TextView txtCountFamille;

            public  ListViewHolder(View v)
            {
                super(v);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        accueilInterface.onProductClick(getAdapterPosition());
                    }
                });
                title=(TextView)v.findViewById(R.id.actualite_item_layout_title);
                tvDate=(TextView)v.findViewById(R.id.actualite_date);
                tvCount=(TextView)v.findViewById(R.id.actualite_Count);
                description=(TextView)v.findViewById(R.id.actualite_item_layout_description);
                image=(DinamicImageView) v.findViewById(R.id.actualite_item_layout_image);
                txtFamille=(TextView)v.findViewById(R.id.famille_name);
                txtCountFamille=(TextView)v.findViewById(R.id.famille_count);
            }


        }


    }
}
