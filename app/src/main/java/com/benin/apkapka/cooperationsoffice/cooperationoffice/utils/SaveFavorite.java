package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class SaveFavorite {
    @Id
    public long id;
    public String saveFavorite;
}
