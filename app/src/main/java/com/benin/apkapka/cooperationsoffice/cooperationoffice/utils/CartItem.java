package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by joel on 04/11/2016.
 */
public class CartItem {

    String name;
    String price;

    public CartItem() {
    }

    public CartItem(String name, String price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }



}
