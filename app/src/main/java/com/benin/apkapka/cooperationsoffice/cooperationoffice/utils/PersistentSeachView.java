package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.lapism.searchview.SearchView;

public class PersistentSeachView extends SearchView {

    public PersistentSeachView(Context context) {
        super(context);
        init(context);
    }

    public PersistentSeachView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PersistentSeachView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public PersistentSeachView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        mRecyclerView.setLayoutTransition(null);
        mRecyclerView.setFocusableInTouchMode(true);
        mRecyclerView.getLayoutManager().setItemPrefetchEnabled(false);



        Drawable myDrawable;
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            myDrawable = context.getResources().getDrawable(R.drawable.baseline_arrow_back_white_24dp, context.getTheme());
        } else {
            myDrawable = context.getResources().getDrawable(R.drawable.baseline_arrow_back_white_24dp);
        }
        if(myDrawable!=null){
            this.mBackImageView.setImageDrawable(myDrawable);
        }
    }

}
