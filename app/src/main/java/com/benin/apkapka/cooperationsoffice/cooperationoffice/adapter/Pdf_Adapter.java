package com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joel on 30/09/2016.
 */
public class Pdf_Adapter  extends BaseAdapter {
    List<String> list=new ArrayList<String>();
    public  Pdf_Adapter(List<String> list)
    {
        this.list=list;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }
    class ViewHolder
    {
       TextView text;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder=null;
                if(convertView==null)
                {
                 convertView= LayoutInflater.from(parent.getContext()).inflate(R.layout.pdf_tem,parent,false);
                 holder=new ViewHolder();
                  holder.text=(TextView)convertView.findViewById(R.id.pdf_item_textview);
                  convertView.setTag(holder);

                }
                else
                {
                    ViewHolder holder1=(ViewHolder)convertView.getTag();
                }
            holder.text.setText(list.get(position));
        return convertView;
    }
}
