package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import java.math.BigDecimal;

/**
 * Created by joel on 23/09/2016.
 */
public class ProductSale {
   private String id;
   private String name;
   private String currecy;
   private BigDecimal price;

    public ProductSale() {
    }

    public ProductSale(String id, String name, String currecy, BigDecimal price) {
        this.id = id;
        this.name = name;
        this.currecy = currecy;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrecy() {
        return currecy;
    }

    public void setCurrecy(String currecy) {
        this.currecy = currecy;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
