package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.Context;
import android.os.SystemClock;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by joel on 20/12/2017.
 */

public class MyDateServeur {
    private Date currentDate;
    private long elapseRealtime;
    private static  MyDateServeur myDateServeur;
    SessionManager manager;
    public static MyDateServeur getInstance(){

        if(myDateServeur==null){
            myDateServeur=new MyDateServeur();
        }
        return  myDateServeur;
    }
    public  boolean isSyncDate(){
        return myDateServeur!=null;
    }
    public  void initSession(Context c){
        manager=new SessionManager(c);

    }


    public  void initDate(Date currentDate,Context c){
        elapseRealtime= SystemClock.elapsedRealtime();
        this.currentDate=currentDate;
        initSession(c);
        manager.serveurPutElapse(elapseRealtime);
         if(currentDate!=null){
             manager.serveurPutTime(currentDate.getTime());
         }
    }

    public  Date getCurrentDate(Context c){
        initSession(c);
         if(manager.serveurGetTime()!=0){
             currentDate=new Date(manager.serveurGetTime());
         }
        elapseRealtime=manager.serveurGetElapse();
        Date mdate=currentDate;
        if(mdate==null ){
            mdate= Calendar.getInstance(Locale.getDefault()).getTime();
        }
        else{
            mdate.setTime(mdate.getTime()+(SystemClock.elapsedRealtime()-elapseRealtime));
        }
        return  mdate;
    }

    public  boolean canRequest(Context c){
        SessionManager manager=new SessionManager(c);

        return  manager.serveurCanRequest();
    }
    public void setCanReque(boolean canRequeste,Context c){
        SessionManager manager=new SessionManager(c);
        manager.putServeurCanRequest(canRequeste);
    }

}
