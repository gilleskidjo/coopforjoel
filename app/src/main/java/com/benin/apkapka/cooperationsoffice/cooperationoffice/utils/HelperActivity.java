package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

/**
 * Created by joel on 30/11/2017.
 */
public class HelperActivity {

    Article article;
    Produit produit;
    Emploi emploi;
    Competence competence;
    Bourse bourse;
    Annuaire annuaire;
    Service service;
    Partenariat partenariat;
    Projet projet;

    public  static  HelperActivity helperActivity ;

    public static HelperActivity getInstance(){
        if(helperActivity==null){
            helperActivity=new HelperActivity();
        }

        return helperActivity;
    }

    public void setArticle(Article article) {
        this.article=null;
        this.article = article;
    }

    public void setProduit(Produit produit) {
        this.produit=null;
        this.produit = produit;
    }

    public void setEmploi(Emploi emploi) {
        this.emploi=null;
        this.emploi = emploi;
    }

    public void setCompetence(Competence competence) {
        this.competence=null;
        this.competence = competence;
    }

    public void setBourse(Bourse bourse) {
        this.bourse=null;
        this.bourse = bourse;
    }

    public void setAnnuaire(Annuaire annuaire) {
        this.annuaire=null;
        this.annuaire = annuaire;
    }

    public void setService(Service service) {
        this.service=null;
        this.service = service;
    }

    public void setPartenariat(Partenariat partenariat) {
        this.partenariat=null;
        this.partenariat = partenariat;
    }

    public void setProjet(Projet projet) {
        this.projet=null;
        this.projet = projet;
    }

    public Article getArticle() {
        return article;
    }

    public Produit getProduit() {
        return produit;
    }

    public Emploi getEmploi() {
        return emploi;
    }

    public Competence getCompetence() {
        return competence;
    }

    public Bourse getBourse() {
        return bourse;
    }

    public Annuaire getAnnuaire() {
        return annuaire;
    }

    public Service getService() {
        return service;
    }

    public Partenariat getPartenariat() {
        return partenariat;
    }

    public Projet getProjet() {
        return projet;
    }
}
