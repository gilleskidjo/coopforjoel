package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

public class ChatMessage {
    public long id;
    long fromUser;
    long toUser;
    String type="";
    String message="";
    String photo="";
    boolean isSend=false;
    String sendPrenom="";
    String  create_at="";
    boolean isFetchFromServer=false;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFromUser() {
        return fromUser;
    }

    public void setFromUser(long fromUser) {
        this.fromUser = fromUser;
    }

    public long getToUser() {
        return toUser;
    }

    public void setToUser(long toUser) {
        this.toUser = toUser;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public boolean isSend() {
        return isSend;
    }

    public void setSend(boolean send) {
        isSend = send;
    }

    public String getSendPrenom() {
        return sendPrenom;
    }

    public void setSendPrenom(String sendPrenom) {
        this.sendPrenom = sendPrenom;
    }

    public boolean isFetchFromServer() {
        return isFetchFromServer;
    }

    public void setFetchFromServer(boolean fetchFromServer) {
        isFetchFromServer = fetchFromServer;
    }
}
