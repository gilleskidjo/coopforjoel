package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Bonus {
    @Id
    public long id;
    public String bonus;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }
}
