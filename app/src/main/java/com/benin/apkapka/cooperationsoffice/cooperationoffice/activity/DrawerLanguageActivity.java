package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.LanguageChaneEvent;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.LanguageNotifier;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.LocaleHelper;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by joel on 19/11/2017.
 */
public class DrawerLanguageActivity extends BaseLanguageActivity {

    TextView tv_fr,tv_en;
    ActionBar actionBar;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_language_layput);
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        tv_fr=(TextView)findViewById(R.id.drawer_language_fr);
        tv_en=(TextView)findViewById(R.id.drawer_language_en);
        tv_en.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            updateUi("en");
            }
        });

        tv_fr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUi("fr");
            }
        });



    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }

    private void updateUi(String  languaguecode){
        Context context= LocaleHelper.setLocale(this,languaguecode);
        Resources resources=context.getResources();
        tv_fr.setText(resources.getString(R.string.drawer_language_francais));
        tv_en.setText(resources.getString(R.string.drawer_language_anglais));
        actionBar.setTitle(resources.getString(R.string.drawer_activity_language));

        LanguageNotifier.getInstance().alertNotification(resources,languaguecode);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       switch (item.getItemId()){
           case android.R.id.home:
                    finish();
               return true;
       }
        return super.onOptionsItemSelected(item);
    }
}
