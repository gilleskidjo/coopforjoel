package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import java.util.List;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToMany;


/**
 * Created by joel on 05/12/2017.
 */
@Entity
public class HomeDatabaseRelation  {

    @Id(assignable = true)
    long id;
    String name;
    @Backlink
    public ToMany<Home> list;

    public HomeDatabaseRelation() {
    }

    public HomeDatabaseRelation(long id) {
        this.id = id;
    }


    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }


    public String getName() {
        return name;
    }
}
