package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.app.Application;
import android.os.Bundle;
import android.renderscript.ScriptC;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Annuaire;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Article;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Bourse;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Competence;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Emploi;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Home;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ListDataHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MySingleton;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Partenariat;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Projet;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Service;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by joel on 15/02/2016.
 */
public class FragmentForNetworkRequest  extends Fragment {
    public List<Home> getListhome() {
        return listhome;
    }

    public void setListhome(List<Home> listhome) {
        this.listhome = listhome;
    }

    public  static  final  String TAG="com.cooperation.office.fragemnr.for_network";
    private boolean isRequestDo=false;
    private boolean isMessage=false;
    private String categorie;
    Home home;
    List<Home> listhome=new ArrayList<Home>();

    public Home getHome() {
        return home;
    }

    public void setHome(Home home) {
        this.home = home;
    }

    List<Produit> listproduit=new ArrayList<>();
    List<Article>  listeArticle=new ArrayList<>();
    List<Annuaire> listAnnuaire=new ArrayList<>();
    List<Bourse> listeBourse=new ArrayList<>();
    List<Competence> listCompetence=new ArrayList<>();
    List<Emploi>listEmploi=new ArrayList<>();
    List<Projet> listProjet=new ArrayList<>();
    List<Service> listeService=new ArrayList<>();
    List<Partenariat> listPartenariat=new ArrayList<>();
    Produit  produit;
    Article article;
    Annuaire annuaire;
    Bourse bourse;
    Competence competence;
    Emploi emploi;
    Projet  projet;
    Service  service;
    Partenariat partenariat;

    long current_id;
    RequestQueue mRequestQueue;
    private String sms="";
    HandlerNetWorkRequestResponse handleResponse;
    HandlerNetWorkRequetFetchProductAtHome fetchProductAtHome;
    boolean isEmpty;

    public boolean isEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }

    public interface  HandlerNetWorkRequestResponse
    {

      public void onResposeSuccess();

      public  void onresponseFailAndPrintErrorResponse();

    }

    public interface  HandlerNetWorkRequetFetchProductAtHome
    {

        public void onFetchProductAtHomeSuccess();

        public  void onFetchProductAtHomeFail();

    }


    public void setArticle(Article article) {
        this.article = article;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public void setCompetence(Competence competence) {
        this.competence = competence;
    }

    public boolean isRequestDo() {
        return isRequestDo;
    }

    public boolean isMessage() {
        return isMessage;
    }

    public void setIsMessage(boolean isMessage) {
        this.isMessage = isMessage;
    }

    public String getSms() {
        return sms;
    }


    public  void setInterface(HandlerNetWorkRequestResponse handleResponse)
    {
      this.handleResponse=handleResponse;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);


    }


public void  doRequestProduit_By_Category(String category, String pays, final String famille, String lang, String type, final int type2,List<Produit> produits,long id,String zone,int page)
{

    listproduit=produits;
    current_id=id;
    Retrofit retrofit=new Retrofit.Builder()
              .baseUrl(Config.GET_BASE_URL_RETROFIT)
               .addConverterFactory(ScalarsConverterFactory.create())
               .build();

    MyInterface2 service=retrofit.create(MyInterface2.class);

    Call<String> call=service.getAllProduct_By_Category(category,pays,famille,lang,type,zone,page);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                    String message = "";
                    if (response != null) {
                        if (response.isSuccessful()) {
                            try {
                                String rep = response.body();
                                JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                                boolean error = o.getBoolean("error");
                                if (error) {

                                    if(getActivity()!=null)
                                    {
                                        listproduit= ListDataHandler.getList_Category_Produit(MyApplication.context,type2,famille);
                                        if(listproduit==null  || listproduit.size()<1) {
                                            message=o.getString("message");
                                            sms=message;
                                            isMessage=true;
                                            if(handleResponse!=null)
                                            {
                                                handleResponse.onresponseFailAndPrintErrorResponse();
                                            }
                                        }
                                        else
                                        {
                                            if(handleResponse!=null)
                                            {
                                                handleResponse.onResposeSuccess();
                                            }
                                        }
                                    }

                                } else {
                                    boolean isVide = o.getBoolean("vide");
                                    if (isVide) {


                                        message = o.getString("message");
                                        sms = message;
                                        isMessage = true;
                                        if (handleResponse != null) {
                                            handleResponse.onresponseFailAndPrintErrorResponse();
                                        }

                                    } else {
                                        listproduit = ParseJson.parseProduitFom(o,listproduit,current_id);
                                        if (handleResponse != null ) {
                                            handleResponse.onResposeSuccess();
                                        }
                                    }

                                }

                            } catch (Exception e) {
                                if(getActivity()!=null)
                                {
                                    listproduit= ListDataHandler.getList_Category_Produit(MyApplication.context,type2,famille);
                                    if(listproduit==null || listproduit.size()<1) {
                                        message=getResources().getString(R.string.error_frag_netw_parse);
                                        sms=message;
                                        isMessage=true;
                                        if(handleResponse!=null)
                                        {
                                            handleResponse.onresponseFailAndPrintErrorResponse();
                                        }
                                    }
                                    else
                                    {
                                        if(handleResponse!=null)
                                        {
                                            handleResponse.onResposeSuccess();
                                        }
                                    }
                                }
                            }

                        }//fin is success,
                        else {
                            if(getActivity()!=null)
                            {
                                listproduit= ListDataHandler.getList_Category_Produit(MyApplication.context,type2,famille);
                                if(listproduit==null || listproduit.size()<1) {
                                    message=getResources().getString(R.string.error_frag_netw_parse);
                                    sms=message;
                                    isMessage=true;
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onresponseFailAndPrintErrorResponse();
                                    }
                                }
                                else
                                {
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onResposeSuccess();
                                    }
                                }
                            }

                        }//fin else is success

                    }

               isRequestDo=true;
                }

                @Override
                public void onFailure(Call<String> call1,Throwable t) {
                   if(getActivity()!=null)
                   {
                       sms="";

                       listproduit= ListDataHandler.getList_Category_Produit(MyApplication.context,type2,famille);
                       if(listproduit==null || listproduit.size()<1) {

                           sms=getResources().getString(R.string.error_frag_netw_error);
                           isMessage=true;
                           if(handleResponse!=null)
                           {
                               handleResponse.onresponseFailAndPrintErrorResponse();
                           }

                       }
                       else
                       {
                           if(handleResponse!=null)
                           {
                               handleResponse.onResposeSuccess();
                           }
                       }
                       isRequestDo = true;
                   }
                }
            });






    }





    public  void doRequestLocationAutommobile_Group_By_Famile(String category,String pays ,String lang,String type,String zone,int page)
    {
        isEmpty=false;
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        MyInterface2 service=retrofit.create(MyInterface2.class);
        Call<String> call=service.getAllProduct_Group_By_Category(category,pays,lang,type,zone,page);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message="";
                if (response != null) {
                    if (response.isSuccessful()) {
                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if(error)
                            {

                              if(getActivity()!=null)
                              {
                                  message=o.getString("message");
                                  sms=message;
                                  isMessage=true;
                                  if(handleResponse!=null)
                                  {
                                      handleResponse.onresponseFailAndPrintErrorResponse();
                                  }
                              }



                            }
                            else
                            {
                                boolean isVide=o.getBoolean("vide");
                                if(isVide)
                                {
                                    isEmpty=true;
                                    message=o.getString("message");
                                    sms=message;
                                    isMessage=true;
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onresponseFailAndPrintErrorResponse();
                                    }


                                }
                                else
                                {
                                    listproduit= ParseJson.parseProduit(o,getContext());
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onResposeSuccess();
                                    }
                                }

                            }
                        } catch (Exception e) {

                                if(getActivity()!=null)
                                {

                                    message=getResources().getString(R.string.error_frag_netw_parse);
                                    sms=message;
                                    isMessage=true;
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onresponseFailAndPrintErrorResponse();
                                    }
                                }



                        }

                    }//fin isSucces
                    else {
                       if(getActivity()!=null)
                       {
                           message=getResources().getString(R.string.error_frag_netw_parse);
                           sms=message;
                           isMessage=true;
                           if(handleResponse!=null)
                           {
                               handleResponse.onresponseFailAndPrintErrorResponse();
                           }
                       }


                    }//fin else isSuuces

                }//fin null response
                isRequestDo=true;
            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {
                        if(getActivity()!=null)
                        {
                            sms="";
                            sms=getResources().getString(R.string.error_frag_netw_error);
                            isMessage=true;
                            if(handleResponse!=null)
                            {
                                handleResponse.onresponseFailAndPrintErrorResponse();
                            }

                            isRequestDo=true;
                        }
            }
        });

    }





    public  void doRequestHome(String zone)
    {
        Retrofit  retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        MyInterface service=retrofit.create(MyInterface.class);
        Call<String> call=service.getListHome(zone);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message="";
                if (response != null) {
                    if (response.isSuccessful()) {
                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if(error)
                            {

                                if (getActivity()!=null)
                                {

                                    message=o.getString("message");
                                    sms=message;
                                    isMessage=true;
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onresponseFailAndPrintErrorResponse();
                                    }

                                }

                            }
                            else
                            {

                                boolean isVide=o.getBoolean("vide");
                                if(isVide)
                                {


                                    message=o.getString("message");
                                    sms=message;
                                    isMessage=true;
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onresponseFailAndPrintErrorResponse();
                                    }


                                }
                                else
                                {

                                    listhome=ParseJson.parseHome(o);
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onResposeSuccess();
                                    }
                                }

                            }
                        } catch (Exception e) {

                            if(getActivity()!=null)
                            {

                                message=getResources().getString(R.string.error_frag_netw_parse);
                                sms=message;
                                isMessage=true;
                                if(handleResponse!=null)
                                {
                                    handleResponse.onresponseFailAndPrintErrorResponse();
                                }
                            }

                        }

                    }//fin isSucces
                    else {

                        if (getActivity()!=null)
                        {
                            message=getResources().getString(R.string.error_frag_netw_parse);
                            sms=message;
                            isMessage=true;
                            if(handleResponse!=null)
                            {
                                handleResponse.onresponseFailAndPrintErrorResponse();
                            }
                        }


                    }//fin else isSuuces

                }//fin null response
                isRequestDo=true;
            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

                if(getActivity()!=null)
                {
                    sms=getResources().getString(R.string.error_frag_netw_error);
                    isMessage=true;
                    if(handleResponse!=null)
                    {
                        handleResponse.onresponseFailAndPrintErrorResponse();
                    }
                    isRequestDo=true;
                }
            }
        });

    }



    public Partenariat getPartenariat() {
        return partenariat;
    }

    public void setPartenariat(Partenariat partenariat) {
        this.partenariat = partenariat;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Article getArticle() {
        return article;
    }


    public void setAnnuaire(Annuaire annuaire) {
        this.annuaire = annuaire;
    }

    public Annuaire getAnnuaire() {
        return annuaire;
    }

    public Bourse getBourse() {
        return bourse;
    }

    public void setBourse(Bourse bourse) {
        this.bourse = bourse;
    }

    public List<Bourse> getListeBourse() {
        return listeBourse;
    }

    public List<Partenariat> getListPartenariat() {
        return listPartenariat;
    }



    public Competence getCompetence() {
        return competence;
    }



    public Emploi getEmploi() {
        return emploi;
    }

    public void setEmploi(Emploi emploi) {
        this.emploi = emploi;
    }

    public Projet getProjet() {
        return projet;
    }

    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }




    public List<Competence> getListCompetence() {
        return listCompetence;
    }



    public List<Emploi> getListEmploi() {
        return listEmploi;
    }


    public List<Projet> getListProjet() {
        return listProjet;
    }


    public List<Service> getListeService() {
        return listeService;
    }




    public List<Produit> getListproduit() {
        return listproduit;
    }


    public List<Article> getListeArticle() {
        return listeArticle;
    }


    public List<Annuaire> getListAnnuaire() {
        return listAnnuaire;
    }



    @Override
    public void onDestroy() {
        super.onDestroy();


    }
}
