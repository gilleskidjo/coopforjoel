package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.LocaleHelper;

public class BaseLanguageActivity   extends AppCompatActivity
{


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }
}
