package com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

/**
 * Created by joel on 18/02/2016.
 */
public class GalleryAdateur   extends BaseAdapter {

    List<String> paths;
    public   GalleryAdateur(List<String> listpath)
    {
       paths=listpath;
    }

    @Override
    public int getCount() {
        return paths.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return paths.get(position);
    }

    class ViewHolder
    {
        ImageView image;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder  holder;

                if(convertView==null)
                {
                  convertView= LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_item_layout,null);
                 holder=new ViewHolder();
                 holder.image=(ImageView)convertView.findViewById(R.id.gallery_image);
                 convertView.setTag(holder);

                }
                else
                {
                   holder=(ViewHolder)convertView.getTag();
                }


        Glide.with(parent.getContext()).load(paths.get(position)).asBitmap().into(holder.image);

        return convertView;
    }
}
