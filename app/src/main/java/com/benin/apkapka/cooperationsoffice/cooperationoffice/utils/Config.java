package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

/**
 * Created by joel on 26/01/2016.
 */
public class Config {
    public static final String    URL_ARTICLE="https://cooperationsoffice.net/cooperationv2/get_list_article.php";
    public static final String    URL_PRODUIT="https://cooperationsoffice.net/cooperationv2/get_list_produit.php";
    public static final String URL_HOME="https://cooperationsoffice.net/cooperation/images/cooperationfondimage.png";
    public static final String URL_PLAY_STORE="https://play.google.com/store/apps/details?id=com.benin.apkapka.cooperationsoffice.cooperationoffice";
    public  static  final  String URL_OFFRE_EMPLOI="https://cooperationsoffice.net/cooperationv2/get_list_emploi.php";
    public  static  final  String URL_COMPETENCE="https://cooperationsoffice.net/cooperationv2/get_list_competence.php";
    public  static  final  String URL_BOURSE="https://cooperationsoffice.net/cooperationv2/get_list_bourse.php";
    public  static final  String URL_ANNUAIRE="https://cooperationsoffice.net/cooperationv2/get_list_annuaire1.php";
    public  static  final  String URL_SERVICE_PROJET_PARTENARIAT="https://cooperationsoffice.net/cooperationv2/get_list_service_projet_partenariat.php";
    public  static  final  String URL_CURRENT_DATE="https://cooperationsoffice.net/cooperationv2/manageandroiddate/get_current_date.php";
    public  static  final  String URL_BACK_IMAGE="https://cooperationsoffice.net/cooperationv2/manageandroiddate/get_background_image.php";

    public static final String GET_BASE_URL_RETROFIT="https://cooperationsoffice.net";


    //animation sur les recyclerview
    public static void setAnimation(View viewToAnimate, int position, int lastPosition, Context c) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(c,android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }


}
