package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Favorite {
    @Id
    public long id;
    public String mfavorite;
}
