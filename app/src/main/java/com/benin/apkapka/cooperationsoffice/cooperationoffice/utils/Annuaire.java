package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import java.math.BigDecimal;

/**
 * Created by joel on 28/01/2016.
 */
public class Annuaire {
    private long id;
    private String title;
    private String title_en ;
    private String description;
    private String description_en;
    private String lien_description;
    private String lien_catalogue;
    private String extend_proprety1;
    private String extend_proprety1_en;
    private String extend_proprety2;
    private String extend_proprety2_en;
    private String nb_share;
    private String nb_read_catalogue;
    private String  choixf;
    private String famille;
    private String pays;
    private String category;

    private String price;
    private String price_en;
    private BigDecimal nb_price;
    private BigDecimal nb_price_en;
    private String text_reduction;
    private String text_reduction_en;
    private String reduction;


    private String url;
    private String url2;
    private String url3;
    private long count;
    private String date;
    private String famille_en;
    private String lien_fichier;
    public Annuaire() {
    }

    public Annuaire(long id, String title, String title_en, String description, String description_en, String lien_description, String lien_catalogue, String extend_proprety1, String extend_proprety1_en, String extend_proprety2, String extend_proprety2_en, String nb_share, String nb_read_catalogue, String choixf, String famille, String pays, String category, String price, String price_en, BigDecimal nb_price, BigDecimal nb_price_en, String text_reduction, String text_reduction_en, String reduction, String url, String url2, String url3, long count, String date, String famille_en, String lien_fichier) {
        this.id = id;
        this.title = title;
        this.title_en = title_en;
        this.description = description;
        this.description_en = description_en;
        this.lien_description = lien_description;
        this.lien_catalogue = lien_catalogue;
        this.extend_proprety1 = extend_proprety1;
        this.extend_proprety1_en = extend_proprety1_en;
        this.extend_proprety2 = extend_proprety2;
        this.extend_proprety2_en = extend_proprety2_en;
        this.nb_share = nb_share;
        this.nb_read_catalogue = nb_read_catalogue;
        this.choixf = choixf;
        this.famille = famille;
        this.pays = pays;
        this.category = category;
        this.price = price;
        this.price_en = price_en;
        this.nb_price = nb_price;
        this.nb_price_en = nb_price_en;
        this.text_reduction = text_reduction;
        this.text_reduction_en = text_reduction_en;
        this.reduction = reduction;
        this.url = url;
        this.url2 = url2;
        this.url3 = url3;
        this.count = count;
        this.date = date;
        this.famille_en = famille_en;
        this.lien_fichier = lien_fichier;
    }

    public String getLien_fichier() {
        return lien_fichier;
    }

    public void setLien_fichier(String lien_fichier) {
        this.lien_fichier = lien_fichier;
    }

    public String getFamille_en() {
        return famille_en;
    }

    public void setFamille_en(String famille_en) {
        this.famille_en = famille_en;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_en() {
        return title_en;
    }

    public void setTitle_en(String title_en) {
        this.title_en = title_en;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription_en() {
        return description_en;
    }

    public void setDescription_en(String description_en) {
        this.description_en = description_en;
    }

    public String getLien_description() {
        return lien_description;
    }

    public void setLien_description(String lien_description) {
        this.lien_description = lien_description;
    }

    public String getLien_catalogue() {
        return lien_catalogue;
    }

    public void setLien_catalogue(String lien_catalogue) {
        this.lien_catalogue = lien_catalogue;
    }

    public String getExtend_proprety1() {
        return extend_proprety1;
    }

    public void setExtend_proprety1(String extend_proprety1) {
        this.extend_proprety1 = extend_proprety1;
    }

    public String getExtend_proprety1_en() {
        return extend_proprety1_en;
    }

    public void setExtend_proprety1_en(String extend_proprety1_en) {
        this.extend_proprety1_en = extend_proprety1_en;
    }

    public String getExtend_proprety2() {
        return extend_proprety2;
    }

    public void setExtend_proprety2(String extend_proprety2) {
        this.extend_proprety2 = extend_proprety2;
    }

    public String getExtend_proprety2_en() {
        return extend_proprety2_en;
    }

    public void setExtend_proprety2_en(String extend_proprety2_en) {
        this.extend_proprety2_en = extend_proprety2_en;
    }

    public String getNb_share() {
        return nb_share;
    }

    public void setNb_share(String nb_share) {
        this.nb_share = nb_share;
    }

    public String getNb_read_catalogue() {
        return nb_read_catalogue;
    }

    public void setNb_read_catalogue(String nb_read_catalogue) {
        this.nb_read_catalogue = nb_read_catalogue;
    }

    public String getChoixf() {
        return choixf;
    }

    public void setChoixf(String choixf) {
        this.choixf = choixf;
    }

    public String getFamille() {
        return famille;
    }

    public void setFamille(String famille) {
        this.famille = famille;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_en() {
        return price_en;
    }

    public void setPrice_en(String price_en) {
        this.price_en = price_en;
    }

    public BigDecimal getNb_price() {
        return nb_price;
    }

    public void setNb_price(BigDecimal nb_price) {
        this.nb_price = nb_price;
    }

    public BigDecimal getNb_price_en() {
        return nb_price_en;
    }

    public void setNb_price_en(BigDecimal nb_price_en) {
        this.nb_price_en = nb_price_en;
    }

    public String getText_reduction() {
        return text_reduction;
    }

    public void setText_reduction(String text_reduction) {
        this.text_reduction = text_reduction;
    }

    public String getText_reduction_en() {
        return text_reduction_en;
    }

    public void setText_reduction_en(String text_reduction_en) {
        this.text_reduction_en = text_reduction_en;
    }

    public String getReduction() {
        return reduction;
    }

    public void setReduction(String reduction) {
        this.reduction = reduction;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl2() {
        return url2;
    }

    public void setUrl2(String url2) {
        this.url2 = url2;
    }

    public String getUrl3() {
        return url3;
    }

    public void setUrl3(String url3) {
        this.url3 = url3;
    }
}
