package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushNotification;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushNotificationInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushNotificationRelation;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dmax.dialog.SpotsDialog;
import io.objectbox.Box;
import io.objectbox.query.Query;
import io.objectbox.query.QueryBuilder;

public class TransparentActivity extends AppCompatActivity implements PushNotificationInterface{


    List<PushNotification> pushNotificationList=new ArrayList<>();
    RecyclerView recyclerView;
    MyPushAdapteur myPushAdapteur;
    ProgressBar progressBar;
    AlertDialog alertDialog;
    boolean  is_fr;
    Box<PushNotificationRelation> pushNotificationRelationBox;
    ShareDialog shareDialog;
    CallbackManager callbackManager;
    AlertDialog mAlertDialog1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manageFacebook();
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        LayoutInflater inflater=getLayoutInflater();
        View view=inflater.inflate(R.layout.main_dialog_push_layout,null);
        recyclerView=(RecyclerView)view.findViewById(R.id.main_dilog_list);
        progressBar=(ProgressBar) view.findViewById(R.id.main_dialog_progress);
        builder.setView(view);
        builder.setPositiveButton(is_fr ? "Fermer" : "Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(alertDialog!=null){
                    alertDialog.dismiss();
                    TransparentActivity.this.finish();
                }
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
           TransparentActivity.this.finish();
            }
        });

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        myPushAdapteur=new MyPushAdapteur(pushNotificationList,this);
        recyclerView.setAdapter(myPushAdapteur);
        alertDialog=builder.show();
        getAllPushNotification();


    }

    private void manageFacebook()
    {
        callbackManager=CallbackManager.Factory.create();
        shareDialog=new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {


                Toast.makeText(TransparentActivity.this,getResources().getString(R.string.error_frag_netw_error),Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void getAllPushNotification(){
        Gson gson=new Gson();
        pushNotificationRelationBox=((MyApplication)getApplicationContext()).getBoxStore().boxFor(PushNotificationRelation.class);
        QueryBuilder<PushNotificationRelation> builder=pushNotificationRelationBox.query();
        Query<PushNotificationRelation> query=builder.build();
        PushNotificationRelation relation=query.findFirst();
         if(relation!=null){
             String relationData=relation.pushValue;
             if(relationData!=null && !relationData.equalsIgnoreCase("")){
                 Type type=new TypeToken<List<PushNotification>>(){}.getType();
                 pushNotificationList=gson.fromJson(relationData,type);
                 if(pushNotificationList!=null && pushNotificationList.size()>0){
                     myPushAdapteur.notifyDataSetChanged();
                     Collections.reverse(pushNotificationList);
                     myPushAdapteur=new MyPushAdapteur(pushNotificationList,this);
                     recyclerView.setAdapter(myPushAdapteur);
                     recyclerView.setVisibility(View.VISIBLE);
                     progressBar.setVisibility(View.GONE);
                 }
             }
         }
    }


    private void sharePush(PushNotification article)
    {
        SessionManager sessionManager=new SessionManager(this);
        final User user=sessionManager.getUser(this);

        Uri.Builder builder=Uri.parse(PushHandler.pushBaseUlr).buildUpon();

        if(article.getType().equalsIgnoreCase(PushHandler.type))
        {
            builder.appendPath(PushHandler.type);
        }
        else if(article.getType().equalsIgnoreCase(PushHandler.type2)){
            builder.appendPath(PushHandler.type2);
        }
        else if(article.getType().equalsIgnoreCase(PushHandler.type3)){
            builder.appendPath(PushHandler.type3);
        }
        else if(article.getType().equalsIgnoreCase(PushHandler.type4)){
            builder.appendPath(PushHandler.type4);
        }
        else if(article.getType().equalsIgnoreCase(PushHandler.type5)){
            builder.appendPath(PushHandler.type5);
        }
        else if(article.getType().equalsIgnoreCase(PushHandler.type6)){
            builder.appendPath(PushHandler.type6);
        }
        else if(article.getType().equalsIgnoreCase(PushHandler.type7)){
            builder.appendPath(PushHandler.type7);
        }
        else if(article.getType().equalsIgnoreCase(PushHandler.type8)){
            builder.appendPath(PushHandler.type8);
        }
        else if(article.getType().equalsIgnoreCase(PushHandler.type9)){
            builder.appendPath(PushHandler.type9);
        }
        builder.appendQueryParameter("id",""+article.getId());
        builder.appendQueryParameter("famille",article.getFamille());
        builder.appendQueryParameter("type",article.getType());
        builder.appendQueryParameter("category",article.getCategory());
        Uri uri=builder.build();

        final android.app.AlertDialog mAlertDialog=new SpotsDialog(TransparentActivity.this,R.style.style_spot_actualite);
        mAlertDialog.show();
        Task<ShortDynamicLink> shortDynamicLinkTask= FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(uri)
                .setDynamicLinkDomain("cooperations0ffices.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.benin.apkapka.cooperationsoffice.cooperationoffice")
                        .setFallbackUrl(Uri.parse("http://cooperationsoffice.net"))
                        .build())
                .setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder()
                        .setTitle(article.getTitle())
                        .setDescription(is_fr?"CLIQUEZ".toUpperCase()+" pour "+"VISITER".toUpperCase()+" et "+"PARTAGER".toUpperCase()+" notre publication ":"CLICK".toUpperCase()+" to "+"VISIT".toUpperCase()+" and "+"SHARE".toUpperCase()+" our publication")
                        .setImageUrl(Uri.parse(""+article.getUrl()))
                        .build())
                .buildShortDynamicLink()

                .addOnCompleteListener(TransparentActivity.this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        mAlertDialog.dismiss();
                        if(task.isSuccessful()){

                            Uri uri=task.getResult().getShortLink();
                            ShareLinkContent linkContent=new ShareLinkContent.Builder()
                                    .setContentUrl(uri)
                                    .build();
                            if(shareDialog.canShow(ShareLinkContent.class)){
                                shareDialog.show(linkContent);

                            }
                        }
                        else {

                            android.support.v7.app.AlertDialog.Builder mBuilder1=new android.support.v7.app.AlertDialog.Builder(TransparentActivity.this)
                                    .setMessage(is_fr?"Impossible de publier l'image":"Can not publish image")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if(mAlertDialog1!=null){
                                                mAlertDialog1.dismiss();
                                            }
                                        }
                                    });
                            mAlertDialog1=mBuilder1.show();
                        }

                    }
                });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    public void onPushItemClick(int position) {
      PushNotification notification=pushNotificationList.get(position);

      pushNotificationList.remove(notification);
      myPushAdapteur.notifyDataSetChanged();
        Gson gson=new Gson();
        Box<PushNotificationRelation> pushNotificationRelationBox=((MyApplication)getApplicationContext()).getBoxStore().boxFor(PushNotificationRelation.class);
        QueryBuilder<PushNotificationRelation> builder1=pushNotificationRelationBox.query();
        Query<PushNotificationRelation> query=builder1.build();
        PushNotificationRelation relation=query.findFirst();
        Type type_push=new TypeToken<List<PushNotification>>(){}.getType();
         if(relation!=null){
             List<PushNotification> list=new ArrayList<>(pushNotificationList);
             Collections.reverse(list);
             String pushvalue2=gson.toJson(list,type_push);
             relation.pushValue=pushvalue2;
             pushNotificationRelationBox.put(relation);
         }

        switch (notification.getViewHolder_type()){
            case 0:
                if(pushNotificationList.isEmpty()){
                    if(alertDialog!=null){
                        alertDialog.dismiss();
                    }
                    finish();
                }
             return;
            case 1:


                if(notification.getType().equalsIgnoreCase(PushHandler.type))
                {

                    Uri.Builder builder=Uri.parse(PushHandler.pushBaseUlr).buildUpon();
                     builder.appendPath(PushHandler.type);
                     builder.appendQueryParameter("id",""+notification.getId());
                     builder.appendQueryParameter("famille",notification.getFamille());
                     builder.appendQueryParameter("type",notification.getType());
                     builder.appendQueryParameter("category",notification.getCategory());
                    Uri uri=builder.build();
                    Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                    intent.putExtra("locale_uri","locale_uri");
                    startActivity(intent);



                }
                else if(notification.getType().equalsIgnoreCase(PushHandler.type2)){
                    Uri.Builder builder=Uri.parse(PushHandler.pushBaseUlr).buildUpon();
                    builder.appendPath(PushHandler.type2);
                    builder.appendQueryParameter("id",""+notification.getId());
                    builder.appendQueryParameter("famille",notification.getFamille());
                    builder.appendQueryParameter("type",notification.getType());
                    builder.appendQueryParameter("category",notification.getCategory());
                    Uri uri=builder.build();
                    Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                    intent.putExtra("locale_uri","locale_uri");
                    startActivity(intent);
                }
                else if(notification.getType().equalsIgnoreCase(PushHandler.type3)){
                    Uri.Builder builder=Uri.parse(PushHandler.pushBaseUlr).buildUpon();
                    builder.appendPath(PushHandler.type3);
                    builder.appendQueryParameter("id",""+notification.getId());
                    builder.appendQueryParameter("famille",notification.getFamille());
                    builder.appendQueryParameter("type",notification.getType());
                    builder.appendQueryParameter("category",notification.getCategory());
                    Uri uri=builder.build();
                    Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                    intent.putExtra("locale_uri","locale_uri");
                    startActivity(intent);
                }
                else if(notification.getType().equalsIgnoreCase(PushHandler.type4)){
                    Uri.Builder builder=Uri.parse(PushHandler.pushBaseUlr).buildUpon();
                    builder.appendPath(PushHandler.type4);
                    builder.appendQueryParameter("id",""+notification.getId());
                    builder.appendQueryParameter("famille",notification.getFamille());
                    builder.appendQueryParameter("type",notification.getType());
                    builder.appendQueryParameter("category",notification.getCategory());
                    Uri uri=builder.build();
                    Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                    intent.putExtra("locale_uri","locale_uri");
                    startActivity(intent);
                }
                else if(notification.getType().equalsIgnoreCase(PushHandler.type5)){
                    Uri.Builder builder=Uri.parse(PushHandler.pushBaseUlr).buildUpon();
                    builder.appendPath(PushHandler.type5);
                    builder.appendQueryParameter("id",""+notification.getId());
                    builder.appendQueryParameter("famille",notification.getFamille());
                    builder.appendQueryParameter("type",notification.getType());
                    builder.appendQueryParameter("category",notification.getCategory());
                    Uri uri=builder.build();
                    Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                    intent.putExtra("locale_uri","locale_uri");
                    startActivity(intent);
                }
                else if(notification.getType().equalsIgnoreCase(PushHandler.type6)){
                    Uri.Builder builder=Uri.parse(PushHandler.pushBaseUlr).buildUpon();
                    builder.appendPath(PushHandler.type6);
                    builder.appendQueryParameter("id",""+notification.getId());
                    builder.appendQueryParameter("famille",notification.getFamille());
                    builder.appendQueryParameter("type",notification.getType());
                    builder.appendQueryParameter("category",notification.getCategory());
                    Uri uri=builder.build();
                    Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                    intent.putExtra("locale_uri","locale_uri");
                    startActivity(intent);
                }
                else if(notification.getType().equalsIgnoreCase(PushHandler.type7)){
                    Uri.Builder builder=Uri.parse(PushHandler.pushBaseUlr).buildUpon();
                    builder.appendPath(PushHandler.type7);
                    builder.appendQueryParameter("id",""+notification.getId());
                    builder.appendQueryParameter("famille",notification.getFamille());
                    builder.appendQueryParameter("type",notification.getType());
                    builder.appendQueryParameter("category",notification.getCategory());
                    Uri uri=builder.build();
                    Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                    intent.putExtra("locale_uri","locale_uri");
                    startActivity(intent);
                }
                else if(notification.getType().equalsIgnoreCase(PushHandler.type8)){
                    Uri.Builder builder=Uri.parse(PushHandler.pushBaseUlr).buildUpon();
                    builder.appendPath(PushHandler.type8);
                    builder.appendQueryParameter("id",""+notification.getId());
                    builder.appendQueryParameter("famille",notification.getFamille());
                    builder.appendQueryParameter("type",notification.getType());
                    builder.appendQueryParameter("category",notification.getCategory());
                    Uri uri=builder.build();
                    Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                    intent.putExtra("locale_uri","locale_uri");
                    startActivity(intent);
                }
                else if(notification.getType().equalsIgnoreCase(PushHandler.type9)){
                    Uri.Builder builder=Uri.parse(PushHandler.pushBaseUlr).buildUpon();
                    builder.appendPath(PushHandler.type9);
                    builder.appendQueryParameter("id",""+notification.getId());
                    builder.appendQueryParameter("famille",notification.getFamille());
                    builder.appendQueryParameter("type",notification.getType());
                    builder.appendQueryParameter("category",notification.getCategory());
                    Uri uri=builder.build();
                    Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                    intent.putExtra("locale_uri","locale_uri");
                    startActivity(intent);
                }

                if(pushNotificationList.isEmpty()){
                    if(alertDialog!=null){
                        alertDialog.dismiss();
                    }
                    finish();
                }
                return;
            case 2:

                Uri.Builder builder=Uri.parse(PushHandler.pushBaseUlr).buildUpon();
                builder.appendPath(PushHandler.type_push);
                builder.appendQueryParameter("conversation_id",notification.getConversation_id());
                Uri uri=builder.build();
                Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                intent.putExtra("conversation_id",notification.getConversation_id());
                intent.putExtra("push","push");
                startActivity(intent);
                if(pushNotificationList.isEmpty()){
                    if(alertDialog!=null){
                        alertDialog.dismiss();
                    }
                    finish();
                }
              return;
        }
    }



    class MyPushAdapteur extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        List<PushNotification> notificationList=new ArrayList<>();
        PushNotificationInterface pushNotificationInterface;
        public static final int PUSH_TYPE_ONE=0;
        public static final int PUSH_TYPE_TWO=1;
        public static final int PUSH_TYPE_THREE=2;

        public MyPushAdapteur (List<PushNotification> notificationList,PushNotificationInterface pushNotificationInterface){
            this.notificationList=notificationList;
            this.pushNotificationInterface=pushNotificationInterface;
        }
        class SimplePushViewHoder extends RecyclerView.ViewHolder{
            TextView tvTitle;
            TextView tvDescription;
            public SimplePushViewHoder(View v){
                super(v);
                tvTitle=(TextView)v.findViewById(R.id.main_dialog_title);
                tvDescription=(TextView)v.findViewById(R.id.main_dialog_description);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pushNotificationInterface.onPushItemClick(getAdapterPosition());
                    }
                });
            }
        }
        class PushPushViewHoder extends RecyclerView.ViewHolder{
            TextView tvTitle;
            TextView tvDescription;
            public PushPushViewHoder(View v){
                super(v);
                tvTitle=(TextView)v.findViewById(R.id.main_dialog_title);
                tvDescription=(TextView)v.findViewById(R.id.main_dialog_description);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pushNotificationInterface.onPushItemClick(getAdapterPosition());
                    }
                });
            }
        }
        class ImagePushViewHolder extends  RecyclerView.ViewHolder{
            TextView tvTitle;
            TextView tvDescription;
            ImageView picture;
            ImageView share;
            public ImagePushViewHolder(View v){
                super(v);
                tvTitle=(TextView)v.findViewById(R.id.main_dialog_title);
                tvDescription=(TextView)v.findViewById(R.id.main_dialog_description);
                picture=(ImageView)v.findViewById(R.id.main_dialog_image);
                share=(ImageView)v.findViewById(R.id.main_dialog_share);
                share.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                     PushNotification pushNotification=notificationList.get(getAdapterPosition());
                     sharePush(pushNotification);
                    }
                });
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        pushNotificationInterface.onPushItemClick(getAdapterPosition());
                    }
                });
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            switch (viewType){

                case PUSH_TYPE_ONE:
                    View v2=LayoutInflater.from(parent.getContext()).inflate(R.layout.main_dialog_item,parent,false);
                    SimplePushViewHoder simplePushViewHoder=new SimplePushViewHoder(v2);
                    return simplePushViewHoder;
                case PUSH_TYPE_TWO:
                    View v1= LayoutInflater.from(parent.getContext()).inflate(R.layout.main_dialog_item_with_image,parent,false);
                    ImagePushViewHolder imagePushViewHolder=new ImagePushViewHolder(v1);
                    return imagePushViewHolder;
                case PUSH_TYPE_THREE:
                    View v3=LayoutInflater.from(parent.getContext()).inflate(R.layout.main_dialog_item,parent,false);
                    PushPushViewHoder pushViewHoder=new PushPushViewHoder(v3);
                    return pushViewHoder;


            }

            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

              if(holder instanceof SimplePushViewHoder){
                  setUpSimpleViewHolder((SimplePushViewHoder)holder,position);

              }
              else if(holder instanceof ImagePushViewHolder){
                  setUpImageViewHolder((ImagePushViewHolder)holder,position);
              }
              else if(holder instanceof PushPushViewHoder){
                  setUpPushViewHolder((PushPushViewHoder) holder,position);
              }
        }

        private void setUpSimpleViewHolder(SimplePushViewHoder holder,int position){
            PushNotification pushNotification=notificationList.get(position);
            holder.tvTitle.setText(pushNotification.getTitle());
            holder.tvDescription.setText(pushNotification.getBody());
        }
        private void setUpPushViewHolder(PushPushViewHoder holder,int position){
            PushNotification pushNotification=notificationList.get(position);
            holder.tvTitle.setText(pushNotification.getTitle());
            holder.tvDescription.setText(pushNotification.getBody());

        }
        private void setUpImageViewHolder(ImagePushViewHolder holder,int position){
            PushNotification pushNotification=notificationList.get(position);
            holder.tvTitle.setText(pushNotification.getTitle());
            holder.tvDescription.setText(pushNotification.getBody());
            Picasso.with(getApplicationContext()).load(pushNotification.getUrl()).resize(100,100).into(holder.picture);
        }

        @Override
        public int getItemCount() {
            return notificationList.size();
        }

        @Override
        public int getItemViewType(int position) {
              PushNotification pushNotification=notificationList.get(position);
              switch (pushNotification.getViewHolder_type()){
                  case 0:
                      return PUSH_TYPE_ONE;
                  case 1:
                      return PUSH_TYPE_TWO;
                  case 2:
                      return PUSH_TYPE_THREE;
              }
            return super.getItemViewType(position);
        }
    }
}
