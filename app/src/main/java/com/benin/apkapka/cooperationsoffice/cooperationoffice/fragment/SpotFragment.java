package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Conf;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

/**
 * Created by joel on 29/11/2017.
 */
public class SpotFragment   extends Fragment{

    String idVideo="";
    YouTubePlayerSupportFragment playerSupportFragment;
    private boolean wifiConnected = false;
    private boolean dataConnected = false;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.spot_fragment_view,container,false);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       playerSupportFragment=YouTubePlayerSupportFragment.newInstance();
       FragmentTransaction transaction=getFragmentManager().beginTransaction();
       transaction.add(R.id.frameforyoutube,playerSupportFragment).commit();

    }


    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    private void init(){



        Bundle bundle=getArguments();
        if(bundle!=null){
            idVideo=bundle.getString("id");
        }

        if(getActivity()!=null){

            if(!getActivity().isFinishing()){


                  if(playerSupportFragment==null)
                      return;

                  try {


                      playerSupportFragment.initialize(Conf.DEVELOPER_KEY, new YouTubePlayer.OnInitializedListener() {
                          @Override
                          public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean b) {
                              ConnectivityManager connectivitymanager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

                              NetworkInfo[] networkInfo = connectivitymanager.getAllNetworkInfo();

                              for (NetworkInfo netInfo : networkInfo
                              ) {
                                  if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                                  {
                                      if (netInfo.isConnected()){
                                          wifiConnected = netInfo.isConnected();
                                      }
                                  }

                                  if (netInfo.getTypeName().equalsIgnoreCase("MOBILE")){
                                      if (netInfo.isConnected()){
                                          dataConnected = netInfo.isConnected();
                                      }
                                  }

                              }

                              if(!b){

                                  if (dataConnected){
                                      final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                      builder.setMessage(R.string.msgDataEnable);
                                      builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                          public void onClick(DialogInterface dialog, int id) {
                                              youTubePlayer.loadVideo(idVideo);

                                              youTubePlayer.play();
                                              youTubePlayer.setFullscreen(true);
                                              dialog.dismiss();
                                          }
                                      });
                                      builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                          public void onClick(DialogInterface dialog, int id) {
                                              /*youTubePlayer.loadVideo(idVideo);

                                              youTubePlayer.pause();
                                              youTubePlayer.setFullscreen(true);*/
                                              dialog.dismiss();
                                          }
                                      });

                                      AlertDialog dialog = builder.create();
                                      dialog.show();

                                  }else if(wifiConnected){
                                      youTubePlayer.loadVideo(idVideo);
                                      youTubePlayer.play();
                                      youTubePlayer.setFullscreen(true);
                                  }

                                  /*youTubePlayer.loadVideo(idVideo);
                                  youTubePlayer.play();
                                  youTubePlayer.setFullscreen(true);*/
                              }

                          }

                          @Override
                          public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

                          }
                      });
                  }
                  catch (IllegalStateException e){

                  }

            }
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
