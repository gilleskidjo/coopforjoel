package com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils;

public interface PushNotificationInterface {

    public void onPushItemClick(int position);
}
