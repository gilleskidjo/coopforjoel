package com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.PushDataEvent;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.MyPushDatabaseHelper;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushDataDao;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joel on 29/08/2017.
 */
public class PushFamilleAsyncLoader3  extends AsyncTaskLoader<List<PushFamille>> {
    List<PushFamille> list1=new ArrayList<PushFamille>();
    PushDataDao pushDataDao;
    String type,category;
    EventBus bus;
    public PushFamilleAsyncLoader3(Context c, String type, String category){
        super(c);
        pushDataDao=new PushDataDao(getContext());
        this.type=type;
        this.category=category;
        bus=EventBus.getDefault();
        bus.register(this);
    }
    @Subscribe
    public void onEvent(PushDataEvent event){
        onContentChanged();

    }

    @Override
    protected void onStartLoading() {
        if(list1.size()>0){
            deliverResult(list1);
        }
        if(list1.size()<1 || takeContentChanged()){
            forceLoad();
        }
    }

    @Override
    public List<PushFamille> loadInBackground() {
        List<PushFamille> list=new ArrayList<PushFamille>();
        Cursor c= pushDataDao.getPushDataFamilleCount(type,category);
        if(c!=null){

            while (c.moveToNext()){

                String mcategory=c.getString(c.getColumnIndexOrThrow(MyPushDatabaseHelper.MYPUSH_HELPER_ENTREY.COLUMN_FAMILLE));
                int count=c.getInt(c.getColumnIndexOrThrow("count"));
                PushFamille pushCategory=new PushFamille(mcategory,count);

                if(count>0)
                {

                    list.add(pushCategory);
                }
            }
            c.close();
        }
        list1=list;
        return list;
    }

    @Override
    public void deliverResult(List<PushFamille> data) {
        if(isReset()){
            return;
        }
        super.deliverResult(data);

    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        bus.unregister(this);
        super.onReset();
    }
}
