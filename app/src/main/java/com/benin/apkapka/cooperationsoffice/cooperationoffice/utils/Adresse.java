package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by joel on 14/10/2016.
 */
public class Adresse {
    SharedPreferences pre;
    SharedPreferences.Editor editor;
    private static  final  String PREF_NAME="user_store_adresse";
    private static  final  String KEY_city="user_city";
    private static  final String KEY_adresse="user_adresss";
    public  Adresse(Context c)
    {
        pre=c.getSharedPreferences(PREF_NAME,c.MODE_PRIVATE);
        editor=pre.edit();
    }

    public  void  setCity(String city)
    {
        editor.putString(KEY_city,city);
        editor.commit();
    }
    public  void  setAdress(String adress)
    {
        editor.putString(KEY_adresse,adress);
        editor.commit();
    }

    public  String getCity()
    {
       return  pre.getString(KEY_city,"");
    }
    public  String getAdress()
    {
        return pre.getString(KEY_adresse,"");
    }
}
