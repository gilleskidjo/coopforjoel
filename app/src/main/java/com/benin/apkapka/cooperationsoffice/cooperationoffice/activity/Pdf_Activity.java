package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.Pdf_Adapter;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joel on 29/09/2016.
 */
public class Pdf_Activity  extends BaseLanguageActivity implements AdapterView.OnItemClickListener{

    ListView liste;
    Pdf_Adapter adapter;
    PdfReceiver receiver;
    ProgressDialog dialog;
    IntentFilter filter;
    List<String> filenname=new ArrayList<String>();
    Toolbar toolbar;
    TextView t;
    String [] PROJECTION={MediaStore.Files.FileColumns.DATA};
    String selectionMimeType= MediaStore.Files.FileColumns.MIME_TYPE+"=?";
    String mimetype= MimeTypeMap.getSingleton().getMimeTypeFromExtension("pdf");
    String sellectionArgs[]={mimetype};
    boolean isok1;
    boolean isok2;
    class  PdfReceiver extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(Intent.ACTION_MEDIA_MOUNTED))
                {
                    getlistPdf();
                    isok1=true;
                }
                else if(intent.getAction().equals(Intent.ACTION_MEDIA_UNMOUNTED))
                {
                    getlistPdf();
                    isok2=true;
                }
                else if (intent.getAction().equals(Intent.ACTION_MEDIA_SCANNER_FINISHED))
                {
                    if(isok1)
                    {

                    }
                    else
                    {
                       getlistPdf();
                        isok1=false;
                    }
                }
                else  if(intent.getAction().equals(Intent.ACTION_MEDIA_SHARED))
                {
                    if(isok2)
                    {

                    }
                    else
                    {
                       getlistPdf();
                        isok2=false;
                    }

                }
        }
    }



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_list);
        liste=(ListView)findViewById(R.id.pdf_list);
        t=(TextView)findViewById(android.R.id.empty);
        toolbar=(Toolbar)findViewById(R.id.pdf_list_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        liste.setOnItemClickListener(this);

        receiver=new PdfReceiver();
        filter=new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_REMOVED);
        filter.addAction(Intent.ACTION_MEDIA_SCANNER_FINISHED);
        filter.addAction(Intent.ACTION_MEDIA_SCANNER_STARTED);
        filter.addAction(Intent.ACTION_MEDIA_SHARED);
        filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_EJECT);
        filter.addDataScheme("file");

    }
    @Override
    protected void onStart() {
        super.onStart();
        if(ContextCompat.checkSelfPermission(Pdf_Activity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
        {

            if(ActivityCompat.shouldShowRequestPermissionRationale(Pdf_Activity.this, Manifest.permission.READ_EXTERNAL_STORAGE))
            {
                Snackbar.make(LayoutInflater.from(Pdf_Activity.this).inflate(R.layout.activity_gallery_layout, null), "L'application a besoin de votre permission avant de pouvoir récupérer une image", Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ActivityCompat.requestPermissions(Pdf_Activity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},100);
                            }
                        })
                        .show();

            }
            else
            {
                ActivityCompat.requestPermissions(Pdf_Activity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},100);


            }

        }
        else
        {
            getlistPdf();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode)
        {
            case 100:
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    getlistPdf();
                }
                else
                {
                    MyToast.show(this,""+getResources().getString(R.string.activity_gallery_no_img_gallerie), true);
                }

                return;



        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(!isFinishing())
        {
            Intent  intent=new Intent();

            intent.putExtra("path",filenname.get(position));
            setResult(RESULT_OK, intent);
            finish();

        }
    }

    private void getlistPdf()
    {
        dialog=new ProgressDialog(this);
        dialog.setMessage(getResources().getString(R.string.layout_activity_pdf_recherche));
        dialog.setIndeterminate(true);
        dialog.show();
       filenname=new ArrayList<>();
        Cursor c=getContentResolver().query(MediaStore.Files.getContentUri("external"),PROJECTION,selectionMimeType,sellectionArgs,null);
        if(c!=null)
        {
            if(c.moveToFirst())
            {
                do {
                    String path=c.getString(0);
                    if(!path.equals(""))
                    {
                        filenname.add(path);
                    }

                }
                while (c.moveToNext());
                c.close();
              adapter=new Pdf_Adapter(filenname);
              liste.setAdapter(adapter);
            }
            else
            {
                t.setText(""+getResources().getString(R.string.layout_activity_pdf_vide));

                if(filenname!=null)
                {
                    if(adapter!=null)
                    {
                        filenname.clear();
                        adapter.notifyDataSetChanged();
                    }
                }
            }


        }
        else
        {
            t.setText(""+getResources().getString(R.string.layout_activity_pdf_vide));

            if(filenname!=null)
            {
                if(adapter!=null)
                {
                    filenname.clear();
                    adapter.notifyDataSetChanged();
                }
            }
        }
            if(dialog!=null)
            {
                dialog.dismiss();
            }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }
}
