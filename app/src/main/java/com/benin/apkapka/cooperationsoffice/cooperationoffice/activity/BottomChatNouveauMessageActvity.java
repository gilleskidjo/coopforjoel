package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.ChatMessageInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ChatMessage;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ChatConversation;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;


import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class BottomChatNouveauMessageActvity extends AppCompatActivity  implements ChatMessageInterface,View.OnClickListener{


    RecyclerView recyclerView;
    MyAdapteur adapteur;
    EditText edtitMessage;
    Button btnSend;
    AppCompatImageView imageViewGallery;
    Toolbar toolbar;
    List<ChatMessage> messageList1=new ArrayList<>();
    boolean isConversationcreate=false;
    long mid;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottom_chat_new_conversation);
        toolbar=(Toolbar)findViewById(R.id.mToolbar);
        edtitMessage=(EditText)findViewById(R.id.edittext_chat);
        btnSend=(Button)findViewById(R.id.button_chat_send);
       imageViewGallery=(AppCompatImageView)findViewById(R.id.img_gallery);
       recyclerView=(RecyclerView)findViewById(R.id.mchat);
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapteur=new MyAdapteur(messageList1,this);
        recyclerView.setAdapter(adapteur);
        btnSend.setOnClickListener(this);
        imageViewGallery.setOnClickListener(this);
        SessionManager sessionManager=new SessionManager(this);
        User user= sessionManager.getUser(this);
       createConversation(user.getId());
    }

    //Chat Message Inteface
    @Override
    public void onclick(int position) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.button_chat_send:
                  if(isConnected()){
                      String text=edtitMessage.getText().toString();
                      edtitMessage.setText("");
                      if(!TextUtils.isEmpty(text)){
                          SessionManager sessionManager=new SessionManager(this);
                          User user=sessionManager.getUser(this);
                          ChatMessage message=new ChatMessage();
                          message.setSend(true);
                          message.setFromUser(user.getId());
                          message.setType("message");
                          message.setMessage(text);
                          messageList1.add(message);

                          adapteur.notifyDataSetChanged();
                      }
                  }
                  else{
                      Toast.makeText(this,getResources().getString(R.string.error_frag_netw_parse),Toast.LENGTH_LONG).show();
                  }
                break;
            case R.id.img_gallery:


                if(isConnected()){
                    Intent intent=new Intent(this,Activity_Gallery2.class);
                    startActivityForResult(intent,0);
                }
                else{
                    Toast.makeText(this,getResources().getString(R.string.error_frag_netw_parse),Toast.LENGTH_LONG).show();
                }
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

         if(resultCode==RESULT_OK){

             boolean candSend=((MyApplication)getApplicationContext()).isCanSendSelectedImage();
             if(candSend){
                 ((MyApplication)getApplicationContext()).setCanSendSelectedImage(false);
            String path=((MyApplication)getApplicationContext()).getBottomChatSelectedImage_path();
                 SessionManager sessionManager=new SessionManager(this);
                 User user=sessionManager.getUser(this);
                 ChatMessage message=new ChatMessage();
                 message.setSend(true);
                 message.setFromUser(user.getId());
                 message.setType("image");
                 message.setPhoto(path);

                 messageList1.add(message);
                 adapteur.notifyDataSetChanged();
             }
         }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
              return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class MyAdapteur extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
        public static final int VIEW_TYPE_UN = 0;
        public static final int VIEW_TYPE_TWO = 1;
        public static final int VIEW_TYPE_TREE = 3;
        public static final int VIEW_TYPE_FOUR = 4;
        public static final String MESSAGE_TYPE_MESSAGE ="message";
        public static final String MESSAGE_TYPE_IMAGE = "image";
        List<ChatMessage> messageList=new ArrayList<>();
        ChatMessageInterface messageInterface;

      public  MyAdapteur(List<ChatMessage> chatMessages,ChatMessageInterface chatMessageInterface){
          this.messageList=chatMessages;
          this.messageInterface=chatMessageInterface;
      }

     public class TextMessageMyViewHolderSender extends RecyclerView.ViewHolder implements View.OnClickListener{
          TextView sendMessageTv;
          int position;
          ChatMessage message;
          public  void bind(int position){
              this.position=position;

          }
         public  TextMessageMyViewHolderSender(View v){
             super(v);
             sendMessageTv=(TextView)v.findViewById(R.id.send_message_body);
             v.setOnClickListener(this);
         }


         @Override
         public void onClick(View v) {
             messageInterface.onclick(getAdapterPosition());
         }
     }
        public class TextMessageMyViewHolderReceiver extends RecyclerView.ViewHolder implements View.OnClickListener{
          TextView receiverNameTv;
          TextView receiverAvatarTv;
          TextView receiverMessageTv;
            int position;
            public  void bind(int position){
                this.position=position;
            }
          public  TextMessageMyViewHolderReceiver(View v){
                super(v);
                receiverNameTv=(TextView)v.findViewById(R.id.receiver_name) ;
              receiverAvatarTv=(TextView)v.findViewById(R.id.receiver_avatar) ;
              receiverMessageTv=(TextView)v.findViewById(R.id.receiver_message_body) ;
                v.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                messageInterface.onclick(getAdapterPosition());
            }
        }
        public class PhotoMessageMyViewHolderSender extends RecyclerView.ViewHolder implements View.OnClickListener{
            ImageView senderImageView ;
            View senderprogressLayout;
            int position;
            public  void bind(int position){
                this.position=position;
            }
            public  PhotoMessageMyViewHolderSender(View v){
                super(v);
                senderImageView=(ImageView)v.findViewById(R.id.sender_chat_imageview);
                senderprogressLayout=(View)v.findViewById(R.id.sender_progress);
                v.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                messageInterface.onclick(getAdapterPosition());
            }
        }
        public class PhotoMessageMyViewHolderReceiver extends RecyclerView.ViewHolder implements View.OnClickListener{
            ImageView receiverImageView ;
            View reciverprogressLayout;
            int position;
            public  void bind(int position){
                this.position=position;
            }
            public  PhotoMessageMyViewHolderReceiver(View v){
                super(v);
                receiverImageView=(ImageView)v.findViewById(R.id.receiver_chat_imageview);
                reciverprogressLayout=(View)v.findViewById(R.id.receiver_progress);
                v.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {
                messageInterface.onclick(getAdapterPosition());
            }
        }


        @Override
        public int getItemCount() {
            return messageList.size();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

             switch (viewType){

                 case VIEW_TYPE_UN:
                     View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.bottom_chat_sender_message_layout,parent,false);
                     TextMessageMyViewHolderSender textMessageMyViewHolder=new TextMessageMyViewHolderSender(v);
                 return  textMessageMyViewHolder;
                 case VIEW_TYPE_TWO:
                     View v2=LayoutInflater.from(parent.getContext()).inflate(R.layout.bottom_chat_new_message_sender_image,parent,false);
                      PhotoMessageMyViewHolderSender  photoMessageMyViewHolderSender=new PhotoMessageMyViewHolderSender(v2);

                     return  photoMessageMyViewHolderSender;
                 case VIEW_TYPE_TREE:
                     View v3=LayoutInflater.from(parent.getContext()).inflate(R.layout.bottom_chat_receiver_message_layout,parent,false);
                     TextMessageMyViewHolderReceiver textMessageMyViewHolderReceiver=new TextMessageMyViewHolderReceiver(v3);

                     return  textMessageMyViewHolderReceiver;
                 case VIEW_TYPE_FOUR:
                     View v4=LayoutInflater.from(parent.getContext()).inflate(R.layout.bottom_chat_new_message_receiver_image,parent,false);
                     PhotoMessageMyViewHolderReceiver photoMessageMyViewHolderReceiver=new PhotoMessageMyViewHolderReceiver(v4);

                     return  photoMessageMyViewHolderReceiver;

             }

            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


             ChatMessage message=messageList.get(position);


             if(holder instanceof TextMessageMyViewHolderReceiver){

                TextMessageMyViewHolderReceiver holderMR=(TextMessageMyViewHolderReceiver)holder;
                holderMR.bind(position);
                holderMR.receiverMessageTv.setText(message.getMessage());
                holderMR.receiverNameTv.setText(message.getSendPrenom());
                holderMR.receiverAvatarTv.setText(message.getSendPrenom().charAt(0));
                if(message.isSend()){

                      if(isConversationcreate){

                          message.setSend(false);
                          SessionManager sessionManager=new SessionManager(getApplicationContext());
                          User user= sessionManager.getUser(getApplicationContext());
                          createChatMessage(mid,user.getId(),message.getMessage());
                      }
                      else {

                      }
                 }

             }
             else if(holder instanceof TextMessageMyViewHolderSender) {

              TextMessageMyViewHolderSender holderMS=(TextMessageMyViewHolderSender)holder;
               holderMS.bind(position);
               holderMS.sendMessageTv.setText(message.getMessage());
                 if(message.isSend()){

                     if(isConversationcreate){

                         message.setSend(false);
                         SessionManager sessionManager=new SessionManager(getApplicationContext());
                         User user= sessionManager.getUser(getApplicationContext());
                         createChatMessage(mid,user.getId(),message.getMessage());
                     }

                 }

             }
             else if(holder instanceof PhotoMessageMyViewHolderReceiver) {
              PhotoMessageMyViewHolderReceiver holderPR=(PhotoMessageMyViewHolderReceiver)holder;
              holderPR.bind(position);
                 BitmapFactory.Options options=new BitmapFactory.Options();
              holderPR.receiverImageView.setImageBitmap(BitmapFactory.decodeFile(message.getPhoto(),options));
                 if(message.isSend()){

                     if(isConversationcreate){
                         message.setSend(false);
                         SessionManager sessionManager=new SessionManager(getApplicationContext());
                         User user= sessionManager.getUser(getApplicationContext());
                         createChatMessagePhoto(mid,user.getId(),message.getPhoto(),holderPR.reciverprogressLayout);
                     }
                 }

             }
             else if(holder instanceof PhotoMessageMyViewHolderSender) {
              PhotoMessageMyViewHolderSender holderPS=(PhotoMessageMyViewHolderSender)holder;
              holderPS.bind(position);
              BitmapFactory.Options options=new BitmapFactory.Options();
              holderPS.senderImageView.setImageBitmap(BitmapFactory.decodeFile(message.getPhoto(),options));
                 if(message.isSend()){

                     if(isConversationcreate){
                         message.setSend(false);
                         SessionManager sessionManager=new SessionManager(getApplicationContext());
                         User user= sessionManager.getUser(getApplicationContext());
                         createChatMessagePhoto(mid,user.getId(),message.getPhoto(),holderPS.senderprogressLayout);
                     }
                 }
             }
        }



        @Override
        public int getItemViewType(int position) {
            SessionManager sm=new SessionManager(getApplicationContext());
            User s=sm.getUser(getApplicationContext());
            ChatMessage  message=messageList.get(position);
            int type=0;
            if(s.getId()==message.getFromUser() && message.getType().equalsIgnoreCase(MESSAGE_TYPE_MESSAGE)){
                type=VIEW_TYPE_UN;
            }
            if(s.getId()==message.getFromUser() && message.getType().equalsIgnoreCase(MESSAGE_TYPE_IMAGE)){
                type=VIEW_TYPE_TWO;
            }
            if(s.getId()!=message.getFromUser() && message.getType().equalsIgnoreCase(MESSAGE_TYPE_MESSAGE)){
                type=VIEW_TYPE_TREE;
            }
            if(s.getId()!=message.getFromUser() && message.getType().equalsIgnoreCase(MESSAGE_TYPE_IMAGE)){
                type=VIEW_TYPE_FOUR;
            }
            return  type;
        }


        public  void createChatMessage(long conversation_id, long user_id, final String message){
            OkHttpClient client=new OkHttpClient();


            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(Config.GET_BASE_URL_RETROFIT)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .client(client)
                    .build();

            MyInterface service=retrofit.create(MyInterface.class);
            Call<String> call=service.create_chat_message(conversation_id,user_id,message);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String>call1, Response<String> response) {
                    if(response!=null)
                    {
                        if(response.isSuccessful())
                        {
                            try {
                                String rep = response.body() != null ? response.body() : "";

                                JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                                boolean error=o.getBoolean("error");
                                if(error==true)
                                {

                                }
                                else
                                {

                                    ((MyApplication)getApplicationContext()).setIs_Active_conversation(true);
                                    ((MyApplication)getApplicationContext()).getConversation().setType("message");
                                    ((MyApplication)getApplicationContext()).getConversation().setLast_message(message);

                                }

                            }
                            catch(Exception e)
                            {

                            }



                        }
                        else
                        {
                        }
                    }


                }

                @Override
                public void onFailure(Call<String> call1,Throwable t) {


                }
            });
        }
        public  void createChatMessagePhoto(long conversation_id, long user_id, String photo, final View v){
            OkHttpClient client=new OkHttpClient();


            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(Config.GET_BASE_URL_RETROFIT)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .client(client)
                    .build();

            BitmapFactory.Options options=new BitmapFactory.Options();
            Bitmap btm=BitmapFactory.decodeFile(photo,options);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            btm.compress(Bitmap.CompressFormat.PNG, 0, bos);
             byte []byteArray = bos.toByteArray();
            MediaType mediaType=MediaType.parse("multipart/form-data");
            RequestBody requestBody = RequestBody.create(mediaType, byteArray);
            MyInterface service=retrofit.create(MyInterface.class);
              if(requestBody!=null){

                  Call<String> call=service.create_create_chat_photo(""+conversation_id,""+user_id,requestBody);
                  call.enqueue(new Callback<String>() {
                      @Override
                      public void onResponse(Call<String> call1,Response<String> response) {
                          if(response!=null)
                          {
                              if(response.isSuccessful())
                              {
                                  try {
                                      String rep = response.body() != null ? response.body() : "";

                                      JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                                      boolean error=o.getBoolean("error");
                                      if(error==true)
                                      {

                                      }
                                      else
                                      {
                                          ((MyApplication)getApplicationContext()).setIs_Active_conversation(true);
                                          ((MyApplication)getApplicationContext()).getConversation().setType("image");
                                          ((MyApplication)getApplicationContext()).getConversation().setLast_message("photo");

                                          v.setVisibility(View.GONE);
                                      }

                                  }
                                  catch(Exception e)
                                  {

                                  }



                              }

                          }


                      }

                      @Override
                      public void onFailure(Call<String> call1,Throwable t) {


                      }
                  });


              }
              else{

              }

        }
    }


    public  void  createConversation(final long id){
        OkHttpClient client=new OkHttpClient();



        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(client)
                .build();

        MyInterface service=retrofit.create(MyInterface.class);
        final Call<String> call=service.create_conversation(id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,Response<String> response) {
                if(response!=null)
                {
                    if(response.isSuccessful())
                    {
                        try {
                            String rep = response.body() != null ? response.body() : "";

                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error=o.getBoolean("error");
                            if(error==true)
                            {

                            }
                            else
                            {
                                isConversationcreate=true;
                                  mid=o.getLong("data");
                                ChatConversation conversation=new ChatConversation();
                                conversation.setId(o.getLong("data"));
                                SessionManager sessionManager=new SessionManager(BottomChatNouveauMessageActvity.this);
                                User user= sessionManager.getUser(BottomChatNouveauMessageActvity.this);
                                conversation.setId(user.getId());
                                ((MyApplication)getApplicationContext()).setConversation(conversation);


                            }

                        }
                        catch(Exception e)
                        {

                        }



                    }
                    else
                    {

                    }
                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {


            }
        });

    }

    private boolean isConnected()
    {
        ConnectivityManager cm=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        if(info!=null && info.isConnected() && info.isAvailable())
        {
            return  true;
        }

        return  false;
    }


}
