package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.FamilleListInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class Activity_famille_list extends AppCompatActivity  implements FamilleListInterface{

    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    View linearvide;
    TextView tvVide;
    FamilleListAdapteur listAdapteur;
    List<String>familleList=new ArrayList<>();
  String type="";
  String category;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.famille_activity_layout);
        toolbar=(Toolbar)findViewById(R.id.mToolbar);
        recyclerView=(RecyclerView)findViewById(R.id.famille_list_recyclerview);
        linearvide=findViewById(R.id.chat_famille_list_linearvide);
        progressBar=(ProgressBar) findViewById(R.id.chat_famille_list_Progress);
        tvVide=(TextView) findViewById(R.id.chat_famille_list_textview_vide);
        setSupportActionBar(toolbar);
        ActionBar actionBar =getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        listAdapteur=new FamilleListAdapteur(familleList,this);
        recyclerView.setAdapter(listAdapteur);
         if(getIntent().getExtras()!=null){
           Bundle bundle=getIntent().getExtras();
           type=bundle.getString("type");
           category=bundle.getString("category");

         }

          if(isConnected()){
              getAllFamille(type,category);
          }
          else{
              progressBar.setVisibility(View.GONE);
              linearvide.setVisibility(View.VISIBLE);
              tvVide.setVisibility(View.VISIBLE);
              recyclerView.setVisibility(View.GONE);
              tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));
          }

    }

    private boolean isConnected()
    {
        ConnectivityManager cm=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        if(info!=null && info.isConnected() && info.isAvailable())
        {
            return  true;
        }

        return  false;
    }
    public void getAllFamille(String type,String category){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.get_All_Famille(type,category);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");

                            if (error) {

                                if(!isFinishing()){
                                    progressBar.setVisibility(View.GONE);
                                    linearvide.setVisibility(View.VISIBLE);
                                    tvVide.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                    tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));
                                }

                            } else {


                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {

                                    progressBar.setVisibility(View.GONE);
                                    linearvide.setVisibility(View.VISIBLE);
                                    tvVide.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                    tvVide.setText(getResources().getString(R.string.form_get_famille));

                                } else {

                                    progressBar.setVisibility(View.GONE);
                                    linearvide.setVisibility(View.GONE);

                                    recyclerView.setVisibility(View.VISIBLE);
                                    familleList = ParseJson.parseFamille(o);
                                    listAdapteur.notifyDataSetChanged();
                                    listAdapteur=new FamilleListAdapteur(familleList,Activity_famille_list.this);
                                    recyclerView.setAdapter(listAdapteur);

                                }

                            }

                        } catch (Exception e) {

                            if(!isFinishing()){
                                progressBar.setVisibility(View.GONE);
                                linearvide.setVisibility(View.VISIBLE);
                                tvVide.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                tvVide.setText(getResources().getString(R.string.form_envoi_error));
                            }
                        }

                    }//fin is success,
                    else {

                        progressBar.setVisibility(View.GONE);
                        linearvide.setVisibility(View.VISIBLE);
                        tvVide.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        tvVide.setText(getResources().getString(R.string.form_envoi_error));

                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1 , Throwable t) {

                if(!isFinishing())
                {
                    progressBar.setVisibility(View.GONE);
                    linearvide.setVisibility(View.VISIBLE);
                    tvVide.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    tvVide.setText(getResources().getString(R.string.form_envoi_error));

                }
            }
        });
    }
    @Override
    public void onFamileItemClick(int position) {

        Intent intent=new Intent();
        String famille=familleList.get(position);
        intent.putExtra("famille",famille);
        setResult(RESULT_OK,intent);
        finish();
    }

    class FamilleListAdapteur extends RecyclerView.Adapter<FamilleListAdapteur.FamilleViewHolder> {

        List<String>fList=new ArrayList<>();
        FamilleListInterface familleListInterface;
        public  FamilleListAdapteur(List<String> mList,FamilleListInterface familleListInterface){
            this.fList=mList;
            this.familleListInterface=familleListInterface;
        }
      class FamilleViewHolder extends RecyclerView.ViewHolder  {
          TextView tvFamille;
          public  FamilleViewHolder(View v){
              super(v);
              tvFamille=(TextView)v.findViewById(R.id.famiile_list_item_tv);
              v.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      familleListInterface.onFamileItemClick(getAdapterPosition());
                  }
              });
          }
      }

        @Override
        public int getItemCount() {
            return fList.size();
        }

        @Override
        public FamilleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.famille_list_layout_item,parent,false);
            FamilleViewHolder viewHolder=new FamilleViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(FamilleViewHolder holder, int position) {
            String  famille=fList.get(position);
            holder.tvFamille.setText(""+famille);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
