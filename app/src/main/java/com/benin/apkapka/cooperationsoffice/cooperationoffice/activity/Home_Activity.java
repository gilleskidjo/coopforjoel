package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentHome;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Home;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.PicassoTransformation;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SwipeGestureDetector;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import dmax.dialog.SpotsDialog;

/**
 * Created by joel on 13/10/2016.
 */
public class Home_Activity extends BaseLanguageActivity {
    private GestureDetectorCompat detector;
    Toolbar toolbar;
    ViewFlipper fliper;
    ImageView left,right;
    String bad_url="https://cooperationsoffice.net/cooperation/images/";
    String bad_pdf="https://cooperationsoffice.net/cooperation/pdf/";
    ProgressBar bar,bar2,bar3;
    DinamicImageView image,image2,image3;
    TextView title,description;
    FragmentHome f;
    View v,v2,v3;
    Button share;
    ShareDialog shareDialog;
    CallbackManager callbackManager;
    int i=0;
boolean is_fr;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.primary_dark));
        }
        manageFacebook();
        setContentView(R.layout.home_layout_activity);
        toolbar=(Toolbar)findViewById(R.id.home_toolbar);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        fliper=(ViewFlipper)findViewById(R.id.home_flipper);
        title=(TextView)findViewById(R.id.home_title);
        description=(TextView)findViewById(R.id.home_description);
        left=(ImageView)findViewById(R.id.un_modele_view_swipe_left);
        right=(ImageView)findViewById(R.id.un_modele_view_swipe_right);
        share=(Button)findViewById(R.id.un_modele_view_partager);
        detector=new GestureDetectorCompat(this, new SwipeGestureDetector(this,fliper) {});

        FragmentManager fm=getSupportFragmentManager();
        f=(FragmentHome)fm.findFragmentByTag(FragmentHome.TAG);
        if(f==null)
        {
            f=new FragmentHome();
            fm.beginTransaction().add(f,FragmentHome.TAG).commit();
        }

        if(getIntent()!=null)
        {
            Bundle bundle=getIntent().getExtras();
            if(bundle!=null)
            {
                com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Home home    =new com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Home(bundle.getLong("id"),bundle.getString("title"),bundle.getString("title_en"),bundle.getString("description"),bundle.getString("description_en"),bundle.getString("url"),bundle.getString("url2"),bundle.getString("url3"),bundle.getString("preview_image"),bundle.getString("link_video"),bundle.getString("content_type"));
                f.setHome(home);

            }

        }

        setSlide(fliper);

        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fliper.setInAnimation(AnimationUtils.loadAnimation(Home_Activity.this,  R.anim.main_slide_in_left));
                fliper.setOutAnimation(AnimationUtils.loadAnimation(Home_Activity.this, R.anim.main_slide_out_left));
                fliper.showNext();
            }
        });
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fliper.setInAnimation(AnimationUtils.loadAnimation(Home_Activity.this, R.anim.main_slide_in_right));
                fliper.setOutAnimation(AnimationUtils.loadAnimation(Home_Activity.this,R.anim.main_slide_out_right));
                fliper.showPrevious();
            }
        });

        title.setText(f.getHome().getTitle());
        description.setText(f.getHome().getDescription());
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startbookShare();
            }
        });
    }
    private void manageFacebook()
    {
        callbackManager= CallbackManager.Factory.create();
        shareDialog=new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

                Toast.makeText(Home_Activity.this,"Erreur lors du  partage",Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void startbookShare()
    {
        final android.app.AlertDialog mAlertDialog=new SpotsDialog(Home_Activity.this,R.style.style_spot_actualite);
        mAlertDialog.show();
        Task<ShortDynamicLink> shortDynamicLinkTask= FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("http://cooperationsoffice.com"))
                .setDynamicLinkDomain("cooperations0ffices.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.benin.apkapka.cooperationsoffice.cooperationoffice")
                        .setFallbackUrl(Uri.parse("http://cooperationsoffice.com"))
                        .build())
                .setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder()
                        .setTitle(""+(f.getHome().getTitle()))
                        .setDescription(is_fr?"CLIQUEZ".toUpperCase()+" pour "+"VISITER".toUpperCase()+" et "+"PARTAGER".toUpperCase()+" notre publication ":"CLICK".toUpperCase()+" to "+"VISIT".toUpperCase()+" and "+"SHARE".toUpperCase()+" our publication")
                        .setImageUrl(Uri.parse(""+f.getHome().getUrl()))
                        .build())
                .buildShortDynamicLink()

                .addOnCompleteListener(Home_Activity.this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        mAlertDialog.dismiss();
                        if(task.isSuccessful()){

                            Uri uri=task.getResult().getShortLink();
                            ShareLinkContent linkContent=new ShareLinkContent.Builder()
                                    .setContentUrl(uri)
                                    .build();
                            if(shareDialog.canShow(ShareLinkContent.class)){
                                shareDialog.show(linkContent);
                            }
                        }
                        else {
                            AlertDialog mAlertDialog=null;
                            AlertDialog.Builder mBuilder1=new AlertDialog.Builder(Home_Activity.this)
                                    .setMessage(is_fr?"Impossible de publier l'image":"Can not publish image")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                        }
                                    });
                            mAlertDialog=mBuilder1.show();
                        }

                    }
                });

    }

    private void setSlide(ViewFlipper fliper)
    {



        if(!f.getHome().getUrl().equals(bad_url))
        {

            i=i+1;
            v= LayoutInflater.from(getApplicationContext()).inflate(R.layout.aa_pager_item,null);
            image=(DinamicImageView)v.findViewById(R.id.un_modele_view_image);

            bar=(ProgressBar)v.findViewById(R.id.un_modele_view_progressBar);
            Picasso.with(getApplicationContext())
                    .load(f.getHome().getUrl())

                    .into(image, new Callback() {
                        @Override
                        public void onSuccess() {
                            bar.setVisibility(View.GONE);
                            image.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError() {
                            bar.setVisibility(View.GONE);
                            image.setVisibility(View.VISIBLE);
                        }
                    });
            fliper.addView(v);
        }
        if(!f.getHome().getUrl2().equals(bad_url))
        {

            i=i+1;
            v2=LayoutInflater.from(getApplicationContext()).inflate(R.layout.aa_pager_item,null);
            image2=(DinamicImageView) v2.findViewById(R.id.un_modele_view_image);
            bar2=(ProgressBar)v2.findViewById(R.id.un_modele_view_progressBar);
            Picasso.with(getApplicationContext())
                    .load(f.getHome().getUrl2())

                    .into(image2, new Callback() {
                        @Override
                        public void onSuccess() {
                            bar2.setVisibility(View.GONE);
                            image2.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError() {
                            bar2.setVisibility(View.GONE);
                            image2.setVisibility(View.VISIBLE);
                        }
                    });
            fliper.addView(v2);
        }
        if(!f.getHome().getUrl3().equals(bad_url))
        {

            i=i+1;
            v3=LayoutInflater.from(getApplicationContext()).inflate(R.layout.aa_pager_item,null);
            image3=(DinamicImageView) v3.findViewById(R.id.un_modele_view_image);
            bar3=(ProgressBar)v3.findViewById(R.id.un_modele_view_progressBar);
            Picasso.with(getApplicationContext())
                    .load(f.getHome().getUrl3())

                    .into(image3, new Callback() {
                        @Override
                        public void onSuccess() {
                            bar3.setVisibility(View.GONE);
                            image3.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError() {
                            bar3.setVisibility(View.GONE);
                            image3.setVisibility(View.VISIBLE);
                        }
                    });
            fliper.addView(v3);
        }
        if(i<=1){
            left.setVisibility(View.GONE);
            right.setVisibility(View.GONE);
        }


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case android.R.id.home:

                finish();
               return true;
            case R.id.autre_share:
                Intent intent2=new Intent(Intent.ACTION_SEND);
                intent2.setType("text/plain");
                String url= "https://play.google.com/store/apps/details?id=com.benin.apkapka.cooperationsoffice.cooperationoffice";
                intent2.putExtra(Intent.EXTRA_TEXT, url);
                intent2.putExtra(Intent.EXTRA_SUBJECT, "Cooperations office");
                if(intent2.resolveActivity(getPackageManager())!=null)
                {

                    String[] blacklist = new String[]{"com.google.android.gm","com.facebook.katana" };
                    startActivity(generateCustomChooserIntent(intent2, blacklist));
                }
                else
                {
                    MyToast.show(this, getResources().getString(R.string.une_activity_installer_app_partage), false);
                }
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private Intent generateCustomChooserIntent(Intent prototype, String[] forbiddenChoices) {
        List<Intent> targetedShareIntents = new ArrayList<>();
        List<HashMap<String, String>> intentMetaInfo = new ArrayList<HashMap<String, String>>();
        Intent chooserIntent;

        Intent dummy = new Intent(prototype.getAction());
        dummy.setType(prototype.getType());
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(dummy, 0);

        if (!resInfo.isEmpty()) {
            for (ResolveInfo resolveInfo : resInfo) {
                if (resolveInfo.activityInfo == null || Arrays.asList(forbiddenChoices).contains(resolveInfo.activityInfo.packageName))
                    continue;

                HashMap<String, String> info = new HashMap<String, String>();
                info.put("packageName", resolveInfo.activityInfo.packageName);
                info.put("className", resolveInfo.activityInfo.name);
                info.put("simpleName", String.valueOf(resolveInfo.activityInfo.loadLabel(getPackageManager())));
                intentMetaInfo.add(info);
            }

            if (!intentMetaInfo.isEmpty()) {
                // sorting for nice readability
                Collections.sort(intentMetaInfo, new Comparator<HashMap<String, String>>() {
                    @Override
                    public int compare(HashMap<String, String> map, HashMap<String, String> map2) {
                        return map.get("simpleName").compareTo(map2.get("simpleName"));
                    }
                });

                // create the custom intent list
                for (HashMap<String, String> metaInfo : intentMetaInfo) {
                    Intent targetedShareIntent = (Intent) prototype.clone();
                    targetedShareIntent.setPackage(metaInfo.get("packageName"));
                    targetedShareIntent.setClassName(metaInfo.get("packageName"), metaInfo.get("className"));
                    targetedShareIntents.add(targetedShareIntent);
                }

                chooserIntent = Intent.createChooser(targetedShareIntents.remove(targetedShareIntents.size() - 1), getResources().getString(R.string.une_activity_partager_via));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
                return chooserIntent;
            }
        }

        return Intent.createChooser(prototype,getResources().getString(R.string.une_activity_partager_via) );
    }


}
