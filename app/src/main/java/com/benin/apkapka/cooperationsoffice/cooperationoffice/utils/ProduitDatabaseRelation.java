package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import java.util.List;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToMany;

/**
 * Created by joel on 05/12/2017.
 */
@Entity
public class ProduitDatabaseRelation  {

    @Id(assignable = true) long id;

    public ToMany<Produit> list;
    String name;
    public ProduitDatabaseRelation() {
    }

    public ProduitDatabaseRelation(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }




    public void setId(long id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
