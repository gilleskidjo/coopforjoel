package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.BitmapScaler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.yalantis.ucrop.UCrop;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import dmax.dialog.SpotsDialog;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by joel on 17/09/2016.
 */
public class Formulaire_location  extends BaseLanguageActivity implements View.OnClickListener{


    EditText edititle,editDescr;

    ImageView image;

    EditText editprice;
    EditText editreduction;



    String type="location";
    private String category;
    private boolean is_fr=false;
    private boolean isTakenImage_from_cam=false;
    private Bitmap btm_from_cam;
    Toolbar toolbar;



    private boolean dialogIsShow=false;
    AlertDialog.Builder builder;
    public  static final  int REQUEST_TAKE_PUCTURE=0;
    public  static  final  int REQUEST_GET_FROM_GALLERY=1;
    public  static  final int REQUEST_PDF=2;
    Bitmap  btm;

    private boolean isTakenImage1=false;




    AlertDialog dialogPro=null;
    Button btnpuplier;
    String title="",description="",price="",reduction="";
    public  static  final String [] actions={"",""};

    String auteur,contact_fournisseur,famille="",
            lien_ensavoir_plus,lien_description,texte_invitation,texte_notification,etat,
            pays="",texte_prix,texte_reduction,prefix="",longitude,latitude;

    String mpays [];
    String countryCode[];
    int paysPosition=0;
    TextView tvFamille;
    public static final int CHOOSE_FAMILLE_RESULt=10;

    boolean isEdit=false;
    private long annonceId=0;
    private String annonceUrl="";
    File file;
    ShareDialog shareDialog;
    CallbackManager callbackManager;
    long mid;
    boolean isReadyPublish=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.primary_dark_location));
        }
        manageFacebook();
        setContentView(R.layout.formulaire_location_layout);
        actions[0]=getResources().getString(R.string.form_action_pt_new);
        actions[1]=getResources().getString(R.string.form_action_pt_chx);
        countryCode=getResources().getStringArray(R.array.contryCode);
        mpays=getResources().getStringArray(R.array.toppays);
        SessionManager sessionManager=new SessionManager(this);
        User user=sessionManager.getUser(this);
        pays=user.getPays();

        for(int i=0;i<mpays.length;i++){
            String Selectpays=mpays[i];
            if(Selectpays.equalsIgnoreCase(pays)){
                paysPosition=i;
                prefix=countryCode[i];
                break;
            }
        }
        if(pays==null){
            pays="";
        }

        image=(ImageView)findViewById(R.id.form_image);

        is_fr=getResources().getBoolean(R.bool.lang_fr);

        editprice=(EditText)findViewById(R.id.form_price);
        editreduction=(EditText)findViewById(R.id.form_reduction);
        edititle=(EditText)findViewById(R.id.form_titre);
        editDescr=(EditText)findViewById(R.id.form_description);

        toolbar=(Toolbar)findViewById(R.id.form_toolbar);
        btnpuplier=(Button)findViewById(R.id.form_publier);
        btnpuplier.setOnClickListener(this);
        tvFamille=(TextView)findViewById(R.id.formulaure_choose_famille_tv);
        tvFamille.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Formulaire_location.this,Activity_famille_list.class);
                intent.putExtra("category",category);
                intent.putExtra("type",type);
                startActivityForResult(intent,CHOOSE_FAMILLE_RESULt);
            }
        });

        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        if(is_fr){
            actionBar.setTitle("Publier une location");
        }
        else {
            actionBar.setTitle("Publish a rental");
        }

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogIsShow=true;
                takePicture();
            }
        });




        if(getIntent()!=null)
        {
            Bundle bundle=getIntent().getExtras();

            if(bundle!=null)
            {
                category=bundle.getString("category","");
            }
            if(bundle.containsKey("edit")){
                isEdit=true;
                category=bundle.getString("category","");
                annonceId=bundle.getLong("id",0);
                tvFamille.setText(bundle.getString("famille",""));
                famille=bundle.getString("famille","");
                edititle.setText(bundle.getString("title",""));
                editDescr.setText(bundle.getString("description",""));
                editprice.setText(bundle.getString("price",""));
                editreduction.setText(bundle.getString("reduction",""));
                annonceUrl=bundle.getString("url","");
                Picasso.with(this).load(annonceUrl).into(image);
                if(annonceUrl.trim().length()>0){
                    String [] urls=annonceUrl.split("/");
                    annonceUrl=urls[urls.length-1];

                }
            }
        }
    }




    private void manageFacebook()
    {
        callbackManager=CallbackManager.Factory.create();
        shareDialog=new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {


                Toast.makeText(Formulaire_location.this,getResources().getString(R.string.error_frag_netw_error),Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void updateShareState(long id){

        final android.app.AlertDialog mAlertDialog=new SpotsDialog(Formulaire_location.this,R.style.style_spot_actualite);
        mAlertDialog.show();
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.updateAnnonceUserShareState(""+id);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");

                            if (error) {
                                mAlertDialog.dismiss();
                                Intent intent=new Intent(Formulaire_location.this,Activity_Publish_Action.class);
                                intent.putExtra("id",mid);
                                startActivity(intent);
                                finish();
                            } else {


                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {
                                    mAlertDialog.dismiss();
                                    Intent intent=new Intent(Formulaire_location.this,Activity_Publish_Action.class);
                                    intent.putExtra("id",mid);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    mAlertDialog.dismiss();
                                    Intent intent=new Intent(Formulaire_location.this,Activity_Publish_Action.class);
                                    intent.putExtra("id",mid);
                                    startActivity(intent);
                                    finish();
                                }

                            }

                        } catch (Exception e) {
                            mAlertDialog.dismiss();

                        }
                        mAlertDialog.dismiss();
                    }//fin is success,
                    else {


                        mAlertDialog.dismiss();
                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {
                mAlertDialog.dismiss();
                Intent intent=new Intent(Formulaire_location.this,Activity_Publish_Action.class);
                intent.putExtra("id",mid);
                startActivity(intent);
                finish();
            }
        });
    }
    private void sharePush(String type ,String category,String famille,String title,String id,String url)
    {
        SessionManager sessionManager=new SessionManager(this);
        final User user=sessionManager.getUser(this);

        Uri.Builder builder=Uri.parse(PushHandler.pushBaseUlr).buildUpon();
        if(type.equalsIgnoreCase(PushHandler.type))
        {
            builder.appendPath(PushHandler.type);
        }
        else if(type.equalsIgnoreCase(PushHandler.type2)){
            builder.appendPath(PushHandler.type2);
        }
        else if(type.equalsIgnoreCase(PushHandler.type3)){
            builder.appendPath(PushHandler.type3);
        }
        else if(type.equalsIgnoreCase(PushHandler.type4)){
            builder.appendPath(PushHandler.type4);
        }
        else if(type.equalsIgnoreCase(PushHandler.type5)){
            builder.appendPath(PushHandler.type5);
        }
        else if(type.equalsIgnoreCase(PushHandler.type6)){
            builder.appendPath(PushHandler.type6);
        }
        else if(type.equalsIgnoreCase(PushHandler.type7)){
            builder.appendPath(PushHandler.type7);
        }
        else if(type.equalsIgnoreCase(PushHandler.type8)){
            builder.appendPath(PushHandler.type8);
        }
        else if(type.equalsIgnoreCase(PushHandler.type9)){
            builder.appendPath(PushHandler.type9);
        }
        builder.appendQueryParameter("id",""+id);
        builder.appendQueryParameter("famille",famille);
        builder.appendQueryParameter("type",type);
        builder.appendQueryParameter("category",category);
        Uri uri=builder.build();

        final android.app.AlertDialog mAlertDialog=new SpotsDialog(Formulaire_location.this,R.style.style_spot_actualite);
        mAlertDialog.show();
        Task<ShortDynamicLink> shortDynamicLinkTask= FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(uri)
                .setDynamicLinkDomain("cooperations0ffices.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.benin.apkapka.cooperationsoffice.cooperationoffice")
                        .setFallbackUrl(Uri.parse("http://cooperationsoffice.net"))
                        .build())
                .setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder()
                        .setTitle(title)
                        .setDescription(is_fr?"CLIQUEZ".toUpperCase()+" pour "+"VISITER".toUpperCase()+" et "+"PARTAGER".toUpperCase()+" notre publication ":"CLICK".toUpperCase()+" to "+"VISIT".toUpperCase()+" and "+"SHARE".toUpperCase()+" our publication")
                        .setImageUrl(Uri.parse(""+url))
                        .build())
                .buildShortDynamicLink()

                .addOnCompleteListener(Formulaire_location.this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        mAlertDialog.dismiss();
                        if(task.isSuccessful()){

                            Uri uri=task.getResult().getShortLink();
                            ShareLinkContent linkContent=new ShareLinkContent.Builder()
                                    .setContentUrl(uri)
                                    .build();
                            if(shareDialog.canShow(ShareLinkContent.class)){
                                shareDialog.show(linkContent);

                            }
                        }
                        else {

                            android.support.v7.app.AlertDialog.Builder mBuilder1=new android.support.v7.app.AlertDialog.Builder(Formulaire_location.this)
                                    .setMessage(is_fr?"Impossible de publier l'image":"Can not publish image")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if(dialogPro!=null){
                                                dialogPro.dismiss();
                                            }
                                        }
                                    });
                            dialogPro=mBuilder1.show();
                        }

                    }
                });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private Bitmap decodeImage(String path)
    {
        int targetW =200;
        int targetH = 200;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = calculateInSampleSize(bmOptions,200,200);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;


        Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);

        return bitmap;
    }
    private Bitmap decodeFile(String imageFilepath)
    {
        int targetW=200;
        int targgetH=200;
        BitmapFactory.Options options=new BitmapFactory.Options();
        options.inJustDecodeBounds=true;
        BitmapFactory.decodeFile(imageFilepath,options);
        options.inSampleSize=calculateInSampleSize(options,targetW,targgetH);
        options.inJustDecodeBounds=false;
        return  BitmapFactory.decodeFile(imageFilepath,options);
    }

    private int calculateInSampleSize(BitmapFactory.Options options,int reqWidth,int reqHeight)
    {
        final  int height=options.outHeight;
        final  int width=options.outWidth;
        int inSampleSize=1;

        if(height>reqHeight || width>reqWidth)
        {
            final  int halfHeight=height/2;
            final  int halfWidth=width/2;

            while ((halfHeight/inSampleSize)>=reqHeight && (halfWidth/inSampleSize)>=reqWidth)
            {
                inSampleSize *=2;
            }


        }

        return inSampleSize;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
        if(requestCode==REQUEST_TAKE_PUCTURE && resultCode==RESULT_OK)
        {
            Bundle  extra=data.getExtras();


            if(dialogIsShow)
            {
                final Bitmap bitmap=(Bitmap)extra.get("data");

                Dexter.withActivity(this)
                        .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {

                                getPrivateAlbumStorageDir(Formulaire_location.this,bitmap);
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {

                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                            }
                        }).check();  ;


            }



        }
        else if(requestCode==REQUEST_GET_FROM_GALLERY && resultCode==RESULT_OK)
        {

            if(dialogIsShow)
            {
                if(data!=null)
                {
                    Bundle  bundle=data.getExtras();
                    if(bundle!=null)

                    {
                        String path=bundle.getString("path");
                        final   Bitmap  bitmap=BitmapFactory.decodeFile(path);

                        Dexter.withActivity(this)
                                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                .withListener(new PermissionListener() {
                                    @Override
                                    public void onPermissionGranted(PermissionGrantedResponse response) {

                                        getPrivateAlbumStorageDir(Formulaire_location.this,bitmap);
                                    }

                                    @Override
                                    public void onPermissionDenied(PermissionDeniedResponse response) {

                                    }

                                    @Override
                                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                                    }
                                }).check();  ;
                    }

                }
            }

        }
        else if(requestCode==CHOOSE_FAMILLE_RESULt){
            if(resultCode==RESULT_OK){
                famille=data.getStringExtra("famille");
                if(famille==null){
                    famille="";

                }
                tvFamille.setText(""+famille);
            }
        }
        else if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {

            final Uri resultUri = UCrop.getOutput(data);
            Bitmap  bitmap=decodeFile(file.getAbsolutePath());
            image.setImageBitmap(bitmap);
            if(btm!=null)
            {
                btm.recycle();
            }
            if(btm_from_cam!=null)
            {
                btm_from_cam.recycle();
            }
            btm=bitmap;
            btm_from_cam=btm;
            isTakenImage1=true;
            isTakenImage_from_cam=true;
            dialogIsShow=false;
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
        }
        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();
                Bitmap bitmap=BitmapFactory.decodeFile(resultUri.getPath());
                image.setImageBitmap(bitmap);
                try {
                    FileOutputStream outputStream=new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG,100,outputStream);
                    outputStream.flush();
                    outputStream.close();
                }
                catch (FileNotFoundException e){

                }
                catch (IOException e){

                }

                if(btm!=null)
                {
                    btm.recycle();
                }
                if(btm_from_cam!=null)
                {
                    btm_from_cam.recycle();
                }
                btm=bitmap;
                btm_from_cam=btm;
                isTakenImage1=true;
                isTakenImage_from_cam=true;
                dialogIsShow=false;

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        else if (isReadyPublish){
            updateShareState(mid);
            isReadyPublish=false;
        }

    }
    public File getPrivateAlbumStorageDir(Context context, Bitmap bitmap) {
        // Get the directory for the app's private pictures directory.
        final android.app.AlertDialog dialog1=new SpotsDialog(this,R.style.style_spot_actualite);
        dialog1.show();
        String path= Environment.getExternalStorageDirectory().getAbsolutePath()+"/CooperationsOffice/";

        File root = new File(path);
        if (!root.exists()) {
            root.mkdirs();
        }
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        timeStamp= "PNG_" + timeStamp + "_.png";
        file=new File(path+timeStamp);
        if(file.exists()){
            file.delete();
        }
        try{
            if(file.createNewFile()){
                FileOutputStream outputStream=new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG,90,outputStream);
                outputStream.flush();
                outputStream.close();
                dialog1.dismiss();
                CropImage.activity(Uri.fromFile(file))
                        .setOutputCompressQuality(100)
                        .setRequestedSize(500,500, CropImageView.RequestSizeOptions.RESIZE_FIT)
                        .start(Formulaire_location.this);
            }

        }
        catch(IOException e){

        }


        return file;

    }

    @Override
    public void onClick(View v) {

        SessionManager sm = new SessionManager(this);
        User user=sm.getUser(this);

        if(!sm.isUserDataSet()){
            AlertDialog.Builder builder=  builder= new AlertDialog.Builder(this);
            builder.setMessage(is_fr?"Vous devez compléter votre profil avant d'envoyer une publication":"You must complete your profile before sending a publication");
            builder.setPositiveButton(is_fr ? "Completer le profil" : "Complete the profile", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(dialogPro!=null){
                        dialogPro.dismiss();
                    }
                    Intent intent=new Intent(Formulaire_location.this,DrawerCompteActivity.class);
                    startActivity(intent);
                }
            });
            dialogPro=builder.show();
          return;
        }

        if(isTakenImage_from_cam)
        {
            btm=btm_from_cam;
            image.setImageBitmap(btm);
        }
        title=edititle.getText().toString();
        description=editDescr.getText().toString();

        price=editprice.getText().toString();
        reduction=editreduction.getText().toString();
        if(TextUtils.isEmpty(famille.trim())){
            MyToast.show(this,is_fr?"Vous devez choisir une famille":"You must choose a family",true);
            return;
        }
        if(title.trim().equals(""))
        {
            edititle.setError(getResources().getString(R.string.form_actualite_titre));
            return;
        }

        if(description.trim().equals(""))
        {

            editDescr.setError(getResources().getString(R.string.layout_form_actualite_description));
            return;
        }
        if(price.trim().equals(""))
        {
            editprice.setError(getResources().getString(R.string.form_ajouter_price_error));
            return;
        }
        if(reduction.trim().equals(""))
        {
            reduction="0";
        }



        if(!isEdit){
            if(!isTakenImage1)
            {

                MyToast.show(this,""+getResources().getString(R.string.form_actualite_img), false);
                return;
            }
        }
        final android.app.AlertDialog dialog1=new SpotsDialog(this,R.style.style_spot_location);
        dialog1.show();

        OkHttpClient client=new OkHttpClient();



        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(client)
                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        RequestBody requestBody=null,requestBody1=null,requestBody2=null,requestBody3=null;
        if(btm!=null)
        {
            byte  bytes[]=bitmapToBytes(btm);
            MediaType mediaType=MediaType.parse("multipart/form-data");
            requestBody =RequestBody.create(mediaType, bytes);
        }


        prefix.replaceAll("\\s+","");
        pays=pays.replaceAll("\\s+","");
        pays=pays.toLowerCase();
        auteur=user.getPrenom()+" "+user.getName();
        contact_fournisseur=prefix+sm.getCompteWhatsapp().replaceAll("\\s+","");
        lien_ensavoir_plus="https://wa.me/"+prefix+sm.getCompteWhatsapp();
        lien_description="https://wa.me/"+prefix+sm.getCompteWhatsapp();
        texte_invitation=is_fr?"Discuter avec le propriétaire":"Chat with the owner";
        texte_notification=is_fr?"A réserver d'avance":"To book in advance";
        etat="attente";
        texte_prix=is_fr?"Loyer":"Rent";
        texte_reduction=is_fr?"Offre limitée":"Limited offer";
        longitude=user.getLongitude();
        latitude=user.getLatitude();
        if(longitude==null){
            longitude="2.4469114999999997";
        }
        if(latitude==null){
            latitude="6.3720032";
        }


       if(!isEdit){
           String zone="";
           zone=user.getPays();
           if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
               zone="france";
           }
           Call<String> call=service.uploadProduit(""+user.getId(),title.trim(),description,price,reduction,texte_prix,texte_reduction,auteur,contact_fournisseur,famille,lien_ensavoir_plus,lien_description,texte_invitation,texte_notification,etat,pays,type,category,longitude,latitude,is_fr?"fr":"en",requestBody,zone);
           call.enqueue(new Callback<String>() {
               @Override
               public void onResponse(Call<String> call1,Response<String> response) {

                   AlertDialog.Builder builder=  builder= new AlertDialog.Builder(Formulaire_location.this);
                   if(response!=null)
                   {
                       if(response.isSuccessful())
                       {
                           try {
                               String rep = response.body() != null ? response.body() : "";

                               JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                               boolean error=o.getBoolean("error");

                               if(error==true)
                               {
                                   Toast.makeText(Formulaire_location.this,""+o.getString("message"),Toast.LENGTH_LONG).show();
                               }
                               else
                               {
                                   mid=o.getLong("data");
                                   final String mtype=o.getString("type");
                                   final String mcategory=o.getString("category");
                                   final String mfamille=o.getString("famille");
                                   final String mtitle=o.getString("title");
                                   final String murl=o.getString("url");

                                   dialog1.dismiss();

                                   builder.setMessage(is_fr?"Félicitation, votre publication est créée et mise en attente.Pour la rendre visible sur l'application vous devez la partager avec vos amis et cliquer de nouveau sur le bouton publier":"Congratulations, your publication is created and put on hold.To make it visible on the application you must share it with your friends and click again on the publish button");
                                   builder.setPositiveButton(is_fr?"Partager":"Share", new DialogInterface.OnClickListener() {
                                       @Override
                                       public void onClick(DialogInterface dialog, int which) {
                                           if (dialogPro != null) {
                                               dialogPro.dismiss();
                                           }

                                           sharePush( mtype , mcategory, mfamille, mtitle, ""+mid,murl);
                                           isReadyPublish=true;

                                       }
                                   });
                                   builder.setNegativeButton(is_fr ? "Fermer" : "Close", new DialogInterface.OnClickListener() {
                                       @Override
                                       public void onClick(DialogInterface dialog, int which) {
                                           finish();
                                       }
                                   });
                               }

                           }
                           catch(Exception e)
                           {

                           }
                           dialog1.dismiss();
                           dialogPro=builder.show();


                       }
                       else
                       {
                           builder=new AlertDialog.Builder(Formulaire_location.this);

                           builder.setMessage(""+getResources().getString(R.string.form_envoi_error));
                           builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   if (dialogPro != null) {
                                       dialogPro.dismiss();
                                   }

                               }
                           });
                           dialog1.dismiss();
                           dialogPro=builder.show();
                       }
                   }

                   if(dialog1.isShowing())dialog1.dismiss();
               }

               @Override
               public void onFailure(Call<String> call1,Throwable t) {

                   final AlertDialog.Builder builder=new AlertDialog.Builder(Formulaire_location.this);

                   builder.setMessage(""+getResources().getString(R.string.form_envoi_error));
                   builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                           if (dialogPro != null) {
                               dialogPro.dismiss();
                           }

                       }
                   });
                   dialog1.dismiss();
                   dialogPro=builder.show();

               }
           });

       }
       else{
           Call<String> call=service.editPostProduit(title,description,famille,""+annonceId,price,reduction,annonceUrl,requestBody);
           call.enqueue(new Callback<String>() {
               @Override
               public void onResponse(Call<String> call1,Response<String> response) {

                   AlertDialog.Builder builder=  builder= new AlertDialog.Builder(Formulaire_location.this);
                   if(response!=null)
                   {
                       if(response.isSuccessful())
                       {
                           try {
                               String rep = response.body() != null ? response.body() : "";

                               JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                               boolean error=o.getBoolean("error");

                               if(error==true)
                               {

                                   dialog1.dismiss();

                                   builder.setMessage(is_fr?"Publication modifiée.":"Modified publication.");
                                   builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                       @Override
                                       public void onClick(DialogInterface dialog, int which) {
                                           if (dialogPro != null) {
                                               dialogPro.dismiss();
                                           }

                                           finish();
                                       }
                                   });
                               }
                               else
                               {

                                   dialog1.dismiss();

                                   builder.setMessage(is_fr?"Publication modifiée.":"Modified publication.");
                                   builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                       @Override
                                       public void onClick(DialogInterface dialog, int which) {
                                           if (dialogPro != null) {
                                               dialogPro.dismiss();
                                           }

                                           finish();
                                       }
                                   });
                               }

                           }
                           catch(Exception e)
                           {

                           }
                           dialog1.dismiss();
                           dialogPro=builder.show();


                       }
                       else
                       {


                           builder=new AlertDialog.Builder(Formulaire_location.this);

                           builder.setMessage(""+getResources().getString(R.string.form_envoi_error));
                           builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   if (dialogPro != null) {
                                       dialogPro.dismiss();
                                   }


                               }
                           });
                           dialog1.dismiss();
                           dialogPro=builder.show();
                       }
                   }

                   if(dialog1.isShowing())dialog1.dismiss();
               }

               @Override
               public void onFailure(Call<String> call1,Throwable t) {


                   final android.support.v7.app.AlertDialog.Builder builder=new AlertDialog.Builder(Formulaire_location.this);

                   builder.setMessage(""+getResources().getString(R.string.form_envoi_error));
                   builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                           if (dialogPro != null) {
                               dialogPro.dismiss();
                           }

                       }
                   });
                   dialog1.dismiss();
                   dialogPro=builder.show();

               }
           });
       }




    }//fin








    private void takePicture()
    {
        builder=new AlertDialog.Builder(this);
        builder.setItems(actions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which)
                {
                    case 0:
                        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if(intent.resolveActivity(getPackageManager())!=null)
                        {
                            startActivityForResult(Intent.createChooser(intent, ""+getResources().getString(R.string.form_envoi_chx_img)), REQUEST_TAKE_PUCTURE);
                        }
                        else
                        {
                            Toast.makeText(Formulaire_location.this,""+getResources().getString(R.string.form_install_photo),Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 1:

                        Intent   intent1=new Intent(Formulaire_location.this,Activity_gallery.class);
                        startActivityForResult(intent1,REQUEST_GET_FROM_GALLERY);

                        break;





                }


            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialogIsShow=false;

            }
        });
        builder.show();
    }


    public byte []bitmapToBytes(Bitmap btm)
    {
        ByteArrayOutputStream out=new ByteArrayOutputStream();
        btm.compress(Bitmap.CompressFormat.PNG,0,out);
        byte bytes[]=out.toByteArray();
        return  bytes;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(btm!=null)
        {
            btm.recycle();
        }

        if(btm_from_cam!=null)
        {
            btm_from_cam.recycle();
        }

    }
}
