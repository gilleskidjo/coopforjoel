package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.AccueilInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetworkSearchByTitleAndType;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.HelperActivity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MenuVitrine;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyNumberFormatUtils;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MydateUtil;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by joel on 27/11/2017.
 */
public class SearchActivity  extends BaseLanguageActivity implements AccueilInterface,FragmentForNetworkSearchByTitleAndType.HandlerNetWorkRequestResponseHomeSearch{



    AppBarLayout barLayout;
    Toolbar toolbar;
    View linearvide;
    TextView txvide;
    RecyclerView recyclerView;

    ProgressBar bar;
    String type="home";
    String reuquest="";
    String category="";
    ActionBar actionBar;
    boolean is_small,is_medium,is_large;
    private boolean is_fr;
    List<Produit> produits=new ArrayList<>();
    MyAdapteurImmobilier adpteur;
    FragmentForNetworkSearchByTitleAndType forNetworkSearchOnHome;



    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(type.equals("boutique")){
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
            {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.boutique_primary_dark));
            }
        }
        else if(type.equals("emploi")){
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
            {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.emploi_primary_dar));
            }

        }
        else if(type.equals("bourse")){
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
            {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.emploi_primary_dar));
            }
        }
        else if(type.equals("annuaire")){
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
            {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.annuaire_primary_dark));
            }
        }
        else if(type.equals("service")){
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
            {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.question_primary_dark));
            }
        }
        else if(type.equals("location")){
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
            {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.primary_dark_location));
            }
        }
        else if(type.equals("partenariat")){
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
            {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.partenariat_primary_dark));
            }
        }
        else if(type.equals("projet")){
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
            {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.projet_primary_dark));
            }
        }
       setContentView(R.layout.search_activity_layout);
       toolbar=(Toolbar)findViewById(R.id.toolbar);
       setSupportActionBar(toolbar);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        is_small=getResources().getBoolean(R.bool.is_small);
        is_medium=getResources().getBoolean(R.bool.is_medium);
        is_large=getResources().getBoolean(R.bool.is_large);
       actionBar=getSupportActionBar();
       actionBar.setDisplayHomeAsUpEnabled(true);
       barLayout=(AppBarLayout)findViewById(R.id.container);
      if(is_fr){
       actionBar.setTitle("Résultat de recherche");
      }
      else {
       actionBar.setTitle("Search result");
      }
       recyclerView=(RecyclerView)findViewById(R.id.search_list);
       RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(this);
       recyclerView.setLayoutManager(layoutManager);
       bar=(ProgressBar)findViewById(R.id.progressBar);
       linearvide=findViewById(R.id.linearvide);
       txvide=(TextView)findViewById(R.id.textview_vide);

       if(getIntent().getExtras()!=null){
           Bundle bundle=getIntent().getExtras();
           if(bundle!=null){
             type=bundle.getString("type","");
             reuquest=bundle.getString("query","");
             category=bundle.getString("category","");
           }

       }
        SessionManager sessionManager=new SessionManager(getApplicationContext());
        User user=sessionManager.getUser(getApplicationContext());
        String zone="";
        zone=user.getPays();
        if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
            zone="france";
        }

        FragmentManager fm=getSupportFragmentManager();
        forNetworkSearchOnHome=(FragmentForNetworkSearchByTitleAndType) fm.findFragmentByTag(FragmentForNetworkSearchByTitleAndType.TAG);
        if(forNetworkSearchOnHome==null){
            forNetworkSearchOnHome=new FragmentForNetworkSearchByTitleAndType();
            fm.beginTransaction().add(forNetworkSearchOnHome,FragmentForNetworkSearchByTitleAndType.TAG).commit();
            forNetworkSearchOnHome.setInterface(this);
        }
        else{
            forNetworkSearchOnHome.setInterface(this);
        }

        preferences=getSharedPreferences("les_options",MODE_PRIVATE);
        editor=preferences.edit();

        final Gson gson2=new Gson();
        final String json=preferences.getString("MES_MENUS",null);
        List<MenuVitrine> data=null;
        if(json != null) {
            MenuVitrine[] menuVitrines = gson2.fromJson(json, MenuVitrine[].class);
            data = Arrays.asList(menuVitrines);
            ArrayList<String> str = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                str.add(data.get(i).getType());
            }

            if (!str.isEmpty()) {

                Log.e("STR1",type);

                if (str.contains(type)) {

                    if(type.equals("boutique")){

                        actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this,R.color.boutique_primary)));
                        barLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.boutique_primary));
                        forNetworkSearchOnHome.doRequestProduitSearchbytype(reuquest,is_fr?"fr":"en","boutique",category,zone);
                    }
                    else if(type.equals("emploi")){
                        actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this,R.color.emploi_primary)));
                        barLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.emploi_primary));
                        String type1="competence";
                        if(category.equals("emploi")){
                            type1="emploi";
                        }
                        forNetworkSearchOnHome.doRequestProduitSearchbytype(reuquest,is_fr?"fr":"en",type1,category,zone);
                    }
                    else if(type.equals("bourse")){
                        actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this,R.color.bourse_primary)));
                        barLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.bourse_primary));
                        forNetworkSearchOnHome.doRequestProduitSearchbytype(reuquest,is_fr?"fr":"en","bourse",category,zone);
                    }
                    else if(type.equals("annuaire")){
                        actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this,R.color.annuaire_primary)));
                        barLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.annuaire_primary));
                        forNetworkSearchOnHome.doRequestProduitSearchbytype(reuquest,is_fr?"fr":"en","annuaire",category,zone);
                    }
                    else if(type.equals("service")){
                        actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this,R.color.service_primary1)));
                        barLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.service_primary1));
                        forNetworkSearchOnHome.doRequestProduitSearchbytype(reuquest,is_fr?"fr":"en","service",category,zone);
                    }
                    else if(type.equals("location")){
                        actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this,R.color.location_primary)));
                        barLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.location_primary));
                        forNetworkSearchOnHome.doRequestProduitSearchbytype(reuquest,is_fr?"fr":"en","location",category,zone);
                    }
                    else if(type.equals("partenariat")){
                        actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this,R.color.partenariat_primary)));
                        barLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.partenariat_primary));
                        forNetworkSearchOnHome.doRequestProduitSearchbytype(reuquest,is_fr?"fr":"en","partenariat",category,zone);
                    }
                    else if(type.equals("projet")){
                        actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this,R.color.projet_primary)));
                        barLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.projet_primary));
                        forNetworkSearchOnHome.doRequestProduitSearchbytype(reuquest,is_fr?"fr":"en","projet",category,zone);
                    }
                    else  if(type.equals("actualite")){
                        forNetworkSearchOnHome.doRequestProduitSearchbytype(reuquest,is_fr?"fr":"en","actualite",category,zone);
                    }

                }
                if (!str.contains(type)){

                    Log.e("STR",type);

                     if(type.equals("home")){
                        forNetworkSearchOnHome.doRequestProduitSearch(reuquest,is_fr?"fr":"en",zone);
                    }

                }
            }
        }



        adpteur=new MyAdapteurImmobilier(produits,this,this);
        recyclerView.setAdapter(adpteur);


    }//fin onCreate


    @Override
    public void itemClicked(int position) {

        Produit p=produits.get(position);
        HelperActivity.getInstance().setProduit(p);
        Intent intent=new Intent();
        String type=p.getType();
        if(type.equals("actualite"))
        {
            intent=new Intent(SearchActivity.this,Une_actualite_activity.class);
        }
        else if(type.equals("boutique")){
            intent =new Intent(SearchActivity.this,Un_produit_activity.class);
        }
        else if(type.equals("emploi")){
            intent =new Intent(SearchActivity.this,Un_emploi_activity.class);
        }
        else if(type.equals("competence")){
            intent =new Intent(SearchActivity.this,Une_competence_annuaire_activity.class);
        }
        else if(type.equals("bourse")){
            intent =new Intent(SearchActivity.this,Une_bourse_activity.class);
        }
        else if(type.equals("annuaire")){

            intent =new Intent(SearchActivity.this,UnAnnuaire.class);
        }
        else if(type.equals("service")){
            intent =new Intent(SearchActivity.this,Un_service_activity.class);
        }
        else if(type.equals("location")){
            intent =new Intent(SearchActivity.this,Une_location_activity.class);
        }
        else if(type.equals("partenariat")){
            intent =new Intent(SearchActivity.this,Un_partenariat.class);
        }
        else if(type.equals("projet")){
            intent =new Intent(SearchActivity.this,Un_projet_activity.class);
        }
        intent.putExtra("id",p.getId());
        if(is_fr==true)
        {
            intent.putExtra("title",p.getTitle());
            intent.putExtra("description",p.getDescription());
            intent.putExtra("price",p.getPrice());
            intent.putExtra("nb_price",p.getNb_price().toString());
            intent.putExtra("nb_price_en",p.getNb_price_en().toString());
            intent.putExtra("text_reduction",p.getText_reduction());
            intent.putExtra("reduction",p.getReduction());
            intent.putExtra("extend_proprety1",p.getExtend_proprety1());
            intent.putExtra("extend_proprety2",p.getExtend_proprety2());
            intent.putExtra("nb_share",p.getNb_share());
            intent.putExtra("nb_read_catalogue",p.getNb_read_catalogue());
        }
        else
        {
            intent.putExtra("title",p.getTitle_en());
            intent.putExtra("description",p.getDescription_en());
            intent.putExtra("price",p.getPrice_en());
            intent.putExtra("nb_price",p.getNb_price().toString());
            intent.putExtra("nb_price_en",p.getNb_price_en().toString());
            intent.putExtra("text_reduction",p.getText_reduction_en());
            intent.putExtra("reduction",p.getReduction());
            intent.putExtra("extend_proprety1",p.getExtend_proprety1_en());
            intent.putExtra("extend_proprety2",p.getExtend_proprety2_en());
            intent.putExtra("nb_share",p.getNb_share());
            intent.putExtra("nb_read_catalogue",p.getNb_read_catalogue());

        }
        intent.putExtra("from_co_direct","oui");
        intent.putExtra("lien_catalogue",p.getLien_catalogue());
        intent.putExtra("lien_description",p.getLien_description());
        intent.putExtra("choixf",p.getChoixf());
        intent.putExtra("url",p.getUrl());
        intent.putExtra("url2",p.getUrl2());
        intent.putExtra("url3",p.getUrl3());
        intent.putExtra("type",p.getType());
        intent.putExtra("famille",p.getFamille());
        intent.putExtra("pays",p.getPays());
        intent.putExtra("category",p.getCategory());
        intent.putExtra("type2",0);
        intent.putExtra("date",p.getDate());
        intent.putExtra("famille_en",p.getFamille_en());
        intent.putExtra("lien_fichier",p.getLien_fichier());
        startActivity(intent);

    }

    @Override
    public void onResposeSuccessSearch(String ss) {
        if(forNetworkSearchOnHome.getListproduit()!=null){
         produits=forNetworkSearchOnHome.getListproduit();
         if(produits.isEmpty()){
             recyclerView.setVisibility(View.GONE);
             linearvide.setVisibility(View.VISIBLE);
             txvide.setVisibility(View.VISIBLE);
         }
         else {
           adpteur=new MyAdapteurImmobilier(produits,this,this);
           recyclerView.setAdapter(adpteur);
             recyclerView.setVisibility(View.VISIBLE);
             linearvide.setVisibility(View.GONE);
             txvide.setVisibility(View.GONE);
         }

        }
        bar.setVisibility(View.GONE);


    }

    @Override
    public void onresponseSearchFailAndPrintErrorResponse() {
        recyclerView.setVisibility(View.GONE);
        linearvide.setVisibility(View.VISIBLE);
        txvide.setVisibility(View.VISIBLE);
        bar.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class  MyAdapteurImmobilier extends  RecyclerView.Adapter<MyAdapteurImmobilier.ViewHolder> implements Filterable
    {
        List<Produit> produitList;
        List<Produit> filterArticle;
        AccueilInterface accueilInterface;
        Context c;
        Produit p;
        String type="";
        public MyAdapteurImmobilier(List<Produit> produitList,AccueilInterface accueilInterface,Context c)
        {
            this.c=c;

            this.produitList=produitList;
            this.accueilInterface=accueilInterface;
            filterArticle=produitList;

        }
        public   class ViewHolder extends RecyclerView.ViewHolder
        {

            TextView title;
            TextView tvCount;
            TextView tvDate;
            TextView price;
            TextView  description;
            TextView tvFamille;
            DinamicImageView image;
            TextView tvFamilleCount;
            public ViewHolder(View v)
            {
                super(v);

                title=(TextView)v.findViewById(R.id.boutique_title_id);
                tvFamille=(TextView)v.findViewById(R.id.famille_name);
                tvCount=(TextView)v.findViewById(R.id.boutique_produit_Count);
                tvCount.setVisibility(View.GONE);
                tvDate=(TextView)v.findViewById(R.id.boutique_produit_date);
                price=(TextView)v.findViewById(R.id.boutique_prix_id);
                description=(TextView)v.findViewById(R.id.boutique_description);
                image=(DinamicImageView) v.findViewById(R.id.boutique_produit_image);
                tvFamilleCount = (TextView)v.findViewById(R.id.famille_count);

            }
        }
        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    String s=constraint.toString();
                    if(s.isEmpty()){
                        produitList=filterArticle;
                    }
                    else{
                        List<Produit> articles=new ArrayList<>();
                        for (Produit row:filterArticle){
                            if(is_fr){
                                if(row.getTitle().toLowerCase().contains(s.toLowerCase())){
                                    articles.add(row);
                                }
                            }
                            else{
                                if(row.getTitle_en().toLowerCase().contains(s.toLowerCase())){
                                    articles.add(row);
                                }
                            }

                        }
                        produitList=articles;

                    }
                    FilterResults results=new FilterResults();
                    results.values=produitList;

                    return results;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    produitList=(ArrayList<Produit>)results.values;
                    notifyDataSetChanged();
                    if(produitList.isEmpty()){
                        recyclerView.setVisibility(View.GONE);
                        linearvide.setVisibility(View.VISIBLE);
                        txvide.setVisibility(View.VISIBLE);
                        txvide.setText(getResources().getString(R.string.empty_liste));
                    }
                    else{
                        recyclerView.setVisibility(View.VISIBLE);
                        linearvide.setVisibility(View.GONE);
                        txvide.setVisibility(View.GONE);
                        txvide.setText("");
                    }
                }
            };
        }
        @Override
        public int getItemCount() {
            return produitList.size();
        }


        @Override
        public MyAdapteurImmobilier.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            v= LayoutInflater.from(parent.getContext()).inflate(R.layout.boutique_item_viewl,parent,false);
            final ViewHolder holder=new ViewHolder(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    accueilInterface.itemClicked(holder.getAdapterPosition());
                }
            });
            return holder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            p=produitList.get(position);
            if(p.getNb_vue_famille()!=null && !p.getNb_vue_famille().trim().equalsIgnoreCase("") && !p.getNb_vue_famille().equalsIgnoreCase("null"))
            {
                holder.tvFamilleCount.setText(p.getNb_vue_famille());
            }
            else{
                holder.tvFamilleCount.setText("0");
            }
            type=p.getType().trim();
            BigDecimal prix=new BigDecimal(p.getNb_price());
            holder.tvDate.setText(MydateUtil.getPastDate(p.getDate(),is_fr));
            String des="";

            if(is_fr)
            {
                holder.tvFamille.setText(""+p.getFamille());
                if(type.equals("actualite")){
                holder.title.setText(p.getTitle());
                holder.price.setVisibility(View.GONE);
                des=p.getDescription();
                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des!=null)
                    {
                        if(des.trim().length()>50)
                        {
                            des=des.substring(0,50)+"...";

                        }
                    }

                }
                else
                {
                    if(des!=null)
                    {
                        if(des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                }
                holder.description.setText(des);

                }//fin

                else if(type.equals("boutique")){
                holder.title.setText(p.getTitle());

                prix=prix.setScale(0, RoundingMode.HALF_UP);
                String s1=prix.toPlainString();
                s1=s1.replace(".",",");
                String s=p.getReduction();
                s=s.replace(".",",");
                holder.price.setText(p.getText_reduction().equals("")?"Réduction":p.getText_reduction()+" "+((s.equals("0")||s.equals("00")||s.equals("0.0"))?"":(s+"%")));

                holder.description.setText(p.getPrice().equals("")?"":p.getPrice()+" "+ MyNumberFormatUtils.getInstance().formateNumber(p.getNb_price(),is_fr)+" FCFA");

                }//fin
                else if(type.equals("emploi")){
                holder.title.setText(p.getTitle());
                holder.price.setVisibility(View.GONE);
                des=p.getDescription();
                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des!=null)
                    {
                        if(des.trim().length()>50)
                        {
                            des=des.substring(0,50)+"...";

                        }
                    }

                }
                else
                {
                    if(des!=null)
                    {
                        if(des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                }
                holder.description.setText(des);
                }//fin
                else if(type.equals("bourse")){
                holder.title.setText(p.getTitle());
                holder.price.setVisibility(View.GONE);
                des=p.getDescription();
                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des!=null)
                    {
                        if(des.trim().length()>50)
                        {
                            des=des.substring(0,50)+"...";

                        }
                    }

                }
                else
                {
                    if(des!=null)
                    {
                        if(des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                }
                holder.description.setText(des);

                }//fin
                else if(type.equals("annuaire")){
                holder.title.setText(p.getTitle());
                holder.price.setVisibility(View.GONE);
                des=p.getDescription();
                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des!=null)
                    {
                        if(des.trim().length()>50)
                        {
                            des=des.substring(0,50)+"...";

                        }
                    }

                }
                else
                {
                    if(des!=null)
                    {
                        if(des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                }
                holder.description.setText(des);
                }//fin
                else if(type.equals("service")){
                holder.title.setText(p.getTitle());
                holder.price.setVisibility(View.GONE);
                des=p.getDescription();
                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des!=null)
                    {
                        if(des.trim().length()>50)
                        {
                            des=des.substring(0,50)+"...";

                        }
                    }

                }
                else
                {
                    if(des!=null)
                    {
                        if(des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                }
                holder.description.setText(des);
                }//fin
                else if(type.equals("location")){
                holder.title.setText(p.getTitle());

                prix=prix.setScale(0, RoundingMode.HALF_UP);
                String s1=prix.toPlainString();
                s1=s1.replace(".",",");
                holder.price.setText(p.getPrice().equals("")?"":p.getPrice()+" "+ MyNumberFormatUtils.getInstance().formateNumber(p.getNb_price(),is_fr)+" FCFA");
                des=p.getDescription();
                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des!=null)
                    {
                        if(des.trim().length()>50)
                        {
                            des=des.substring(0,50)+"...";

                        }
                    }

                }
                else
                {
                    if(des!=null)
                    {
                        if(des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                }
                holder.description.setText(des);
                }//
                else if(type.equals("partenariat")){
                holder.title.setText(p.getTitle());
                holder.price.setVisibility(View.GONE);
                des=p.getDescription();
                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des!=null)
                    {
                        if(des.trim().length()>50)
                        {
                            des=des.substring(0,50)+"...";

                        }
                    }

                }
                else
                {
                    if(des!=null)
                    {
                        if(des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                }
                holder.description.setText(des);
                }//
                else if(type.equals("projet")){
                holder.title.setText(p.getTitle());
                holder.price.setVisibility(View.GONE);
                des=p.getDescription();
                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des!=null)
                    {
                        if(des.trim().length()>50)
                        {
                            des=des.substring(0,50)+"...";

                        }
                    }

                }
                else
                {
                    if(des!=null)
                    {
                        if(des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                }
                holder.description.setText(des);
                }//

            }
            else
            {
                holder.tvFamille.setText(""+p.getFamille_en());
                if(type.equals("actualite")){
                    holder.title.setText(p.getTitle_en());
                    holder.price.setVisibility(View.GONE);
                    des=p.getDescription_en();
                    if(c.getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT)
                    {
                        if(des!=null)
                        {
                            if(des.length()>50)
                            {
                                des=des.substring(0,50)+"...";

                            }

                        }
                    }
                    else
                    {
                        if (des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                    holder.description.setText(des);
                }

                else if(type.equals("boutique")){

                    holder.title.setText(p.getTitle_en());
                    BigDecimal taux=new BigDecimal(589.7833);
                    BigDecimal val=prix.divide(taux,0,RoundingMode.HALF_UP);
                    String i=val.toString();
                    int val1=(int)Double.parseDouble(i);
                    String s=""+val1;
                    holder.price.setText(p.getText_reduction_en().equals("")?"Reduction":p.getText_reduction_en()+" "+((p.equals("0")||p.equals("00")||p.equals("0.0"))?"":p.getReduction()+"%"));
                    holder.description.setText(p.getPrice_en().equals("")?"":p.getPrice_en()+" "+MyNumberFormatUtils.getInstance().formateNumber(s,is_fr)+" $");
                }
                else if(type.equals("emploi")){
                    holder.title.setText(p.getTitle_en());
                    holder.price.setVisibility(View.GONE);
                    des=p.getDescription_en();
                    if(c.getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT)
                    {
                        if(des!=null)
                        {
                            if(des.length()>50)
                            {
                                des=des.substring(0,50)+"...";

                            }

                        }
                    }
                    else
                    {
                        if (des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                    holder.description.setText(des);
                }
                else if(type.equals("bourse")){
                    holder.title.setText(p.getTitle_en());
                    holder.price.setVisibility(View.GONE);
                    des=p.getDescription_en();
                    if(c.getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT)
                    {
                        if(des!=null)
                        {
                            if(des.length()>50)
                            {
                                des=des.substring(0,50)+"...";

                            }

                        }
                    }
                    else
                    {
                        if (des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                    holder.description.setText(des);
                }
                else if(type.equals("annuaire")){
                    holder.title.setText(p.getTitle_en());
                    holder.price.setVisibility(View.GONE);
                    des=p.getDescription_en();
                    if(c.getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT)
                    {
                        if(des!=null)
                        {
                            if(des.length()>50)
                            {
                                des=des.substring(0,50)+"...";

                            }

                        }
                    }
                    else
                    {
                        if (des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                    holder.description.setText(des);
                }
                else if(type.equals("service")){
                    holder.title.setText(p.getTitle_en());
                    holder.price.setVisibility(View.GONE);
                    des=p.getDescription_en();
                    if(c.getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT)
                    {
                        if(des!=null)
                        {
                            if(des.length()>50)
                            {
                                des=des.substring(0,50)+"...";

                            }

                        }
                    }
                    else
                    {
                        if (des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                    holder.description.setText(des);
                }
                else if(type.equals("location")){
                    holder.title.setText(p.getTitle_en());
                    BigDecimal taux=new BigDecimal(589.7833);
                    BigDecimal val=prix.divide(taux,0,RoundingMode.HALF_UP);
                    String i=val.toString();
                    int val1=(int)Double.parseDouble(i);
                    String s=""+val1;
                    holder.price.setText(p.getPrice_en().equals("")?"":p.getPrice_en()+" "+MyNumberFormatUtils.getInstance().formateNumber(s,is_fr)+" $");
                    des=p.getDescription_en();
                    if(c.getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT)
                    {
                        if(des!=null)
                        {
                            if(des.length()>50)
                            {
                                des=des.substring(0,50)+"...";

                            }

                        }
                    }
                    else
                    {
                        if (des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                    holder.description.setText(des);
                }//fin
                else if(type.equals("partenariat")){
                    holder.title.setText(p.getTitle_en());
                    holder.price.setVisibility(View.GONE);
                    des=p.getDescription_en();
                    if(c.getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT)
                    {
                        if(des!=null)
                        {
                            if(des.length()>50)
                            {
                                des=des.substring(0,50)+"...";

                            }

                        }
                    }
                    else
                    {
                        if (des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                    holder.description.setText(des);

                }
                else if(type.equals("projet")){
                    holder.title.setText(p.getTitle_en());
                    holder.price.setVisibility(View.GONE);
                    des=p.getDescription_en();
                    if(c.getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT)
                    {
                        if(des!=null)
                        {
                            if(des.length()>50)
                            {
                                des=des.substring(0,50)+"...";

                            }

                        }
                    }
                    else
                    {
                        if (des!=null)
                        {
                            if(des.length()>140)
                            {
                                des=des.substring(0,140)+"...";
                            }
                        }
                    }
                    holder.description.setText(des);

                }

            }




            final  float scale=getResources().getDisplayMetrics().density;
            if(is_small)
            {
                Picasso.with(c).load(p.getUrl()).resize((int)(120*scale+0.5f),(int)(100*scale+0.5f)).into(holder.image);
            }
            else if(is_medium)
            {
                Picasso.with(c).load(p.getUrl()).resize((int)(140*scale+0.5f),(int)(120*scale+0.5f)).into(holder.image);
            }
            else if (is_large)
            {
                Picasso.with(c).load(p.getUrl()).resize((int)(150*scale+0.5f),(int)(130*scale+0.5f)).into(holder.image);
            }

        }
    }
}
