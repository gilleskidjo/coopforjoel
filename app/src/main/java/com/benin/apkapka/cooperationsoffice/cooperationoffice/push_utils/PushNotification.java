package com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class PushNotification {
 @Id long mid;
 long id;
 String type;
 String category;
 String famille;
 String action;
 String type_n;
 String body;
 String title;
 String id_notification;
 String icon;

 String url;
 String conversation_id;
 int viewHolder_type;

    public long getMid() {
        return mid;
    }

    public void setMid(long mid) {
        this.mid = mid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFamille() {
        return famille;
    }

    public void setFamille(String famille) {
        this.famille = famille;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getType_n() {
        return type_n;
    }

    public void setType_n(String type_n) {
        this.type_n = type_n;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId_notification() {
        return id_notification;
    }

    public void setId_notification(String id_notification) {
        this.id_notification = id_notification;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getViewHolder_type() {
        return viewHolder_type;
    }

    public void setViewHolder_type(int viewHolder_type) {
        this.viewHolder_type = viewHolder_type;
    }

    public String getConversation_id() {
        return conversation_id;
    }

    public void setConversation_id(String conversation_id) {
        this.conversation_id = conversation_id;
    }
}
