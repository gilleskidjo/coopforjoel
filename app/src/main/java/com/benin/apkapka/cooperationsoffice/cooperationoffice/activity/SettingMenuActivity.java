package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.LanguageNotifier;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MenuVitrine;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SettingMenuActivity extends BaseLanguageActivity implements LanguageNotifier.LanguageNotifierInterface
{

    ListView listView;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Button dummy_button;
    SparseBooleanArray selectedRows;
    private  boolean is_fr;

     String json;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_menu);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        LanguageNotifier.getInstance().setNotifierInterface(this);

        final Gson gson=new Gson();
        preferences=getSharedPreferences("les_options",MODE_PRIVATE);
        editor=preferences.edit();
        listView = findViewById(R.id.list);
        dummy_button=findViewById(R.id.dummy_button);


        if (getIntent().hasExtra("setting"))
        {
            dummy_button.setText(is_fr?"Enregistrer ":"Save");
        }
       generateList(getDataList());

        json=preferences.getString("MES_MENUS",null);
        if(json != null) {
            MenuVitrine[] menuVitrines = gson.fromJson(json, MenuVitrine[].class);
            List<MenuVitrine> data = Arrays.asList(menuVitrines);

            selectedRows = listView.getCheckedItemPositions();
            for (int i = 0; i < data.size(); i++)
            {
                checkBox(data.get(i).getId(),selectedRows.get(i));
            }

        }

        dummy_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               String jso=preferences.getString("MES_MENUS",null);
                if(jso != null)
                {
                    MenuVitrine[] menuVitrines =gson.fromJson(jso, MenuVitrine[].class);
                    List<MenuVitrine> data = Arrays.asList(menuVitrines);
                    if (data.size() > 0)
                    {
                        Intent intent=new Intent(SettingMenuActivity.this,BaseActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                        {
                        Toast.makeText(getApplicationContext(),"Veuillez choisir au moins une preference avant de continuer",Toast.LENGTH_LONG).show();
                    }

                }else{
                    Toast.makeText(getApplicationContext(),"Veuillez choisir au moins une preference avant de continuer",Toast.LENGTH_LONG).show();
                }
            }
        });



    }



    public void checkBox(int position,boolean value)
    {
        selectedRows.put(position,true);
    }

    public List<MenuVitrine> getDataList()
    {

        List<MenuVitrine> menuVitrineList =new ArrayList<>();
        MenuVitrine menuVitrine1 =new MenuVitrine(0, is_fr?"ACTUALITES":"NEWS",R.drawable.ic_folded_newspaper, PushHandler.type);
        menuVitrineList.add(menuVitrine1);


        MenuVitrine menuVitrine2 =new MenuVitrine(1,is_fr?"BOUTIQUES":"SHOPS",R.drawable.ic_shopping_store_cart_,PushHandler.type2);
        menuVitrineList.add(menuVitrine2);


        MenuVitrine menuVitrine3 =new MenuVitrine(2,is_fr?"EMPLOIS":"JOBS",R.drawable.ic_briefcase,PushHandler.type3);
        menuVitrineList.add(menuVitrine3);


        MenuVitrine menuVitrine4 =new MenuVitrine(3,is_fr?"BOURSES":"BOURSES",R.drawable.ic_world,PushHandler.type4);
        menuVitrineList.add(menuVitrine4);


        MenuVitrine menuVitrine5 =new MenuVitrine(4,is_fr?"ANNUAIRES":"DIRECTORY",R.drawable.c_home_annuaire1,PushHandler.type5);
        menuVitrineList.add(menuVitrine5);


        MenuVitrine menuVitrine6 =new MenuVitrine(5,is_fr?"SERVICES":"SERVICES",R.drawable.ic_group_profile_users,PushHandler.type6);
        menuVitrineList.add(menuVitrine6);


        MenuVitrine menuVitrine7 =new MenuVitrine(6,is_fr?"LOCATIONS":"RENTALS",R.drawable.ic_local_taxi_black_24dp,PushHandler.type7);
        menuVitrineList.add(menuVitrine7);


        MenuVitrine menuVitrine8 =new MenuVitrine(7,is_fr?"ALLIANCES":"ALLIANCES",R.drawable.c_home_partenariat1,PushHandler.type8);
        menuVitrineList.add(menuVitrine8);


        MenuVitrine menuVitrine9 =new MenuVitrine(8,is_fr?"PROJETS":"PROJECTS",R.drawable.bar,PushHandler.type9);
        menuVitrineList.add(menuVitrine9);

        return menuVitrineList;

    }

    private void generateList(final List<MenuVitrine> menuVitrines) {
        final ArrayList<HashMap<String, String>> listItem = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> map;

        for (int i = 0; i< menuVitrines.size(); i++)
        {
            map = new HashMap<String, String>();
            map.put("label", menuVitrines.get(i).getLabel());
            map.put("id",""+ menuVitrines.get(i).getId());
            listItem.add(map);
        }

        final ListAdapter listAdapter=new SimpleAdapter(this,listItem,android.R.layout.simple_list_item_multiple_choice,
                new String[]{"label"}, new int[]{android.R.id.text1});
        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                selectedRows = listView.getCheckedItemPositions();
                if (selectedRows.size() > 0) {
                    List<MenuVitrine> temp=new ArrayList<>();

                    for (int i = 0; i < selectedRows.size(); i++)
                    {
                        if (selectedRows.valueAt(i))
                        {
                            MenuVitrine menuVitrine =new MenuVitrine(getDataList().get(selectedRows.keyAt(i)).getId(),
                                    getDataList().get(selectedRows.keyAt(i)).getLabel(),
                                    getDataList().get(selectedRows.keyAt(i)).getImg(),
                                    getDataList().get(selectedRows.keyAt(i)).getType());
                            temp.add(menuVitrine);
                        }
                    }

                    Gson gson=new Gson();
                        String all=gson.toJson(temp);
                        editor.putString("MES_MENUS",all);
                        editor.apply();
            }
            }
        });


    }


    @Override
    public void languageChange(Resources resources, String langague) {
        is_fr=resources.getBoolean(R.bool.lang_fr);
    }
}
