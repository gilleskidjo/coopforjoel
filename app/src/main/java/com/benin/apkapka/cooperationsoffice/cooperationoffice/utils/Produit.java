package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import java.math.BigDecimal;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToOne;


/**
 * Created by joel on 21/01/2016.
 */
@Entity
public class Produit  {

    @Id long mId;

    long id;
    String title;
    String title_en ;
    String description;
    String description_en;
    String lien_description;
    String price;
    String price_en;
    String nb_price;
    String nb_price_en;
    String text_reduction;
    String text_reduction_en;
    String reduction;
    String lien_catalogue;
    String extend_proprety1;
    String extend_proprety1_en;
    String extend_proprety2;
    String extend_proprety2_en;
    String nb_share;
    String nb_read_catalogue;
    String  choixf;
    String type;
    String famille;
    String pays;
    String category;
    String url;
    String url2;
    String url3;
    long count;
    String date;
    String famille_en;
    String lien_fichier;
    String auteur;
    public boolean isFavorite=false;
    public  String lien_bonus;
    public  String nb_vue;
    public  String nb_vue_famille;
    public long user_id;
    public String latitude;
    public String longitude;
    public String email;
    public  String admin_update;
    public String content_type;
    public String lien_gift;
    public String video_id;
 public Produit()
 {

 }

    public Produit(long id, String title, String title_en, String description, String description_en, String lien_description, String price, String price_en, String nb_price, String nb_price_en, String text_reduction, String text_reduction_en, String reduction, String lien_catalogue, String extend_proprety1, String extend_proprety1_en, String extend_proprety2, String extend_proprety2_en, String nb_share, String nb_read_catalogue, String choixf, String type, String famille, String pays, String category, String url, String url2, String url3, long count, String date, String famille_en, String lien_fichier,String auteur) {
        this.id = id;
        this.title = title;
        this.title_en = title_en;
        this.description = description;
        this.description_en = description_en;
        this.lien_description = lien_description;
        this.price = price;
        this.price_en = price_en;
        this.nb_price = nb_price;
        this.nb_price_en = nb_price_en;
        this.text_reduction = text_reduction;
        this.text_reduction_en = text_reduction_en;
        this.reduction = reduction;
        this.lien_catalogue = lien_catalogue;
        this.extend_proprety1 = extend_proprety1;
        this.extend_proprety1_en = extend_proprety1_en;
        this.extend_proprety2 = extend_proprety2;
        this.extend_proprety2_en = extend_proprety2_en;
        this.nb_share = nb_share;
        this.nb_read_catalogue = nb_read_catalogue;
        this.choixf = choixf;
        this.type = type;
        this.famille = famille;
        this.pays = pays;
        this.category = category;
        this.url = url;
        this.url2 = url2;
        this.url3 = url3;
        this.count = count;
        this.date = date;
        this.famille_en = famille_en;
        this.lien_fichier = lien_fichier;
        this.auteur=auteur;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public long getmId() {
        return mId;
    }

    public String getLien_fichier() {
        return lien_fichier;
    }

    public void setLien_fichier(String lien_fichier) {
        this.lien_fichier = lien_fichier;
    }

    public String getFamille_en() {
        return famille_en;
    }

    public void setFamille_en(String famille_en) {
        this.famille_en = famille_en;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_en() {
        return title_en;
    }

    public void setTitle_en(String title_en) {
        this.title_en = title_en;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription_en() {
        return description_en;
    }

    public void setDescription_en(String description_en) {
        this.description_en = description_en;
    }

    public String getLien_description() {
        return lien_description;
    }

    public void setLien_description(String lien_description) {
        this.lien_description = lien_description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_en() {
        return price_en;
    }

    public void setPrice_en(String price_en) {
        this.price_en = price_en;
    }

    public String getNb_price() {
        return nb_price;
    }

    public void setNb_price(String nb_price) {
        this.nb_price = nb_price;
    }

    public String getNb_price_en() {
        return nb_price_en;
    }

    public void setNb_price_en(String nb_price_en) {
        this.nb_price_en = nb_price_en;
    }

    public String getText_reduction() {
        return text_reduction;
    }

    public void setText_reduction(String text_reduction) {
        this.text_reduction = text_reduction;
    }

    public String getText_reduction_en() {
        return text_reduction_en;
    }

    public void setText_reduction_en(String text_reduction_en) {
        this.text_reduction_en = text_reduction_en;
    }

    public String getReduction() {
        return reduction;
    }

    public void setReduction(String reduction) {
        this.reduction = reduction;
    }

    public String getLien_catalogue() {
        return lien_catalogue;
    }

    public void setLien_catalogue(String lien_catalogue) {
        this.lien_catalogue = lien_catalogue;
    }

    public String getExtend_proprety1() {
        return extend_proprety1;
    }

    public void setExtend_proprety1(String extend_proprety1) {
        this.extend_proprety1 = extend_proprety1;
    }

    public String getExtend_proprety1_en() {
        return extend_proprety1_en;
    }

    public void setExtend_proprety1_en(String extend_proprety1_en) {
        this.extend_proprety1_en = extend_proprety1_en;
    }

    public String getExtend_proprety2() {
        return extend_proprety2;
    }

    public void setExtend_proprety2(String extend_proprety2) {
        this.extend_proprety2 = extend_proprety2;
    }

    public String getExtend_proprety2_en() {
        return extend_proprety2_en;
    }

    public void setExtend_proprety2_en(String extend_proprety2_en) {
        this.extend_proprety2_en = extend_proprety2_en;
    }

    public String getNb_share() {
        return nb_share;
    }

    public void setNb_share(String nb_share) {
        this.nb_share = nb_share;
    }

    public String getNb_read_catalogue() {
        return nb_read_catalogue;
    }

    public void setNb_read_catalogue(String nb_read_catalogue) {
        this.nb_read_catalogue = nb_read_catalogue;
    }

    public String getChoixf() {
        return choixf;
    }

    public void setChoixf(String choixf) {
        this.choixf = choixf;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFamille() {
        return famille;
    }

    public void setFamille(String famille) {
        this.famille = famille;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl2() {
        return url2;
    }

    public void setUrl2(String url2) {
        this.url2 = url2;
    }

    public String getUrl3() {
        return url3;
    }

    public void setUrl3(String url3) {
        this.url3 = url3;
    }

    public void setmId(long mId) {
        this.mId = mId;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getLien_bonus() {
        return lien_bonus;
    }

    public void setLien_bonus(String lien_bonus) {
        this.lien_bonus = lien_bonus;
    }

    public String getNb_vue() {
        return nb_vue;
    }

    public void setNb_vue(String nb_vue) {
        this.nb_vue = nb_vue;
    }

    public String getNb_vue_famille() {
        return nb_vue_famille;
    }

    public void setNb_vue_famille(String nb_vue_famille) {
        this.nb_vue_famille = nb_vue_famille;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdmin_update() {
        return admin_update;
    }

    public void setAdmin_update(String admin_update) {
        this.admin_update = admin_update;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getLien_gift() {
        return lien_gift;
    }

    public void setLien_gift(String lien_gift) {
        this.lien_gift = lien_gift;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }
}
