package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ListDataHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by joel on 16/02/2016.
 */
public class FragmentNetworkRequestNouvelArrivage   extends Fragment {

    public  static  final  String TAG="com.cooperation.office.fragemnt.for_network_nouvelarrivage";
    public interface  HandlerNetWorkRequestResponse
    {

        public void onResposeSuccess();

        public  void onresponseFailAndPrintErrorResponse();

    }

    List<Produit> listproduit=new ArrayList<>();
    private String sms="";
    HandlerNetWorkRequestResponse handleResponse;
    private boolean isRequestDo=false;
    private boolean isMessage=false;
    boolean isEmpty;

    public boolean isEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

    }
    public List<Produit> getListproduit() {
        return listproduit;
    }


    public boolean isRequestDo() {
        return isRequestDo;
    }

    public boolean isMessage() {
        return isMessage;
    }

    public void setIsMessage(boolean isMessage) {
        this.isMessage = isMessage;
    }

    public String getSms() {
        return sms;
    }


    public  void setInterface(HandlerNetWorkRequestResponse handleResponse)
    {
        this.handleResponse=handleResponse;
    }



    public  void  doRequestProduitNouveauproduit_Group_By_Famille(String category,String pays,String lang,String type,String zone,int page)
    {
        isEmpty=false;
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        MyInterface2 service=retrofit.create(MyInterface2.class);
        Call<String> call=service.getAllProduct_Group_By_Category(category,pays,lang,type,zone,page);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message="";
                if (response != null) {
                    if (response.isSuccessful()) {
                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if(error)
                            {

                                if (getActivity()!=null)
                                {
                                    message=o.getString("message");
                                    sms=message;
                                    isMessage=true;
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onresponseFailAndPrintErrorResponse();
                                    }
                                }


                            }
                            else
                            {
                                boolean isVide=o.getBoolean("vide");
                                if(isVide)
                                {
                                    isEmpty=true;
                                    message=o.getString("message");
                                    sms=message;
                                    isMessage=true;
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onresponseFailAndPrintErrorResponse();
                                    }


                                }
                                else
                                {

                                    listproduit= ParseJson.parseProduit(o,getContext());
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onResposeSuccess();
                                    }
                                }

                            }
                        } catch (Exception e) {

                            if (getActivity()!=null)
                            {
                                message=getResources().getString(R.string.error_frag_netw_parse);
                                sms=message;
                                isMessage=true;
                                if(handleResponse!=null)
                                {
                                    handleResponse.onresponseFailAndPrintErrorResponse();
                                }
                            }
                        }

                    }//fin isSucces
                    else {

                        if (getActivity()!=null)
                        {
                            message=getResources().getString(R.string.error_frag_netw_parse);
                            sms=message;
                            isMessage=true;
                            if(handleResponse!=null)
                            {
                                handleResponse.onresponseFailAndPrintErrorResponse();
                            }
                        }

                    }//fin else isSuuces

                }//fin null response
                isRequestDo=true;
            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {
                if (getActivity()!=null)
                {
                    sms="";

                    sms=getResources().getString(R.string.error_frag_netw_error);
                    isMessage=true;
                    if(handleResponse!=null)
                    {
                        handleResponse.onresponseFailAndPrintErrorResponse();
                    }
                    isRequestDo=true;
                }
            }
        });

    }


}
