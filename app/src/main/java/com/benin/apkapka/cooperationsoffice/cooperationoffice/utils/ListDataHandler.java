package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by joel on 14/02/2016.
 */
public class ListDataHandler {



    public static final String SHARE_NAME="com.benin.cooperation_office";

    public static final String REF_LIST_PRODUIT_STOQUE_LIMITE="liste_stoque_limite";
    public static final String REF_LIST_PRODUIT_AUTOMOBILE="liste_nouvel_arrivage";
    public static final String REF_LIST_PRODUIT_IMMOBILIER="liste_nouveau_produit";
    public  static  final  String REF_LIST_PRODUCT_SALE="liste_product_sale";
   public  static final  String REF_LIST_LOCATION_AUTOMOBILE="liste_location_automobile";
    public  static  final  String REF_LIST_LOCATION_IMMOBILIER="liste_location_IMMOBILIER";
    public  static  final  String REF_LIST_LOCATION_AUTRE="liste_location_AUTRE";

    public  static  final  String REF_LIST_ARTICLE_INFORMATION="liste_article_information";
    public  static  final  String REF_LIST_ARTICLE_KIOSQUE="liste_article_kiosque";
    public  static  final  String REF_LIST_ARTICLE_BIBLIOTHEQUE="liste_article_ bibliotheque";

    public  static  final  String REF_LIST_OFFRE_EMPLOI="liste_des_offre_emploi";
    public  static  final  String REF_LIST_COMPETENCE_CV="liste_des_competence_cv";
    public  static  final  String REF_LIST_COMPETENCE_FREELANCE="liste_des_competence_freelance";

    public  static  final  String REF_LIST_BOURSE_FORMATION="liste_des_bourse_formation";
    public  static  final  String REF_LIST_BOURSE_voyage="liste_des_bourse_voyage";
    public  static  final  String REF_LIST_BOURSE_VALEUR_="liste_des_bourse_valeur";

    public  static  final  String REF_LIST_ANNUAIRE_ADRESSE="liste_des_annuiare_adresse";
    public  static  final  String REF_LIST_ANNUAIRE_REPERTOIRE="liste_des_annuiare_repertoire";
    public  static  final  String REF_LIST_ANNUAIRE_TROMBINOSCOPE="liste_des_annuiare_trombonoscope";

    public  static  final  String REF_LIST_SERVICE_PUBLIC="liste_des_service_public";
    public  static  final  String REF_LIST_SERVICE_ENTREPRISE="liste_des_service_entreprise";
    public  static  final  String REF_LIST_SERVICE_CLIENT="liste_des_service_client";

    public  static  final  String REF_LIST_PROJET_APPEL_OFFRE="liste_des_projet_appel_offre";
    public  static  final  String REF_LIST_PROJET_FINANCEMENT="liste_des_projet_financement";
    public  static  final  String REF_LIST_PROJET_DEVELOPPEMENT="liste_des_projet_developpement";

    public  static  final  String REF_LIST_PARTENARIAT_COMMERCIALE="liste_des_partenariat_commercial";
    public  static  final  String REF_LIST_PARTENARIAT_INDUSTRIEL="liste_des_partenariat_industriel";
    public  static  final  String REF_LIST_PARTENARIAT_TECHNOLOGIQUE="liste_des_partenariat_technologique";

    public  static  final  String REF_LIST_HOME="liste_des_element_du_home";

    //article
    public static  final  String REF_CATEGORY_ARTICLE_1="cooperation_article_1";
    public static  final  String REF_CATEGORY_ARTICLE_2="cooperation_article_2";
    public static  final  String REF_CATEGORY_ARTICLE_3="cooperation_article_3";
    //produit
    public static  final  String REF_CATEGORY_BOUTIQUE_1="cooperation_boutique_1";
    public static  final  String REF_CATEGORY_BOUTIQUE_2="cooperation_boutique_2";
    public static  final  String REF_CATEGORY_BOUTIQUE_3="cooperation_boutique_3";

    //emploi
    public static  final  String REF_CATEGORY_EMPLOI_OFFRE_1="cooperation_emploi_offre_1";
    public static  final  String REF_CATEGORY_EMPLOI_COMPETENCE_1="cooperation_emploi_competence_1";
    public static  final  String REF_CATEGORY_EMPLOI_COMPETENCE_2="cooperation_emploi_competence_2";

    //bourse
    public static  final  String REF_CATEGORY_BOURSE_1="cooperation_bourse_1";
    public static  final  String REF_CATEGORY_BOURSE_2="cooperation_bourse_2";
    public static  final  String REF_CATEGORY_BOURSE_3="cooperation_bourse_3";
    //annuaire
    public static  final  String REF_CATEGORY_ANNUAIRE_1="cooperation_annuaire_1";
    public static  final  String REF_CATEGORY_ANNUAIRE_2="cooperation_annuaire_2";
    public static  final  String REF_CATEGORY_ANNUAIRE_3="cooperation_annuaire_3";
    //service
    public static  final  String REF_CATEGORY_SERVICE_1="cooperation_service_1";
    public static  final  String REF_CATEGORY_SERVICE_2="cooperation_service_2";
    public static  final  String REF_CATEGORY_SERVICE_3="cooperation_service_3";
    //partenariat
    public static  final  String REF_CATEGORY_PARTENARIAT_1="cooperation_partenariat_1";
    public static  final  String REF_CATEGORY_PARTENARIAT_2="cooperation_partenariat_2";
    public static  final  String REF_CATEGORY_PARTENARIAT_3="cooperation_partenariat_3";
    //projet
    public static  final  String REF_CATEGORY_PROJET_1="cooperation_projet_1";
    public static  final  String REF_CATEGORY_PROJET_2="cooperation_projet_2";
    public static  final  String REF_CATEGORY_PROJET_3="cooperation_projet_3";

    //location
    public static  final  String REF_CATEGORY_LOCATION_1="cooperation_location_1";
    public static  final  String REF_CATEGORY_LOCATION_2="cooperation_location_2";
    public static  final  String REF_CATEGORY_LOCATION_3="cooperation_location_3";
    //Date
    public  static  final String REF_DATE="cooperation_currente_date";

    public  static void saveDate(Context c,long date)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        edit.putLong(REF_DATE,date);
        edit.commit();

    }
    public  static long getDate(Context c)
    {
        long liste;
        SharedPreferences pref;
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        if(pref.contains(REF_DATE))
        {
            liste=pref.getLong(REF_DATE,0);
        }
        else
        {
            return 0;
        }

        return liste;
    }
    public  static void saveCategoy_article(Context c,List<Article> liste,int type,String category)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();
        switch (type) {
            case 0:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_ARTICLE_1+category, encoded_liste);
                edit.commit();
                break;
            case 1:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_ARTICLE_2+category, encoded_liste);
                edit.commit();
                break;
            case 2:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_ARTICLE_3+category, encoded_liste);
                edit.commit();
                break;


        }

    }

    public  static void saveCategoy_Emploi_Competence(Context c,List<Competence> liste,int type,String category)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();
        switch (type) {
            case 0:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_EMPLOI_COMPETENCE_1+category, encoded_liste);
                edit.commit();
                break;
            case 1:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_EMPLOI_COMPETENCE_2+category, encoded_liste);
                edit.commit();
                break;


        }

    }
    public  static void saveCategoy_Emploi_Offre(Context c,List<Emploi> liste,String category)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();
        encoded_liste=gson.toJson(liste);
        edit.putString(REF_CATEGORY_EMPLOI_OFFRE_1+category, encoded_liste);
        edit.commit();

    }

    public  static void saveCategoy_bourse(Context c,List<Bourse> liste,int type,String category)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();
        switch (type) {
            case 0:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_BOURSE_1+category, encoded_liste);
                edit.commit();
                break;
            case 1:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_BOURSE_2+category, encoded_liste);
                edit.commit();
                break;
            case 2:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_BOURSE_3+category, encoded_liste);
                edit.commit();
                break;


        }

    }
    public  static void saveCategoy_annuaire(Context c,List<Annuaire> liste,int type,String category)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();
        switch (type) {
            case 0:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_ANNUAIRE_1+category, encoded_liste);
                edit.commit();
                break;
            case 1:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_ANNUAIRE_2+category, encoded_liste);
                edit.commit();
                break;
            case 2:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_ANNUAIRE_3+category, encoded_liste);
                edit.commit();
                break;


        }

    }
    public  static void saveCategoy_service(Context c,List<Service> liste,int type,String category)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();
        switch (type) {
            case 0:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_SERVICE_1+category, encoded_liste);
                edit.commit();
                break;
            case 1:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_SERVICE_2+category, encoded_liste);
                edit.commit();
                break;
            case 2:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_SERVICE_3+category, encoded_liste);
                edit.commit();
                break;


        }

    }
    public  static void saveCategoy_partenariat(Context c,List<Partenariat> liste,int type,String category)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();
        switch (type) {
            case 0:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_PARTENARIAT_1+category, encoded_liste);
                edit.commit();
                break;
            case 1:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_PARTENARIAT_2+category, encoded_liste);
                edit.commit();
                break;
            case 2:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_PARTENARIAT_3+category, encoded_liste);
                edit.commit();
                break;


        }

    }
    public  static void saveCategory_projet(Context c,List<Projet> liste,int type,String category)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();
        switch (type) {
            case 0:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_PROJET_1+category, encoded_liste);
                edit.commit();
                break;
            case 1:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_PROJET_2+category, encoded_liste);
                edit.commit();
                break;
            case 2:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_PROJET_3+category, encoded_liste);
                edit.commit();
                break;


        }

    }
    public  static void saveCategory_produit(Context c,List<Produit> liste,int type,String category)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();
        switch (type) {
            case 0:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_BOUTIQUE_1+category, encoded_liste);
                edit.commit();
                break;
            case 1:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_BOUTIQUE_2+category, encoded_liste);
                edit.commit();
                break;
            case 2:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_BOUTIQUE_3+category, encoded_liste);
                edit.commit();
                break;

            case 3:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_LOCATION_1+category, encoded_liste);
                edit.commit();
                break;
            case 4:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_LOCATION_2+category, encoded_liste);
                edit.commit();
                break;
            case 5:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_CATEGORY_LOCATION_1+category, encoded_liste);
                edit.commit();
                break;

        }

    }
    public  static  void saveProductSale(Context c,List<ProductSale> sales)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();

        if(sales.size()>0)
        {
            Gson gson=new Gson();
            encoded_liste=gson.toJson(sales);
            edit.putString(REF_LIST_PRODUCT_SALE, encoded_liste);
            edit.commit();
        }
        else
        {
            edit.clear().commit();

        }

    }
    public  static  void saveHome(Context c,List<Home> sales)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();
        encoded_liste=gson.toJson(sales);
        edit.putString(REF_LIST_HOME, encoded_liste);
        edit.commit();

    }

    public  static void saveProduit(Context c,List<Produit> liste,int type)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();
        switch (type) {
            case 0:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_PRODUIT_STOQUE_LIMITE, encoded_liste);
                edit.commit();
                break;
            case 1:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_PRODUIT_AUTOMOBILE, encoded_liste);
                edit.commit();
                break;
            case 2:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_PRODUIT_IMMOBILIER, encoded_liste);
                edit.commit();
                break;

            case 3:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_LOCATION_IMMOBILIER, encoded_liste);
                edit.commit();
                break;
            case 4:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_LOCATION_AUTOMOBILE, encoded_liste);
                edit.commit();
                break;
            case 5:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_LOCATION_AUTRE, encoded_liste);
                edit.commit();
                break;

        }

    }
    public  static void saveEMploi(Context c,List<Emploi> liste)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();

                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_OFFRE_EMPLOI, encoded_liste);
                edit.commit();

    }
    public  static void saveCompetence(Context c,List<Competence> liste,int type)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();
            switch (type)
            {
                case 0:
                    encoded_liste=gson.toJson(liste);
                    edit.putString(REF_LIST_COMPETENCE_CV, encoded_liste);
                    edit.commit();
                  break;
                case 1:
                    encoded_liste=gson.toJson(liste);
                    edit.putString(REF_LIST_COMPETENCE_FREELANCE, encoded_liste);
                    edit.commit();
                   break;


            }


    }
    public  static void saveBourse(Context c,List<Bourse> liste,int type)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();


        switch (type)
        {
            case 0:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_BOURSE_FORMATION, encoded_liste);
                edit.commit();
                break;
            case 1:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_BOURSE_voyage, encoded_liste);
                edit.commit();
                break;
            case 2:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_BOURSE_VALEUR_, encoded_liste);
                edit.commit();
                break;

        }

    }
    public  static void saveAnnuaire(Context c,List<Annuaire> liste,int type)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();


        switch (type)
        {
            case 0:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_ANNUAIRE_ADRESSE, encoded_liste);
                edit.commit();
                break;
             case 1:
                 encoded_liste=gson.toJson(liste);
                 edit.putString(REF_LIST_ANNUAIRE_REPERTOIRE, encoded_liste);
                 edit.commit();
                break;
            case 2:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_ANNUAIRE_TROMBINOSCOPE, encoded_liste);
                edit.commit();
                break;
        }

    }
    public  static void saveService(Context c,List<Service> liste,int type)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();


        switch (type)
        {
            case 0:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_SERVICE_PUBLIC, encoded_liste);
                edit.commit();
                break;
            case 1:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_SERVICE_ENTREPRISE, encoded_liste);
                edit.commit();
                break;
            case 2:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_SERVICE_CLIENT, encoded_liste);
                edit.commit();
                break;
        }

    }
    public  static void savePartenariat(Context c,List<Partenariat> liste,int type)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();


        switch (type)
        {
            case 0:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_PARTENARIAT_COMMERCIALE, encoded_liste);
                edit.commit();
                break;
            case 1:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_PARTENARIAT_INDUSTRIEL, encoded_liste);
                edit.commit();
                break;
            case 2:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_PARTENARIAT_TECHNOLOGIQUE, encoded_liste);
                edit.commit();
                break;
        }

    }
    public  static void saveProjet(Context c,List<Projet> liste,int type)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();


        switch (type)
        {
           case 0:
               encoded_liste=gson.toJson(liste);
               edit.putString(REF_LIST_PROJET_APPEL_OFFRE, encoded_liste);
               edit.commit();
               break;
            case 1:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_PROJET_FINANCEMENT, encoded_liste);
                edit.commit();
                break;
            case 2:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_PROJET_DEVELOPPEMENT, encoded_liste);
                edit.commit();
                break;
        }

    }
    public  static void saveArticle(Context c,List<Article> liste,int type)
    {
        SharedPreferences pref;
        SharedPreferences.Editor edit;
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        edit=pref.edit();
        Gson gson=new Gson();


        switch (type)
        {
            case 0:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_ARTICLE_INFORMATION, encoded_liste);
                edit.commit();
                break;
            case 1:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_ARTICLE_KIOSQUE, encoded_liste);
                edit.commit();
                break;
            case 2:
                encoded_liste=gson.toJson(liste);
                edit.putString(REF_LIST_ARTICLE_BIBLIOTHEQUE, encoded_liste);
                edit.commit();
                break;
        }

    }
    public  static List<Article> getListArticle(Context c,int type)
    {
        SharedPreferences pref;
        List<Article> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);


       switch (type)
       {
           case 0:
               if(pref.contains(REF_LIST_ARTICLE_INFORMATION))
               {
                   encoded_liste=pref.getString(REF_LIST_ARTICLE_INFORMATION, "");
                   Gson gson=new Gson();
                   Article [] listp=gson.fromJson(encoded_liste, Article[].class);
                   liste=Arrays.asList(listp);
                   liste=new ArrayList<Article>(liste);
               }
               else
               {
                   return null;
               }
               break;
           case 1:
               if(pref.contains(REF_LIST_ARTICLE_KIOSQUE))
               {
                   encoded_liste=pref.getString(REF_LIST_ARTICLE_KIOSQUE, "");
                   Gson gson=new Gson();
                   Article [] listp=gson.fromJson(encoded_liste, Article[].class);
                   liste=Arrays.asList(listp);
                   liste=new ArrayList<Article>(liste);
               }
               else
               {
                   return null;
               }
               break;
           case 2:
               if(pref.contains(REF_LIST_ARTICLE_BIBLIOTHEQUE))
               {
                   encoded_liste=pref.getString(REF_LIST_ARTICLE_BIBLIOTHEQUE, "");
                   Gson gson=new Gson();
                   Article [] listp=gson.fromJson(encoded_liste, Article[].class);
                   liste=Arrays.asList(listp);
                   liste=new ArrayList<Article>(liste);
               }
               else
               {
                   return null;
               }
               break;
       }

        return liste;
    }

    public  static List<Emploi> getListEmploi(Context c)
    {
        SharedPreferences pref;
        List<Emploi> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);

                if(pref.contains(REF_LIST_OFFRE_EMPLOI))
                {
                    encoded_liste=pref.getString(REF_LIST_OFFRE_EMPLOI, "");
                    Gson gson=new Gson();
                    Emploi [] listp=gson.fromJson(encoded_liste, Emploi[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Emploi>(liste);
                }
                else
                {
                    return null;
                }

        return liste;
    }

    public  static List<Home> getListHome(Context c)
    {
        SharedPreferences pref;
        List<Home> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);

        if(pref.contains(REF_LIST_HOME))
        {
            encoded_liste=pref.getString(REF_LIST_HOME, "");
            Gson gson=new Gson();
            Home [] listp=gson.fromJson(encoded_liste, Home[].class);
            liste=Arrays.asList(listp);
            liste=new ArrayList<Home>(liste);
        }
        else
        {
            return null;
        }

        return liste;
    }

    public  static List<Competence> getListCompetence(Context c,int type)
    {
        SharedPreferences pref;
        List<Competence> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);


        switch (type)
        {
            case 0:
                if(pref.contains(REF_LIST_COMPETENCE_CV))
                {
                    encoded_liste=pref.getString(REF_LIST_COMPETENCE_CV, "");
                    Gson gson=new Gson();
                    Competence [] listp=gson.fromJson(encoded_liste, Competence[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Competence>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 1:
                if(pref.contains(REF_LIST_COMPETENCE_FREELANCE))
                {
                    encoded_liste=pref.getString(REF_LIST_COMPETENCE_FREELANCE, "");
                    Gson gson=new Gson();
                    Competence [] listp=gson.fromJson(encoded_liste, Competence[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Competence>(liste);
                }
                else
                {
                    return null;
                }
                break;
        }

        return liste;
    }
    public  static List<Bourse> getListBourse(Context c,int type)
    {
        SharedPreferences pref;
        List<Bourse> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);


        switch (type)
        {
            case 0:
                if(pref.contains(REF_LIST_BOURSE_FORMATION))
                {
                    encoded_liste=pref.getString(REF_LIST_BOURSE_FORMATION, "");
                    Gson gson=new Gson();
                    Bourse [] listp=gson.fromJson(encoded_liste, Bourse[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Bourse>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 1:
                if(pref.contains(REF_LIST_BOURSE_voyage))
                {
                    encoded_liste=pref.getString(REF_LIST_BOURSE_voyage, "");
                    Gson gson=new Gson();
                    Bourse [] listp=gson.fromJson(encoded_liste, Bourse[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Bourse>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 2:
                if(pref.contains(REF_LIST_BOURSE_VALEUR_))
                {
                    encoded_liste=pref.getString(REF_LIST_BOURSE_VALEUR_, "");
                    Gson gson=new Gson();
                    Bourse [] listp=gson.fromJson(encoded_liste, Bourse[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Bourse>(liste);
                }
                else
                {
                    return null;
                }
                break;
        }

        return liste;
    }
    public  static List<Annuaire> getListAnnuaire(Context c,int type)
    {
        SharedPreferences pref;
        List<Annuaire> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);


        switch (type)
        {
            case 0:
                if(pref.contains(REF_LIST_ANNUAIRE_ADRESSE))
                {
                    encoded_liste=pref.getString(REF_LIST_ANNUAIRE_ADRESSE, "");
                    Gson gson=new Gson();
                    Annuaire [] listp=gson.fromJson(encoded_liste, Annuaire[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Annuaire>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 1:
                if(pref.contains(REF_LIST_ANNUAIRE_REPERTOIRE))
                {
                    encoded_liste=pref.getString(REF_LIST_ANNUAIRE_REPERTOIRE, "");
                    Gson gson=new Gson();
                    Annuaire [] listp=gson.fromJson(encoded_liste, Annuaire[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Annuaire>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 2:
                if(pref.contains(REF_LIST_ANNUAIRE_TROMBINOSCOPE))
                {
                    encoded_liste=pref.getString(REF_LIST_ANNUAIRE_TROMBINOSCOPE, "");
                    Gson gson=new Gson();
                    Annuaire [] listp=gson.fromJson(encoded_liste, Annuaire[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Annuaire>(liste);
                }
                else
                {
                    return null;
                }
                break;
        }
        return liste;
    }
    public  static List<Service> getListService(Context c,int type)
    {
        SharedPreferences pref;
        List<Service> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);


        switch (type)
        {
            case 0:
                if(pref.contains(REF_LIST_SERVICE_PUBLIC))
                {
                    encoded_liste=pref.getString(REF_LIST_SERVICE_PUBLIC, "");
                    Gson gson=new Gson();
                    Service [] listp=gson.fromJson(encoded_liste, Service[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Service>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 1:
                if(pref.contains(REF_LIST_SERVICE_ENTREPRISE))
                {
                    encoded_liste=pref.getString(REF_LIST_SERVICE_ENTREPRISE, "");
                    Gson gson=new Gson();
                    Service [] listp=gson.fromJson(encoded_liste, Service[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Service>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 2:
                if(pref.contains(REF_LIST_SERVICE_CLIENT))
                {
                    encoded_liste=pref.getString(REF_LIST_SERVICE_CLIENT, "");
                    Gson gson=new Gson();
                    Service [] listp=gson.fromJson(encoded_liste, Service[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Service>(liste);
                }
                else
                {
                    return null;
                }
                break;
        }

        return liste;
    }
    public  static List<Partenariat> getListPartenariat(Context c,int type)
    {
        SharedPreferences pref;
        List<Partenariat> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);


        switch (type)
        {
            case 0:
                if(pref.contains(REF_LIST_PARTENARIAT_COMMERCIALE))
                {
                    encoded_liste=pref.getString(REF_LIST_PARTENARIAT_COMMERCIALE, "");
                    Gson gson=new Gson();
                    Partenariat [] listp=gson.fromJson(encoded_liste, Partenariat[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Partenariat>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 1:
                if(pref.contains(REF_LIST_PARTENARIAT_INDUSTRIEL))
                {
                    encoded_liste=pref.getString(REF_LIST_PARTENARIAT_INDUSTRIEL, "");
                    Gson gson=new Gson();
                    Partenariat [] listp=gson.fromJson(encoded_liste, Partenariat[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Partenariat>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 2:
                if(pref.contains(REF_LIST_PARTENARIAT_TECHNOLOGIQUE))
                {
                    encoded_liste=pref.getString(REF_LIST_PARTENARIAT_TECHNOLOGIQUE, "");
                    Gson gson=new Gson();
                    Partenariat [] listp=gson.fromJson(encoded_liste, Partenariat[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Partenariat>(liste);
                }
                else
                {
                    return null;
                }
                break;
        }

        return liste;
    }
    public  static List<Projet> getListProjet(Context c,int type)
    {
        SharedPreferences pref;
        List<Projet> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);


        switch (type)
        {
            case 0:
                if(pref.contains(REF_LIST_PROJET_APPEL_OFFRE))
                {
                    encoded_liste=pref.getString(REF_LIST_PROJET_APPEL_OFFRE, "");
                    Gson gson=new Gson();
                    Projet [] listp=gson.fromJson(encoded_liste, Projet[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Projet>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 1:
                if(pref.contains(REF_LIST_PROJET_FINANCEMENT))
                {
                    encoded_liste=pref.getString(REF_LIST_PROJET_FINANCEMENT, "");
                    Gson gson=new Gson();
                    Projet [] listp=gson.fromJson(encoded_liste, Projet[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Projet>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 2:
                if(pref.contains(REF_LIST_PROJET_DEVELOPPEMENT))
                {
                    encoded_liste=pref.getString(REF_LIST_PROJET_DEVELOPPEMENT, "");
                    Gson gson=new Gson();
                    Projet [] listp=gson.fromJson(encoded_liste, Projet[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Projet>(liste);
                }
                else
                {
                    return null;
                }
                break;

        }
        return liste;
    }
    public  static List<ProductSale> getListproductSale(Context c )
    {
        SharedPreferences pref;
        List<ProductSale> liste= new ArrayList<ProductSale>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        if(pref.contains(REF_LIST_PRODUCT_SALE))
        {
            encoded_liste=pref.getString(REF_LIST_PRODUCT_SALE, "");
            Gson gson=new Gson();
            ProductSale [] listp=gson.fromJson(encoded_liste, ProductSale[].class);
            liste=Arrays.asList(listp);
            liste=new ArrayList<ProductSale>(liste);
        }
        else
        {
            return null;
        }
        return  liste;
    }
    public  static List<Produit> getListProduit(Context c,int type)
    {
        SharedPreferences pref;
        List<Produit> liste= new ArrayList<Produit>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        switch (type) {

            case 0:
                if(pref.contains(REF_LIST_PRODUIT_STOQUE_LIMITE))
                {
                    encoded_liste=pref.getString(REF_LIST_PRODUIT_STOQUE_LIMITE, "");
                    Gson gson=new Gson();
                    Produit [] listp=gson.fromJson(encoded_liste, Produit[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Produit>(liste);
                }
                else
                {
                    return null;
                }

                break;

            case 1:
                if(pref.contains(REF_LIST_PRODUIT_AUTOMOBILE))
                {
                    encoded_liste=pref.getString(REF_LIST_PRODUIT_AUTOMOBILE, "");
                    Gson gson=new Gson();
                    Produit [] listp=gson.fromJson(encoded_liste, Produit[].class);
                    liste= Arrays.asList(listp);
                    liste=new ArrayList<Produit>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 2:
                if(pref.contains(REF_LIST_PRODUIT_IMMOBILIER))
                {
                    encoded_liste=pref.getString(REF_LIST_PRODUIT_IMMOBILIER, "");
                    Gson gson=new Gson();
                    Produit [] listp=gson.fromJson(encoded_liste, Produit[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Produit>(liste);
                }
                else
                {
                    return null;
                }
                break;


            case 3:
                if(pref.contains(REF_LIST_LOCATION_IMMOBILIER))
                {
                    encoded_liste=pref.getString(REF_LIST_LOCATION_IMMOBILIER, "");
                    Gson gson=new Gson();
                    Produit [] listp=gson.fromJson(encoded_liste, Produit[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Produit>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 4:
                if(pref.contains(REF_LIST_LOCATION_AUTOMOBILE))
                {
                    encoded_liste=pref.getString(REF_LIST_LOCATION_AUTOMOBILE, "");
                    Gson gson=new Gson();
                    Produit [] listp=gson.fromJson(encoded_liste, Produit[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Produit>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 5:
                if(pref.contains(REF_LIST_LOCATION_AUTRE))
                {
                    encoded_liste=pref.getString(REF_LIST_LOCATION_AUTRE, "");
                    Gson gson=new Gson();
                    Produit [] listp=gson.fromJson(encoded_liste, Produit[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Produit>(liste);
                }
                else
                {
                    return null;
                }
                break;
        }

        return liste;
    }


    public  static List<Article> getList_Category_article(Context c,int type,String category)
    {
        SharedPreferences pref;
        List<Article> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);


        switch (type)
        {
            case 0:
                if(pref.contains(REF_CATEGORY_ARTICLE_1+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_ARTICLE_1+category, "");
                    Gson gson=new Gson();
                    Article [] listp=gson.fromJson(encoded_liste, Article[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Article>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 1:
                if(pref.contains(REF_CATEGORY_ARTICLE_2+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_ARTICLE_2+category, "");
                    Gson gson=new Gson();
                    Article [] listp=gson.fromJson(encoded_liste, Article[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Article>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 2:
                if(pref.contains(REF_CATEGORY_ARTICLE_3+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_ARTICLE_3+category, "");
                    Gson gson=new Gson();
                    Article [] listp=gson.fromJson(encoded_liste, Article[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Article>(liste);
                }
                else
                {
                    return null;
                }
                break;
        }

        return liste;
    }
    public  static List<Emploi> getList_Category_Emploi(Context c,String category)
    {
        SharedPreferences pref;
        List<Emploi> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);

        if(pref.contains(REF_CATEGORY_EMPLOI_OFFRE_1+category))
        {
            encoded_liste=pref.getString(REF_CATEGORY_EMPLOI_OFFRE_1+category, "");
            Gson gson=new Gson();
            Emploi [] listp=gson.fromJson(encoded_liste, Emploi[].class);
            liste=Arrays.asList(listp);
            liste=new ArrayList<Emploi>(liste);
        }
        else
        {
            return null;
        }

        return liste;
    }


    public  static List<Competence> getList_Category_Competence(Context c,int type,String category)
    {
        SharedPreferences pref;
        List<Competence> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);


        switch (type)
        {
            case 0:
                if(pref.contains(REF_CATEGORY_EMPLOI_COMPETENCE_1+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_EMPLOI_COMPETENCE_1+category, "");
                    Gson gson=new Gson();
                    Competence [] listp=gson.fromJson(encoded_liste, Competence[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Competence>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 1:
                if(pref.contains(REF_CATEGORY_EMPLOI_COMPETENCE_2+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_EMPLOI_COMPETENCE_2+category, "");
                    Gson gson=new Gson();
                    Competence [] listp=gson.fromJson(encoded_liste, Competence[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Competence>(liste);
                }
                else
                {
                    return null;
                }
                break;
        }

        return liste;
    }

    public  static List<Bourse> getList_category_Bourse(Context c,int type,String category)
    {
        SharedPreferences pref;
        List<Bourse> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);


        switch (type)
        {
            case 0:
                if(pref.contains(REF_CATEGORY_BOURSE_1+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_BOURSE_1+category, "");
                    Gson gson=new Gson();
                    Bourse [] listp=gson.fromJson(encoded_liste, Bourse[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Bourse>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 1:
                if(pref.contains(REF_CATEGORY_BOURSE_2+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_BOURSE_2+category, "");
                    Gson gson=new Gson();
                    Bourse [] listp=gson.fromJson(encoded_liste, Bourse[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Bourse>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 2:
                if(pref.contains(REF_CATEGORY_BOURSE_3+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_BOURSE_3+category, "");
                    Gson gson=new Gson();
                    Bourse [] listp=gson.fromJson(encoded_liste, Bourse[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Bourse>(liste);
                }
                else
                {
                    return null;
                }
                break;
        }

        return liste;
    }

    public  static List<Annuaire> getList_Category_Annuaire(Context c,int type,String category)
    {
        SharedPreferences pref;
        List<Annuaire> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);


        switch (type)
        {
            case 0:
                if(pref.contains(REF_CATEGORY_ANNUAIRE_1+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_ANNUAIRE_1+category, "");
                    Gson gson=new Gson();
                    Annuaire [] listp=gson.fromJson(encoded_liste, Annuaire[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Annuaire>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 1:
                if(pref.contains(REF_CATEGORY_ANNUAIRE_2+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_ANNUAIRE_2+category, "");
                    Gson gson=new Gson();
                    Annuaire [] listp=gson.fromJson(encoded_liste, Annuaire[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Annuaire>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 2:
                if(pref.contains(REF_CATEGORY_ANNUAIRE_3+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_ANNUAIRE_3+category, "");
                    Gson gson=new Gson();
                    Annuaire [] listp=gson.fromJson(encoded_liste, Annuaire[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Annuaire>(liste);
                }
                else
                {
                    return null;
                }
                break;
        }
        return liste;
    }
    public  static List<Service> getList_Category_Service(Context c,int type,String category)
    {
        SharedPreferences pref;
        List<Service> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);


        switch (type)
        {
            case 0:
                if(pref.contains(REF_CATEGORY_SERVICE_1+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_SERVICE_1+category, "");
                    Gson gson=new Gson();
                    Service [] listp=gson.fromJson(encoded_liste, Service[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Service>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 1:
                if(pref.contains(REF_CATEGORY_SERVICE_2+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_SERVICE_2+category, "");
                    Gson gson=new Gson();
                    Service [] listp=gson.fromJson(encoded_liste, Service[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Service>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 2:
                if(pref.contains(REF_CATEGORY_SERVICE_2+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_SERVICE_2+category, "");
                    Gson gson=new Gson();
                    Service [] listp=gson.fromJson(encoded_liste, Service[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Service>(liste);
                }
                else
                {
                    return null;
                }
                break;
        }

        return liste;
    }
    public  static List<Partenariat> getList_Category_Partenariat(Context c,int type,String category)
    {
        SharedPreferences pref;
        List<Partenariat> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);


        switch (type)
        {
            case 0:
                if(pref.contains(REF_CATEGORY_PARTENARIAT_1+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_PARTENARIAT_1+category, "");
                    Gson gson=new Gson();
                    Partenariat [] listp=gson.fromJson(encoded_liste, Partenariat[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Partenariat>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 1:
                if(pref.contains(REF_CATEGORY_PARTENARIAT_2+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_PARTENARIAT_2+category, "");
                    Gson gson=new Gson();
                    Partenariat [] listp=gson.fromJson(encoded_liste, Partenariat[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Partenariat>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 2:
                if(pref.contains(REF_CATEGORY_PARTENARIAT_3+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_PARTENARIAT_3+category, "");
                    Gson gson=new Gson();
                    Partenariat [] listp=gson.fromJson(encoded_liste, Partenariat[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Partenariat>(liste);
                }
                else
                {
                    return null;
                }
                break;
        }

        return liste;
    }

    public  static List<Projet> getList_Category_Projet(Context c,int type,String category)
    {
        SharedPreferences pref;
        List<Projet> liste= new ArrayList<>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);


        switch (type)
        {
            case 0:
                if(pref.contains(REF_CATEGORY_PROJET_1+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_PROJET_1+category, "");
                    Gson gson=new Gson();
                    Projet [] listp=gson.fromJson(encoded_liste, Projet[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Projet>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 1:
                if(pref.contains(REF_CATEGORY_PROJET_2+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_PROJET_2+category, "");
                    Gson gson=new Gson();
                    Projet [] listp=gson.fromJson(encoded_liste, Projet[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Projet>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 2:
                if(pref.contains(REF_CATEGORY_PROJET_3+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_PROJET_3+category, "");
                    Gson gson=new Gson();
                    Projet [] listp=gson.fromJson(encoded_liste, Projet[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Projet>(liste);
                }
                else
                {
                    return null;
                }
                break;

        }
        return liste;
    }

    public  static List<Produit> getList_Category_Produit(Context c,int type,String category)
    {
        SharedPreferences pref;
        List<Produit> liste= new ArrayList<Produit>();
        String encoded_liste="";
        pref=c.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE);
        switch (type) {

            case 0:
                if(pref.contains(REF_CATEGORY_BOUTIQUE_1+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_BOUTIQUE_1+category, "");
                    Gson gson=new Gson();
                    Produit [] listp=gson.fromJson(encoded_liste, Produit[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Produit>(liste);
                }
                else
                {
                    return null;
                }

                break;

            case 1:
                if(pref.contains(REF_CATEGORY_BOUTIQUE_2+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_BOUTIQUE_2+category, "");
                    Gson gson=new Gson();
                    Produit [] listp=gson.fromJson(encoded_liste, Produit[].class);
                    liste= Arrays.asList(listp);
                    liste=new ArrayList<Produit>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 2:
                if(pref.contains(REF_CATEGORY_BOUTIQUE_3+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_BOUTIQUE_3+category, "");
                    Gson gson=new Gson();
                    Produit [] listp=gson.fromJson(encoded_liste, Produit[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Produit>(liste);
                }
                else
                {
                    return null;
                }
                break;


            case 3:
                if(pref.contains(REF_CATEGORY_LOCATION_1+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_LOCATION_1+category, "");
                    Gson gson=new Gson();
                    Produit [] listp=gson.fromJson(encoded_liste, Produit[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Produit>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 4:
                if(pref.contains(REF_CATEGORY_LOCATION_2+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_LOCATION_2+category, "");
                    Gson gson=new Gson();
                    Produit [] listp=gson.fromJson(encoded_liste, Produit[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Produit>(liste);
                }
                else
                {
                    return null;
                }
                break;
            case 5:
                if(pref.contains(REF_CATEGORY_LOCATION_3+category))
                {
                    encoded_liste=pref.getString(REF_CATEGORY_LOCATION_3+category, "");
                    Gson gson=new Gson();
                    Produit [] listp=gson.fromJson(encoded_liste, Produit[].class);
                    liste=Arrays.asList(listp);
                    liste=new ArrayList<Produit>(liste);
                }
                else
                {
                    return null;
                }
                break;
        }

        return liste;
    }
    public static String convsertBitmapToString(Bitmap btm)
    {

        ByteArrayOutputStream out=new ByteArrayOutputStream();
        btm.compress(Bitmap.CompressFormat.PNG, 100, out);
        byte [] image=out.toByteArray();
        String  bmpString= Base64.encodeToString(image, Base64.DEFAULT);
        return bmpString;

    }

    public static Bitmap convertStringToBitmap(String btmString)
    {
        byte [] image=Base64.decode(btmString, Base64.DEFAULT);
        Bitmap bmp= BitmapFactory.decodeByteArray(image, 0, image.length);

        return bmp;

    }
}
