package com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils;

/**
 * Created by joel on 15/08/2017.
 */
public class PushType{
    private  String type;
    private  int count;

    public PushType() {
    }



    public PushType(String type, int count) {
        this.type = type;
        this.count = count;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }
}
