package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.GalleryAdateur;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;

import java.io.File;
import java.util.ArrayList;

public class Activity_Gallery2 extends BaseLanguageActivity implements AdapterView.OnItemClickListener {
    GalleryAdateur adapter;
    GridView gridPhoto;
    ArrayList<String> listPhotoPath=new ArrayList<String>();
    TextView t;
    Toolbar toolbar;
    ProgressDialog dialog;
    IntentFilter filter;
    PhotoReceiver receiver;
    public static final  String projection []= {MediaStore.Images.Media.DATA};

    class   PhotoReceiver  extends BroadcastReceiver
    {



        @Override
        public void onReceive(Context context, Intent intent) {


            if(intent.getAction().equals(Intent.ACTION_MEDIA_MOUNTED))
            {

                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.GINGERBREAD_MR1 && Build.VERSION.SDK_INT<Build.VERSION_CODES.HONEYCOMB )
                {


                    dialog=ProgressDialog.show(context,"",""+getResources().getString(R.string.activity_gallery_load));
                    dialog.show();




                }
                else if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB && Build.VERSION.SDK_INT<Build.VERSION_CODES.KITKAT)

                {
                    dialog=ProgressDialog.show(context,"",""+getResources().getString(R.string.activity_gallery_load));
                    dialog.show();


                }
                else if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT)
                {

                    dialog=ProgressDialog.show(context,"",""+getResources().getString(R.string.activity_gallery_load));
                    dialog.show();


                }



            }


            else if(intent.getAction().equals(Intent.ACTION_MEDIA_UNMOUNTED))
            {

                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.GINGERBREAD_MR1 && Build.VERSION.SDK_INT<Build.VERSION_CODES.HONEYCOMB )
                {



                    if(listPhotoPath!=null)
                    {

                        if(adapter!=null)
                        {

                            listPhotoPath.clear();
                            adapter.notifyDataSetChanged();
                        }

                    }
                    if(dialog!=null)
                    {
                        dialog.dismiss();
                    }


                }
                else if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB && Build.VERSION.SDK_INT<Build.VERSION_CODES.KITKAT)

                {
                    dialog=ProgressDialog.show(context,"",""+getResources().getString(R.string.activity_gallery_load));
                    dialog.show();




                    if(ContextCompat.checkSelfPermission(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                    {

                        if(ActivityCompat.shouldShowRequestPermissionRationale(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                        {
                            ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);

                        }
                        else
                        {

                            ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);


                        }



                    }
                    else
                    {
                        listImage();
                    }

                    if(dialog!=null)
                    {
                        dialog.dismiss();
                    }

                }
                else if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT)
                {


                    dialog=ProgressDialog.show(context,"",""+getResources().getString(R.string.activity_gallery_load));
                    dialog.show();


                    if(ContextCompat.checkSelfPermission(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                    {

                        if(ActivityCompat.shouldShowRequestPermissionRationale(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                        {
                            ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);

                        }
                        else
                        {

                            ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);


                        }



                    }
                    else
                    {
                        listImage();
                    }

                    if(dialog!=null)
                    {
                        dialog.dismiss();
                    }
                }

            }

            else if(intent.getAction().equals(Intent.ACTION_MEDIA_SCANNER_FINISHED))
            {
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.GINGERBREAD_MR1 && Build.VERSION.SDK_INT<Build.VERSION_CODES.HONEYCOMB )
                {
                    if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)||  Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY))

                    {
                        if(ContextCompat.checkSelfPermission(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                        {

                            if(ActivityCompat.shouldShowRequestPermissionRationale(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                            {
                                ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);

                            }
                            else
                            {

                                ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);


                            }



                        }
                        else
                        {
                            listImage();
                        }
                        if(dialog!=null)
                        {
                            dialog.dismiss();
                        }
                    }

                    else
                    {
                        LayoutInflater inflater=  LayoutInflater.from(context);
                        View v=inflater.inflate(R.layout.activity_gallery_layout,null);
                        TextView t=(TextView)v.findViewById(android.R.id.empty);
                        t.setText(""+getResources().getString(R.string.activity_gallery_installez_sc));
                        if(dialog!=null)
                        {
                            dialog.dismiss();
                        }

                        //inseer Votre Carte SD

                    }
                }
                else if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB && Build.VERSION.SDK_INT<Build.VERSION_CODES.KITKAT)

                {

                    if(ContextCompat.checkSelfPermission(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                    {

                        if(ActivityCompat.shouldShowRequestPermissionRationale(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                        {
                            ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);

                        }
                        else
                        {

                            ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);


                        }



                    }
                    else
                    {
                        listImage();
                    }
                    if(dialog!=null)
                    {
                        dialog.dismiss();
                    }

                }
                else if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT)
                {
                    if(ContextCompat.checkSelfPermission(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                    {

                        if(ActivityCompat.shouldShowRequestPermissionRationale(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                        {
                            ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);

                        }
                        else
                        {

                            ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);


                        }



                    }
                    else
                    {
                        listImage();
                    }
                    if(dialog!=null)
                    {
                        dialog.dismiss();
                    }
                }

            }



            else if(intent.getAction().equals(Intent.ACTION_MEDIA_SHARED))
            {

                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.GINGERBREAD_MR1 && Build.VERSION.SDK_INT<Build.VERSION_CODES.HONEYCOMB )
                {
                    LayoutInflater inflater=  LayoutInflater.from(context);
                    View v=inflater.inflate(R.layout.activity_gallery_layout,null);
                    TextView t=(TextView)v.findViewById(android.R.id.empty);
                    t.setText(""+getResources().getString(R.string.activity_gallery_insdip));

                    if(listPhotoPath!=null)
                    {
                        if(adapter!=null)
                        {

                            listPhotoPath.clear();
                            adapter.notifyDataSetChanged();
                        }
                        else
                        {

                        }

                    }


                }
                else if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB && Build.VERSION.SDK_INT<Build.VERSION_CODES.KITKAT)

                {
                    if(ContextCompat.checkSelfPermission(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                    {

                        if(ActivityCompat.shouldShowRequestPermissionRationale(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                        {
                            ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);

                        }
                        else
                        {

                            ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);


                        }



                    }
                    else
                    {
                        listImage();
                    }
                    if(dialog!=null)
                    {
                        dialog.dismiss();

                    }

                }
                else if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT)
                {


                    if(ContextCompat.checkSelfPermission(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                    {

                        if(ActivityCompat.shouldShowRequestPermissionRationale(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                        {
                            ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);

                        }
                        else
                        {

                            ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);


                        }



                    }
                    else
                    {
                        listImage();

                    }
                    if(dialog!=null)
                    {
                        dialog.dismiss();

                    }


                }


            }









        }


    }
    private boolean is_fr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_layout);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        gridPhoto=(GridView)findViewById(R.id.gridphoto);
        t=(TextView)findViewById(android.R.id.empty);
        toolbar=(Toolbar)findViewById(R.id.gallery_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        gridPhoto.setEmptyView(t);
        gridPhoto.setOnItemClickListener(this);

        dialog=new ProgressDialog(this);
        dialog.setMessage(""+getResources().getString(R.string.activity_gallery_load));
        dialog.show();

        receiver=new PhotoReceiver();
        filter=new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_REMOVED);
        filter.addAction(Intent.ACTION_MEDIA_SCANNER_FINISHED);
        filter.addAction(Intent.ACTION_MEDIA_SCANNER_STARTED);
        filter.addAction(Intent.ACTION_MEDIA_SHARED);
        filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_EJECT);
        filter.addDataScheme("file");


    }

    @Override
    protected void onStart() {
        super.onStart();


        if(ContextCompat.checkSelfPermission(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
        {

            if(ActivityCompat.shouldShowRequestPermissionRationale(Activity_Gallery2.this, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            {
                ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);

            }
            else
            {

                ActivityCompat.requestPermissions(Activity_Gallery2.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);


            }



        }
        else
        {
            getAllImages();
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode)
        {
            case 100:
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    getAllImages();
                }
                else
                {
                    MyToast.show(this,""+getResources().getString(R.string.activity_gallery_no_img_gallerie), true);
                }
                if(dialog!=null)
                    dialog.dismiss();
                return;



        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(!isFinishing())
        {
            Intent  intent=new Intent(this,BottomChatPreviewImage.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
            startActivity(intent);
            ((MyApplication)getApplicationContext()).setBottomChatSelectedImage_path(listPhotoPath.get(position));

            setResult(RESULT_OK);
            finish();

        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    private void getAllImages()
    {

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.GINGERBREAD_MR1 && Build.VERSION.SDK_INT<Build.VERSION_CODES.HONEYCOMB )
        {
            if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)||  Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY))

            {
                listPhotoPath=new ArrayList<>();
                listImage();
            }

            else
            {
                //inseer Votre Carte SD
                if(listPhotoPath!=null)
                {
                    if(adapter!=null)
                    {
                        listPhotoPath.clear();
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }
        else if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB && Build.VERSION.SDK_INT<Build.VERSION_CODES.KITKAT)

        {
            listPhotoPath=new ArrayList<>();
            listImage();

        }
        else if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT)
        {
            listPhotoPath=new ArrayList<>();
            listImage();

        }



        if(dialog!=null)
            dialog.dismiss();


    }


    private void listImage()
    {

        Cursor c=getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, null);
        if(c!=null)
        {
            if(c.moveToFirst())
            {
                listPhotoPath=new ArrayList<>();
                do {
                    String path=c.getString(0);

                    if (!path.equals(""))
                    {
                        File f=new File(path);
                        if(f.exists() && f.canRead() &&!f.isHidden())
                            listPhotoPath.add(path);
                    }
                }
                while (c.moveToNext());
                c.close();
                adapter=new GalleryAdateur(listPhotoPath);
                gridPhoto.setAdapter(adapter);

            }
            else
            {
                t.setText(getResources().getString(R.string.activity_gallery_no_img));

                if(listPhotoPath!=null)
                {
                    if(adapter!=null)
                    {
                        listPhotoPath.clear();
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }
        else
        {

            LayoutInflater inflater=  LayoutInflater.from(this);
            View v=inflater.inflate(R.layout.activity_gallery_layout,null);
            TextView t=(TextView)v.findViewById(android.R.id.empty);
            t.setText(""+getResources().getString(R.string.activity_gallery_no_data));

            if(listPhotoPath!=null)
            {
                if(adapter!=null)
                {
                    listPhotoPath.clear();
                    adapter.notifyDataSetChanged();
                }

            }


        }



    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

}
