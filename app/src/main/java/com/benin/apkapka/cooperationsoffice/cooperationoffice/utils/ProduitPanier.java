package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

/**
 * Created by joel on 22/02/2017.
 */
public class ProduitPanier {

    String title;
    String price;
    String reduction;
    String numero;
    public ProduitPanier()
    {

    }
    public ProduitPanier(String title,String price,String reduction,String numero)
    {
       this.title=title;
       this.price=price;
       this.reduction=reduction;
       this.numero=numero;
    }
    public String getPrice() {
        return price;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTitle() {
        return title;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReduction() {
        return reduction;
    }

    public void setReduction(String reduction) {
        this.reduction = reduction;
    }
}
