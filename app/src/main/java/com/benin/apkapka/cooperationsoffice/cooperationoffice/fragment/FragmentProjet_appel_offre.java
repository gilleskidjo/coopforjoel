package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.BusEventFragmentNetwokRequest1;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.SearchEvent1;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Formulaire_Projet;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Un_produit_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Un_projet_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.AccueilInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushFamille;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushFamilleAsyncLoader;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.EndlessRecyclerViewScrollListener;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.HelperActivity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ListDataHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MydateUtil;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ProduitDatabaseRelation;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Projet;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;


/**
 * Created by joel on 06/08/2016.
 */
public class FragmentProjet_appel_offre extends Fragment implements AccueilInterface,FragmentForNetworkRequestStock.HandlerNetWorkRequestResponse,BusEventFragmentNetwokRequest1.HandlerNetWorkRequestResponse,LoaderManager.LoaderCallbacks<List<PushFamille>> {
    RecyclerView liste;
    ProgressBar progressBar;
    private  int animate;
    MyAdapteurProjet  adapteur;
    List<Produit> listP=new ArrayList<>();
    List<PushFamille> familles=new ArrayList<PushFamille>();
    PushFamilleAsyncLoader loader;
    LinearLayoutManager manager;

    TextView vide;
    View linearvide;
    FragmentForNetworkRequestStock netWork;
    private boolean istablette;
    private boolean is_fr=false;
    String payss_code [];
    String pays="tout";
    SwipeRefreshLayout swipe;
    BusEventFragmentNetwokRequest1 net;
    boolean is_small,is_medium,is_large;
    boolean is_search;
    Box<ProduitDatabaseRelation> databaseRelationBox;
    ProduitDatabaseRelation produitDatabaseRelation;
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private  SessionManager sessionSaverData;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        is_small=getResources().getBoolean(R.bool.is_small);
        is_medium=getResources().getBoolean(R.bool.is_medium);
        is_large=getResources().getBoolean(R.bool.is_large);
        sessionSaverData = new SessionManager(getContext());
        payss_code=getResources().getStringArray(R.array.pays_code);
        View v = inflater.inflate(R.layout.fragment_projet_appel_offre, container, false);
        swipe=(SwipeRefreshLayout)v.findViewById(R.id.fragment_projet_appel_offre_swipe);
        swipe.setRefreshing(false);
        swipe.setColorSchemeColors(Color.BLUE,Color.GREEN,Color.RED,Color.YELLOW);

        liste=(RecyclerView)v.findViewById(R.id.fragment_projet_appel_offre_RecycleView);
        progressBar=(ProgressBar)v.findViewById(R.id.fragment_projet_appel_offre_Progress);
        linearvide=v.findViewById(R.id.fragment_projet_appel_offre_linearvide);
        vide=(TextView)v.findViewById(R.id.fragment_projet_appel_offre_textview_vide);
        BoxStore boxStore=((MyApplication) getActivity().getApplicationContext()).getBoxStore();
        databaseRelationBox=boxStore.boxFor(ProduitDatabaseRelation.class);
        produitDatabaseRelation=new ProduitDatabaseRelation(25);
        animate=getResources().getInteger(android.R.integer.config_shortAnimTime);
        liste.setVisibility(View.GONE);
        manager=new LinearLayoutManager(getActivity());
        liste.setLayoutManager(manager);
        adapteur=new MyAdapteurProjet(listP,familles,this,getActivity());
        endlessRecyclerViewScrollListener=new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(page);
            }
        };
        liste.addOnScrollListener(endlessRecyclerViewScrollListener);
        FragmentManager fm=getFragmentManager();
        net=(BusEventFragmentNetwokRequest1) fm.findFragmentByTag(BusEventFragmentNetwokRequest1.TAG);
        if(net==null)
        {
            net=new BusEventFragmentNetwokRequest1();
            fm.beginTransaction().add(net,BusEventFragmentNetwokRequest1.TAG).commit();
        }
        else
        {
            if(net.isHasRequest())
            {
                net.setInterface(this);
                swipe.setRefreshing(true);
            }
        }
        netWork=(FragmentForNetworkRequestStock)fm.findFragmentByTag(FragmentForNetworkRequestStock.TAG);
        if(netWork==null)
        {

            netWork=new FragmentForNetworkRequestStock();
            fm.beginTransaction().add(netWork,FragmentForNetworkRequestStock.TAG).commit();
            netWork.setInterface(this);

            if(isConnect()){
               loadNextDataFromApi(0);
            }
            else {


                ProduitDatabaseRelation relation=databaseRelationBox.get(25);
                if(relation!=null){
                    listP=relation.list;
                }
                if(listP!=null && listP.size()>0){
                    adapteur=new MyAdapteurProjet(listP,familles,FragmentProjet_appel_offre.this,getActivity());
                    liste.setAdapter(adapteur);
                    liste.setVisibility(View.VISIBLE);
                    linearvide.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                }
                else {
                    progressBar.setVisibility(View.GONE);
                    vide.setText(getResources().getString(R.string.error_frag_netw_parse));
                    liste.setVisibility(View.GONE);
                    linearvide.setVisibility(View.VISIBLE);
                }
            }

        }
        else
        {
           if(isConnect()){
               netWork.setInterface(this);
               if(net.isEmpty() || netWork.isEmpty){
                   vide.setText(netWork.getSms());
                   if(net.isReady() && net.getSms()!=null)
                   {
                       vide.setText(net.getSms());
                   }
                   liste.setVisibility(View.GONE);
                   linearvide.setVisibility(View.VISIBLE);
                   progressBar.setVisibility(View.GONE);
               }
               else {
                   listP=netWork.getListproduit();
                   if(net.isReady()){
                       listP=net.getListproduit();
                   }
                   if(listP!=null && listP.size()>0)
                   {
                       adapteur=new MyAdapteurProjet(listP,familles,this,getActivity());
                       liste.setAdapter(adapteur);
                       progressBar.setVisibility(View.GONE);
                       linearvide.setVisibility(View.GONE);
                       liste.setVisibility(View.VISIBLE);

                   }
                   else
                   {

                       if(netWork.isMessage())
                       {
                           progressBar.setVisibility(View.GONE);
                           vide.setText(getResources().getString(R.string.error_frag_netw_parse));
                           liste.setVisibility(View.GONE);
                           linearvide.setVisibility(View.VISIBLE);
                       }
                   }

               }

           }
           else {
               ProduitDatabaseRelation relation=databaseRelationBox.get(25);
               if(relation!=null){
                   listP=relation.list;
               }
               if(listP!=null && listP.size()>0){
                   adapteur=new MyAdapteurProjet(listP,familles,FragmentProjet_appel_offre.this,getActivity());
                   liste.setAdapter(adapteur);
                   liste.setVisibility(View.VISIBLE);
                   linearvide.setVisibility(View.GONE);
                   progressBar.setVisibility(View.GONE);
               }
               else {
                   progressBar.setVisibility(View.GONE);
                   vide.setText(getResources().getString(R.string.error_frag_netw_parse));
                   liste.setVisibility(View.GONE);
                   linearvide.setVisibility(View.VISIBLE);
               }
           }
        }

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                swipe.setRefreshing(false);
            }
        });

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(PushHandler.FRAGMENT1,null,this);
    }
    public void loadNextDataFromApi(int offset) {
        // Send an API request to retrieve appropriate paginated data
        //  --> Send the request including an offset value (i.e `page`) as a query parameter.
        //  --> Deserialize and construct new model objects from the API response
        //  --> Append the new data objects to the existing set of items inside the array of items
        //  --> Notify the adapter of the new items made with `notifyItemRangeInserted()
        //
        if(isConnect()){

            SessionManager sessionManager=new SessionManager(getActivity());
            User user=sessionManager.getUser(getActivity());
            String zone="";
            zone=user.getPays();
            if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
                zone="france";
            }
            netWork.doRequestProduit_Group_By_Famille("appel_offre",pays,is_fr==true?"fr":"en","projet",zone,offset);
        }
    }
    @Override
    public Loader<List<PushFamille>> onCreateLoader(int id, Bundle args) {
        loader=new PushFamilleAsyncLoader(getContext(),PushHandler.type9,PushHandler.PRO_OFFRE);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<PushFamille>> loader, List<PushFamille> data) {
        if(data!=null ){
            familles=data;
            adapteur=new MyAdapteurProjet(listP,familles,this,getActivity());
            liste.setAdapter(adapteur);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<PushFamille>> loader) {

    }

    @Override
    public void onResposeSuccess() {
        if(net.isReady())
        {


            if(is_search){
                is_search=false;
                if(net.getListproduit()!=null && net.getListproduit().size()>0){
                    if(net.getListproduit()!=null && net.getListproduit().size()==1){
                        Intent intent =new Intent(getActivity(),Un_projet_activity.class);
                        Produit  p=net.getListproduit().get(0);
                        HelperActivity.getInstance().setProduit(p);
                        intent.putExtra("id",p.getId());

                        intent.putExtra("reduction",p.getReduction());

                        intent.putExtra("lien_catalogue",p.getLien_catalogue());
                        intent.putExtra("type",p.getType());
                        intent.putExtra("lien_description",p.getLien_description());
                        intent.putExtra("choixf",p.getChoixf());
                        intent.putExtra("url",p.getUrl());
                        intent.putExtra("nb_price_en",p.getNb_price_en().toString());
                        intent.putExtra("url2",p.getUrl2());
                        intent.putExtra("from_co_direct","oui");
                        intent.putExtra("url3",p.getUrl3());
                        intent.putExtra("famille",p.getFamille());
                        intent.putExtra("pays",p.getPays());
                        intent.putExtra("category",p.getCategory());
                        intent.putExtra("type2",0);
                        intent.putExtra("date",p.getDate());
                        intent.putExtra("famille_en",p.getFamille_en());
                        intent.putExtra("lien_fichier",p.getLien_fichier());
                        startActivity(intent);

                    }
                    else {
                        listP.clear();
                        listP.addAll(net.getListproduit());
                        endlessRecyclerViewScrollListener.resetState();
                    }

                }
            }
            else{
                listP.clear();
                listP.addAll(net.getListproduit());
                endlessRecyclerViewScrollListener.resetState();
                if(listP!=null && listP.size()>0){


                    try {
                        ProduitDatabaseRelation relation=new ProduitDatabaseRelation(25);
                        databaseRelationBox.remove(relation);
                        databaseRelationBox.attach(relation);
                        relation.list.addAll(listP);
                        databaseRelationBox.put(relation);
                    }
                    finally {

                    }
                }
            }
        }
        else
        {
            listP.addAll(netWork.getListproduit());
            if(listP!=null && listP.size()>0){


                try {
                    ProduitDatabaseRelation relation=new ProduitDatabaseRelation(25);
                    databaseRelationBox.remove(relation);
                    databaseRelationBox.attach(relation);
                    relation.list.addAll(listP);
                    databaseRelationBox.put(relation);
                }
                finally {

                }
            }
        }
        adapteur.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
        linearvide.setVisibility(View.GONE);
        liste.setVisibility(View.VISIBLE);

        if(net.isHasRequest())
        {
            swipe.setRefreshing(false);
            net.setHasRequest(false);
        }

    }

    @Override
    public void onresponseFailAndPrintErrorResponse() {
        is_search=false;
        if(net.isEmpty() || netWork.isEmpty){
           if(endlessRecyclerViewScrollListener.currentPage<1){
               vide.setText(netWork.getSms());
               if(net.isHasRequest() && net.getSms()!=null)
               {
                   vide.setText(net.getSms());
               }
               liste.setVisibility(View.GONE);
               linearvide.setVisibility(View.VISIBLE);
               progressBar.setVisibility(View.GONE);
           }
        }
        else{


            ProduitDatabaseRelation relation=databaseRelationBox.get(25);
            if(relation!=null){
                listP=relation.list;
            }
            if(listP!=null && listP.size()>0){
                adapteur=new MyAdapteurProjet(listP,familles,FragmentProjet_appel_offre.this,getActivity());
                liste.setAdapter(adapteur);
                liste.setVisibility(View.VISIBLE);
                linearvide.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            else {
                progressBar.setVisibility(View.GONE);
                vide.setText(getResources().getString(R.string.error_frag_netw_parse));
                liste.setVisibility(View.GONE);
                linearvide.setVisibility(View.VISIBLE);
            }
        }
        if(net.isHasRequest())
        {
            swipe.setRefreshing(false);
            net.setHasRequest(false);
        }
    }

    private boolean isConnect()
    {
        ConnectivityManager cm=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        if(info!=null&& info.isConnected() )
            return true;


        return false;
    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }
    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
    @Subscribe
    public void onEvent(SearchEvent1 event)
    {
        SessionManager sessionManager=new SessionManager(getActivity());
        User user=sessionManager.getUser(getActivity());
        String zone="";
        zone=user.getPays();
        if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
            zone="france";
        }
        net.setReady(true);
        swipe.setRefreshing(true);
        switch (event.getType())
        {
            case 1 :
                liste.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                linearvide.setVisibility(View.GONE);
                net.doRequestProduit_Group_By_Famille("appel_offre",event.getQuery(),is_fr==true?"fr":"en","projet",zone,0);
                net.setInterface(this);
                net.setHasRequest(true);
                pays=event.getQuery();
                break;
            case 2:
                is_search=true;
                net.doRequestProduit_Search("appel_offre",event.getPays(),event.getQuery(),is_fr==true?"fr":"en","projet",zone);
                net.setInterface(this);
                net.setHasRequest(true);
                break;
        }

    }
    @Override
    public void itemClicked(int position) {

            Intent intent =new Intent(getActivity(),Un_projet_activity.class);
           Produit p=listP.get(position);
        HelperActivity.getInstance().setProduit(p);
        intent.putExtra("id",p.getId());
            intent.putExtra("reduction",p.getReduction());
            intent.putExtra("lien_catalogue",p.getLien_catalogue());
            intent.putExtra("type",p.getType());
            intent.putExtra("lien_description",p.getLien_description());
            intent.putExtra("choixf",p.getChoixf());
            intent.putExtra("url",p.getUrl());
            intent.putExtra("nb_price_en",p.getNb_price_en().toString());
            intent.putExtra("url2",p.getUrl2());
            intent.putExtra("url3",p.getUrl3());
            intent.putExtra("famille",p.getFamille());
            intent.putExtra("pays",p.getPays());
            intent.putExtra("category",p.getCategory());
        intent.putExtra("from_co_direct","oui");
            intent.putExtra("type2",0);
            intent.putExtra("date",p.getDate());
            intent.putExtra("famille_en",p.getFamille_en());
        intent.putExtra("lien_fichier",p.getLien_fichier());
            startActivity(intent);


    }

    @Override
    public void onPause() {
        super.onPause();

    }

    class  MyAdapteurProjet extends  RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        public static final int VIEW_TYPE_UN = 0;
        public static final int VIEW_TYPE_TWO = 1;
        private int lastPosition = -1;

        List<Produit> produitList;
        List<PushFamille> familleList;
         AccueilInterface accueilInterface;
        Context c;
        Produit p;
        PushFamille f;
        public MyAdapteurProjet(List<Produit> produitList,List<PushFamille> familleList,AccueilInterface accueilInterface,Context c)
        {

            this.produitList=produitList;
            this.familleList=familleList;
            this.accueilInterface=accueilInterface;
            this.c=c;
        }
        public   class ViewHolder extends RecyclerView.ViewHolder
        {
            TextView tvCount,txtFamille;
            TextView tvDate;
            TextView title;
            TextView description;
            DinamicImageView image;
            TextView txtCountFamille;
            private boolean isNewFamille;
            LinearLayout parent;
            public ViewHolder(View v)
            {
                super(v);
                tvCount=(TextView)v.findViewById(R.id.projet_item_layout_Count);
                tvDate=(TextView)v.findViewById(R.id.projet_item_layout_date);
                title=(TextView)v.findViewById(R.id.projet_item_layout_title);
                description=(TextView)v.findViewById(R.id.projet_item_layout_description);
                image=(DinamicImageView) v.findViewById(R.id.projet_item_layout_image);
                txtFamille=(TextView)v.findViewById(R.id.famille_name);
                txtCountFamille=(TextView)v.findViewById(R.id.famille_count);
                parent=v.findViewById(R.id.parent);
            }
        }
        public class PrincipaleViewHolder  extends RecyclerView.ViewHolder
        {
            TextView  titlePrinciale,tvCount,tvDate,txtFamille;
            DinamicImageView imagePrincipale;
            ProgressBar  bar;
            TextView txtCountFamille;
            private boolean isNewFamille;
            public  PrincipaleViewHolder(View  v)
            {
                super(v);
                titlePrinciale=(TextView)v.findViewById(R.id.actualite_title_principale);
                tvCount=(TextView)v.findViewById(R.id.actualite_Count);
                tvDate=(TextView)v.findViewById(R.id.actualite_date);
                imagePrincipale=(DinamicImageView) v.findViewById(R.id.actualite_image_principale);
                bar=(ProgressBar)v.findViewById(R.id.actualite_progressBar1);
                txtFamille=(TextView)v.findViewById(R.id.famille_name);
                txtCountFamille=(TextView)v.findViewById(R.id.famille_count);
            }

        }

        @Override
        public int getItemCount() {
            return produitList.size();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            switch (viewType)
            {
                case VIEW_TYPE_UN:

                    View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.projet_la_une_layout,parent,false);
                    final PrincipaleViewHolder holder=new PrincipaleViewHolder(v);
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            accueilInterface.itemClicked(holder.getAdapterPosition());
                        }
                    });
                    return holder;

                case  VIEW_TYPE_TWO:
                    View v1=LayoutInflater.from(parent.getContext()).inflate(R.layout.projet_item_layout,parent,false);
                    final ViewHolder holder1=new ViewHolder(v1);
                    v1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            accueilInterface.itemClicked(holder1.getAdapterPosition());
                        }
                    });
                    return holder1;
            }
            return null;



        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            p=produitList.get(position);


            Predicate<PushFamille> predicate=new Predicate<PushFamille>() {
                @Override
                public boolean apply(@javax.annotation.Nullable PushFamille input) {

                    return input.getFamille().equalsIgnoreCase(p.getFamille());
                }
            };
            if(familleList.size()>0){
                List<PushFamille> familles= Lists.newArrayList(Iterables.filter(familleList,predicate));
                if(familles!=null && familles.size()>0){
                    f=familles.get(0);
                    if(f!=null && f.getCount()>0)
                    {
                        p.setCount(f.getCount());
                    }

                }
                else{
                    p.setCount(0);
                }

            }
            else{
                p.setCount(0);
            }

            if(holder instanceof PrincipaleViewHolder)
            {
                PrincipaleViewHolder holder1=(PrincipaleViewHolder)holder;
                setUpMainPrincipaleHoder(holder1,produitList.get(position));
            }
            else
            {
                ViewHolder holder2=(ViewHolder)holder;
                setUpListViewHolder(holder2,produitList.get(position));
                Config.setAnimation(holder2.parent,position,lastPosition,c);
            }





        }




        public void setUpMainPrincipaleHoder(final PrincipaleViewHolder holder, Produit article)
        {
            if(article.getNb_vue_famille()!=null && !article.getNb_vue_famille().trim().equalsIgnoreCase("") && !article.getNb_vue_famille().equalsIgnoreCase("null"))
            {
                holder.txtCountFamille.setText(article.getNb_vue_famille());
            }
            else{
                holder.txtCountFamille.setText("0");
            }
            holder.txtFamille.setText(article.getFamille().trim().toUpperCase());
            if(article.getCount()>0)
            {
                holder.tvCount.setText(String.valueOf(article.getCount()));
                holder.tvCount.setVisibility(View.VISIBLE);
            }
            else{
                holder.tvCount.setText("");
                holder.tvCount.setVisibility(View.GONE);
            }

            if(article.getAdmin_update().equalsIgnoreCase("oui")){
                holder.tvDate.setText(article.getExtend_proprety1().trim());
                holder.tvDate.setVisibility(View.VISIBLE);
            }
            else{
                holder.tvDate.setText(article.getExtend_proprety1());
                holder.tvDate.setVisibility(View.VISIBLE);
            }
            holder.titlePrinciale.setText(article.getTitle().trim());

            DisplayMetrics metrics=c.getResources().getDisplayMetrics();
            int deviceWidth = metrics.widthPixels;
            int deviceHieght=metrics.heightPixels;

            if (sessionSaverData.getKeySaverDataProjet().equals("Oui")){
                Picasso.with(c)
                        .load(R.drawable.saverdatacoop)
                        .into(holder.imagePrincipale, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder.bar.setVisibility(View.GONE) ;
                                holder.imagePrincipale.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                holder.bar.setVisibility(View.GONE);
                                holder.imagePrincipale.setVisibility(View.VISIBLE);
                            }
                        });
            }

            if (sessionSaverData.getKeySaverDataProjet().equals("Non")){
                Picasso.with(c)
                        .load(article.getUrl())
                        .into(holder.imagePrincipale, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder.bar.setVisibility(View.GONE) ;
                                holder.imagePrincipale.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                holder.bar.setVisibility(View.GONE);
                                holder.imagePrincipale.setVisibility(View.VISIBLE);
                            }
                        });
            }
        }

        public void setUpListViewHolder(ViewHolder  holder, final Produit article)
        {
            if(article.getNb_vue_famille()!=null && !article.getNb_vue_famille().trim().equalsIgnoreCase("") && !article.getNb_vue_famille().equalsIgnoreCase("null"))
            {
                holder.txtCountFamille.setText(article.getNb_vue_famille());
            }
            else{
                holder.txtCountFamille.setText("0");
            }
            holder.txtFamille.setText(article.getFamille().trim().toUpperCase());
            Predicate<PushFamille> predicate=new Predicate<PushFamille>() {
                @Override
                public boolean apply(@javax.annotation.Nullable PushFamille input) {

                    return input.getFamille().equalsIgnoreCase(article.getFamille());
                }
            };
            if(familleList.size()>0){
                List<PushFamille> familles= Lists.newArrayList(Iterables.filter(familleList,predicate));
                if(familles!=null && familles.size()>0){
                    f=familles.get(0);
                    if(f!=null && f.getCount()>0)
                    {
                        article.setCount(f.getCount());
                    }

                }
                else {
                    article.setCount(0);
                }
            }
            else {
                article.setCount(0);
            }
            if(article.getCount()>0)
            {
                holder.tvCount.setText(String.valueOf(article.getCount()));
                holder.tvCount.setVisibility(View.VISIBLE);
            }
            else{
                holder.tvCount.setText("");
                holder.tvCount.setVisibility(View.GONE);
            }

            String des="";

            holder.title.setText(article.getTitle().trim());
            if(article.getAdmin_update().equalsIgnoreCase("oui")){
                holder.tvDate.setText(article.getExtend_proprety1().trim());
                des=article.getDescription().trim();
                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des.length()>50)
                    {
                        des=des.substring(0,50)+"...";

                    }

                }
                else
                {
                    if(des.length()>110)
                    {
                        des=des.substring(0,110)+"...";
                    }
                }
            }
            else{
                holder.tvDate.setVisibility(View.VISIBLE);
                holder.tvDate.setText(article.getExtend_proprety1());
                des=article.getDescription().trim();
                if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                {
                    if(des.length()>50)
                    {
                        des=des.substring(0,50)+"...";

                    }

                }
                else
                {
                    if(des.length()>110)
                    {
                        des=des.substring(0,110)+"...";
                    }
                }
            }



            holder.description.setText(des);
            final  float scale=getResources().getDisplayMetrics().density;
            if(is_small)
            {
                if (sessionSaverData.getKeySaverDataProjet().equals("Oui")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).resize((int)(120*scale+0.5f),(int)(100*scale+0.5f)).into(holder.image);
                }

                if(sessionSaverData.getKeySaverDataProjet().equals("Non")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).resize((int)(120*scale+0.5f),(int)(100*scale+0.5f)).into(holder.image);
                }


            }
            else if(is_medium)
            {
                if (sessionSaverData.getKeySaverDataProjet().equals("Oui")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).resize((int)(140*scale+0.5f),(int)(120*scale+0.5f)).into(holder.image);
                }

                if(sessionSaverData.getKeySaverDataProjet().equals("Non")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).resize((int)(140*scale+0.5f),(int)(120*scale+0.5f)).into(holder.image);
                }


            }
            else if (is_large)
            {
                if (sessionSaverData.getKeySaverDataProjet().equals("Oui")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).resize((int)(150*scale+0.5f),(int)(130*scale+0.5f)).into(holder.image);

                }

                if(sessionSaverData.getKeySaverDataProjet().equals("Non")){
                    Picasso.with(c).load(R.drawable.saverdatacoop).resize((int)(150*scale+0.5f),(int)(130*scale+0.5f)).into(holder.image);
                }


            }

        }

        @Override
        public int getItemViewType(int position) {
            return position==0?VIEW_TYPE_UN:VIEW_TYPE_TWO;
        }




    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
