package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.AccueilInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Divertissement;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.EndlessRecyclerViewScrollListener;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class BottomDivertissementSingleActvity extends AppCompatActivity implements  AccueilInterface {


    Toolbar toolbar;
    View linearVide;
    TextView tvVide;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    private boolean is_fr=false;
    boolean is_small,is_medium,is_large;
    MyAdapteurDivertissement myAdapteurDivertissement;
    List<Divertissement> divertissementList=new ArrayList<>();
    String famille="";
    LinearLayoutManager manager;
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private  SessionManager sessionSaverData;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottom_divertissement_single_layout);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        is_small=getResources().getBoolean(R.bool.is_small);
        is_medium=getResources().getBoolean(R.bool.is_medium);
        is_large=getResources().getBoolean(R.bool.is_large);
        toolbar=(android.support.v7.widget.Toolbar)findViewById(R.id.activity_divertissement_layout_toolbar);
        recyclerView=(RecyclerView)findViewById(R.id.divertissement_RecycleView);
        progressBar=(ProgressBar)findViewById(R.id.divertissement_Progress);
        linearVide=findViewById(R.id.divertissement_linearvide);
        tvVide=(TextView)findViewById(R.id.divertissement_textview_vide);
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        sessionSaverData = new SessionManager(this);
        actionBar.setDisplayHomeAsUpEnabled(true);
        manager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        myAdapteurDivertissement=new MyAdapteurDivertissement(divertissementList,this,this);
        recyclerView.setAdapter(myAdapteurDivertissement);
        endlessRecyclerViewScrollListener=new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(page);
            }
        };
        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

        if(getIntent().getExtras()!=null){
         Bundle bundle=getIntent().getExtras();
         famille=bundle.getString("famille");
            if(actionBar!=null){
                actionBar.setTitle(famille);
            }
        }
       loadNextDataFromApi(0);
    }
    public void loadNextDataFromApi(int offset) {
        // Send an API request to retrieve appropriate paginated data
        //  --> Send the request including an offset value (i.e `page`) as a query parameter.
        //  --> Deserialize and construct new model objects from the API response
        //  --> Append the new data objects to the existing set of items inside the array of items
        //  --> Notify the adapter of the new items made with `notifyItemRangeInserted()
        //
        SessionManager sessionManager=new SessionManager(getApplicationContext());
        User user=sessionManager.getUser(getApplicationContext());
        String zone="";
        zone=user.getPays();
        if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
            zone="france";
        }
        getALlDivertissementInFamille(zone,offset);
    }

    @Override
    public void itemClicked(int position) {
        /*Divertissement divertissement=divertissementList.get(position);
        Intent intent=new Intent(this,SpotActivity.class);
        intent.putExtra("id",divertissement.getLink_id());
        intent.putExtra("image",divertissement.getUrl());
        intent.putExtra("invitaion",is_fr?divertissement.getDescription():divertissement.getDescription_en());
        intent.putExtra("title",is_fr?divertissement.getTitle():divertissement.getTitle_en());
        intent.putExtra("type","divertissement");
        startActivity(intent);*/
    }
    public  void getALlDivertissementInFamille(String zone,int page){
        OkHttpClient client=new OkHttpClient();



        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(client)
                .build();

        MyInterface service=retrofit.create(MyInterface.class);
        Call<String> call=service.get_All_Divertissement_In_famille(famille,zone,page);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,Response<String> response) {
                if(response!=null)
                {
                    if(response.isSuccessful())
                    {
                        try {
                            String rep = response.body() != null ? response.body() : "";

                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error=o.getBoolean("error");
                            if(error==true)
                            {

                                progressBar.setVisibility(View.GONE);
                                linearVide.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                tvVide.setVisibility(View.VISIBLE);
                                tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));
                            }
                            else
                            {
                                progressBar.setVisibility(View.GONE);
                                linearVide.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                divertissementList.addAll(ParseJson.parseDivertissement(o));
                                if(divertissementList!=null && divertissementList.size()>0){
                                    myAdapteurDivertissement.notifyDataSetChanged();

                                }

                            }

                        }
                        catch(Exception e)
                        {

                            progressBar.setVisibility(View.GONE);
                            linearVide.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                            tvVide.setVisibility(View.VISIBLE);
                            tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));
                        }



                    }
                    else
                    {

                    }
                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

                progressBar.setVisibility(View.GONE);
                linearVide.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                tvVide.setVisibility(View.VISIBLE);
                tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));

            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class  MyAdapteurDivertissement extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

        List<Divertissement> divertissements;
        AccueilInterface accueilInterface;
        Context c;
        Divertissement ar;

        public MyAdapteurDivertissement(List<Divertissement> divertissements, AccueilInterface accueilInterface, Context c) {

            this.divertissements = divertissements;
            this.accueilInterface = accueilInterface;
            this.c = c;

        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v1= LayoutInflater.from(parent.getContext()).inflate(R.layout.bottom_divertissement_list_item,parent,false);
            ListViewHolder holder1=new ListViewHolder(v1);

            return holder1;


        }


        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ar=divertissements.get(position);
            ListViewHolder holder2=(ListViewHolder)holder;
            setUpListViewHolder(holder2,divertissements.get(position));
            holder2.btn.setTag(position);
            holder2.btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=(int)v.getTag();
                  Divertissement divertissement=divertissements.get(position);
                    Intent intent=new Intent(BottomDivertissementSingleActvity.this,SpotActivity.class);
                    intent.putExtra("id",divertissement.getLink_id());
                    intent.putExtra("image",divertissement.getUrl());
                    intent.putExtra("invitaion",is_fr?divertissement.getDescription():divertissement.getDescription_en());
                    intent.putExtra("title",is_fr?divertissement.getTitle():divertissement.getTitle_en());
                    intent.putExtra("type","divertissement");
                    startActivity(intent);
                }
            });
        }



        public void setUpListViewHolder(final ListViewHolder holder, final Divertissement article)
        {

            String des="";
            //holder.tvDate.setText(MydateUtil.getDate(article.getDate(),is_fr));
            //holder.tvDate.setText(is_fr?article.getExtend_proprety1().trim():article.getExtend_proprety1_en().trim());

            if(is_fr)
            {
                holder.title.setText(article.getTitle().toUpperCase().trim());
                des=article.getDescription().trim();

            }
            else
            {
                holder.title.setText(article.getTitle_en().toUpperCase().trim());
                des=article.getDescription_en().trim();

            }


            holder.description.setText(des);

            if(sessionSaverData.getKeySaverDataDivertissement().equals("Oui")){
                Picasso.with(c).load(R.drawable.saverdatacoop).into(holder.image);
            }else{
                Picasso.with(c).load(article.getUrl()).into(holder.image);
            }

            holder.switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        if(sessionSaverData.getKeySaverDataDivertissement().equals("Oui"))
                        {
                            Picasso.with(c).load(article.getUrl()).into(holder.image);
                        }
                        if(sessionSaverData.getKeySaverDataDivertissement().equals("Non")){
                            Picasso.with(c).load(R.drawable.saverdatacoop).into(holder.image);
                        }
                    }

                    if(!b){
                        if(sessionSaverData.getKeySaverDataDivertissement().equals("Non"))
                        {
                            Picasso.with(c).load(article.getUrl()).into(holder.image);

                        }
                        if(sessionSaverData.getKeySaverDataDivertissement().equals("Oui")){
                            Picasso.with(c).load(R.drawable.saverdatacoop).into(holder.image);
                        }


                    }
                }
            });

        }



        @Override
        public int getItemCount() {
            return divertissements.size();
        }


        public  class ListViewHolder extends  RecyclerView.ViewHolder
        {
            TextView  title;
            TextView description;
            DinamicImageView  image;
            Button btn;
            SwitchCompat switchCompat;

            public  ListViewHolder(View v)
            {
                super(v);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        accueilInterface.itemClicked(getAdapterPosition());
                    }
                });
                title=(TextView)v.findViewById(R.id.divertissemeny_item_titre);
                description=(TextView)v.findViewById(R.id.divertissemeny_item_description);
                image=(DinamicImageView) v.findViewById(R.id.divertissemeny_item_image);
                btn=(Button)v.findViewById(R.id.divertissemeny_item_btn);
                switchCompat = (SwitchCompat) v.findViewById(R.id.switchVitrineDivertissement);

            }


        }


    }
}

