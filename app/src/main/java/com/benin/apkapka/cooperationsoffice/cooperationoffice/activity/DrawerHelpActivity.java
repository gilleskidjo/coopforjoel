package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.app.Application;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment.FragmentForNetworkDrawerAvis;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;

/**
 * Created by joel on 19/11/2017.
 */
public class DrawerHelpActivity extends BaseLanguageActivity implements View.OnClickListener,FragmentForNetworkDrawerAvis.HandlerNetWorkRequestResponseAvis {

    EditText editText;
    Button btn;
    FragmentManager fm;
    FragmentForNetworkDrawerAvis f;
    private boolean is_fr=false;
    AlertDialog alertDialogPro;
    TextInputLayout inputLayout;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_help_layout);
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        editText=(EditText)findViewById(R.id.drawer_avis_value);
        inputLayout=(TextInputLayout)findViewById(R.id.drawer_avis_value_input);
        inputLayout.setHint("");

        btn=(Button)findViewById(R.id.drawer_avis_btn);
        btn.setOnClickListener(this);
        fm=getSupportFragmentManager();
        f=(FragmentForNetworkDrawerAvis) fm.findFragmentByTag(FragmentForNetworkDrawerAvis.TAG);
        if(f==null){
            f=new FragmentForNetworkDrawerAvis();
            fm.beginTransaction().add(f,FragmentForNetworkDrawerAvis.TAG).commit();
            f.setInterface(this);
        }
        else{
            f.setInterface(this);
        }
    }

    @Override
    public void onClick(View v) {

        if(editText.getText().toString().equals("")){
           editText.setError(is_fr?"Entrez un texte":"Enter a text");
            return;

        }

        SessionManager sm=new SessionManager(this);
        User user=sm.getUser(this);

        f.doRequestProduit_By_Category(user.getId(),editText.getText().toString(),is_fr?"fr":"en");

    }

    @Override
    public void onResposeSuccess() {

         AlertDialog.Builder  builder=new AlertDialog.Builder(this);
        builder.setMessage(is_fr?"Nous vous remercions pour votre avis, il sera transmi à l'équipe de dévéloppement. \n Bonne continuation avec Cooperations office":"We thank you for your opinion, it will be transmitted to the development team.\n" +
                " \n Good luck with Cooperations office");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialogPro.dismiss();
                finish();
            }
        });
        alertDialogPro=builder.show();
    }

    @Override
    public void onresponseFailAndPrintErrorResponse() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
