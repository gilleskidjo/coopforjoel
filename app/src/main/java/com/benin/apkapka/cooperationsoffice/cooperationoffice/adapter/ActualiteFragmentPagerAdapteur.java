package com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joel on 04/08/2016.
 */
public class ActualiteFragmentPagerAdapteur   extends FragmentStatePagerAdapter {

    List<Fragment> list=new ArrayList<>();

    String nom1,nom2,nom3;
    public ActualiteFragmentPagerAdapteur(FragmentManager fm, String nom1,String nom2,String nom3)
    {
        super(fm);
        this.nom1=nom1;
        this.nom2=nom2;
        this.nom3= nom3;


    }
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position)
        {
            case 0:
                return nom1;
            case 1:
                return  nom2;
            case 2:
                return nom3;

        }

        return super.getPageTitle(position);
    }


    @Override
    public int getCount() {
        return list.size();
    }

    public void addFragment(Fragment f)
    {
        list.add(f);
    }
    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }


}
