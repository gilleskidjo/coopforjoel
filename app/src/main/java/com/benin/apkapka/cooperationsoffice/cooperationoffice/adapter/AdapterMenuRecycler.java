package com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Activity_Actualite;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Activity_Boutique;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Activity_Location_Voiture;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Activity_Partenariat;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Activity_Service;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Annuaire;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Bourse;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Emploi;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Projet;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushTypeAsyncLoader;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MenuVitrine;

import java.util.ArrayList;
import java.util.List;

public class AdapterMenuRecycler extends RecyclerView.Adapter<AdapterMenuRecycler.Holder> {
    List<MenuVitrine> menuVitrineList = new ArrayList();
    Context context;
    PushTypeAsyncLoader loader;
    Activity mActivity;


    public AdapterMenuRecycler(Context context,List<MenuVitrine> menuVitrines)
    {
        this.menuVitrineList = menuVitrines;
        this.context = context;


    }

    @NonNull
    @Override
    public AdapterMenuRecycler.Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new AdapterMenuRecycler.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMenuRecycler.Holder holder, int i) {
       holder.bind(menuVitrineList.get(i),context);
    }


    @Override
    public int getItemCount() {
        return this.menuVitrineList.size();
    }




    public class Holder extends RecyclerView.ViewHolder  {
        TextView textmenu;
        ImageView img5;
        CardView card;
        TextView notification_tv_bloc1;
        RelativeLayout relative;
        RecyclerView recyclerView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            this.textmenu = (TextView) itemView.findViewById(R.id.textmenu);
            this.img5 = (ImageView) itemView.findViewById(R.id.img5);
            this.card=(CardView) itemView.findViewById(R.id.card);
            this.notification_tv_bloc1=itemView.findViewById(R.id.notification_tv_bloc1);
            this.relative=itemView.findViewById(R.id.relative);
            this.recyclerView=itemView.findViewById(R.id.recycler2);


        }


        public void bind(final MenuVitrine menuVitrine, final Context context)
        {
            this.textmenu.setText(menuVitrine.getLabel());
            this.img5.setImageResource(menuVitrine.getImg());


            if (menuVitrine.getNotif() !=null && !menuVitrine.getNotif().equalsIgnoreCase("") ){
                this.notification_tv_bloc1.setText(menuVitrine.getNotif());
                this.notification_tv_bloc1.setVisibility(View.VISIBLE);
            }


            this.card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Animation animation = AnimationUtils.loadAnimation(context, R.anim.main_slide_out_left);
                    card.startAnimation(animation);
                    Intent intent=new Intent();
                    if ( menuVitrine.getId()==0)
                    {
                        intent=new Intent(context, Activity_Actualite.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                    if ( menuVitrine.getId()==1)
                    {
                        intent=new Intent(context, Activity_Boutique.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                    if ( menuVitrine.getId()==2)
                    {
                        intent=new Intent(context, Emploi.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                    if ( menuVitrine.getId()==3)
                    {
                        intent=new Intent(context, Bourse.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                    if ( menuVitrine.getId()==4)
                    {
                        intent=new Intent(context, Annuaire.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                    if ( menuVitrine.getId()==5)
                    {
                        intent=new Intent(context, Activity_Service.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                    if ( menuVitrine.getId()==6)
                    {
                        intent=new Intent(context, Activity_Location_Voiture.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                    if ( menuVitrine.getId()==7)
                    {
                        intent=new Intent(context, Activity_Partenariat.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                    if ( menuVitrine.getId()==8)
                    {
                        intent=new Intent(context, Projet.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }


                }
            });

            if ( menuVitrine.getId()==0)
            {
               // this.img5.setColorFilter(ContextCompat.getColor(context,R.color.primary_dark), PorterDuff.Mode.MULTIPLY);
                this.relative.setBackgroundColor(context.getResources().getColor(R.color.primary_dark));
            }
//
            if (menuVitrine.getId()==1)
            {
                this.relative.setBackgroundColor(context.getResources().getColor(R.color.boutique_primary_dark));
               // this.img5.setColorFilter(ContextCompat.getColor(context,R.color.boutique_primary_dark), PorterDuff.Mode.MULTIPLY);
            }
//
            if (menuVitrine.getId()==2)
            {
                this.relative.setBackgroundColor(context.getResources().getColor(R.color.emploi_primary_dar));
               // this.img5.setColorFilter(ContextCompat.getColor(context,R.color.emploi_primary_dar), PorterDuff.Mode.MULTIPLY);
            }
//
//
            if (menuVitrine.getId()==3)
            {
                this.relative.setBackgroundColor(context.getResources().getColor(R.color.bourse_primary_dark));
                //this.img5.setColorFilter(ContextCompat.getColor(context,R.color.bourse_primary_dark), PorterDuff.Mode.MULTIPLY);
            }
//
            if (menuVitrine.getId()==4)
            {
                this.relative.setBackgroundColor(context.getResources().getColor(R.color.annuaire_primary_dark));
                //this.img5.setColorFilter(ContextCompat.getColor(context,R.color.annuaire_primary_dark), PorterDuff.Mode.MULTIPLY);
            }
//
            if (menuVitrine.getId()==5)
            {
                this.relative.setBackgroundColor(context.getResources().getColor(R.color.service_primary_dark));
                //this.img5.setColorFilter(ContextCompat.getColor(context,R.color.service_primary_dark), PorterDuff.Mode.MULTIPLY);
            }
//
            if (menuVitrine.getId()==6)
            {
                this.relative.setBackgroundColor(context.getResources().getColor(R.color.location_primary_dark));
               // this.img5.setColorFilter(ContextCompat.getColor(context,R.color.location_primary_dark), PorterDuff.Mode.MULTIPLY);
            }
//
            if (menuVitrine.getId()==7)
            {
                this.relative.setBackgroundColor(context.getResources().getColor(R.color.partenariat_primary_dark));
                //this.img5.setColorFilter(ContextCompat.getColor(context,R.color.partenariat_primary_dark), PorterDuff.Mode.MULTIPLY);
            }
//
            if (menuVitrine.getId()==8)
            {
                this.relative.setBackgroundColor(context.getResources().getColor(R.color.projet_primary_dark));
              //  this.img5.setColorFilter(ContextCompat.getColor(context,R.color.projet_primary_dark), PorterDuff.Mode.MULTIPLY);
            }
           // Glide.with(context).load(menuVitrine.getImg()).into(this.img5);

        }



    }
}
