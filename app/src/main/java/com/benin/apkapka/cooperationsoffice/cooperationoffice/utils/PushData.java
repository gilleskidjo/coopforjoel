package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

/**
 * Created by joel on 12/08/2017.
 */
public class PushData {

   private long id;
   private String type;
   private String category;
   private String famille;
   private long ref;
    public PushData() {
    }

    public PushData(long id, String type, String category, String famille, long ref) {
        this.id = id;
        this.type = type;
        this.category = category;
        this.famille = famille;
        this.ref = ref;
    }

    public long getRef() {
        return ref;
    }

    public long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getCategory() {
        return category;
    }

    public String getFamille() {
        return famille;
    }

    public void setRef(long ref) {
        this.ref = ref;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setFamille(String famille) {
        this.famille = famille;
    }
}
