package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Historique {
    @Id
    public long id;
   public String mHistorique;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getmHistorique() {
        return mHistorique;
    }

    public void setmHistorique(String mHistorique) {
        this.mHistorique = mHistorique;
    }
}
