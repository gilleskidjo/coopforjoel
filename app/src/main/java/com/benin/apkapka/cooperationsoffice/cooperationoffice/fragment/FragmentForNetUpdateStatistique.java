package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Annuaire;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Article;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Bourse;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Competence;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Emploi;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ListDataHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Partenariat;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Projet;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Service;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by joel on 10/10/2017.
 */
public class FragmentForNetUpdateStatistique  extends Fragment{
    public  static  final  String TAG="com.cooperation.office.fragemnr.for_network_update_statistique";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
    public void  doRequestUpdateStatistique(int id)
    {
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        MyInterface2 service=retrofit.create(MyInterface2.class);
        Call<String> call=service.updatestatistique(id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

            }
        });

    }
    public void  addSession(long id)
    {
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        MyInterface2 service=retrofit.create(MyInterface2.class);
        Call<String> call=service.add_session(id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

            }
        });

    }
    public void  doRequestUpdateStatistiqueItem(long id,String table)
    {
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        MyInterface2 service=retrofit.create(MyInterface2.class);
        Call<String> call=service.updatestatistiqueItem(id,table);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

            }
        });

    }
    public void  doRequestUpdateStatistiqueItemFamille(String table,String type,String category,String famille)
    {
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        MyInterface2 service=retrofit.create(MyInterface2.class);
        Call<String> call=service.updatestatistiqueItemFamille(table,type,category,famille);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

            }
        });

    }

}
