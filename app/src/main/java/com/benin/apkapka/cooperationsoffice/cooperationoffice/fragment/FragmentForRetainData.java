package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.AppClass;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.AppClass2;

/**
 * Created by joel on 05/09/2016.
 */
public class FragmentForRetainData extends Fragment {
    public  static  final  String TAG="com.cooperationsoffice.fragment.tag";
    AppClass c;
    AppClass2 c1;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    public AppClass getC() {
        return c;
    }

    public AppClass2 getC1() {
        return c1;
    }

    public void setC(AppClass c) {
        this.c = c;
    }

    public void setC1(AppClass2 c1) {
        this.c1 = c1;
    }
}
