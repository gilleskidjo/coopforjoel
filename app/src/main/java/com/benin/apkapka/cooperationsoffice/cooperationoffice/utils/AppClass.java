package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import java.math.BigDecimal;

/**
 * Created by joel on 05/09/2016.
 */
public class AppClass {

    private long id;
    private String title;
    private String description;
    private String extend_proprety1;
    private String  extend_proprety2;
    private String nb_share;
    private String nb_read_catalogue;
    private String url;
    private String url2;
    private String url3;
    private String pays;
    private String famille;
    private String category;
    private String type;
    private  String date;
    private String price;
    private String nb_price;
    private String nb_price_en;
    private String text_reduction;
    private String reduction;
    private String lien_catalogue;
    private  String lien_description;
    private String choixf;
    private String famille_en;
    private String lien_fichier;
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLien_catalogue() {
        return lien_catalogue;
    }

    public void setLien_catalogue(String lien_catalogue) {
        this.lien_catalogue = lien_catalogue;
    }

    public String getLien_description() {
        return lien_description;
    }

    public void setLien_description(String lien_description) {
        this.lien_description = lien_description;
    }

    public String getChoixf() {
        return choixf;
    }

    public void setChoixf(String choixf) {
        this.choixf = choixf;
    }

    public AppClass(long id, String title, String description, String extend_proprety1, String extend_proprety2, String nb_share, String nb_read_catalogue, String url, String url2, String url3, String pays, String famille, String category, String type, String date, String price, String nb_price, String nb_price_en, String text_reduction, String reduction, String lien_catalogue, String lien_description, String choixf, String famille_en, String lien_fichier) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.extend_proprety1 = extend_proprety1;
        this.extend_proprety2 = extend_proprety2;
        this.nb_share = nb_share;
        this.nb_read_catalogue = nb_read_catalogue;
        this.url = url;
        this.url2 = url2;
        this.url3 = url3;
        this.pays = pays;
        this.famille = famille;
        this.category = category;
        this.type = type;
        this.date = date;
        this.price = price;
        this.nb_price = nb_price;
        this.nb_price_en = nb_price_en;
        this.text_reduction = text_reduction;
        this.reduction = reduction;
        this.lien_catalogue = lien_catalogue;
        this.lien_description = lien_description;
        this.choixf = choixf;
        this.famille_en = famille_en;
        this.lien_fichier = lien_fichier;
    }

    public String getLien_fichier() {
        return lien_fichier;
    }

    public void setLien_fichier(String lien_fichier) {
        this.lien_fichier = lien_fichier;
    }

    public String getFamille_en() {
        return famille_en;
    }

    public void setFamille_en(String famille_en) {
        this.famille_en = famille_en;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNb_price() {
        return nb_price;
    }

    public void setNb_price(String nb_price) {
        this.nb_price = nb_price;
    }

    public String getText_reduction() {
        return text_reduction;
    }

    public void setText_reduction(String text_reduction) {
        this.text_reduction = text_reduction;
    }

    public String getReduction() {
        return reduction;
    }

    public void setReduction(String reduction) {
        this.reduction = reduction;
    }

    public String getNb_price_en() {
        return nb_price_en;
    }

    public void setNb_price_en(String nb_price_en) {
        this.nb_price_en = nb_price_en;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExtend_proprety1() {
        return extend_proprety1;
    }

    public void setExtend_proprety1(String extend_proprety1) {
        this.extend_proprety1 = extend_proprety1;
    }

    public String getExtend_proprety2() {
        return extend_proprety2;
    }

    public void setExtend_proprety2(String extend_proprety2) {
        this.extend_proprety2 = extend_proprety2;
    }

    public String getNb_share() {
        return nb_share;
    }

    public void setNb_share(String nb_share) {
        this.nb_share = nb_share;
    }

    public String getNb_read_catalogue() {
        return nb_read_catalogue;
    }

    public void setNb_read_catalogue(String nb_read_catalogue) {
        this.nb_read_catalogue = nb_read_catalogue;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl2() {
        return url2;
    }

    public void setUrl2(String url2) {
        this.url2 = url2;
    }

    public String getUrl3() {
        return url3;
    }

    public void setUrl3(String url3) {
        this.url3 = url3;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getFamille() {
        return famille;
    }

    public void setFamille(String famille) {
        this.famille = famille;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return   "Category: "+getCategory()+" famille: "+getFamille()+" type"+getType();
    }
}
