package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.UnAnnuaire;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Un_emploi_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Un_partenariat;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Un_produit_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Un_projet_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Un_service_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Une_actualite_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Une_bourse_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Une_competence_annuaire_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Une_location_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.BottomProfilInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.EndlessRecyclerViewScrollListener;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Favorite;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.HelperActivity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Historique;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.query.Query;
import io.objectbox.query.QueryBuilder;


public class BottomProfileHistorique extends Fragment implements BottomProfilInterface {

    RecyclerView recyclerView;
    ProgressBar progressBar;
    View linearVide;
    TextView tvVide;
    boolean is_small,is_medium,is_large;
    private boolean is_fr;
    List<Produit> produits=new ArrayList<>();
    MyAdatteurBottomProfileHistorique myAdatteurBottomProfileHistorique;
    Box<Historique> historiqueBox;
    LinearLayoutManager manager;
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View  v=inflater.inflate(R.layout.bottom_profil_layout,container,false);
        recyclerView=(RecyclerView)v.findViewById(R.id.bottom_profil_recycleView);
        progressBar=(ProgressBar)v.findViewById(R.id.bottom_profil_progressBar2);
        linearVide=v.findViewById(R.id.bottom_profil_linearvide);
        tvVide=(TextView)v.findViewById(R.id.bottom_profil_textview_vide);
        recyclerView.setVisibility(View.GONE);
        linearVide.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        is_small=getResources().getBoolean(R.bool.is_small);
        is_medium=getResources().getBoolean(R.bool.is_medium);
        is_large=getResources().getBoolean(R.bool.is_large);
        myAdatteurBottomProfileHistorique=new MyAdatteurBottomProfileHistorique(getActivity(),produits,this);
        manager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(myAdatteurBottomProfileHistorique);
        endlessRecyclerViewScrollListener=new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(page);
            }
        };
        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
        historiqueBox =((MyApplication)getActivity().getApplicationContext()).getBoxStore().boxFor(Historique.class);
        loadNextDataFromApi(0);
        return v;
    }


    public void loadNextDataFromApi(int offset) {
        // Send an API request to retrieve appropriate paginated data
        //  --> Send the request including an offset value (i.e `page`) as a query parameter.
        //  --> Deserialize and construct new model objects from the API response
        //  --> Append the new data objects to the existing set of items inside the array of items
        //  --> Notify the adapter of the new items made with `notifyItemRangeInserted()
        //
       FetchHistorique(offset);
    }
    public void FetchHistorique(int offset){
        //Historique
        Gson  gson=new Gson();
        QueryBuilder<Historique> builder2=historiqueBox.query();
        Query<Historique> query2=builder2.build();
        Historique historique=query2.findFirst();
        if(historique!=null){
            String val=historique.mHistorique;
            if(!val.equalsIgnoreCase("") && val!=null){
                Type type=new TypeToken<List<Produit>>(){}.getType();
               List<Produit> produitList=gson.fromJson(val,type);
                if(produitList!=null ){
                    int start=offset*15;
                     if(start<produitList.size()){
                         recyclerView.setVisibility(View.VISIBLE);
                         linearVide.setVisibility(View.GONE);
                         tvVide.setVisibility(View.GONE);
                         progressBar.setVisibility(View.GONE);

                         int size=produitList.size();
                         int end=15*(offset+1);
                         if(end>size){
                             end=size;
                         }
                         List<Produit> mList=produitList.subList(start,end);
                         Collections.reverse(mList);
                         produits.addAll(mList);
                         myAdatteurBottomProfileHistorique=new MyAdatteurBottomProfileHistorique(getActivity(),produits,BottomProfileHistorique.this);
                         recyclerView.setAdapter(myAdatteurBottomProfileHistorique);
                     }
                }
                else{
                    recyclerView.setVisibility(View.GONE);
                    linearVide.setVisibility(View.VISIBLE);
                    tvVide.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    tvVide.setText(is_fr?"Votre liste d'historique est vide":"Your history list is empty");
                }

            }
            else{
                recyclerView.setVisibility(View.GONE);
                linearVide.setVisibility(View.VISIBLE);
                tvVide.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                tvVide.setText(is_fr?"Votre liste d'historique est vide":"Your history list is empty");
            }

        }
        else{
            recyclerView.setVisibility(View.GONE);
            linearVide.setVisibility(View.VISIBLE);
            tvVide.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            tvVide.setText(is_fr?"Votre liste d'historique est vide":"Your history list is empty");
        }
    }

    @Override
    public void onBottomProfilItemClick(int position) {
      Produit p=produits.get(position);
        HelperActivity.getInstance().setProduit(p);
        Intent intent=new Intent();
        String type=p.getType();
        if(type.equals("actualite"))
        {
            intent=new Intent(getContext(),Une_actualite_activity.class);
        }
        else if(type.equals("boutique")){
            intent =new Intent(getContext(),Un_produit_activity.class);
        }
        else if(type.equals("emploi")){
            intent =new Intent(getContext(),Un_emploi_activity.class);
        }
        else if(type.equals("competence")){
            intent =new Intent(getContext(),Une_competence_annuaire_activity.class);
        }
        else if(type.equals("bourse")){
            intent =new Intent(getContext(),Une_bourse_activity.class);
        }
        else if(type.equals("annuaire")){

            intent =new Intent(getContext(),UnAnnuaire.class);
        }
        else if(type.equals("service")){
            intent =new Intent(getContext(),Un_service_activity.class);
        }
        else if(type.equals("location")){
            intent =new Intent(getContext(),Une_location_activity.class);
        }
        else if(type.equals("partenariat")){
            intent =new Intent(getContext(),Un_partenariat.class);
        }
        else if(type.equals("projet")){
            intent =new Intent(getContext(),Un_projet_activity.class);
        }
        intent.putExtra("id",p.getId());

        intent.putExtra("lien_catalogue",p.getLien_catalogue());
        intent.putExtra("lien_description",p.getLien_description());
        intent.putExtra("choixf",p.getChoixf());
        intent.putExtra("url",p.getUrl());
        intent.putExtra("url2",p.getUrl2());
        intent.putExtra("url3",p.getUrl3());
        intent.putExtra("type",p.getType());
        intent.putExtra("from_co_direct","oui");
        intent.putExtra("famille",p.getFamille());
        intent.putExtra("pays",p.getPays());
        intent.putExtra("category",p.getCategory());
        intent.putExtra("type2",0);
        intent.putExtra("date",p.getDate());
        intent.putExtra("famille_en",p.getFamille_en());
        intent.putExtra("lien_fichier",p.getLien_fichier());
        startActivity(intent);
    }

    class MyAdatteurBottomProfileHistorique  extends RecyclerView.Adapter<MyAdatteurBottomProfileHistorique.MyBottomProfilHistoriqueViewholder> {

        List<Produit> listF = new ArrayList<>();
        BottomProfilInterface profilInterface;
        Produit p;
        Context c;

        public MyAdatteurBottomProfileHistorique(Context c, List<Produit> listF, BottomProfilInterface bottomProfilInterface) {
            this.listF = listF;
            this.profilInterface = bottomProfilInterface;
            this.c = c;
        }


        class MyBottomProfilHistoriqueViewholder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView description, txtFamille;
            DinamicImageView image;
            TextView title;
            TextView tvCount;
            TextView tvDate;
            TextView tvFamilleCount;
            public MyBottomProfilHistoriqueViewholder(View v) {
                super(v);
                v.setOnClickListener(this);
                title = (TextView) v.findViewById(R.id.actualite_item_layout_title);
                image = (DinamicImageView) v.findViewById(R.id.actualite_item_layout_image);
                tvCount = (TextView) v.findViewById(R.id.actualite_Count);
                tvDate = (TextView) v.findViewById(R.id.actualite_date);
                description = (TextView) v.findViewById(R.id.actualite_item_layout_description);
                txtFamille = (TextView) v.findViewById(R.id.famille_name);
                tvFamilleCount = (TextView)v.findViewById(R.id.famille_count);
            }

            @Override
            public void onClick(View v) {
                profilInterface.onBottomProfilItemClick(getAdapterPosition());
            }
        }

        @Override
        public int getItemCount() {
            return listF.size();
        }


        @Override
        public MyBottomProfilHistoriqueViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;

            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.actualite_item_layout, parent, false);

            MyBottomProfilHistoriqueViewholder holder = new MyBottomProfilHistoriqueViewholder(v);

            return holder;
        }

        @Override
        public void onBindViewHolder(MyBottomProfilHistoriqueViewholder holder, int position) {
            p = listF.get(position);
            if (is_fr) {
                holder.txtFamille.setText(p.getFamille().trim().toUpperCase());
            } else {
                holder.txtFamille.setText(p.getFamille_en().trim().toUpperCase());
            }
            if(p.getNb_vue_famille()!=null && !p.getNb_vue_famille().trim().equalsIgnoreCase("") && !p.getNb_vue_famille().equalsIgnoreCase("null"))
            {
                holder.tvFamilleCount.setText(p.getNb_vue_famille());
            }
            else{
                holder.tvFamilleCount.setText("0");
            }

            holder.tvCount.setVisibility(View.GONE);

            String des = "";
            holder.title.setText(p.getTitle().trim());
            if(p.getAdmin_update().equalsIgnoreCase("oui")){
                holder.tvDate.setText( p.getExtend_proprety1().trim());
                des = p.getDescription().trim();
                if (c.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    if (des.length() > 50) {
                        des = des.substring(0, 50) + "...";

                    }

                } else {
                    if (des.length() > 156) {
                        des = des.substring(0, 110) + "...";
                    }
                }
            }
            else{
                holder.tvDate.setVisibility(View.VISIBLE);
                holder.tvDate.setText(p.getExtend_proprety1());
                des = p.getDescription().trim();
                if (c.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    if (des.length() > 50) {
                        des = des.substring(0, 50) + "...";

                    }

                } else {
                    if (des.length() > 156) {
                        des = des.substring(0, 110) + "...";
                    }
                }
            }




            holder.description.setText(des);
            final float scale = getResources().getDisplayMetrics().density;
            if (is_small) {
                Picasso.with(c).load(p.getUrl()).resize((int) (120 * scale + 0.5f), (int) (100 * scale + 0.5f)).into(holder.image);
            } else if (is_medium) {
                Picasso.with(c).load(p.getUrl()).resize((int) (140 * scale + 0.5f), (int) (120 * scale + 0.5f)).into(holder.image);
            } else if (is_large) {
                Picasso.with(c).load(p.getUrl()).resize((int) (150 * scale + 0.5f), (int) (130 * scale + 0.5f)).into(holder.image);
            }
        }
    }
    private boolean isConnected()
    {
        ConnectivityManager cm=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        if(info!=null && info.isConnected() && info.isAvailable())
        {
            return  true;
        }

        return  false;
    }
}
