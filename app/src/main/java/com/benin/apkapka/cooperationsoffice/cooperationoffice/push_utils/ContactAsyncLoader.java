package com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.content.AsyncTaskLoader;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Contact;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

/**
 * Created by joel on 28/08/2017.
 */
public class ContactAsyncLoader extends AsyncTaskLoader<List<Contact>> {

    ContentResolver resolver;
    Cursor c,cursor;
    public ContactAsyncLoader(Context c){
        super(c);
        resolver=getContext().getContentResolver();
    }

    @Override
    public List<Contact> loadInBackground() {
        List<Contact> contacts=new ArrayList<Contact>();
        List<String> list=new ArrayList<>();
        list.add("0");
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.add("8");
        list.add("9");
        cursor =resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null, null, null);
        if(cursor!=null && cursor.getCount()>0){
            while (cursor.moveToNext()) {
              if(cursor!=null){
                Contact contact=new Contact();
                 String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY));
                 String phonenumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                 phonenumber=phonenumber.replace(" ","");
                  String s=""+(phonenumber.trim()).charAt(0);
                 contact.setName(name);
                 contact.setNumber(phonenumber);
                  if(s.equalsIgnoreCase("+")){
                      contacts.add(contact);
                  }
                  else if(list.contains(s)){
                      contacts.add(contact);
                  }
              }
            }
        }
        HashSet<Contact> set=new HashSet<>();
        set.addAll(contacts);
        contacts.clear();
        contacts.addAll(set);
        Collections.sort(contacts, new Comparator<Contact>() {
            @Override
            public int compare(Contact contact, Contact t1) {
                if(contact.getName().compareToIgnoreCase(t1.getName())>0){
                    return 1;
                }
                else if(contact.getName().compareToIgnoreCase(t1.getName())<0){
                    return -1;
                }


                return 0;
            }
        });
        return contacts;
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        cancelLoad();
    }

    @Override
    public void onCanceled(List<Contact> data) {
        super.onCanceled(data);
        if(c!=null){
            c.close();
            c=null;
        }

        if(cursor!=null){
            cursor.close();
            cursor=null;
        }
    }

    @Override
    protected void onReset() {
        super.onReset();

        if(c!=null){
            c.close();
            c=null;
        }

        if(cursor!=null){
            cursor.close();
            cursor=null;
        }

    }
}
