package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by joel on 05/12/2017.
 */
public class ImageSaver {
    Context c;
    public static  final  String dirname="images";
    public ImageSaver(Context c){
        this.c=c;
    }

    public File createFile(String fileName){

        File f=c.getDir(dirname,c.MODE_PRIVATE);
        return  new File(f,fileName);
    }
    public String saveImage( String fileName, Bitmap bitmap){

        File f=createFile(fileName);
        FileOutputStream outputStream;
        try{
            outputStream=new FileOutputStream(f);
            if(fileName.endsWith(".png")){
                bitmap.compress(Bitmap.CompressFormat.PNG,100,outputStream);
            }
            else {
                bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
            }

            return f.getAbsolutePath();
        }
        catch (IOException e){

        }


        return null;
    }
    public  String getImageNameFromPath(String path){
        String [] tab=path.split("/");
        return tab[tab.length-1];
    }
}
