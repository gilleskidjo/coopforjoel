package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Home;

/**
 * Created by joel on 13/10/2016.
 */
public class FragmentHome extends Fragment {
public  static  final  String TAG="home_activity_tag";

    Home home;

    public Home getHome() {
        return home;
    }

    public void setHome(Home home) {
        this.home = home;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}
