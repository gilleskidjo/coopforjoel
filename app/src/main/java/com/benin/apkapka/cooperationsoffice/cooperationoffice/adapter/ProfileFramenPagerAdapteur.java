package com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ProfileFramenPagerAdapteur  extends FragmentPagerAdapter{
    List<Fragment> fragmentList=new ArrayList<>();
    List<String>  fragmentTitle=new ArrayList<>();
    public ProfileFramenPagerAdapteur(FragmentManager fm){
        super(fm);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitle.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }
    public void add(Fragment fragment,String title){
        fragmentList.add(fragment);
        fragmentTitle.add(title);
    }
}
