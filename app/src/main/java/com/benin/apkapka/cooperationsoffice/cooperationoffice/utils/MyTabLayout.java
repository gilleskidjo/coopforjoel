package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;

/**
 * Created by joel on 14/08/2017.
 */
public class MyTabLayout  extends TabLayout {
    static  final int DEFAULT_COUNT_COLOR= Color.WHITE;
    float subTextSize;
    int countTextColor;
    float countTextSize;
    Drawable counTextBackgroundColor;
    public MyTabLayout(Context c){
        super(c);
    }
    public MyTabLayout(Context c, AttributeSet attrs){
        super(c,attrs);
        init(attrs, 0);
    }
    public  MyTabLayout(Context c,AttributeSet attrs,int defStyleAttr){
        super(c,attrs,defStyleAttr);
        init(attrs, defStyleAttr);
    }
    private void init(AttributeSet attrs, int defStyleAttr) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs,
                R.styleable.MyTabLayout,
                defStyleAttr,
                R.style.TabLayoutPlus_DAFULT
        );

        subTextSize = typedArray.getDimensionPixelSize(R.styleable.MyTabLayout_subTextSize, 0);

        countTextColor = typedArray.getColor(R.styleable.MyTabLayout_countTextColor, DEFAULT_COUNT_COLOR);
        countTextSize = typedArray.getDimensionPixelSize(R.styleable.MyTabLayout_countTextSize, 0);
        counTextBackgroundColor = typedArray.getDrawable(R.styleable.MyTabLayout_countTextBackground);

        typedArray.recycle();
    }
    @Override
    public final void setupWithViewPager(@Nullable ViewPager viewPager) {
        super.setupWithViewPager(viewPager);

        if (viewPager == null || viewPager.getAdapter() == null) {
            return;
        }
        PagerAdapter adapter = viewPager.getAdapter();
        for (int i = 0; i < adapter.getCount(); i++) {
            TabLayout.Tab tab = getTabAt(i);
            if (tab != null) {
                TabLayoutCustomView view = initTab(tab);
                view.setSelected(i == 0);
            }
        }
    }

    private TabLayoutCustomView initTab(TabLayout.Tab tab) {
        TabLayoutCustomView customView = new TabLayoutCustomView(getContext());
        customView.tabText.setTextColor(getTabTextColors());
        customView.tabSubtext.setTextColor(getTabTextColors());
        if (subTextSize > 0) {
            customView.tabSubtext.setTextSize(TypedValue.COMPLEX_UNIT_PX, subTextSize);
        }

        customView.tabCount.setTextColor(countTextColor);
        if (countTextSize > 0) {
            customView.tabCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, countTextSize);
        }

         if(Build.VERSION.SDK_INT<Build.VERSION_CODES.JELLY_BEAN){
             customView.tabCount.setBackgroundDrawable(counTextBackgroundColor);
         }
         else {
            customView.tabCount.setBackground(counTextBackgroundColor);
         }
        customView.setTabText(tab.getText());

        tab.setCustomView(customView);

        return customView;
    }

    public Tab newTabPlus() {
        Tab tab = super.newTab();
        initTab(tab);
        return tab;
    }

    @Nullable
    public  TabLayoutCustomView getTabCustomViewAt(int index) {
        Tab tab = getTabAt(index);
        if (tab != null && tab.getCustomView() instanceof TabLayoutCustomView) {
            return (TabLayoutCustomView) tab.getCustomView();
        }
        return null;
    }
}
