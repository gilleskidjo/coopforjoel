package com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils;

/**
 * Created by joel on 16/08/2017.
 */
public class PushFamille {
    private String famille;
    private int count;

    public PushFamille() {
    }

    public PushFamille(String famille, int count) {
        this.famille = famille;
        this.count = count;
    }

    public String getFamille() {
        return famille;
    }

    public int getCount() {
        return count;
    }

    public void setFamille(String famille) {
        this.famille = famille;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Famille :"+famille+"; count :"+count;
    }
}
