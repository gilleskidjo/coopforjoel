package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by joel on 04/10/2016.
 */
public class SessionManager {

   SharedPreferences pref;
   SharedPreferences.Editor editor;
  int PRIVATE_MODE=0;
   Context context;
    private static  final  String PREF_NAME="user_store_pref_login";
    private static  final  String KEY_IS_LOGIN="isLogin";
    private static  final  String KEY_HAVE_ADRESSE="user_have_adresse";
    private static  final  String KEY_TYPE_LOGIN="user_login_type";
    private  static  final  String KEY_HAVE_FA_GOO_ID="user_is_from_sociel";
    public  static  final String KEY_USER="user_key_store";
    public  static  final String KEY_IS_FACEBOOK="user_key_is_facebook";
    public  static  final String KEY_IS_GOOGLE="user_key_is_google";
    public  static  final String KEY_IS_NUM_WHATSAPP="user_key_is_nume";
    public static  final  String KEY_COMPTE_WHATSAPP="user_key_compte_whatsapp";
    public static  final  String KEY_COMPTE_FACEBOOK="user_key_compte_facebook";
    public static  final  String KEY_COMPTE_GOOGLE="user_key_compte_google";
    public static final  String KEY_SERVEUR_DATE="cooperation_serveur_date";
    public  static final String KEY_SERVEUR_ELAPSEREAL="cooperation_serveur_elapse";
    public  static final  String KEY_SERVEUR_CAN_REQUEST_DATE="cooperation_office_serveur_date_request";
    public static  final String KEY__LINK_CONDITION="cooperationsofficelinkcondition";
    public static  final String KEY__LINK_A_PROPOS="cooperationsofficelinkapropos";
    public static  final String KEY__LINK_FACEBOOK="cooperationsofficelinfacebook";
    public static  final String KEY__LINK_YOUTUBE="cooperationsofficelinkyoutube";
    public static  final String KEY__LINK_COOPERATION="cooperationsofficelinkcooperation";
    public static  final String KEY__LINK_COOPERATION_VERSION_CODE="cooperationsofficelinkcooperation_version_code";
    public  static  final  String KEY_LOCATION_IS_SAVE="cooperationsofficelocationisSave";
    public  static  final  String KEY_USER_LOCATION_LATITUDE="cooperationsofficeuserlocationlatitude";
    public static  final  String KEY_USER_LOCATION_LONGITUDE="cooperationsofficeuserlongitude";
    public static  final  String KEY_CAN_REQUEST_LOCATION="cooperationsofficeuseraccepterequestlocation";
    public static  final  String KEY_CAN_REQUEST_LOCATION_AT_HOME="cooperationsofficeuseraccepterequestlocationhome";
    public static  final  String KEY_CAN_REQUEST_LOCATION_UPDATE="cooperationsofficeuseraccepterequestlocationupdate";
    public static  final  String KEY_ACTIF_USER="cooperationsofficeusercountactif";
    public static  final  String KEY_APP_UNINSTALL__USER_NOTIFICATION="cooperationsofficeappuninstallusernotification";
    public static final String KEY_SAVER_DATA_ACTUALITY = "user_activate_saver_data_actuality";
    public static final String KEY_SAVER_DATA_BOUTIQUE = "user_activate_saver_data_boutique";
    public static final String KEY_SAVER_DATA_EMPLOI = "user_activate_saver_data_emploi";
    public static final String KEY_SAVER_DATA_BOURSE= "user_activate_saver_data_bourse";
    public static final String KEY_SAVER_DATA_ANNUAIRE = "user_activate_saver_data_annuaire";
    public static final String KEY_SAVER_DATA_SERVICE = "user_activate_saver_data_service";
    public static final String KEY_SAVER_DATA_LOCATION = "user_activate_saver_data_location";
    public static final String KEY_SAVER_DATA_ALLIANCE = "user_activate_saver_data_alliance";
    public static final String KEY_SAVER_DATA_PROJET = "user_activate_saver_data_projet";
    public static final String KEY_SAVER_DATA_DIVERTISSEMENT = "user_activate_saver_data_divertissement";
    public  SessionManager(Context c)
    {
       this.context=c;
        pref=context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor=pref.edit();


    }

    public boolean isUserDataSet(){

        User user=getUser(context);
        if((user.getName()!=null && !user.getName().equalsIgnoreCase("vide") && !user.getName().equalsIgnoreCase("")) && (user.getPrenom()!=null && !user.getPrenom().equalsIgnoreCase("vide") && !user.getPrenom().equalsIgnoreCase("")) && (user.getPays()!=null && !user.getPays().equalsIgnoreCase("vide") && !user.getPays().equalsIgnoreCase("")) && (getCompteWhatsapp()!=null && !getCompteWhatsapp().equalsIgnoreCase("vide") && !getCompteWhatsapp().equalsIgnoreCase("")) && ((getCompteGoogle()!=null && getCompteFacebook()!=null ) && (!getCompteFacebook().equalsIgnoreCase("") && !getCompteGoogle().equalsIgnoreCase("")) ) && (user.getLatitude()!=null && !user.getLatitude().equalsIgnoreCase("")) && (user.getLongitude()!=null && !user.getLongitude().equalsIgnoreCase("")) ){

          return true;
        }
       return false;
    }


    public void putSaverDataDivertissement(String value){
        editor.putString(KEY_SAVER_DATA_DIVERTISSEMENT, value);
        editor.commit();
    }

    public  String getKeySaverDataDivertissement() {
        return pref.getString(KEY_SAVER_DATA_DIVERTISSEMENT, "") ;
    }


    public void putSaverDataActuality(String value){
        editor.putString(KEY_SAVER_DATA_ACTUALITY, value);
        editor.commit();
    }

    public  String getKeySaverDataActuality() {
        return pref.getString(KEY_SAVER_DATA_ACTUALITY, "") ;
    }

    public void putSaverDataBoutique(String value){
        editor.putString(KEY_SAVER_DATA_BOUTIQUE, value);
        editor.commit();
    }

    public  String getKeySaverDataBoutique() {
        return pref.getString(KEY_SAVER_DATA_BOUTIQUE, "") ;
    }

    public void putSaverDataEmploi(String value){
        editor.putString(KEY_SAVER_DATA_EMPLOI, value);
        editor.commit();
    }

    public  String getKeySaverDataEmploi() {
        return pref.getString(KEY_SAVER_DATA_EMPLOI, "") ;
    }

    public void putSaverDataBourse(String value){
        editor.putString(KEY_SAVER_DATA_BOURSE, value);
        editor.commit();
    }

    public  String getKeySaverDataBourse() {
        return pref.getString(KEY_SAVER_DATA_BOURSE, "") ;
    }

    public void putSaverDataAnnuaire(String value){
        editor.putString(KEY_SAVER_DATA_ANNUAIRE, value);
        editor.commit();
    }

    public  String getKeySaverDataAnnuaire() {
        return pref.getString(KEY_SAVER_DATA_ANNUAIRE, "") ;
    }

    public void putSaverDataService(String value){
        editor.putString(KEY_SAVER_DATA_SERVICE, value);
        editor.commit();
    }

    public  String getKeySaverDataService() {
        return pref.getString(KEY_SAVER_DATA_SERVICE, "") ;
    }
    public void putSaverDataLocation(String value){
        editor.putString(KEY_SAVER_DATA_LOCATION, value);
        editor.commit();
    }

    public  String getKeySaverDataLocation() {
        return pref.getString(KEY_SAVER_DATA_LOCATION, "") ;
    }

    public void putSaverDataAlliance(String value){
        editor.putString(KEY_SAVER_DATA_ALLIANCE, value);
        editor.commit();
    }

    public  String getKeySaverDataAlliance() {
        return pref.getString(KEY_SAVER_DATA_ALLIANCE, "") ;
    }

    public void putSaverDataProjet(String value){
        editor.putString(KEY_SAVER_DATA_PROJET, value);
        editor.commit();
    }

    public  String getKeySaverDataProjet() {
        return pref.getString(KEY_SAVER_DATA_PROJET, "") ;
    }

    public  void putCanRequestLocation(boolean canrequest){

        editor.putBoolean(KEY_CAN_REQUEST_LOCATION,canrequest);
        editor.commit();
    }

    public void putActifUser(String nbUser){
        editor.putString(KEY_ACTIF_USER,nbUser);
        editor.commit();
    }
    public void putAppUnInstall(String ok){
        editor.putString(KEY_APP_UNINSTALL__USER_NOTIFICATION,ok);
        editor.commit();
    }
    public  void putCanRequestLocationUpdate(boolean canrequest){

        editor.putBoolean(KEY_CAN_REQUEST_LOCATION_UPDATE,canrequest);
        editor.commit();
    }
    public  void putCanRequestLocationAtHome(boolean canrequest){

        editor.putBoolean(KEY_CAN_REQUEST_LOCATION_AT_HOME,canrequest);
        editor.commit();
    }
    public  void putUserLatitude(String latitude){

        editor.putString(KEY_USER_LOCATION_LATITUDE,latitude);
        editor.commit();
    }
    public  void putUserLongitude(String longitude){

        editor.putString(KEY_USER_LOCATION_LONGITUDE,longitude);
        editor.commit();
    }
    public  void putLocation_IS_Save(String locationState){

        editor.putString(KEY_LOCATION_IS_SAVE,locationState);
        editor.commit();
    }
    public  void putLinkCondition(String link){

        editor.putString(KEY__LINK_CONDITION,link);
        editor.commit();
    }
    public  void putLinkFacebook(String link){

        editor.putString(KEY__LINK_FACEBOOK,link);
        editor.commit();
    }
    public  void putLinkYoutube(String link){

        editor.putString(KEY__LINK_YOUTUBE,link);
        editor.commit();
    }
    public  void putLinkCooperation(String link){

        editor.putString(KEY__LINK_COOPERATION,link);
        editor.commit();
    }
    public  void putLinkApropos(String link){
        editor.putString(KEY__LINK_A_PROPOS,link);
        editor.commit();

    }
    public  void putLinkCooperationVersion(String link){
        editor.putString(KEY__LINK_COOPERATION_VERSION_CODE,link);
        editor.commit();

    }

    public  boolean getCanRequestLocation(){

        return pref.getBoolean(KEY_CAN_REQUEST_LOCATION,true);
    }


    public  String getActifuser(){

        return pref.getString(KEY_ACTIF_USER,"0");
    }
    public  String getAppUnInstall(){

        return pref.getString(KEY_APP_UNINSTALL__USER_NOTIFICATION,"non");
    }
    public  boolean getCanRequestLocationUpdate(){

        return pref.getBoolean(KEY_CAN_REQUEST_LOCATION_UPDATE,true);
    }
    public  boolean getCanRequestLocationAtHome(){
        return pref.getBoolean(KEY_CAN_REQUEST_LOCATION_AT_HOME,true);
    }
    public  String getUserLatitude(){

        return pref.getString(KEY_USER_LOCATION_LATITUDE,"");
    }
    public  String getUserLongitude(){

        return pref.getString(KEY_USER_LOCATION_LONGITUDE,"");
    }
    public  String getLocationIsSave(){

        return pref.getString(KEY_LOCATION_IS_SAVE,"non");
    }
    public  String getLinkFacebook(){

        return pref.getString(KEY__LINK_FACEBOOK,"");
    }
    public  String getLinkCooperationVersion(){

        return pref.getString(KEY__LINK_COOPERATION_VERSION_CODE,"9");
    }
    public  String getLinkYoutube(){

        return pref.getString(KEY__LINK_YOUTUBE,"");
    }
    public  String getLinkCooperation(){

        return pref.getString(KEY__LINK_COOPERATION,"");
    }
    public  String getLinkConddition(){

       return pref.getString(KEY__LINK_CONDITION,"");
    }
    public  String getLinkApropos(){

        return pref.getString(KEY__LINK_A_PROPOS,"");
    }
    public void putServeurCanRequest(boolean canRequest){
        editor.putBoolean(KEY_SERVEUR_CAN_REQUEST_DATE,canRequest);
        editor.commit();
    }
    public  boolean serveurCanRequest(){
       return pref.getBoolean(KEY_SERVEUR_CAN_REQUEST_DATE,false);
    }
    public void serveurPutTime(long time){
        editor.putLong(KEY_SERVEUR_DATE,time);
        editor.commit();
    }
    public long serveurGetTime(){

      return   pref.getLong(KEY_SERVEUR_DATE,0);
    }
    public  void serveurPutElapse(long time){
        editor.putLong(KEY_SERVEUR_ELAPSEREAL,time);
        editor.commit();
    }
    public  long serveurGetElapse(){
        return pref.getLong(KEY_SERVEUR_ELAPSEREAL,0);
    }


    public  void setIsFromSocial(boolean isFromSocial)
    {
        editor.putBoolean(KEY_HAVE_FA_GOO_ID,isFromSocial);
        editor.commit();
    }

    public boolean getIsFromSocial()
    {
        return pref.getBoolean(KEY_HAVE_FA_GOO_ID,false);
    }
    public void setLogin(boolean isLogedin)
    {
        editor.putBoolean(KEY_IS_LOGIN,isLogedin);
        editor.commit();
    }
    public  void setLogginType(int type)
    {
       editor.putInt(KEY_TYPE_LOGIN,type);
        editor.commit();
    }

    public  int getLogginType()
    {
        return  pref.getInt(KEY_TYPE_LOGIN,0);
    }
    public void  setKeyHaveAdresse(boolean haveAdresse)
    {
       editor.putBoolean(KEY_HAVE_ADRESSE,haveAdresse);
        editor.commit();
    }

    public void setIs_Facebook(boolean is_facebook){
        editor.putBoolean(KEY_IS_FACEBOOK,is_facebook);
        editor.commit();
    }
    public void setIs_Google(boolean is_google){
        editor.putBoolean(KEY_IS_GOOGLE,is_google);
        editor.commit();
    }
    public void setIs_num_whatsapp(boolean is_num_whatsapp){
        editor.putBoolean(KEY_IS_NUM_WHATSAPP,is_num_whatsapp);
        editor.commit();
    }
    public void setCompteWhatsapp(String compteWhatsapp){
        editor.putString(KEY_COMPTE_WHATSAPP,compteWhatsapp);
        editor.commit();
    }
    public void setCompteFacebook(String compteFacebook){
        editor.putString(KEY_COMPTE_FACEBOOK,compteFacebook);
        editor.commit();
    }
    public void setCompteGoogle(String compteGoogle){
        editor.putString(KEY_COMPTE_GOOGLE,compteGoogle);
        editor.commit();
    }

    public String getCompteWhatsapp(){
        return  pref.getString(KEY_COMPTE_WHATSAPP,null);
    }
    public String getCompteFacebook(){
        return  pref.getString(KEY_COMPTE_FACEBOOK,null);
    }
    public String getCompteGoogle(){

        return  pref.getString(KEY_COMPTE_GOOGLE,null);
    }
    public  boolean getIS_Facebook(){

        return  pref.getBoolean(KEY_IS_FACEBOOK,false);
    }
    public boolean getIS_Google(){

        return  pref.getBoolean(KEY_IS_GOOGLE,false);
    }
    public boolean getIS_Num_Whatsapp(){

        return  pref.getBoolean(KEY_IS_NUM_WHATSAPP,false);
    }
    public  boolean HaveAdresse()
    {
        return  pref.getBoolean(KEY_HAVE_ADRESSE,false);
    }
    public  boolean isLoggedIn()
    {
        return pref.getBoolean(KEY_IS_LOGIN,false);
    }

    public  void setUser(Context c, User user)
    {   List<User> liste=new ArrayList<User>();
        liste.add(user);
        pref=c.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor=pref.edit();
        String encoded_liste="";
        Gson gson=new Gson();
        encoded_liste=gson.toJson(liste);
        editor.putString(KEY_USER,encoded_liste);
        editor.commit();


    }

    public User  getUser(Context c)
    {
        User user=new User();
        List<User> liste=new ArrayList<User>();
        pref=c.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        editor=pref.edit();
        String encoded_liste="";
        Gson gson=new Gson();
        if (pref.contains(KEY_USER))
        {
           encoded_liste=pref.getString(KEY_USER,"");
           User [] users=gson.fromJson(encoded_liste,User[].class);
            liste=Arrays.asList(users);
            liste=new ArrayList<User>(liste);
            user=liste.get(0);
        }
        else
        {
            return null;
        }

        return  user;

    }

}
