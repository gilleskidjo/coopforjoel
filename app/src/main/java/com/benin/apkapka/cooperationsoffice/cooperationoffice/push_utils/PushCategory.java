package com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils;

/**
 * Created by joel on 15/08/2017.
 */
public class PushCategory {
    private String category;
    private int count;

    public PushCategory() {
    }

    public PushCategory(String category, int count) {
        this.category = category;
        this.count = count;
    }

    public String getCategory() {
        return category;
    }

    public int getCount() {
        return count;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "categoty "+category+" nombre "+count;
    }
}
