package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by joel on 21/11/2017.
 */
public class FragmentForNetworkDrawerAvis extends Fragment {

    public  static  final  String TAG="com.cooperation.office.fragemnr.for_network_save_avis";
    private boolean isRequestDo=false;

    HandlerNetWorkRequestResponseAvis handleResponse;

    public interface  HandlerNetWorkRequestResponseAvis
    {

        public void onResposeSuccess();

        public  void onresponseFailAndPrintErrorResponse();

    }

    public boolean isRequestDo() {
        return isRequestDo;
    }

    public  void setInterface(HandlerNetWorkRequestResponseAvis handleResponse)
    {
        this.handleResponse=handleResponse;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    public void  doRequestProduit_By_Category(long id, String description, String lang)
    {

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.save_users_avis(id,description,lang);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {
                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {

                                if (getActivity()!=null)
                                {
                                    if(handleResponse!=null)
                                    {
                                        handleResponse.onresponseFailAndPrintErrorResponse();
                                    }
                                }

                            } else {
                                if(getActivity()!=null){
                                    if (handleResponse != null ) {
                                        handleResponse.onResposeSuccess();
                                    }

                                }
                            }

                        } catch (Exception e) {
                            if (getActivity()!=null)
                            {
                                if(handleResponse!=null)
                                {
                                    handleResponse.onresponseFailAndPrintErrorResponse();
                                }
                            }
                        }

                    }//fin is success,
                    else {
                        if (getActivity()!=null)
                        {
                            if(handleResponse!=null)
                            {
                                handleResponse.onresponseFailAndPrintErrorResponse();
                            }
                        }
                    }//fin else is success

                }

                isRequestDo=true;
            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {
                if (getActivity()!=null)
                {
                    if(handleResponse!=null)
                    {
                        handleResponse.onresponseFailAndPrintErrorResponse();
                    }
                    isRequestDo = true;
                }
            }
        });






    }
}
