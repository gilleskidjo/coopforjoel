package com.benin.apkapka.cooperationsoffice.cooperationoffice.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;

/**
 * Created by joel on 02/10/2016.
 */
public class ShowCaseActivity   extends BaseLanguageActivity implements View.OnClickListener{

    TextView t_cooperate,text2,text3;
    Button btn_ignorer,btn_comte;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    setContentView(R.layout.show_case_activity);
       btn_comte=(Button)findViewById(R.id.btn_compte);
       btn_ignorer=(Button)findViewById(R.id.btn_ignorer);
       t_cooperate=(TextView)findViewById(R.id.cooperate);
       t_cooperate.setTypeface(Typer.set(this).getFont(Font.ROBOTO_BOLD));
       text2=(TextView)findViewById(R.id.show_case_text1);
       text3=(TextView)findViewById(R.id.show_case_text2);
       btn_ignorer.setOnClickListener(this);
       btn_comte.setOnClickListener(this);


    }



    @Override
    public void onClick(View v) {
            switch (v.getId())
            {
                case R.id.btn_ignorer:
                    Intent intent=new Intent(this,HomeFragment.class);
                    startActivity(intent);
                    finish();
                    break;

                case R.id.btn_compte:
                    Intent intent1=new Intent(this,LoginActivity.class);
                    startActivity(intent1);
                    finish();

                    break;
            }
    }
}
