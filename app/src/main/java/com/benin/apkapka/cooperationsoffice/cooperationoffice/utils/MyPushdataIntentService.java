package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;


import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.BaseActivity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.HomeFragment;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.TransparentActivity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushDataDao;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import androidx.work.Data;
import androidx.work.Worker;

/**
 * IntentService that save push data into database
 * Created by joel on 15/08/2017.
 */
public class MyPushdataIntentService  extends Worker {

    PushDataDao pushDataDao;
    List<Target> targets=new ArrayList<>();
    int resId;
    boolean is_fr;
    String channel_id;
    NotificationManager notificationManager;
    private boolean SHOULD_WORK=false;
    @NonNull
    @Override
    public Result doWork() {

        notificationManager=(NotificationManager)getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent=new Intent(getApplicationContext(), TransparentActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        final PendingIntent pendingIntent=PendingIntent.getActivity(getApplicationContext(),0,intent,PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder builder=null;
        channel_id=getApplicationContext().getResources().getString(R.string.default_notification_channel_id);
        is_fr=getApplicationContext().getResources().getBoolean(R.bool.lang_fr);
        final Data bundle=getInputData();
        if(bundle!=null){
            String type_n=bundle.getString("type_n","");

              if(type_n.equalsIgnoreCase("message")){
                  pushDataDao=new PushDataDao(getApplicationContext());
                  long id=bundle.getLong("id",0);
                  final String type=bundle.getString("type",null);
                  String category=bundle.getString("category",null);
                  String famille=bundle.getString("famille",null);
                  String action=bundle.getString("action",null);
                  PushData   data=new PushData(id,type,category,famille,id);

                  if(action.equals("add")){
                      pushDataDao.addPushData(data);
                  }
                  else if(action.equals("update")){
                      if(pushDataDao.chechPushData(id)){

                      }
                      else {
                          pushDataDao.addPushData(data);
                      }

                  }
                  SHOULD_WORK=true;
              }

            final String body1=bundle.getString("body","");
            final String id_notification1=bundle.getString("id_notification","");
            String icon=bundle.getString("icon","");
            resId=getApplicationContext().getResources().getIdentifier(icon,"drawable",getApplicationContext().getPackageName());


            if(type_n.equalsIgnoreCase("message")){
                final String type=bundle.getString("type",null);
                final   String url=bundle.getString("url","");
                NotificationCompat.BigTextStyle bigTextStyle= new NotificationCompat.BigTextStyle();
                bigTextStyle.bigText(body1);
                NotificationCompat.Builder builder1=new NotificationCompat.Builder(getApplicationContext(),channel_id)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setContentTitle(type.equalsIgnoreCase("actualite")?"ACTUALITE":type.equalsIgnoreCase("boutique")?"BOUTIQUE":type.equalsIgnoreCase("emploi")?"EMPLOI":type.equalsIgnoreCase("competence")?"EMPLOI":type.equalsIgnoreCase("bourse")?"BOURSE":type.equalsIgnoreCase("annuaire")?"ANNUAIRE":type.equalsIgnoreCase("service")?"SERVICE":type.equalsIgnoreCase("location")?"LOCATION":type.equalsIgnoreCase("partenariat")?"ALLIANCES":"PROJET")
                        .setStyle(bigTextStyle)
                        .setSmallIcon(resId)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent)
                        ;
                Notification notification=builder1.build();
                int notificationId=CanparseIntService(id_notification1)==false? 1:Integer.parseInt(id_notification1);
                if(notificationManager!=null){
                    if(Build.VERSION.SDK_INT>Build.VERSION_CODES.O){
                        NotificationChannel channel=new NotificationChannel(channel_id,getApplicationContext().getResources().getString(R.string.default_notification_channel_name),NotificationManager.IMPORTANCE_LOW);
                        channel.setDescription(getApplicationContext().getResources().getString(R.string.notification_channel_description));
                        notificationManager.createNotificationChannel(channel);
                    }
                    notificationManager.notify(notificationId,notification);
                }
            }
            else if(type_n.equalsIgnoreCase("push")){
                final int resId1=getApplicationContext().getResources().getIdentifier("ic_stat_onesignal_default","drawable",getApplicationContext().getPackageName());
                String id_notification=bundle.getString("id_notification","");
                final String title=bundle.getString("title","");
                final String body=bundle.getString("body","");

                NotificationCompat.BigTextStyle bigTextStyle= new NotificationCompat.BigTextStyle();
                bigTextStyle.bigText(body);
                builder=new NotificationCompat.Builder(getApplicationContext(),channel_id)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setDefaults(0)
                        .setStyle(bigTextStyle)
                        .setContentIntent(pendingIntent)
                        .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(),resId))
                        .setSmallIcon(resId1)
                ;
                Notification notification=builder.build();

                int notificationId=CanparseIntService(id_notification)==false? 1:Integer.parseInt(id_notification);
                if(notificationManager!=null){
                    if(Build.VERSION.SDK_INT>Build.VERSION_CODES.O){
                        NotificationChannel channel=new NotificationChannel(channel_id,getApplicationContext().getResources().getString(R.string.default_notification_channel_name),NotificationManager.IMPORTANCE_LOW);
                        channel.setDescription(getApplicationContext().getResources().getString(R.string.notification_channel_description));
                        notificationManager.createNotificationChannel(channel);
                    }
                    notificationManager.notify(notificationId,notification);
                }
            }
            else{
                final int resId1=getApplicationContext().getResources().getIdentifier("ic_stat_onesignal_default","drawable",getApplicationContext().getPackageName());
                String id_notification=bundle.getString("id_notification","");
                final String title=bundle.getString("title","");
                final String body=bundle.getString("body","");

                NotificationCompat.BigTextStyle bigTextStyle= new NotificationCompat.BigTextStyle();
                bigTextStyle.bigText(body);
                builder=new NotificationCompat.Builder(getApplicationContext(),channel_id)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setDefaults(0)
                        .setStyle(bigTextStyle)
                        .setContentIntent(pendingIntent)
                        .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(),resId))
                        .setSmallIcon(resId1)
                ;
                Notification notification=builder.build();

                int notificationId=CanparseIntService(id_notification)==false? 1:Integer.parseInt(id_notification);
                if(notificationManager!=null){
                    if(Build.VERSION.SDK_INT>Build.VERSION_CODES.O){
                        NotificationChannel channel=new NotificationChannel(channel_id,getApplicationContext().getResources().getString(R.string.default_notification_channel_name),NotificationManager.IMPORTANCE_LOW);
                        channel.setDescription(getApplicationContext().getResources().getString(R.string.notification_channel_description));
                        notificationManager.createNotificationChannel(channel);
                    }
                    notificationManager.notify(notificationId,notification);
                }

            }//fin

        }
        return Result.SUCCESS;
    }


    private boolean CanparseIntService(String s){
        try{
            int num =Integer.parseInt(s);
            return true;
        }
        catch (NumberFormatException e){

        }

        return false;
    }
}
