package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Competence;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ListDataHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joel on 09/10/2017.
 */
public class FragmentForNetBackImage extends Fragment {
    public  static  final  String TAG="com.cooperation.office.fragemntnetworkrequest_back_image";
    public interface  HandlerNetWorkRequestResponseImage
    {

        public void onResposeSuccessImage(String url,String date);

        public  void onresponseFailAndPrintErrorResponseImage();

    }

    HandlerNetWorkRequestResponseImage handleResponse;
    private boolean isRequestDo=false;
    private boolean isMessage=false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    public  void setInterface(HandlerNetWorkRequestResponseImage handleResponse)
    {
        this.handleResponse=handleResponse;
    }

    public void doRequestImage()
    {

        String url= Config.URL_BACK_IMAGE;


        JsonObjectRequest request=new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(response!=null)
                {
                    try {
                        boolean error = response.getBoolean("error");
                        if(error)
                        {
                        }
                        else
                        {
                            boolean isVide=response.getBoolean("vide");
                            if(isVide)
                            {
                            }
                            else
                            {
                                JSONArray produits=response.getJSONArray("images");
                                JSONObject  o=produits.getJSONObject(0);
                                if(handleResponse!=null)
                                {
                                    handleResponse.onResposeSuccessImage(o.getString("url"),o.getString("create_at"));
                                }
                            }

                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
                else
                {
                }

                isRequestDo=true;
            }//fin onResponse
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isRequestDo=true;
                    }
                });


        MyApplication.getInstance().addToRequestQueue(request);
    }
}
