package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;


import android.content.Context;
import android.content.Intent;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.BusEventFragmentNetworkRequest3;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.Utils_EventBus.SearchEvent3;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Formulaire_actualite;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.UnAnnuaire;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Une_actualite_activity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.AccueilInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.daos.PushHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushFamille;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushFamilleAsyncLoader;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.push_utils.PushFamilleAsyncLoader3;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Article;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.BitmapScaler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView2;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.EndlessRecyclerViewScrollListener;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.HelperActivity;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ListDataHandler;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyToast;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MydateUtil;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ProduitDatabaseRelation;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;


/**
 * Created by joel on 05/08/2016.
 */
public class FragmentActualite_biblio  extends Fragment implements AccueilInterface,FragmentNetworkRequestNouveauProduit.HandlerNetWorkRequestResponse,BusEventFragmentNetworkRequest3.HandlerNetWorkRequestResponse,LoaderManager.LoaderCallbacks<List<PushFamille>> {
        ProgressBar bar2;
        RecyclerView list;
        LinearLayoutManager manager;
        MyAdapteurActualite adapteur;
        SwipeRefreshLayout swipe;
        List<Produit> listA=new ArrayList<>();
        List<PushFamille> familles=new ArrayList<PushFamille>();
    FragmentNetworkRequestNouveauProduit  netWork;
        FragmentConnectionFail  fc;
        private boolean istablette=false;
        private int animate;
        Toolbar toolbar;
        TextView vide;
        View linearvide;
         private boolean is_fr=false;

        String payss_code [];
        String pays="tout";
        BusEventFragmentNetworkRequest3 net;
    boolean is_small,is_medium,is_large;
        boolean is_search=false;
    PushFamilleAsyncLoader3 loader;
    Box<ProduitDatabaseRelation> produitDatabaseRelationBox;
    ProduitDatabaseRelation produitDatabaseRelation;
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private SessionManager sessionManagerSaverData;

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            is_fr=getResources().getBoolean(R.bool.lang_fr);
            is_small=getResources().getBoolean(R.bool.is_small);
            is_medium=getResources().getBoolean(R.bool.is_medium);
            is_large=getResources().getBoolean(R.bool.is_large);
            payss_code=getResources().getStringArray(R.array.pays_code);
            View  v=inflater.inflate(R.layout.fragment_actualite_biblio_layout,container,false);
            BoxStore boxStore=((MyApplication)getActivity().getApplicationContext()).getBoxStore();
            produitDatabaseRelationBox=boxStore.boxFor(ProduitDatabaseRelation.class);
            produitDatabaseRelation=new ProduitDatabaseRelation(3);
            list=(RecyclerView)v.findViewById(R.id.fragment_actualite_biblio_recycleview);
            swipe=(SwipeRefreshLayout) v.findViewById(R.id.fragment_actualite_biblio_swipe);
            swipe.setColorSchemeColors(Color.BLUE,Color.GREEN,Color.RED,Color.YELLOW);
            swipe.setRefreshing(false);
            linearvide=v.findViewById(R.id.fragment_actualite_biblio_linearvide);
            vide=(TextView)v.findViewById(R.id.fragment_actualite_biblio_textview_vide);

            bar2=(ProgressBar)v.findViewById(R.id.fragment_actualite_biblio_Progress);
            animate=getResources().getInteger(android.R.integer.config_shortAnimTime);
            istablette=getResources().getBoolean(R.bool.tablette);
            list.setVisibility(View.GONE);

            manager=new LinearLayoutManager(getActivity());
            list.setLayoutManager(manager);
            sessionManagerSaverData = new SessionManager(getContext());


            FragmentManager fm=getFragmentManager();
            net=(BusEventFragmentNetworkRequest3)fm.findFragmentByTag(BusEventFragmentNetworkRequest3.TAG);
            if(net==null)
            {
                net=new BusEventFragmentNetworkRequest3();
                fm.beginTransaction().add(net,BusEventFragmentNetworkRequest3.TAG).commit();
            }
            else
            {
                if(net.isHasRequest())
                {
                    net.setInterface(this);
                    swipe.setRefreshing(true);
                }
            }
            netWork=(FragmentNetworkRequestNouveauProduit) fm.findFragmentByTag(FragmentNetworkRequestNouveauProduit.TAG);
            if(netWork==null)
            {

                netWork=new FragmentNetworkRequestNouveauProduit();
                fm.beginTransaction().add(netWork,FragmentNetworkRequestNouveauProduit.TAG).commit();
                netWork.setInterface(this);
                if(isConnected()){
                  loadNextDataFromApi(0);

                }
                else{

                        ProduitDatabaseRelation relation=produitDatabaseRelationBox.get(3);
                        if(relation!=null){
                            listA=relation.list;
                        }
                        if(listA!=null && listA.size()>0){
                            adapteur=new MyAdapteurActualite(listA,familles,FragmentActualite_biblio.this,getActivity());
                            list.setAdapter(adapteur);
                            list.setVisibility(View.VISIBLE);
                            linearvide.setVisibility(View.GONE);
                            bar2.setVisibility(View.GONE);
                        }
                        else {
                            bar2.setVisibility(View.GONE);
                            vide.setText(getResources().getString(R.string.error_frag_netw_parse));
                            list.setVisibility(View.GONE);
                            linearvide.setVisibility(View.VISIBLE);
                    }
                }

            }
            else
            {
                if(isConnected()){
                    netWork.setInterface(this);
                    if(net.isEmpty() || netWork.isEmpty){
                        vide.setText(netWork.getSms());
                        if(net.isReady() && net.getSms()!=null)
                        {
                            vide.setText(net.getSms());
                        }
                        list.setVisibility(View.GONE);
                        linearvide.setVisibility(View.VISIBLE);
                        bar2.setVisibility(View.GONE);
                    }
                    else{
                        listA=netWork.getListproduit();
                        if(net.isReady()){
                            listA=net.getListproduit();
                        }
                        if(listA!=null && listA.size()>0)
                        {
                            adapteur=new MyAdapteurActualite(listA,familles,this,getActivity());
                            list.setAdapter(adapteur);
                            bar2.setVisibility(View.GONE);
                            linearvide.setVisibility(View.GONE);
                            list.setVisibility(View.VISIBLE);

                        }
                        else
                        {

                            if(netWork.isMessage())
                            {
                                bar2.setVisibility(View.GONE);
                                vide.setText(getResources().getString(R.string.error_frag_netw_parse));
                                list.setVisibility(View.GONE);
                                linearvide.setVisibility(View.VISIBLE);
                            }
                        }

                    }

                }
                else{
                    ProduitDatabaseRelation relation=produitDatabaseRelationBox.get(3);
                    if(relation!=null){
                        listA=relation.list;
                    }
                    if(listA!=null && listA.size()>0){
                        adapteur=new MyAdapteurActualite(listA,familles,FragmentActualite_biblio.this,getActivity());
                        list.setAdapter(adapteur);
                        list.setVisibility(View.VISIBLE);
                        linearvide.setVisibility(View.GONE);
                        bar2.setVisibility(View.GONE);
                    }
                    else {
                        bar2.setVisibility(View.GONE);
                        vide.setText(getResources().getString(R.string.error_frag_netw_parse));
                        list.setVisibility(View.GONE);
                        linearvide.setVisibility(View.VISIBLE);
                    }
                }
            }


            swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    swipe.setRefreshing(false);
                }
            });

            adapteur=new MyAdapteurActualite(listA,familles,this,getActivity());
            endlessRecyclerViewScrollListener=new EndlessRecyclerViewScrollListener(manager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    loadNextDataFromApi(page);
                }
            };
            list.addOnScrollListener(endlessRecyclerViewScrollListener);
            return v;
        }//fin on create view

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(PushHandler.FRAGMENT3 ,null,this);
    }

    public void loadNextDataFromApi(int offset) {
        // Send an API request to retrieve appropriate paginated data
        //  --> Send the request including an offset value (i.e `page`) as a query parameter.
        //  --> Deserialize and construct new model objects from the API response
        //  --> Append the new data objects to the existing set of items inside the array of items
        //  --> Notify the adapter of the new items made with `notifyItemRangeInserted()
        //
        if(isConnected()){

            SessionManager sessionManager=new SessionManager(getActivity());
            User user=sessionManager.getUser(getActivity());
            String zone="";
            zone=user.getPays();
            if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
                zone="france";
            }
            netWork.doRequestProduitNouveauproduit_Group_By_Famille("bibliotheque",pays,is_fr==true?"fr":"en","actualite",zone,offset);
        }
    }
    @Override
    public Loader<List<PushFamille>> onCreateLoader(int id, Bundle args) {

        loader=new PushFamilleAsyncLoader3(getContext(),PushHandler.type,PushHandler.ACT_BIBLIO);

        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<PushFamille>> loader, List<PushFamille> data) {

       switch (loader.getId()){

           case PushHandler.FRAGMENT3:
               if(data!=null ){
                   familles=data;
                   adapteur=new MyAdapteurActualite(listA,familles,this,getActivity());
                   list.setAdapter(adapteur);
               }
             return;
       }
    }

    @Override
    public void onLoaderReset(Loader<List<PushFamille>> loader) {

    }


    @Override
        public void onResposeSuccess() {

            if(net.isReady())
            {

                if(is_search){
                    is_search=false;
                    if(net.getListproduit()!=null && net.getListproduit().size()>0){
                        if(net.getListproduit()!=null && net.getListproduit().size()==1){
                            Intent intent =new Intent(getActivity(),Une_actualite_activity.class);
                            Produit  p=net.getListproduit().get(0);
                            HelperActivity.getInstance().setProduit(p);
                            intent.putExtra("id",p.getId());


                            intent.putExtra("reduction",p.getReduction());
                            intent.putExtra("lien_catalogue",p.getLien_catalogue());
                            intent.putExtra("lien_description",p.getLien_description());
                            intent.putExtra("choixf",p.getChoixf());
                            intent.putExtra("url",p.getUrl());
                            intent.putExtra("url2",p.getUrl2());
                            intent.putExtra("url3",p.getUrl3());
                            intent.putExtra("famille",p.getFamille());
                            intent.putExtra("from_co_direct","oui");
                            intent.putExtra("pays",p.getPays());
                            intent.putExtra("category",p.getCategory());
                            intent.putExtra("famille_en",p.getFamille_en());
                            intent.putExtra("type2",2);
                            intent.putExtra("date",p.getDate());
                            intent.putExtra("famille_en",p.getFamille_en());
                            intent.putExtra("lien_fichier",p.getLien_fichier());
                            startActivity(intent);

                        }
                        else {
                            listA.clear();
                            listA.addAll(net.getListproduit());
                            endlessRecyclerViewScrollListener.resetState();
                        }

                    }
                }
                else {
                    listA.clear();
                    listA.addAll(net.getListproduit());
                    endlessRecyclerViewScrollListener.resetState();
                    if(listA!=null && listA.size()>0){
                        ProduitDatabaseRelation relation=new ProduitDatabaseRelation(3);
                       produitDatabaseRelationBox.remove(relation);
                       produitDatabaseRelationBox.attach(relation);
                        relation.list.addAll(listA);
                        produitDatabaseRelationBox.put(relation);
                    }
                }
            }
            else
            {

                listA.addAll(netWork.getListproduit());
                if(listA!=null && listA.size()>0){
                    ProduitDatabaseRelation relation=new ProduitDatabaseRelation(3);
                    produitDatabaseRelationBox.attach(relation);
                    produitDatabaseRelationBox.remove(relation);
                    relation.list.addAll(listA);
                    produitDatabaseRelationBox.put(relation);
                }
            }

            adapteur.notifyDataSetChanged();

            bar2.setVisibility(View.GONE);
            linearvide.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
            if(net.isHasRequest())
            {
               swipe.setRefreshing(false);
               net.setHasRequest(false);
            }
        }


        @Override
        public void onresponseFailAndPrintErrorResponse() {

            is_search=false;
            if(net.isEmpty() || netWork.isEmpty){
               if(endlessRecyclerViewScrollListener.currentPage<1){
                   vide.setText(netWork.getSms());
                   if(net.isHasRequest() && net.getSms()!=null)
                   {
                       vide.setText(net.getSms());
                   }
                   list.setVisibility(View.GONE);
                   linearvide.setVisibility(View.VISIBLE);
                   bar2.setVisibility(View.GONE);
               }
            }
            else{


                ProduitDatabaseRelation relation=produitDatabaseRelationBox.get(3);
                 if(relation!=null){
                     listA=relation.list;
                 }
                if(listA!=null && listA.size()>0){
                    adapteur=new MyAdapteurActualite(listA,familles,FragmentActualite_biblio.this,getActivity());
                    list.setAdapter(adapteur);
                    list.setVisibility(View.VISIBLE);
                    linearvide.setVisibility(View.GONE);
                    bar2.setVisibility(View.GONE);
                }
                else {
                    bar2.setVisibility(View.GONE);
                    vide.setText(getResources().getString(R.string.error_frag_netw_parse));
                    list.setVisibility(View.GONE);
                    linearvide.setVisibility(View.VISIBLE);
                }

            }

            if(net.isHasRequest())
            {
                swipe.setRefreshing(false);
                net.setHasRequest(false);
            }
        }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
        public void onPause() {
            super.onPause();



        }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
    @Subscribe
    public void onEvent(SearchEvent3 event)
    {
      net.setReady(true);
        swipe.setRefreshing(true);
        SessionManager sessionManager=new SessionManager(getActivity());
        User user=sessionManager.getUser(getActivity());
        String zone="";
        zone=user.getPays();
        if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
            zone="france";
        }
        switch (event.getType())
        {
            case 1 :
                list.setVisibility(View.GONE);
                bar2.setVisibility(View.VISIBLE);
                linearvide.setVisibility(View.GONE);
                net.doRequestProduit_Group_By_Famille("bibliotheque",event.getQuery(),is_fr==true?"fr":"en","actualite",zone,0);
                net.setInterface(this);
                net.setHasRequest(true);
                pays=event.getQuery();
                break;
            case 2:
                is_search=true;
                net.doRequestProduit_Search("bibliotheque",event.getPays(),event.getQuery(),is_fr==true?"fr":"en","actualite",zone);
                net.setInterface(this);
                net.setHasRequest(true);
                break;
        }

    }
    @Override
        public void itemClicked(int position) {

            Intent intent =new Intent(getActivity(), Une_actualite_activity.class);
            Produit  p=listA.get(position);

        HelperActivity.getInstance().setProduit(p);

            intent.putExtra("id",p.getId());

            intent.putExtra("reduction",p.getReduction());
            intent.putExtra("lien_catalogue",p.getLien_catalogue());
            intent.putExtra("lien_description",p.getLien_description());
            intent.putExtra("choixf",p.getChoixf());
            intent.putExtra("url",p.getUrl());
            intent.putExtra("url2",p.getUrl2());
            intent.putExtra("url3",p.getUrl3());
            intent.putExtra("famille",p.getFamille());
            intent.putExtra("pays",p.getPays());
            intent.putExtra("from_co_direct","oui");
            intent.putExtra("category",p.getCategory());
            intent.putExtra("famille_en",p.getFamille_en());
            intent.putExtra("type2",2);
            intent.putExtra("date",p.getDate());
            intent.putExtra("famille_en",p.getFamille_en());
            intent.putExtra("lien_fichier",p.getLien_fichier());
            startActivity(intent);

        }

        class  MyAdapteurActualite extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {
            public static final int VIEW_TYPE_UN = 0;
            public static final int VIEW_TYPE_TWO = 1;
            private int lastPosition = -1;
            List<PushFamille> familleList;
            List<Produit> articlestList;
            AccueilInterface accueilInterface;
            Context c;
            Produit ar;
            PushFamille f;
            public MyAdapteurActualite(List<Produit> articlestList,List<PushFamille> familleList, AccueilInterface accueilInterface, Context c) {

                this.articlestList = articlestList;
                this.accueilInterface = accueilInterface;
                this.c = c;
                this.familleList=familleList;
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

                switch (viewType)
                {
                    case VIEW_TYPE_UN:
                        View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.la_une_layout,parent,false);
                        final PrincipaleViewHolder holder=new PrincipaleViewHolder(v);
                        v.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                accueilInterface.itemClicked(holder.getAdapterPosition());
                            }
                        });
                        return holder;
                    case  VIEW_TYPE_TWO:
                        View v1=LayoutInflater.from(parent.getContext()).inflate(R.layout.actualite_item_layout,parent,false);
                        final ListViewHolder holder1=new ListViewHolder(v1);
                        v1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                accueilInterface.itemClicked(holder1.getAdapterPosition());
                            }
                        });
                        return holder1;
                }


                return null;
            }


            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                 ar=articlestList.get(position);
                Predicate<PushFamille> predicate=new Predicate<PushFamille>() {
                    @Override
                    public boolean apply(@javax.annotation.Nullable PushFamille input) {

                        return input.getFamille().equalsIgnoreCase(ar.getFamille());
                    }
                };
                if(familleList.size()>0){
                    List<PushFamille> familles= Lists.newArrayList(Iterables.filter(familleList,predicate));
                    if(familles!=null && familles.size()>0){
                        f=familles.get(0);
                        if(f!=null && f.getCount()>0)
                        {

                            ar.setCount(f.getCount());
                        }

                    }
                    else{
                        ar.setCount(0);
                    }
                }
                else{
                    ar.setCount(0);
                }
                if(holder instanceof  PrincipaleViewHolder)
                {
                    PrincipaleViewHolder holder1=(PrincipaleViewHolder)holder;
                    setUpMainPrincipaleHoder(holder1,articlestList.get(position));
                }
                else
                {
                    ListViewHolder holder2=(ListViewHolder)holder;
                    setUpListViewHolder(holder2,articlestList.get(position));
                    Config.setAnimation(holder2.parent,position,lastPosition,c);
                }
            }

            public void setUpMainPrincipaleHoder(final PrincipaleViewHolder   holder,  Produit article)
            {

                if(article.getNb_vue_famille()!=null && !article.getNb_vue_famille().trim().equalsIgnoreCase("") && !article.getNb_vue_famille().equalsIgnoreCase("null"))
                {

                    holder.txtCountFamille.setText(article.getNb_vue_famille());
                }
                else{

                    holder.txtCountFamille.setText("0");
                }

                if(is_fr){
                    holder.txtFamille.setText(article.getFamille().trim().toUpperCase());
                }
                else{
                    holder.txtFamille.setText(article.getFamille_en().trim().toUpperCase());
                }
                if(article.getCount()>0)
                {
                    holder.tvCount.setText(String.valueOf(article.getCount()));
                    holder.tvCount.setVisibility(View.VISIBLE);
                }
                else{
                    holder.tvCount.setText("");
                    holder.tvCount.setVisibility(View.GONE);
                }

                if(article.getAdmin_update().equalsIgnoreCase("oui")){
                    holder.tvDate.setText(article.getExtend_proprety1().trim());
                    holder.tvDate.setVisibility(View.VISIBLE);
                }
                else {
                    holder.tvDate.setText(article.getExtend_proprety1().trim());
                    holder.tvDate.setVisibility(View.VISIBLE);
                }
                holder.titlePrinciale.setText(article.getTitle().trim());
                DisplayMetrics metrics=c.getResources().getDisplayMetrics();
                int deviceWidth = metrics.widthPixels;
                int deviceHieght=metrics.heightPixels;

                if (sessionManagerSaverData.getKeySaverDataActuality().equals("Oui")){
                    Picasso.with(c)
                            .load(R.drawable.saverdatacoop)
                            .into(holder.imagePrincipale, new Callback() {
                                @Override
                                public void onSuccess() {
                                    holder.bar.setVisibility(View.GONE) ;
                                    holder.imagePrincipale.setVisibility(View.VISIBLE);


                                }

                                @Override
                                public void onError() {
                                    holder.bar.setVisibility(View.GONE);
                                    holder.imagePrincipale.setVisibility(View.VISIBLE);
                                }
                            });
                }

                if(sessionManagerSaverData.getKeySaverDataActuality().equals("Non")){
                    Picasso.with(c)
                            .load(article.getUrl())
                            .into(holder.imagePrincipale, new Callback() {
                                @Override
                                public void onSuccess() {
                                    holder.bar.setVisibility(View.GONE) ;
                                    holder.imagePrincipale.setVisibility(View.VISIBLE);


                                }

                                @Override
                                public void onError() {
                                    holder.bar.setVisibility(View.GONE);
                                    holder.imagePrincipale.setVisibility(View.VISIBLE);
                                }
                            });
                }

            }

            public void setUpListViewHolder(ListViewHolder  holder,  Produit article)
            {

                 if(article.getNb_vue_famille()!=null && !article.getNb_vue_famille().trim().equalsIgnoreCase("") && !article.getNb_vue_famille().equalsIgnoreCase("null"))
                 {

                     holder.txtCountFamille.setText(article.getNb_vue_famille());
                 }
                 else{

                     holder.txtCountFamille.setText("0");
                 }

                holder.txtFamille.setText(article.getFamille().trim().toUpperCase());
                if(article.getCount()>0)
                {
                    holder.tvCount.setText(String.valueOf(article.getCount()));
                    holder.tvCount.setVisibility(View.VISIBLE);
                }
                else{
                    holder.tvCount.setText("");
                    holder.tvCount.setVisibility(View.GONE);
                }
                String des="";
                //holder.tvDate.setText(MydateUtil.getDate(article.getDate(),is_fr));

                if(article.getAdmin_update().equalsIgnoreCase("oui")){
                    holder.tvDate.setText(article.getExtend_proprety1().trim());
                    holder.tvDate.setVisibility(View.VISIBLE);
                    holder.title.setText(article.getTitle().toUpperCase().trim());
                    des=article.getDescription().trim();

                    if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                    {
                        if(des.length()>50)
                        {
                            des=des.substring(0,50)+"...";

                        }

                    }
                    else
                    {
                        if(des.length()>110)
                        {
                            des=des.substring(0,110)+"...";
                        }
                    }

                    holder.description.setText(des);
                }
                else{

                    holder.tvDate.setVisibility(View.VISIBLE);
                    holder.tvDate.setText(article.getExtend_proprety1());
                    holder.title.setText(article.getTitle().toUpperCase().trim());
                    des=article.getDescription().trim();

                    if(c.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                    {
                        if(des.length()>50)
                        {
                            des=des.substring(0,50)+"...";

                        }

                    }
                    else
                    {
                        if(des.length()>110)
                        {
                            des=des.substring(0,110)+"...";
                        }
                    }

                    holder.description.setText(des);
                }
                final  float scale=getResources().getDisplayMetrics().density;
                if(is_small)
                {
                    if (sessionManagerSaverData.getKeySaverDataActuality().equals("Oui")) {
                        Picasso.with(c).load(R.drawable.saverdatacoop).resize((int) (120 * scale + 0.5f), (int) (100 * scale + 0.5f)).into(holder.image);
                    }

                    if (sessionManagerSaverData.getKeySaverDataActuality().equals("Non")){
                        Picasso.with(c).load(article.getUrl()).resize((int)(120*scale+0.5f),(int)(100*scale+0.5f)).into(holder.image);
                    }
                }
                else if(is_medium)
                {
                    if (sessionManagerSaverData.getKeySaverDataActuality().equals("Oui")){
                        Picasso.with(c).load(R.drawable.saverdatacoop).resize((int)(140*scale+0.5f),(int)(120*scale+0.5f)).into(holder.image);
                    }
                    if (sessionManagerSaverData.getKeySaverDataActuality().equals("Non")){
                        Picasso.with(c).load(article.getUrl()).resize((int)(140*scale+0.5f),(int)(120*scale+0.5f)).into(holder.image);
                    }
                }
                else if (is_large)
                {
                    if (sessionManagerSaverData.getKeySaverDataActuality().equals("Oui")){
                        Picasso.with(c).load(R.drawable.saverdatacoop).resize((int)(150*scale+0.5f),(int)(130*scale+0.5f)).into(holder.image);
                    }
                    if (sessionManagerSaverData.getKeySaverDataActuality().equals("Non")){
                        Picasso.with(c).load(article.getUrl()).resize((int)(150*scale+0.5f),(int)(130*scale+0.5f)).into(holder.image);
                    }

                }

            }


            @Override
            public int getItemViewType(int position) {
                return position==0?VIEW_TYPE_UN:VIEW_TYPE_TWO;
            }

            @Override
            public int getItemCount() {
                return articlestList.size();
            }

            public class PrincipaleViewHolder  extends RecyclerView.ViewHolder
            {
                TextView  titlePrinciale,tvCount,tvDate,txtFamille;
                DinamicImageView imagePrincipale;
                ProgressBar  bar;
                TextView txtCountFamille;
                private  boolean isNewFamille;
                public  PrincipaleViewHolder(View  v)
                {
                    super(v);
                    titlePrinciale=(TextView)v.findViewById(R.id.actualite_title_principale);
                    tvCount=(TextView)v.findViewById(R.id.actualite_Count);
                    tvDate=(TextView)v.findViewById(R.id.actualite_date);
                    imagePrincipale=(DinamicImageView) v.findViewById(R.id.actualite_image_principale);
                    bar=(ProgressBar)v.findViewById(R.id.actualite_progressBar1);
                    txtFamille=(TextView)v.findViewById(R.id.famille_name);
                    txtCountFamille=(TextView)v.findViewById(R.id.famille_count);

                }

            }

            public  class ListViewHolder extends  RecyclerView.ViewHolder
            {
                TextView  title,tvCount,tvDate;
                TextView description;
                DinamicImageView  image;
                TextView  txtFamille;
                TextView txtCountFamille;
                private  boolean isNewFamille;
                LinearLayout parent;
                public  ListViewHolder(View v)
                {
                    super(v);
                    title=(TextView)v.findViewById(R.id.actualite_item_layout_title);
                    tvDate=(TextView)v.findViewById(R.id.actualite_date);
                    tvCount=(TextView)v.findViewById(R.id.actualite_Count);
                    description=(TextView)v.findViewById(R.id.actualite_item_layout_description);
                    image=(DinamicImageView) v.findViewById(R.id.actualite_item_layout_image);
                    txtFamille=(TextView)v.findViewById(R.id.famille_name);
                    txtCountFamille=(TextView)v.findViewById(R.id.famille_count);
                    parent=v.findViewById(R.id.parent);
                }


            }


        }


        private boolean isConnected()
        {
            ConnectivityManager cm=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info=cm.getActiveNetworkInfo();
            if(info!=null && info.isConnected() && info.isAvailable())
            {
                return  true;
            }

            return  false;
        }
}
