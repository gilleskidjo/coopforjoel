package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TimeUtils;
import android.widget.Toast;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by joel on 28/08/2017.
 */
public class MydateUtil {
    static SimpleDateFormat sdf,sdf1;
    public static String getPastDate(String date,boolean is_fr){
        try{
          SimpleDateFormat  sdfa=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdfa.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
            Date date2=sdfa.parse(date);
           sdfa.setTimeZone(TimeZone.getDefault());
            date=sdfa.format(date2);
            DateTimeFormatter df= DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
            DateTime startTime=df.parseDateTime(date);
            DateTime endTime=new DateTime(new Date());
            Period period=new Period(startTime, endTime, PeriodType.yearMonthDayTime());


       PeriodFormatterBuilder builder=new PeriodFormatterBuilder();
            if(period.getDays()!=0){
               SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date1=sdf.parse(date);
             SimpleDateFormat sdf2=null;
                if(is_fr){
                     sdf2=new SimpleDateFormat("dd.MMM.yyyy à HH:mm", Locale.FRENCH);
                    String elapsed=sdf2.format(date1);
                    return ""+elapsed;
                }
                else{
                     sdf2=new SimpleDateFormat("dd.MMM.yyyy at HH:mm",Locale.ENGLISH);
                    String elapsed=sdf2.format(date1);
                    return ""+elapsed;
                }




            }
            else if (period.getHours()!=0){

                if(is_fr){
                    builder.appendHours().appendSuffix(" heure"," heures");
                    if(period.getHours()<0){
                        return "Publié il y a 0 heure";
                    }
                }
                else{
                    builder.appendHours().appendSuffix(" hour"," hours");
                    if(period.getHours()<0){
                        return "Publish  0 hour ago";
                    }
                }
            }
            else if(period.getMinutes()!=0){

                if(is_fr){
                    builder.appendMinutes().appendSuffix(" minute"," minutes");
                    if(period.getMinutes()<0){
                        return "Publié il y a 0 minute";
                    }
                }
                else {
                    builder.appendMinutes().appendSuffix(" minute"," minutes");
                    if(period.getMinutes()<0){
                        return "Publish  0 minute ago";
                    }
                }
            }
            else if(period.getSeconds()!=0){

                if(is_fr){
                    builder.appendSeconds().appendSuffix(" seconde"," secondes");
                    if(period.getSeconds()<0){
                        return "Publié maintenant";
                    }
                }
                else {
                    builder.appendSeconds().appendSuffix(" second"," seconds");
                    if(period.getSeconds()<0){
                        return "Publish  now";
                    }
                }
            }
            else if(period.getSeconds()==0){

                if(is_fr){
                    return "Publié maintenant";
                }
                else{
                    return "Publish now";
                }
            }
          PeriodFormatter formatter=builder.printZeroNever().toFormatter();
          String elapsed=formatter.print(period);

            if(is_fr){
                return "Publié il y a "+elapsed;
            }
            else{
                return "Publish "+elapsed+" ago";
            }

        }
        catch (ParseException e){

        }
        return "";
    }
    public static String getPastDate2(String date,boolean is_fr){
        try{
            SimpleDateFormat  sdfa=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdfa.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
            Date date2=sdfa.parse(date);
            sdfa.setTimeZone(TimeZone.getDefault());
            date=sdfa.format(date2);
            DateTimeFormatter df= DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
            DateTime startTime=df.parseDateTime(date);
            DateTime endTime=new DateTime(new Date());
            Period period=new Period(startTime, endTime, PeriodType.yearMonthDayTime());


            PeriodFormatterBuilder builder=new PeriodFormatterBuilder();

            if(period.getDays()!=0){
                SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date1=sdf.parse(date);
                SimpleDateFormat sdf2=null;
                if(is_fr){
                    sdf2=new SimpleDateFormat("dd.MM.yyyy à HH:mm", Locale.FRENCH);
                    String elapsed=sdf2.format(date1);
                    return "Publié le "+elapsed;
                }
                else{
                    sdf2=new SimpleDateFormat("dd.MM.yyyy at HH:mm",Locale.ENGLISH);
                    String elapsed=sdf2.format(date1);
                    return "Publish in "+elapsed;
                }




            }
            else if (period.getHours()!=0){

                if(is_fr){
                    builder.appendHours().appendSuffix(" heure"," heures");
                    if(period.getHours()<0){
                        return "Publié il y a 0 heure";
                    }
                }
                else{
                    builder.appendHours().appendSuffix(" hour"," hours");
                    if(period.getHours()<0){
                        return "Publish  0 hour ago";
                    }
                }
            }
            else if(period.getMinutes()!=0){

                if(is_fr){
                    builder.appendMinutes().appendSuffix(" minute"," minutes");
                    if(period.getMinutes()<0){
                        return "Publié il y a 0 minute";
                    }
                }
                else {
                    builder.appendMinutes().appendSuffix(" minute"," minutes");
                    if(period.getMinutes()<0){
                        return "Publish  0 minute ago";
                    }
                }
            }
            else if(period.getSeconds()!=0){

                if(is_fr){
                    builder.appendSeconds().appendSuffix(" seconde"," secondes");
                    if(period.getSeconds()<0){
                        return "Publié maintenant";
                    }
                }
                else {
                    builder.appendSeconds().appendSuffix(" second"," seconds");
                    if(period.getSeconds()<0){
                        return "Publish  now";
                    }
                }
            }
            else if(period.getSeconds()==0){

                if(is_fr){
                    return "Publié maintenant";
                }
                else{
                    return "Publish now";
                }
            }
            PeriodFormatter formatter=builder.printZeroNever().toFormatter();
            String elapsed=formatter.print(period);

            if(is_fr){
                return "Publié il y a "+elapsed;
            }
            else{
                return "Publish "+elapsed+" ago";
            }

        }
        catch (ParseException e){

        }
        return "";
    }



    public  static  String getDate(String date, boolean is_fr){

      try
      {

          SimpleDateFormat  sdfa=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         // sdfa.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
          Date startDate=sdfa.parse(date);
          sdfa.setTimeZone(TimeZone.getDefault());
          Date endDate=new Date();
          long different = endDate.getTime() - startDate.getTime();

          long secondsInMilli = 1000;
          long minutesInMilli = secondsInMilli * 60;
          long hoursInMilli = minutesInMilli * 60;
          long daysInMilli = hoursInMilli * 24;


          long elapsedDays = different / daysInMilli;
          different = different % daysInMilli;

          long elapsedHours = different / hoursInMilli;
          different = different % hoursInMilli;

          long elapsedMinutes = different / minutesInMilli;
          different = different % minutesInMilli;

          long elapsedSeconds = different / secondsInMilli;
          String elapsed="";
          if(elapsedDays!=0){

              SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
              Date date1=sdf.parse(date);
              SimpleDateFormat sdf2=null;
              if(is_fr){
                  sdf2=new SimpleDateFormat("dd.MM.yyyy 'à' HH:mm", Locale.FRENCH);
                 elapsed=sdf2.format(date1);
                  return "Publié le "+elapsed;
              }
              else{
                  sdf2=new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm",Locale.ENGLISH);
                 elapsed=sdf2.format(date1);
                  return "Publish in "+elapsed;
              }

          }
          else if (elapsedHours!=0){

                if(is_fr){
                  if(elapsedHours==1){
                    return "Publié il y a 1 heure";
                  }
                  else {
                      return "Publié il y a "+elapsedHours+" heures";
                  }
                }
               else {
                    if(elapsedHours==1){
                        return "Publish 1 hour ago";
                    }
                    else {
                        return "Publish "+elapsedHours+" hours ago";
                    }
                }

          }
          else if(elapsedMinutes!=0){

              if(is_fr){
                  if(elapsedMinutes==1){
                      return "Publié il y a 1 minute";
                  }
                  else {
                      return "Publié il y a "+elapsedMinutes+" minutes";
                  }
              }
              else {
                  if(elapsedMinutes==1){
                      return "Publish 1 minute ago";
                  }
                  else {
                      return "Publish "+elapsedMinutes+" minutes ago";
                  }
              }
          }
          else if(elapsedSeconds!=0){
              if(is_fr){
                  if(elapsedSeconds==1){
                      return "Publié il y a 1 second";
                  }
                  else {
                      return "Publié il y a "+elapsedSeconds+" secondes";
                  }
              }
              else {
                  if(elapsedSeconds==1){
                      return "Publish 1 second ago";
                  }
                  else {
                      return "Publish "+elapsedSeconds+" seconds ago";
                  }
              }
          }
          else if(elapsedSeconds==0){

              if(is_fr){
                  return "Publié maintenant";
              }
              else{
                  return "Publish now";
              }
          }


      }
      catch (ParseException e){

      }
        return "";
    }

    public  static  String getDateFromChat(String date, boolean is_fr){

        try
        {

            SimpleDateFormat  sdfa=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // sdfa.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
            Date startDate=sdfa.parse(date);
            sdfa.setTimeZone(TimeZone.getDefault());
            Date endDate=new Date();
            long different = endDate.getTime() - startDate.getTime();

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;


            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;
            String elapsed="";
            if(elapsedDays!=0){

                SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date1=sdf.parse(date);

                if(elapsedDays==1){
                    if(is_fr){

                        return "Hier";
                    }
                    else {
                      return "Yesterday" ;
                    }
                }
                else{
                    SimpleDateFormat sdf2=null;
                    if(is_fr){
                        sdf2=new SimpleDateFormat("dd.MM.yyyy 'à' HH:mm", Locale.FRENCH);
                        elapsed=sdf2.format(date1);
                        return ""+elapsed;
                    }
                    else{
                        sdf2=new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm",Locale.ENGLISH);
                        elapsed=sdf2.format(date1);
                        return ""+elapsed;
                    }
                }

            }
            else if (elapsedHours!=0){
                SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date1=sdf.parse(date);
                SimpleDateFormat sdf2=null;
                if(is_fr){
                    sdf2=new SimpleDateFormat("HH:mm", Locale.FRENCH);
                    elapsed=sdf2.format(date1);
                    return ""+elapsed;
                }
                else{
                    sdf2=new SimpleDateFormat("HH:mm",Locale.ENGLISH);
                    elapsed=sdf2.format(date1);
                    return ""+elapsed;
                }

            }
            else if(elapsedMinutes!=0){
                SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date1=sdf.parse(date);
                SimpleDateFormat sdf2=null;
                if(is_fr){
                    sdf2=new SimpleDateFormat("HH:mm", Locale.FRENCH);
                    elapsed=sdf2.format(date1);
                    return ""+elapsed;
                }
                else{
                    sdf2=new SimpleDateFormat("HH:mm",Locale.ENGLISH);
                    elapsed=sdf2.format(date1);
                    return ""+elapsed;
                }
            }
            else if(elapsedSeconds!=0){
                SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date1=sdf.parse(date);
                SimpleDateFormat sdf2=null;
                if(is_fr){
                    sdf2=new SimpleDateFormat("HH:mm", Locale.FRENCH);
                    elapsed=sdf2.format(date1);
                    return ""+elapsed;
                }
                else{
                    sdf2=new SimpleDateFormat("HH:mm",Locale.ENGLISH);
                    elapsed=sdf2.format(date1);
                    return ""+elapsed;
                }
            }
            else if(elapsedSeconds==0){

                SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date1=sdf.parse(date);
                SimpleDateFormat sdf2=null;
                if(is_fr){
                    sdf2=new SimpleDateFormat("HH:mm", Locale.FRENCH);
                    elapsed=sdf2.format(date1);
                    return ""+elapsed;
                }
                else{
                    sdf2=new SimpleDateFormat("HH:mm",Locale.ENGLISH);
                    elapsed=sdf2.format(date1);
                    return ""+elapsed;
                }
            }


        }
        catch (ParseException e){

        }
        return "";
    }

}
