package com.benin.apkapka.cooperationsoffice.cooperationoffice.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.MyApplication;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Activity_Publish_Action;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.Formulaire_actualite;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.adapter.BottomProfilInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Config;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.DinamicImageView;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.EndlessRecyclerViewScrollListener;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Favorite;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.MyInterface;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ParseJson;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.Produit;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.SessionManager;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.ToStringConverterFactory;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.utils.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.objectbox.Box;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class BottomProfileVitrine extends Fragment implements BottomProfilInterface {

    RecyclerView recyclerView;
    ProgressBar progressBar;
    View linearVide;
    TextView tvVide;
    boolean is_small,is_medium,is_large;
    private boolean is_fr;
    List<Produit> produits=new ArrayList<>();
    MyAdatteurBottomProfileVitrine myAdatteurBottomProfileVitrine;
    Box<Favorite> favoriteBox;
    long id;
    private  boolean is_public;
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    LinearLayoutManager manager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View  v=inflater.inflate(R.layout.bottom_profil_layout,container,false);
        recyclerView=(RecyclerView)v.findViewById(R.id.bottom_profil_recycleView);
        progressBar=(ProgressBar)v.findViewById(R.id.bottom_profil_progressBar2);
        linearVide=v.findViewById(R.id.bottom_profil_linearvide);
        tvVide=(TextView)v.findViewById(R.id.bottom_profil_textview_vide);
        recyclerView.setVisibility(View.GONE);
        linearVide.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        is_fr=getResources().getBoolean(R.bool.lang_fr);
        is_small=getResources().getBoolean(R.bool.is_small);
        is_medium=getResources().getBoolean(R.bool.is_medium);
        is_large=getResources().getBoolean(R.bool.is_large);
        myAdatteurBottomProfileVitrine=new MyAdatteurBottomProfileVitrine(getActivity(),produits,this);
        manager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(myAdatteurBottomProfileVitrine);
        endlessRecyclerViewScrollListener=new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(page);
            }
        };
        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
        favoriteBox =((MyApplication)getActivity().getApplicationContext()).getBoxStore().boxFor(Favorite.class);


        Bundle  bundle=getArguments();
        if(bundle!=null){
           id=bundle.getLong("id");
           is_public=bundle.getBoolean("public");
        }
        if(isConnected()){
         loadNextDataFromApi(0);
        }
        else {
            HandleNoConnexion();
        }
        return v;
    }
    private void HandleNoConnexion(){
        recyclerView.setVisibility(View.GONE);
        linearVide.setVisibility(View.VISIBLE);
        tvVide.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));
    }
    public void loadNextDataFromApi(int offset) {
        // Send an API request to retrieve appropriate paginated data
        //  --> Send the request including an offset value (i.e `page`) as a query parameter.
        //  --> Deserialize and construct new model objects from the API response
        //  --> Append the new data objects to the existing set of items inside the array of items
        //  --> Notify the adapter of the new items made with `notifyItemRangeInserted()
        //
        if(isConnected()){
            SessionManager sessionManager=new SessionManager(getActivity());
            User user=sessionManager.getUser(getActivity());
            String zone="";
            zone=user.getPays();
            if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
                zone="france";
            }
            if(is_public){
                FetchVitrine(id,zone,offset);

            }
            else{
                FetchVitrine(user.getId(),zone,offset);

            }
        }
    }
    public void FetchVitrine(long id,String zone,int page){

        final Gson gson=new Gson();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(ScalarsConverterFactory.create())

                .build();

        MyInterface service=retrofit.create(MyInterface.class);

        Call<String> call=service.get_All_user_vitrine(""+id,zone,page);

        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call1,retrofit2.Response<String> response) {
                String message = "";
                if (response != null) {
                    if (response.isSuccessful()) {

                        try {
                            String rep = response.body();
                            JSONObject o = new JSONObject(rep.substring(rep.indexOf("{"), rep.lastIndexOf("}") + 1));
                            boolean error = o.getBoolean("error");
                            if (error) {

                                recyclerView.setVisibility(View.GONE);
                                linearVide.setVisibility(View.VISIBLE);
                                tvVide.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);
                                tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));

                            } else {

                                boolean isVide = o.getBoolean("vide");
                                if (isVide) {

                                    recyclerView.setVisibility(View.GONE);
                                    linearVide.setVisibility(View.VISIBLE);
                                    tvVide.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                    tvVide.setText(is_fr?"Créez votre première publication":"Create your first publication");
                                } else {

                                    recyclerView.setVisibility(View.VISIBLE);
                                    linearVide.setVisibility(View.GONE);
                                    tvVide.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                    produits.addAll(ParseJson.parseProduit(o,getContext()));
                                    if(produits!=null && produits.size()>0){
                                    myAdatteurBottomProfileVitrine.notifyDataSetChanged();
                                    }

                                }

                            }

                        } catch (Exception e) {

                            recyclerView.setVisibility(View.GONE);
                            linearVide.setVisibility(View.VISIBLE);
                            tvVide.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));
                        }

                    }//fin is success,
                    else {

                        recyclerView.setVisibility(View.GONE);
                        linearVide.setVisibility(View.VISIBLE);
                        tvVide.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));
                    }//fin else is success

                }


            }

            @Override
            public void onFailure(Call<String> call1,Throwable t) {

                recyclerView.setVisibility(View.GONE);
                linearVide.setVisibility(View.VISIBLE);
                tvVide.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                tvVide.setText(getResources().getString(R.string.error_frag_netw_parse));
            }

        });
    }

    @Override
    public void onBottomProfilItemClick(int position) {

       if(produits.size()-1>=position){
           Produit produit=produits.get(position);
           Intent intent=new Intent(getActivity(),Activity_Publish_Action.class);
           intent.putExtra("id",produit.getId());
           startActivityForResult(intent,200);
       }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==200){

            if(resultCode==getActivity().RESULT_OK){
                SessionManager sessionManager=new SessionManager(getActivity());
                User user=sessionManager.getUser(getActivity());
                 if(isConnected()){
                     String zone="";
                     zone=user.getPays();
                     if(zone==null || zone.trim().equalsIgnoreCase("") || zone.equalsIgnoreCase("null") || zone.equalsIgnoreCase("vide")){
                         zone="france";
                     }
                     FetchVitrine(user.getId(),zone,0);
                     produits.clear();
                     endlessRecyclerViewScrollListener.resetState();
                 }
                 else{
                     HandleNoConnexion();
                 }
            }
        }
    }

    class MyAdatteurBottomProfileVitrine  extends RecyclerView.Adapter<MyAdatteurBottomProfileVitrine.MyBottomProfilVitrineViewholder> {

        List<Produit> listF = new ArrayList<>();
        BottomProfilInterface profilInterface;
        Produit p;
        Context c;

        public MyAdatteurBottomProfileVitrine(Context c, List<Produit> listF, BottomProfilInterface bottomProfilInterface) {
            this.listF = listF;
            this.profilInterface = bottomProfilInterface;
            this.c = c;
        }


        class MyBottomProfilVitrineViewholder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView description, txtFamille;
            DinamicImageView image;
            TextView title;
            TextView tvCount;
            TextView tvDate;
            TextView tvFamilleCount;

            public MyBottomProfilVitrineViewholder(View v) {
                super(v);
                v.setOnClickListener(this);
                title = (TextView) v.findViewById(R.id.actualite_item_layout_title);
                image = (DinamicImageView) v.findViewById(R.id.actualite_item_layout_image);
                tvCount = (TextView) v.findViewById(R.id.actualite_Count);
                tvDate = (TextView) v.findViewById(R.id.actualite_date);
                description = (TextView) v.findViewById(R.id.actualite_item_layout_description);
                txtFamille = (TextView) v.findViewById(R.id.famille_name);
                tvFamilleCount = (TextView)v.findViewById(R.id.famille_count);
            }

            @Override
            public void onClick(View v) {
                profilInterface.onBottomProfilItemClick(getAdapterPosition());
            }
        }

        @Override
        public int getItemCount() {
            return listF.size();
        }


        @Override
        public MyBottomProfilVitrineViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;

            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.actualite_item_layout, parent, false);

            MyBottomProfilVitrineViewholder holder = new MyBottomProfilVitrineViewholder(v);

            return holder;
        }

        @Override
        public void onBindViewHolder(MyBottomProfilVitrineViewholder holder, int position) {
            p = listF.get(position);
            if (is_fr) {
                holder.txtFamille.setText(p.getFamille().trim().toUpperCase());
            } else {
                holder.txtFamille.setText(p.getFamille_en().trim().toUpperCase());
            }
            if(p.getNb_vue_famille()!=null && !p.getNb_vue_famille().trim().equalsIgnoreCase("") && !p.getNb_vue_famille().equalsIgnoreCase("null"))
            {
                holder.tvFamilleCount.setText(p.getNb_vue_famille());
            }
            else{
                holder.tvFamilleCount.setText("0");
            }

            holder.tvCount.setVisibility(View.GONE);

            String des = "";

            holder.title.setText(p.getTitle().trim());

              if(p.getAdmin_update().equalsIgnoreCase("oui")){

                  des = p.getDescription().trim();
                  holder.tvDate.setText(p.getExtend_proprety1().trim());
                  if (c.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                      if (des.length() > 50) {
                          des = des.substring(0, 50) + "...";

                      }

                  } else {
                      if (des.length() > 156) {
                          des = des.substring(0, 110) + "...";
                      }
                  }
              }
              else {
                  des = p.getDescription().trim();
                  holder.tvDate.setVisibility(View.VISIBLE);
                  holder.tvDate.setText(p.getExtend_proprety1());
                  if (c.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                      if (des.length() > 50) {
                          des = des.substring(0, 50) + "...";

                      }

                  } else {
                      if (des.length() > 156) {
                          des = des.substring(0, 110) + "...";
                      }
                  }

              }



            holder.description.setText(des);
            final float scale = getResources().getDisplayMetrics().density;
            if (is_small) {
                Picasso.with(c).load(p.getUrl()).resize((int) (120 * scale + 0.5f), (int) (100 * scale + 0.5f)).into(holder.image);
            } else if (is_medium) {
                Picasso.with(c).load(p.getUrl()).resize((int) (140 * scale + 0.5f), (int) (120 * scale + 0.5f)).into(holder.image);
            } else if (is_large) {
                Picasso.with(c).load(p.getUrl()).resize((int) (150 * scale + 0.5f), (int) (130 * scale + 0.5f)).into(holder.image);
            }
        }
    }
    private boolean isConnected()
    {
        ConnectivityManager cm=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        if(info!=null && info.isConnected() && info.isAvailable())
        {
            return  true;
        }

        return  false;
    }
}
