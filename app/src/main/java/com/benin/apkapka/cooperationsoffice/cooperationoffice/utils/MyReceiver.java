package com.benin.apkapka.cooperationsoffice.cooperationoffice.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.benin.apkapka.cooperationsoffice.cooperationoffice.R;
import com.benin.apkapka.cooperationsoffice.cooperationoffice.activity.HomeFragment;

/**
 * Created by joel on 10/03/2016.
 */
public class MyReceiver   extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationCompat.Builder  builder=new NotificationCompat.Builder(context)
                .setWhen(System.currentTimeMillis())
                .setTicker("Cooperations Office")
                .setSmallIcon(R.mipmap.cooperatelogofin)
                .setAutoCancel(true)
                .setContentTitle("Cooperations Office")
                .setContentText("Nouvelles informations disponibles.");
        Intent intent1=new Intent(context,HomeFragment.class);
        PendingIntent pendingIntent=PendingIntent.getActivity(context,0,intent1,PendingIntent.FLAG_ONE_SHOT);
        builder.setContentIntent(pendingIntent);
        NotificationManager nm=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(1,builder.build());

    }
}
